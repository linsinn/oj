//
// Created by sinn on 6/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MOD2 = 1e9 + 6;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K;
ll F[10], C;
using Matrix = vector<vector<ll>>;

Matrix Multiply(const Matrix& a, const Matrix& b) {
  int n = a.size(), m = a[0].size();
  int p = b.size(), q = b[0].size();
  Matrix ret(n, vector<ll>(m, 0));
  fora (i, 0, n) {
    fora (j, 0, m) {
      fora (k, 0, p) {
        (ret[i][j] += a[i][k] * b[k][j]) %= MOD2;
      }
    }
  }
  return ret;
}

Matrix FastPowM(Matrix& base, ll exp) {
  int l = base.size();
  Matrix ret(l, vector<ll>(l, 0));
  fora (i, 0, l) ret[i][i] = 1;
  while (exp != 0) {
    if (exp & 1) {
      ret = Multiply(ret, base);
    }
    base = Multiply(base, base);
    exp >>= 1;
  }
  return ret;
}

ll FastPowL(ll base, ll exp) {
  ll ret = 1;
  while (exp != 0) {
    if (exp & 1) {
      ret = ret * base % MOD;
    }
    base = base * base % MOD;
    exp >>= 1;
  }
  return ret;
}

ll Solution() {
  Matrix m = {{1, 1, 0}, {1, 0, 1}, {1, 0, 0}};
  m = FastPowM(m, N-3);
  Matrix alpha = {{0, 0, 1}}, beta = {{0, 1, 0}}, sigma = {{1, 0, 0}};
  alpha = Multiply(alpha, m);
  beta = Multiply(beta, m);
  sigma = Multiply(sigma, m);
  m = {{1, 1, 0, 0, 0},
       {1, 0, 1, 0, 0},
       {1, 0, 0, 0, 0},
       {2, 0, 0, 1, 0},
       {-6, 0, 0, 1, 1}};
  m = FastPowM(m, N-3);
  Matrix gamma = {{0, 0, 0, 4, 1}};
  gamma = Multiply(gamma, m);
  ll ans = 1;
  C %= MOD;
  fora (i, 0, 3) F[i] %= MOD;
  (ans *= FastPowL(C, gamma[0][0])) %= MOD;
  (ans *= FastPowL(F[0], alpha[0][0])) %= MOD;
  (ans *= FastPowL(F[1], beta[0][0])) %= MOD;
  (ans *= FastPowL(F[2], sigma[0][0])) %= MOD;
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> F[0] >> F[1] >> F[2] >> C) {
    cout << Solution() << '\n';
  }
  return 0;
}
