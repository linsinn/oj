//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int A[MAXN];

ll Solution() {
  map<int, int> mp;
  fora (i, 0, N) {
    mp[A[i]]++;
  }
  map<int, int> cnts;
  for (auto& p : mp) {
    cnts[p.second] += 1;
  }
  auto it = cnts.rbegin();
  ll ans = 0;
  int cur = it->first;
  while (it != cnts.rend() && cur > 0) {
    int c = min(cur, it->first);
    int k = min(c, it->second);
    ans += 1LL * (c + c - k + 1) * k / 2;
    cur = c - it->second;
    ++it;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      fora (i, 0, N) {
        cin >> A[i];
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}
