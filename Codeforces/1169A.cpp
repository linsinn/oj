//
// Created by sinn on 5/28/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int a, x, b, y;

int Solution() {
  --a, --x, --b, --y;
  while (true) {
    if (a == b)
      return 1;
    if (a == x || b == y)
      break ;
    a = (a + 1) % N;
    b = (b - 1 + N) % N;
  }
  return 0;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> a >> x >> b >> y) {
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}
