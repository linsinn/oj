//
// Created by sinn on 6/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
string s, t;

bool Solution() {
  vector<int> pos;
  int i = 0, j = 0;
  while (i < s.size() && j < t.size()) {
    if (s[i] == t[j]) {
      ++i;
      pos.emplace_back(j);
    }
    ++j;
  }
  if (i != s.size()) return false;
  if (pos.empty() || pos[0] != 0) return false;
  i = 0;
  while (i < pos.size()) {
    int x = pos[i];
    int y = i == pos.size() - 1 ? t.size() : pos[i+1];
    fora (k, x, y-1) {
      if (t[k] != t[k+1]) return false;
    }
    ++i;
  }
  return true;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> s >> t;
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
