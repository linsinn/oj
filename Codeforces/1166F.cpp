//
// Created by sinn on 5/23/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K, C, Q;
int pa[MAXN];
map<int, int> mp[MAXN];
set<int> own[MAXN];

int find_pa(int v) {
  return pa[v] < 0 ? v : pa[v] = find_pa(pa[v]);
}

void merge(int u, int v) {
  u = find_pa(u), v = find_pa(v);
  if (u == v) return ;
  if (own[u].size() > own[v].size())
    swap(u, v);
  pa[u] = v;
  for (auto i : own[u])
    own[v].emplace(i);
  own[u].clear();
}

void add_edge(int u, int v, int c) {
  if (mp[u].count(c))
    merge(v, mp[u][c]);
  if (mp[v].count(c))
    merge(u, mp[v][c]);
  mp[u][c] = v; mp[v][c] = u;
  own[find_pa(v)].emplace(u);
  own[find_pa(u)].emplace(v);
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> C >> Q) {
    memset(pa, -1, sizeof(pa));
    for (int i = 0; i < M; i++) {
      int u, v, c;
      cin >> u >> v >> c;
      --u, --v;
      add_edge(u, v, c);
    }
    while (Q--) {
      char op;
      int x, y, z;
      cin >> op >> x >> y;
      --x, --y;
      if (op == '+') {
        cin >> z;
        add_edge(x, y, z);
      } else {
        int rx = find_pa(x);
        if (rx == find_pa(y) || own[rx].find(y) != own[rx].end())
          cout << "Yes\n";
        else
          cout << "No\n";
      }
    }
  }
  return 0;
}
