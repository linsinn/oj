//
// Created by sinn on 7/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e6 + 10;

using ll = long long;

ll N, M, K, T;
pair<int, int> ivl[MAXN];
int mr[MAXN];
int memos[MAXN][20];
int x, y;

void Init() {
  sort(ivl, ivl+N);
  int idx = 0;
  int r = -1;
  fora (i, 0, MAXN) {
    while (idx < N && ivl[idx].first <= i) {
      r = max(r, ivl[idx].second);
      ++idx;
    }
    if (i > r)
      r = -1;
    mr[i] = r;
  }
  ford (i, MAXN-1, 0) {
    memos[i][0] = mr[i];
    fora (j, 1, 20) {
      memos[i][j] = memos[i][j-1];
      if (memos[i][j-1] != -1)
        memos[i][j] = max(memos[i][j], memos[memos[i][j-1]][j-1]);
    }
  }
}

ll Solution() {
  int ans = 0;
  while (x < y) {
    int t = 0;
    bool ok = false;
    while (t < 20) {
      if (memos[x][t] >= y) {
        ok = true;
        break;
      }
      ++t;
    }
    if (!ok) return -1;
    if (t == 0) {
      ++ans;
      x = memos[x][t];
    } else {
      ans += (1 << (t - 1));
      x = memos[x][t-1];
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) cin >> ivl[i].first >> ivl[i].second;
    Init();
    while (M--) {
      cin >> x >> y;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
