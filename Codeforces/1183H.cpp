//
// Created by sinn on 6/27/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
string s;

ll Solution() {
  vector<vector<ll>> memos(N+1, vector<ll>(N, 0));
  fora (j, 0, N) {
    memos[1][j] = 1;
  }
  fora (i, 2, N+1) {
    ford (j, N-1, 0) {
      set<char> ss;
      ford (k, j-1, 0) {
        if (ss.count(s[k]) == 0) {
          memos[i][j] += memos[i-1][k];
          ss.emplace(s[k]);
        }
      }
    }
  }
  ll ans = 0;
  ford (i, N, 1) {
    set<char> ss;
    ford (j, N-1, 0) {
      if (ss.count(s[j]) == 0 && memos[i][j] != 0) {
        ans += (N - i) * min(memos[i][j], K);
        K -= memos[i][j];
        if (K <= 0)
          break;
        ss.emplace(s[j]);
      }
    }
    if (K <= 0)
      break;
  }
  if (K >= 1) {
    --K;
    ans += N;
  }
  if (K > 0) return -1;
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}
