//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int A[MAXN];
pair<ll, ll> h[MAXN], sh[MAXN];
pair<ll, ll> pre_sum[MAXN];

void Upd(pair<ll, ll> &a, pair<ll, ll> b) {
  a.first ^= b.first;
  a.second ^= b.second;
}

pair<ll, ll> F(int l, int r) {
  auto ret = pre_sum[r];
  if (l > 0)
    Upd(ret, pre_sum[l-1]);
  return ret;
}

ll Calc(int pos) {
  int r = pos, len = 0, ret = 0;
  while (r < N-1 && A[r+1] != 0) {
    len = max(len, A[r+1]+1);
    ++r;
    if (r - len + 1 >= 0 && F(r-len+1, r) == sh[len-1])
      ++ret;
  }
  return ret;
}

ll Solution() {
  ll x = 0;
  fora (i, 0, N) {
    --A[i];
    x ^= A[i];
  }
  mt19937 rnd(time(nullptr));
  fora (i, 0, N) {
    h[i].first = rnd() ^ x;
    h[i].second = rnd() ^ x;
    sh[i] = h[i];
    if (i > 0) {
      Upd(sh[i], sh[i-1]);
    }
  }
  ll ans = 0;
  fora (tc, 0, 2) {
    fora (i, 0, N) {
      pre_sum[i] = h[A[i]];
      if (i > 0)
        Upd(pre_sum[i], pre_sum[i-1]);
    }
    fora (i, 0, N) {
      if (A[i] == 0)
        ans += Calc(i) + (tc == 0);
    }
    reverse(A, A+N);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}