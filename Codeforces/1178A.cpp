//
// Created by sinn on 7/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];

void Solution() {
  vector<int> P{1};
  int rest = 0, cur = A[0];
  fora (i, 1, N) {
    if (A[i] <= A[0] / 2) {
      cur += A[i];
      P.emplace_back(i+1);
    } else {
      rest += A[i];
    }
  }
  if (cur > rest) {
    cout << P.size() << '\n';
    for (int v : P) {
      cout << v << ' ';
    }
    cout << '\n';
  } else {
    cout << "0\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
