//
// Created by sinn on 12/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
	map<int, int> s0, s1;
	for (int i = N-1; i >= 0; --i) {
		int t = A[i] - i;
		auto p = s0.lower_bound(t + 1);
		if (p != s0.end()) {
			cout << "YES\n";
			cout << i + 1 << ' ' << p->second + 1 << '\n';
			return ;
		}
		s0[t] = i;

		t = A[i] + i;
		auto q = s1.upper_bound(t - 1);
		if (q != s1.begin()) {
			q = prev(q);
			cout << "YES\n";
			cout << i + 1 << ' ' << q->second + 1 << '\n';
			return ;
		}
		s1[t] = i;
	}
	cout << "NO\n";
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
  	while (T--) {
  		cin >> N;
  		A.resize(N);
  		fora (i, 0, N) cin >> A[i];
  		Solution();
  	}
  }
  return 0;
}
