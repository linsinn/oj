#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void extended_gcd(ll &x, ll &y, ll a, ll b) {
  if (b == 0) {
    x = 1, y = 0;
    return ;
  }
  extended_gcd(x, y, b, a % b);
  ll tmp = x;
  x = y;
  y = tmp - a / b * y;
}

void Solution() {
  sort(A.begin(), A.end());
  ll z = A[1] - A[0];
  fora (i, 1, N) {
    z = __gcd(z, (ll)A[i] - A[i-1]);
  }

  ll s = std::accumulate(A.begin(), A.end(), 0LL);
  ll g = __gcd(N, z);
  N /= g, z /= g;
  ll x, y;
  extended_gcd(x, y, N, z);
  x *= s / g;
  y *= s / g;
  ll maxi = *max_element(A.begin(), A.end());
  if (x < maxi) {
    ll k = (maxi - x) / z;
    if ((maxi - x) % z)
      ++k;
    x += k * z;
    y -= k * N;
  } else {
    ll k = -(maxi - x) / z;
    x -= k * z;
    y += k * N;
  }
  cout << -y << ' ' << z * g<< '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
