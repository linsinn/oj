//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 10;

using ll = long long;

int N, M, K;
int A[MAXN], pos[MAXN];
vector<pair<int, int>> ans;

void change(int i, int j) {
  int a = A[i], b = A[j];
  swap(A[i], A[j]);
  pos[a] = j;
  pos[b] = i;
}

void Solution() {
  ans.clear();
  int bnd = N / 2;
  fora (i, 1, N+1) {
    pos[A[i]] = i;
  }
  fora (i, 1, N+1) {
    if (A[i] != i) {
      int p = pos[i];
      while (abs(p - i) < bnd) {
        if (p > bnd) {
          change(1, p);
          ans.emplace_back(make_pair(1, p));
          p = 1;
        } else {
          change(p, N);
          ans.emplace_back(make_pair(p, N));
          p = N;
        }
      }
      change(i, p);
      ans.emplace_back(make_pair(min(i, p), max(i, p)));
    }
  }
  cout << ans.size() << '\n';
  for (auto& p : ans) {
    cout << p.first << ' ' << p.second << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 1, N+1) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}