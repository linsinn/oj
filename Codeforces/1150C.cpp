//
// Created by sinn on 4/29/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int a[MAXN];

void Solution() {
    int ones = 0, twos = 0;
    for (int i = 0; i < N; i++) {
        if (a[i] == 1) ones++;
        else twos++;
    }
    if (twos > 0) {
        cout << "2 ";
        twos--;
    }
    if (ones > 0) {
        cout << "1 ";
        ones--;
    }
    while (twos > 0) {
        cout << "2 ";
        twos--;
    }
    while (ones > 0) {
        cout << "1 ";
        ones--;
    }
    cout << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++) {
            cin >> a[i];
        }
        Solution();
    }
    return 0;
}
