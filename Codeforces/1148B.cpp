//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
ll A[MAXN], B[MAXN];
ll ta, tb;

ll Solution() {
  if (K >= N || K >= M)
    return -1;
  fora (i, 0, N) {
    A[i] += ta;
  }
  ll ans = -1;
  fora (i, 0, K+1) {
    ll ari = A[i];
    auto it = lower_bound(B, B+M, ari);
    if (it == B+M) {
      return -1;
    }
    int p = it - B;
    p += K - i;
    if (p >= M)
      return -1;
    ans = max(ans, B[p] + tb);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> ta >> tb >> K) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    fora (i, 0, M) {
      cin >> B[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
