//
// Created by sinn on 7/2/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int A[MAXN], B[MAXN];
ll BIT[MAXN];

void Insert(int x) {
  while (x <= N) {
    BIT[x] += 1;
    x += x & -x;
  }
}

int GetSum(int x) {
  int ret = 0;
  while (x > 0) {
    ret += BIT[x];
    x -= x & -x;
  }
  return ret;
}

ll Solution() {
  int one = N;
  fora (i, 0, N) {
    if (B[i] == 1)
      one = i;
  }
  bool f = true;
  fora (i, one + 1, N) {
    if (B[i] - B[i-1] != 1)
      f = false;
  }
  fora (i, 0, N) {
    if (A[i] != 0)
      Insert(A[i]);
  }
  if (f) {
    bool ok = true;
    fora (i, 0, one) {
      if (B[i] != 0) {
        int g = GetSum(B[i] - 1);
        if (g < i + 1) {
          ok = false;
          break;
        }
        Insert(B[i]);
      }
    }
    if (ok) {
      return one;
    }
  }
  memset(BIT, 0, sizeof(BIT));
  fora (i, 0, N) {
    if (A[i] != 0)
      Insert(A[i]);
  }
  int ans = 0;
  if (one != N) {
    fora (i, 0, one + 1) {
      if (B[i] != 0) {
        Insert(B[i]);
      }
    }
  } else {
    one = -1;
  }
  int idx = one + 1;
  int maxi = 0;
  while (idx < N) {
    int t = B[idx];
    if (t != 0) {
      int g = GetSum(t-1);
      maxi = max(maxi, idx - one - g);
      Insert(t);
    }
    ++idx;
  }
  return one + 1 + maxi + N;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    fora (i, 0, N) cin >> B[i];
    cout << Solution() << '\n';
  }
  return 0;
}
