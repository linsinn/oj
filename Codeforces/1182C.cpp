//
// Created by sinn on 6/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
string words[MAXN];
bool vowels[256];

pair<int, char> Extract(string& s) {
  int v = 0;
  char lastv = '-';
  for (char ch : s) {
    if (vowels[ch]) {
      ++v;
      lastv = ch;
    }
  }
  return {v, lastv};
}

void Solution() {
  memset(vowels, 0, sizeof(vowels));
  vowels['a'] = true;
  vowels['e'] = true;
  vowels['i'] = true;
  vowels['o'] = true;
  vowels['u'] = true;
  map<pair<int, char>, set<int>> mp;
  fora (i, 0, N) {
    auto p = Extract(words[i]);
    if (p.first != 0)
      mp[p].emplace(i);
  }
  vector<int> fw, sw;
  for (auto &p : mp) {
    while (p.second.size() >= 2) {
      sw.emplace_back(*p.second.begin());
      p.second.erase(p.second.begin());
      sw.emplace_back(*p.second.begin());
      p.second.erase(p.second.begin());
    }
  }
  map<int, vector<int>> t;
  for (const auto& p : mp) {
    for (int idx : p.second)
      t[p.first.first].emplace_back(idx);
  }
  for (auto& p : t) {
    while (p.second.size() >= 2) {
      fw.emplace_back(*p.second.begin());
      p.second.erase(p.second.begin());
      fw.emplace_back(*p.second.begin());
      p.second.erase(p.second.begin());
    }
  }
  while (sw.size() > fw.size()) {
    fw.emplace_back(sw.back());
    sw.pop_back();
    fw.emplace_back(sw.back());
    sw.pop_back();
  }
  cout << sw.size() / 2 << '\n';
  fora (i, 0, sw.size()) {
    cout << words[fw[i]] << ' ' << words[sw[i]] << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> words[i];
    }
    Solution();
  }
  return 0;
}
