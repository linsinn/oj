import math


def gcd(a, b):
    return a if b == 0 else gcd(b, a % b)


def solve(divs):
    ss = set(divs)
    ans = 1
    for div in divs:
        for i in range(2, int(math.sqrt(div)) + 1):
            if div % i == 0:
                if i not in ss or div // i not in ss:
                    return -1
        g = gcd(ans, div)
        ans = ans // g * div
    if ans in ss:
        ans *= sorted(divs)[0]
    if ans > (1 << 64):
        return -1
    for div in divs:
        if ans // div not in ss:
            return -1
    return ans


T = int(input())
for i in range(T):
    N = int(input())
    D = list(map(int, input().strip().split()))
    print(solve(D))

N = 61
D = [15295, 256795, 6061, 42427, 805, 115, 30305, 63365, 19285, 667, 19, 5, 4669, 77, 1463, 253, 139403, 1771, 209, 1045, 168245, 975821, 3857, 7337, 2233, 1015, 8855, 1595, 665, 161, 212135, 33649, 7, 55, 24035, 7315, 35, 51359, 4807, 1265, 12673, 145, 551, 385, 319, 95, 36685, 23, 443555, 3335, 2185, 133, 23345, 3059, 437, 203, 11, 11165, 88711, 2755, 29]

print(len(D))
print(solve(D))