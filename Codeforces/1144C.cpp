//
// Created by sinn on 4/13/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int a[MAXN], incre[MAXN], decre[MAXN];

void Solution() {
    map<int, int> mps;
    for (int i = 0; i < N; i++) {
        mps[a[i]]++;
    }
    int l0 = 0, l1 = 0;
    for (auto p : mps) {
        if (p.second > 2) {
            cout << "NO\n";
            return ;
        } else if (p.second == 2) {
            incre[l0++] = p.first;
            decre[l1++] = p.first;
        } else {
            incre[l0++] = p.first;
        }
    }
    cout << "YES\n";
    cout << l0 << '\n';
    for (int i = 0; i < l0; i++)
        cout << incre[i] << ' ';
    cout << l1 << '\n';
    for (int i = l1-1; i >= 0; i--)
        cout << decre[i] << ' ';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++) {
            cin >> a[i];
        }
        Solution();
    }
    return 0;
}
