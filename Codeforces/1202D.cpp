#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
  Vec cnt;
  while (N != 0) {
    ll k = max(2LL, ll(sqrt(N * 2)) - 2);
    while ((k + 1) * k <= N * 2)
      ++k;
    cnt.emplace_back(k);
    N -= k * (k - 1) / 2;
  }
  int t = 0;
  int i = cnt.size() - 1;
  cout << '1';
  while (i >= 0) {
    while (t < cnt[i]) {
      cout << '3';
      ++t;
    }
    cout << '7';
    --i;
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      Solution();
    }
  }
  return 0;
}
