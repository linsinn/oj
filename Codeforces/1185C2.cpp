//
// Created by sinn on 6/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
int A[MAXN];
ll su[MAXN], seen[MAXN];

void Insert(int x, int val, ll* arr) {
  while (x <= N) {
    arr[x] += val;
    x += x & -x;
  }
}

ll GetSum(int x, ll *arr) {
  ll ret = 0;
  while (x > 0) {
    ret += arr[x];
    x -= x & -x;
  }
  return ret;
}

bool Cmp1(const tuple<int, int, int>& a, const tuple<int, int, int>& b) {
  return get<0>(a) < get<0>(b);
}

bool Cmp2(const tuple<int, int, int>& a, const tuple<int, int, int>& b) {
  return get<1>(a) < get<1>(b);
}

bool Check(int idx, int target) {
  return GetSum(idx, su) <= target;
}

void Solution() {
  vector<tuple<int, int, int>> vec(N);
  fora (i, 0, N) {
    get<0>(vec[i]) = A[i];
    get<1>(vec[i]) = i;
  }
  sort(vec.begin(), vec.end(), Cmp1);
  fora (i, 0, N) {
    get<2>(vec[i]) = i;
  }
  sort(vec.begin(), vec.end(), Cmp2);
  fora (i, 0, N) {
    int a, b, c;
    tie(a, b, c) = vec[i];
    int lb = 1, rb = N;
    int idx = 0;
    while (lb <= rb) {
      int mid = lb + ((rb - lb) >> 1);
      if (Check(mid, M-a)) {
        idx = mid;
        lb = mid + 1;
      } else {
        rb = mid - 1;
      }
    }
    cout << GetSum(N, seen) - GetSum(idx, seen) << ' ';
    Insert(c+1, a, su);
    Insert(c+1, 1, seen);
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}