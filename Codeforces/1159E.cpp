//
// Created by sinn on 5/15/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 5e5 + 1;

using ll = long long;

int N, M, K;
int nexts[MAXN], timer;
vector<int> v[MAXN];
int ans[MAXN];

void dfs(int t) {
  ans[t] = timer--;
  for (int i : v[t])
    dfs(i);
}

void Solution() {
  for (int i = 0; i <= N; ++i) v[i].clear();
  for (int i = 0; i < N; ++i) {
    nexts[i]--;
    if (nexts[i] == -2)
      nexts[i] = i+1;
    v[nexts[i]].emplace_back(i);
  }
  timer = N;
  dfs(N);
  vector<int> stk;
  for (int i = N-1; i >= 0; i--) {
    while (!stk.empty() && ans[stk.back()] < ans[i])
      stk.pop_back();
    if ((stk.empty() && nexts[i] != N) || (!stk.empty() && stk.back() != nexts[i])) {
      cout << "-1\n";
      return ;
    }
    stk.emplace_back(i);
  }
  for (int i = 0; i < N; i++)
    cout << ans[i] + 1 << ' ';
  cout << '\n';
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  while (cin >> T) {
    while (T--) {
      cin >> N;
      for (int i = 0; i < N; ++i) {
        cin >> nexts[i];
      }
      Solution();
    }
  }
  return 0;
}
