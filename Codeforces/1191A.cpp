//
// Created by sinn on 7/13/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

void Solution() {
  int cat[4] = {-1, -1, -1, -1};
  fora (i, 0, 3) {
    int g = N + i;
    if (g % 4 == 0)
      cat[3] = i;
    else if (g % 4 == 2)
      cat[2] = i;
    else if (g % 4 == 3)
      cat[1] = i;
    else
      cat[0] = i;
  }
  fora (i, 0, 4) {
    if (cat[i] != -1) {
      cout << cat[i] << ' ' << char('A' + i) << '\n';
      break;
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}
