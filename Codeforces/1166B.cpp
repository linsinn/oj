//
// Created by sinn on 5/22/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
char vowels[] = {'a', 'e', 'i', 'o', 'u'};

void Solution() {
  string ans;
  for (int i = 5; i <= sqrt(K); i++) {
    if (K % i == 0 && K / i >= 5) {
      int rows = i, cols = K / i;
      for (int x = 0; x < rows; ++x) {
        for (int y = 0; y < cols; ++y) {
          ans += vowels[(x + y) % 5];
        }
      }
      cout << ans << '\n';
      return;
    }
  }
  cout << "-1\n";
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> K) {
    Solution();
  }
  return 0;
}
