//
// Created by sinn on 12/30/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> points;
vector<vector<ll>> dists;

vector<int> Generate() {
	ll d = dists[0][1];
	vector<int> sa = {0};
	for (int i = 0; i < N; ++i) {
		if (dists[1][i] == d) {
			
		}
	}
}

void Solution() {
	fora (i, 0, N) {
		fora (j, i+1, N) {
			auto tmp = make_pair(points[i].first - points[j].first, points[i].second - points[j].second);
			dists[i][j] = dists[j][i] = tmp.first * tmp.first + tmp.second * tmp.second;
		}
	}


}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
  	points.resize(N);
  	fora (i, 0, N) {
  		cin >> points[i].first >> points[i].second;
  	}
  	Solution();
  }
  return 0;
}