//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int MAXN = 1e5 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];
ll memos[MAXN];
ll pre_sum[MAXN];
int pre_idx[MAXN];

ll Solution() {
  sort(A, A+N);
  memset(pre_idx, 0, sizeof(pre_idx));
  fora (i, 0, N) pre_idx[A[i]]++;
  fora (i, 1, MAXN) pre_idx[i] += pre_idx[i-1];
  ll maxi = A[N-1];
  ll ans = 0;
  fora (p, 1, maxi / (K-1) + 1) {
    pre_sum[0] = 1;
    fora (i, 1, N) {
      pre_sum[i] = pre_sum[i-1] + 1;
    }
    fora (i, 2, K+1) {
      fora (j, 0, N) {
        memos[j] = 0;
        int g = A[j] - p >= 0 ? pre_idx[A[j] - p] : 0;
        if (g != 0) {
          memos[j] = pre_sum[g-1];
        }
      }
      pre_sum[0] = memos[0];
      fora (j, 1, N) {
        pre_sum[j] = (pre_sum[j-1] + memos[j]) % MOD;
      }
    }
    ans += pre_sum[N-1];
    ans %= MOD;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
