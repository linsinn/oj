//
// Created by sinn on 5/15/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

void Solution() {
  int a = (N - K) / 2;
  string ans;
  for (int i = 0; i < N; i++) {
    for (int k = 0; k < a && i < N; k++, i++)
      ans += '0';
    if (i < N) {
      ans += '1';
    }
  }
  cout << ans << '\n';
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    Solution();
  }
  return 0;
}

