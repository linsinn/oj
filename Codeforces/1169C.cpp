//
// Created by sinn on 5/28/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 1;

using ll = long long;

ll N, M, K;
ll A[MAXN], B[MAXN];

bool Can(ll t) {
  memcpy(B, A, sizeof(A));
  B[0] = min(B[0], (B[0] + min(M-B[0], t)) % M);
  for (int i = 1; i < N; ++i) {
    if (B[i-1] <= B[i]) {
      ll d = B[i-1] + M - B[i];
      if (d <= t) B[i] = B[i-1];
    } else {
      ll d = B[i-1] - B[i];
      if (d <= t) B[i] = B[i-1];
      else return false;
    }
  }
  return true;
}

ll Solution() {
  ll lb = 0,  rb = 1LL * N * M;
  while (lb < rb) {
    ll m = lb + (rb - lb) / 2;
    if (Can(m)) {
      rb = m;
    } else {
      lb = m + 1;
    }
  }
  return rb;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    for (int i = 0; i < N; ++i)
      cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
