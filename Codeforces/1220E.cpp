#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<ll> A;
vector<Vec> gph;
int st;

ll Solution() {
  queue<int> que;
  Vec deg(N, 0);
  fora (i, 0, N) {
    deg[i] = gph[i].size();
    if (deg[i] == 1 && i != st)
      que.emplace(i);
  }
  vector<ll> aux(N, 0);
  while (!que.empty()) {
    int u = que.front(); que.pop();
    deg[u] = -1;
    for (int v : gph[u]) {
      if (deg[v] > 0) {
        deg[v]--;
        Max(aux[v], aux[u] + A[u]);
      }
      if (deg[v] == 1 && v != st)
        que.emplace(v);
    }
  }
  ll ans = 0;
  ll mx = 0;
  fora (i, 0, N) {
    if (deg[i] != -1) {
      ans += A[i];
      Max(mx, aux[i]);
    }
  }
  return ans + mx;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    A.resize(N);
    gph.assign(N, Vec());
    fora (i, 0, N) cin >> A[i];
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      --u, --v;
      gph[u].emplace_back(v);
      gph[v].emplace_back(u);
    }
    cin >> st;
    --st;
    cout << Solution() << '\n';
  }
  return 0;
}
