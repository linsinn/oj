//
// Created by sinn on 5/13/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string str;

int Solution() {
  int mini = 0;
  int cur = 0;
  for (char ch : str) {
    if (ch == '-')
      cur--;
    else
      cur++;
    mini = min(mini, cur);
  }
  return max(0, abs(mini) + cur);
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> str;
    cout << Solution() << '\n';
  }
  return 0;
}