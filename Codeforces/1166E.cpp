//
// Created by sinn on 5/23/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e4 + 1;

using ll = long long;

int N, M, K;
bitset<MAXN> s[102];

bool Solution() {
  for (int i = 0; i < M; i++) {
    for (int j = i + 1; j< M; j++) {
      bitset<MAXN> t = s[i] & s[j];
      if (t.count() == 0)
        return false;
    }
  }
  return true;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> M >> N) {
    int a, b;
    for (int i = 0; i < M; ++i) {
      s[i].reset();
      cin >> a;
      for (int j = 0; j < a; ++j) {
        cin >> b;
        s[i].set(b, 1);
      }
    }
    if (Solution())
      cout << "possible\n";
    else
      cout << "impossible\n";
  }
  return 0;
}
