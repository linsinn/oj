//
// Created by sinn on 7/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];
ll B[MAXN];
ll pre_sum[MAXN];

ll Check(ll x) {
  fora (i, 0, N) B[i] = abs(x - A[i]);
  sort(B, B+N);
  return B[K];
}

ll Solution() {
  pre_sum[0] = 0;
  fora (i, 1, N) {
    pre_sum[i] = pre_sum[i-1] + A[i] - A[i-1];
  }
  ll mini = 1e12;
  ll mini_idx = -1;
  fora (i, 0, N-K) {
    if (pre_sum[i+K] - pre_sum[i] < mini) {
      mini_idx = i;
      mini = min(mini, pre_sum[i+K] - pre_sum[i]);
    }
  }
  ll ans = (A[mini_idx] + A[mini_idx+K]) / 2;
  if ((A[mini_idx+K] - A[mini_idx]) % 2 == 1) {
    if (Check(ans) > Check(ans+1))
      ++ans;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      fora (i, 0, N) cin >> A[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}
