//
// Created by sinn on 6/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e3 + 10;

using ll = long long;

int N, M, K, T;
string blanket[MAXN];
int right_diff_idx[MAXN][MAXN];
int down_diff_idx[MAXN][MAXN];
int mini_right_idx[MAXN][MAXN];

void Init() {
  memset(right_diff_idx, 0, sizeof(right_diff_idx));
  memset(down_diff_idx, 0, sizeof(down_diff_idx));
  fora (i, 0, N) {
    right_diff_idx[i][M-1] = M;
    ford (j, M-2, 0) {
      right_diff_idx[i][j] = right_diff_idx[i][j+1];
      if (blanket[i][j] != blanket[i][j+1])
        right_diff_idx[i][j] = j+1;
    }
  }
  fora (j, 0, M) {
    down_diff_idx[N-1][j] = N;
    ford (i, N-2, 0) {
      down_diff_idx[i][j] = down_diff_idx[i+1][j];
      if (blanket[i][j] != blanket[i+1][j])
        down_diff_idx[i][j] = i+1;
    }
  }
  fora (j, 0, M) {
    mini_right_idx[N-1][j] = right_diff_idx[N-1][j];
    ford (i, N-2, 0) {
      if (blanket[i][j] == blanket[i+1][j]) {
        mini_right_idx[i][j] = min(right_diff_idx[i][j], mini_right_idx[i+1][j]);
      } else {
        mini_right_idx[i][j] = right_diff_idx[i][j];
      }
    }
  }
}

int Solution() {
  Init();
  int ans = 0;
  fora (i, 0, N) {
    int j = 0;
    while (j < M) {
      int a = i;
      int b = down_diff_idx[i][j];
      if (b == N) { ++j; continue; }
      int c = down_diff_idx[b][j];
      if (c == N) { ++j; continue; }
      int d = down_diff_idx[c][j];
      if (b - a == c - b && c - b <= d - c) {
        int r = min(mini_right_idx[a][j], mini_right_idx[b][j]);
        fora (k, c, c + b - a) {
          r = min(r, right_diff_idx[k][j]);
        }
        ans += (r - j) * (r - j + 1) / 2;
        j = r;
      } else {
        ++j;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) {
      cin >> blanket[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}