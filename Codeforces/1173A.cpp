//
// Created by sinn on 7/2/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll x, y, z;

char Solution() {
  if (x > y + z)
    return '+';
  if (y > x + z)
    return '-';
  if (x == y && z == 0)
    return '0';
  return '?';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> x >> y >> z) {
    cout << Solution() << '\n';
  }
  return 0;
}
