//
// Created by sinn on 12/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
	sort(A.begin(), A.end());
	int cnt = 0;
	fora (i, 1, A.size()) {
		if (A[i] != A[i-1]) {
			cnt = i;
			break;
		}
	}
	return K + 1 - cnt;
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
  	A.resize(K + 1);
  	int a, b;
  	for (int i = 0; i <= K; ++i) {
  		cout << "? ";
  		for (int j = 0; j <= K; ++j) {
  			if (i != j) {
					cout << j + 1 << ' ';
				}
  		}
  		cout << '\n';
  		cout.flush();
  		cin >> a >> b;
  		A[i] = b;
  	}
  	cout << "! " << Solution() << '\n';
  	cout.flush();
  }
  return 0;
}