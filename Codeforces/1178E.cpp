//
// Created by sinn on 7/21/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string s;

void Solution() {
  if (s.size() <=3) {
    cout << s[0] << '\n';
    return ;
  }
  string ans;
  int i = 0, j = s.size() - 1;
  while (i < j - 2) {
    if (s[i] == s[j] || s[i] == s[j-1]) {
      ans += s[i];
    } else {
      ans += s[i+1];
    }
    i += 2;
    j -= 2;
  }
  cout << ans;
  if (i <= j)
    cout << s[i];
  reverse(ans.begin(), ans.end());
  cout << ans << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    Solution();
  }
  return 0;
}
