//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 10;

using ll = long long;

int N, M, K;
ll S[MAXN], T[MAXN];
ll sigma[MAXN];

void Solution() {
  vector<pair<ll, int>> sorted(N);
  fora (i, 0, N) {
    sorted[i] = {S[i], i};
  }
  sort(sorted.begin(), sorted.end());
  sort(T, T+N);
  ll su = 0;
  fora (i, 0, N) {
    sigma[i] = T[i] - sorted[i].first;
    su += sigma[i];
  }
  if (su != 0) {
    cout << "NO\n";
    return ;
  }
  stack<pair<int, int>> stk;
  vector<tuple<int, int, int>> ans;
  fora (i, 0, N) {
    if (sigma[i] > 0) {
      stk.emplace(i, 0);
    } else {
      while (!stk.empty()) {
        auto t = stk.top();
        stk.pop();
        ll mini = min(sigma[t.first] - t.second, -sigma[i]);
        t.second += mini;
        if (sigma[t.first] - t.second != 0) {
          stk.emplace(t);
        }
        ans.emplace_back(sorted[t.first].second+1, sorted[i].second+1, mini);
        sigma[i] += mini;
        if (sigma[i] == 0)
          break;
      }
      if (sigma[i] != 0) {
        cout << "NO\n";
        return ;
      }
    }
  }
  if (!stk.empty()) {
    cout << "NO\n";
    return ;
  }
  cout << "YES\n" << ans.size() << '\n';
  for (auto& t : ans) {
    cout << get<0>(t) << ' ' << get<1>(t) << ' ' << get<2>(t) << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> S[i];
    }
    fora (i, 0, N) {
      cin >> T[i];
    }
    Solution();
  }
  return 0;
}
