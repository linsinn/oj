#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> pts;
Vec B;

ll Solution() {
  sort(B.begin(), B.end());
  map<int, Vec> mp;
  for (auto &p : pts) {
    mp[p.first].emplace_back(p.second);
  }
  for (auto &p : mp) 
    sort(p.second.begin(), p.second.end());
  vector<vector<ll>> dp(mp.size(), vector<ll>(2, 0));
  if (mp.begin()->first == 1) {
    dp[0][0] = dp[0][1] = mp.begin()->second.back() - 1;
    dp[0][0] += mp.begin()->second.back() - mp.begin()->second.front();
  } else {
    int p = *lower_bound(B.begin(), B.end(), 1);
    ll g = p - 1 + mp.begin()->first - 1;
    auto &v = mp.begin()->second;
    dp[0][1] = g + abs(v.front() - p) + v.back() - v.front();
    dp[0][0] = g + abs(v.back() - p) + v.back() - v.front();
  }
  auto it = next(mp.begin());
  int i = 1;
  for(; i < mp.size(); ++it, ++i) {
    auto &v = it->second;
    auto pit = prev(it);
    auto &pv = pit->second;
    ll h = it->first - pit->first;
    dp[i][0] = dp[i][1] = INT64_MAX;
    auto p = upper_bound(B.begin(), B.end(), pv.front());
    if (p != B.begin()) {
      auto a = prev(p);
      Min(dp[i][0], dp[i-1][0] + abs(*a - pv.front()) + abs(v.back() - *a) + h + v.back() - v.front());
      Min(dp[i][1], dp[i-1][0] + abs(*a - pv.front()) + abs(v.front() - *a) + h + v.back() - v.front());
    }
    if (p != B.end()) {
      Min(dp[i][0], dp[i-1][0] + abs(*p - pv.front()) + abs(v.back() - *p) + h + v.back() - v.front());
      Min(dp[i][1], dp[i-1][0] + abs(*p - pv.front()) + abs(v.front() - *p) + h + v.back() - v.front());
    }

    p = upper_bound(B.begin(), B.end(), pv.back());
    if (p != B.begin()) {
      auto a = prev(p);
      Min(dp[i][0], dp[i-1][1] + abs(*a - pv.back()) + abs(v.back() - *a) + h + v.back() - v.front());
      Min(dp[i][1], dp[i-1][1] + abs(*a - pv.back()) + abs(v.front() - *a) + h + v.back() - v.front());
    }
    if (p != B.end()) {
      Min(dp[i][0], dp[i-1][1] + abs(*p - pv.back()) + abs(v.back() - *p) + h + v.back() - v.front());
      Min(dp[i][1], dp[i-1][1] + abs(*p - pv.back()) + abs(v.front() - *p) + h + v.back() - v.front());
    }
  }
  return min(dp[mp.size()-1][0], dp[mp.size()-1][1]);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K >> T) {
    pts.resize(K);
    fora (i, 0, K) {
      cin >> pts[i].first >> pts[i].second;
    }
    B.resize(T);
    fora (i, 0, T) cin >> B[i];
    cout << Solution() << '\n';
  }
  return 0;
}
