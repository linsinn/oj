//
// Created by sinn on 6/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
string board[MAXN];

bool Check(int x, int y) {
  int l = y-1, r = y+1, u = x-1, b = x+1;
  while (l >= 0 && board[x][l] == '*') {
    --l;
  }
  while (r < M && board[x][r] == '*') {
    ++r;
  }
  while (u >= 0 && board[u][y] == '*') {
    --u;
  }
  while (b < N && board[b][y] == '*') {
    ++b;
  }
  if (y-l-1 == 0 || r-y-1 == 0 || x-u-1 == 0 || b-x-1 == 0) {
    return false;
  }
  fora (i, 0, N) {
    fora (j, 0, M) {
      if (board[i][j] == '*' && ((i != x && j != y) || i <= u || i >= b || j <= l || j >= r))
        return false;
    }
  }
  return true;
}

int Solution() {
  fora (i, 0, N) {
    fora (j, 0, M) {
      if (board[i][j] == '*') {
        if (Check(i, j)) {
          return true;
        }
      }
    }
  }
  return false;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) {
      cin >> board[i];
    }
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}
