#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> gph;
Vec in, out;

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    gph.assign(N+1, Vec());
    in.assign(N+1, 0);
    out.assign(N+1, 0);
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      if (u > v) swap(u, v);
      out[v]++;
      in[u]++;
      gph[u].emplace_back(v);
    }
    ll ans = 0;
    fora (i, 1, N+1) {
      for (int j : gph[i]) {
        ans += in[j];
      }
    }
    cout << ans << '\n';
    cin >> T;
    while (T--) {
      cin >> u;
      fora (i, 0, gph[u].size()) {
        v = gph[u][i];
        ans -= out[u];
        ans += out[v] - 1;
        ans += in[u] - 1;
        ans -= in[v];
        in[u]--;
        in[v]++;
        out[u]++;
        out[v]--;
        gph[v].emplace_back(u);
      }
      gph[u].clear();
      cout << ans << '\n';
    }
  }
  return 0;
}
