//
// Created by sinn on 7/13/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string tiles[10];

int Check(map<int, int>& mp) {
  if (mp.empty())
    return 1e8;
  if (mp.size() == 1) {
    return 3 - mp.begin()->second;
  } else if (mp.size() == 2) {
    if (mp.begin()->second == 2 || mp.rbegin()->second == 2)
      return 1;
    int a = mp.begin()->first;
    int b = next(mp.begin())->first;
    if (b - a == 1)
      return 1;
    if (b - a == 2)
      return 1;
    return 2;
  } else {
    int a = mp.begin()->first;
    int b = next(mp.begin())->first;
    int c = mp.rbegin()->first;
    if (b - a == 1 && c - b == 1)
      return 0;
    if (b - a == 1 || c - b == 1)
      return 1;
    if (c - a == 2 || c - b == 2 || b - a == 2)
      return 1;
    return 2;
  }
}

ll Solution() {
  map<int, int> m, p, s;
  fora (i, 0, 3) {
    if (tiles[i][1] == 'm') {
      m[tiles[i][0] - '0']++;
    }
    else if (tiles[i][1] == 'p') {
      p[tiles[i][0] - '0']++;
    }
    else {
      s[tiles[i][0] - '0']++;
    }
  }
  int mini = 1e8;
  mini = min(mini, Check(m));
  mini = min(mini, Check(p));
  mini = min(mini, Check(s));
  return mini;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> tiles[0] >> tiles[1] >> tiles[2]) {
    cout <<Solution() << '\n';
  }
  return 0;
}
