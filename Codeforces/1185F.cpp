//
// Created by sinn on 6/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
int A[MAXN], B[MAXN], C[MAXN];
int mp1[1<<10], mp2[1<<10];

void Solution() {
  memset(mp1, 0, sizeof(mp1));
  memset(mp2, -1, sizeof(mp2));
  fora (i, 0, 1 << 10) {
    fora (j, 0, N) {
      if ((B[j] & i) == B[j]) {
        mp1[i]++;
      }
    }
    fora (j, 0, M) {
      if ((A[j] & i) == i) {
        if (mp2[i] == -1 || C[mp2[i]] > C[j])
          mp2[i] = j;
      }
    }
  }
  int maxi = 0;
  ll cost = 1e18;
  int ans0 = -1, ans1 = -1;
  fora (i, 0, 1 << 10) {
    if (mp1[i] < maxi) continue;
    fora (j, 0, M) {
      if ((A[j] & i) == A[j]) {
        int g = mp2[A[j] ^ i];
        if (g != -1 && g != j) {
          if (mp1[i] > maxi) {
            ans0 = j, ans1 = g;
            maxi = mp1[i];
            cost = C[j] + C[g];
          } else if (C[j] + C[g] < cost) {
            ans0 = j, ans1 = g;
            cost = C[j] + C[g];
          }
        }
      }
    }
  }
  cout << ans0+1 << ' ' << ans1+1 << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    int f, g;
    fora (i, 0, N) {
      B[i] = 0;
      cin >> f;
      fora (j, 0, f) {
        cin >> g;
        B[i] |= 1 << (g-1);
      }
    }
    fora (i, 0, M) {
      A[i] = 0;
      cin >> C[i];
      cin >> f;
      fora (j, 0, f) {
        cin >> g;
        A[i] |= 1 << (g-1);
      }
    }
    Solution();
  }
  return 0;
}
