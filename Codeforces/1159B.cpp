//
// Created by sinn on 5/13/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

int Solution() {
  int mini = A[0];
  for (int i = 0; i < N; i++) {
    int gap = 0;
    if (i != 0)
      gap = i - 0;
    if (i != N-1)
      gap = max(gap, N-1-i);
    mini = min(mini, min(A[i], min(A[0], A[N-1])) / gap);
  }
  return mini;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    for (int i = 0; i < N; ++i) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
