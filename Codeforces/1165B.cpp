//
// Created by sinn on 5/19/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

int Solution() {
  multiset<int> ss;
  for (int i = 0; i < N; ++i)
    ss.emplace(A[i]);
  int ans = 1;
  while (ss.lower_bound(ans) != ss.end()) {
    auto it = ss.lower_bound(ans);
    int k = *it - ans;
    ss.erase(it);
    ans += 1;
  }
  return ans - 1;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    for (int i = 0; i < N; ++i) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}