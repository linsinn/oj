#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

using ll = long long;

ll N, M, K, T;
ll x, y;
Vec A;

ll Solution() {
  fora (i, 0, N) {
    ll a = max(0LL, i - x), b = min(i + y, N-1);
    bool f = true;
    fora (j, a, b+1) {
      if (j != i && A[j] <= A[i]) {
        f = false;
        break;
      }
    }
    if (f) return i + 1;
  }
  return N;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> x >> y) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
