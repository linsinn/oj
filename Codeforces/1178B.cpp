//
// Created by sinn on 7/21/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string s;

ll Solution() {
  ll w = 0, wo = 0, ans = 0;
  fora (i, 0, s.size()) {
    if (s[i] == 'o') {
      wo += w;
    } else {
      if (i > 0 && s[i-1] == 'v') {
        w += 1;
        ans += wo;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    cout << Solution() << '\n';
  }
  return 0;
}
