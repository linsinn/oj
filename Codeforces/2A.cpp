//
// Created by sinn on 12/8/18.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        string name;
        int score;
        map<string, int> mps;
        string winner;
        int maxi = 0;
        vector<pair<string, int>> input(N);
        for (int i = 0; i < N; i++) {
            cin >> name >> score;
            mps[name] += score;
            input[i] = make_pair(name, score);
        }
        for (const auto &p : mps) {
            maxi = max(maxi, p.second);
        }
        map<string, int> r;
        for (const auto &p : input) {
            r[p.first] += p.second;
            if (r[p.first] >= maxi && mps[p.first] >= maxi) {
                winner = p.first;
                break;
            }
        }

        cout << winner << '\n';
    }
    return 0;
}

