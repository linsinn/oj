//
// Created by sinn on 7/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

bool Solution() {
  if (K % 3 == 0) {
    ll np = N % (K + 1);
    if (np % 3 == 0 && np != K)
      return false;
  } else {
    if (N % 3 == 0)
      return false;
  }
  return true;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      if (Solution()) cout << "Alice\n";
      else cout << "Bob\n";
    }
  }
  return 0;
}
