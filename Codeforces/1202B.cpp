#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;
int ans[10][10];
int dp[10][10][10][10];

void Solution() {
  memset(dp, -1, sizeof dp);
  fora (i, 0, 10) {
    fora (j, i, 10) {
      fora (f, 0, 10) {
        dp[i][j][f][(f+i) % 10] = 1;
        dp[i][j][f][(f+j) % 10] = 1;
        while (true) {
          bool n = false;
          fora (t, 0, 10) {
            if (dp[i][j][f][t] != -1) {
             if (dp[i][j][f][(t+i) % 10] == -1 || dp[i][j][f][(t+i) % 10] > dp[i][j][f][t] + 1) {
               dp[i][j][f][(t+i) % 10] = dp[i][j][f][t] + 1;
               n = true;
             }
             if (dp[i][j][f][(t+j) % 10] == -1 || dp[i][j][f][(t+j) % 10] > dp[i][j][f][t] + 1) {
               dp[i][j][f][(t+j) % 10] = dp[i][j][f][t] + 1;
               n = true;
             }
            }
          }
          if (!n)
            break;
        }
      }
    }
  }

  fora (i, 0, 10) {
    fora (j, i, 10) {
      ll t = 0;
      fora (k, 1, s.size()) {
        int a = s[k-1] - '0';
        int b = s[k] - '0';
        if (dp[i][j][a][b] == -1) {
          t = -1;
          break;
        } else {
          t += dp[i][j][a][b] - 1;
        }
      }
      ans[i][j] = ans[j][i] = t;
    }
  }
  fora (i, 0, 10) {
    fora (j, 0, 10) {
      cout << ans[i][j] << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    Solution();
  }
  return 0;
}
