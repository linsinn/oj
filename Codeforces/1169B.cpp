//
// Created by sinn on 5/28/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 1;

using ll = long long;

int N, M, K;
pair<int ,int> ps[MAXN];

int Solution() {
  set<pair<int, int>> edges;
  for (int i = 0; i < M; ++i) {
    int a = ps[i].first, b = ps[i].second;
    edges.emplace(make_pair(min(a, b), max(a, b)));
  }
  map<int, int> cnts;
  for (auto& e : edges) {
    cnts[e.first]++;
    cnts[e.second]++;
  }
  int fi = 0, fi_t = 0;
  for (auto& p : cnts) {
    if (p.second > fi_t) {
      fi = p.first;
      fi_t = p.second;
    }
  }
  cnts.clear();
  int se_t = 0;
  for (auto& e : edges) {
    int a = e.first, b = e.second;
    if (a == fi || b == fi)
      continue;
    cnts[a]++;
    cnts[b]++;
    se_t = max(se_t, cnts[a]);
    se_t = max(se_t, cnts[b]);
  }
  return se_t == edges.size() - fi_t;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    for (int i = 0; i < M; ++i) {
      cin >> ps[i].first >> ps[i].second;
    }
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}
