//
// Created by sinn on 10/11/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

ll Solution() {
  K = (1 << M);
  int F = K - 1;
  vector<Vec> cnt(M, Vec(M, 0));
  fora (i, 1, s.size()) {
    cnt[s[i] - 'a'][s[i-1] - 'a']++;
    cnt[s[i-1] - 'a'][s[i] - 'a']++;
  }
  Vec dp(K, INF);
  Vec cntBit(K, 0);
  Vec minBit(K, 0);
  vector<Vec> d(K, Vec(M, 0));
  dp[0] = 0;
  fora (msk, 1, K) {
    cntBit[msk] = 1 + cntBit[msk & (msk - 1)];
    fora (i, 0, M) {
      if ((msk >> i) & 1) {
        minBit[msk] = i;
        break;
      }
    }
  }
  fora (msk, 1, K) {
    int b = minBit[msk];
    fora (i, 0, M) {
      d[msk][i] = d[msk & (msk - 1)][i] + cnt[b][i];
    }
  }
  fora (msk, 0, K) {
    fora (i, 0, M) {
      if ((msk >> i) & 1) continue;
      int pos = cntBit[msk];
      int nmsk = msk | (1 << i);
      Min(dp[nmsk], dp[msk] + pos * (d[msk][i] - d[F ^ nmsk][i]));
    }
  }
  return dp[F];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}
