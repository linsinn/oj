//
// Created by sinn on 4/29/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int s[MAXN], b[MAXN];

int Solution() {
    int mini = s[0], maxi = b[0];
    for (int i = 0; i < N; i++)
        mini = min(mini, s[i]);
    for (int i = 0; i < M; i++)
        maxi = max(maxi, b[i]);
    return max(K, K + (K / mini) * (maxi - mini));
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N >> M >> K) {
        for (int i = 0; i < N; i++) {
            cin >> s[i];
        }
        for (int i = 0; i < M; i++) {
            cin >> b[i];
        }
        cout << Solution() << '\n';
    }
    return 0;
}