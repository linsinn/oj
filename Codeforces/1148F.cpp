//
// Created by sinn on 6/2/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 1;

using ll = long long;

int N, M, K;
ll val[MAXN], mask[MAXN];

ll Solution() {
  ll sum = accumulate(val, val+N, 0LL);
  ll ans = 0;
  fora (b, 0, 62) {
    ll cur = 0;
    fora (i, 0, N) {
      if ((mask[i] & (1LL << b)) && (mask[i] < (1LL << (b+1))))
        cur += val[i];
    }
    if ((cur > 0 && sum > 0) || (cur < 0 && sum < 0)) {
      ans ^= 1LL << b;
      fora (i, 0, N) {
        if (mask[i] & (1LL << b))
          val[i] *= -1;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> val[i] >> mask[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}