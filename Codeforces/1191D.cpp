//
// Created by sinn on 7/13/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];

bool Solution() {
  map<int, int> mp;
  fora (i, 0, N) {
    mp[A[i]]++;
  }
  int twos = 0;
  for (auto it = mp.begin(); it != mp.end(); ++it) {
    auto p = *it;
    if (p.first == 0 && p.second > 1)
      return false;
    if (p.second > 2)
      return false;
    if (p.second >= 2) {
      twos++;
      if (it != mp.begin() && prev(it)->first == p.first - 1)
        return false;
    }
  }
  if (twos > 1)
    return false;
  sort(A, A + N);
  ll maxi = -1;
  int idx = 0;
  ll tot = 0;
  while (idx < N) {
    tot += max(0LL, A[idx] - (maxi + 1));
    if (A[idx] > maxi)
      maxi += 1;
    ++idx;
  }
  return tot % 2 == 1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    if (Solution()) {
      cout << "sjfnb\n";
    } else {
      cout << "cslnb\n";
    }
  }
  return 0;
}

