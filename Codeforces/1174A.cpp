//
// Created by sinn on 7/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int A[MAXN];

void Solution() {
  sort(A, A+2*N);
  if (A[0] == A[2*N-1]) {
    cout << -1 << '\n';
    return ;
  }
  fora (i, 0, 2 * N) {
    cout << A[i] << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, 2 * N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}
