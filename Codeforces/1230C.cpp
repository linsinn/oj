#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> gph;
Vec c;
bool seen[8];

void dfs(int u, set<Pii>& s) {
  seen[u] = true;
  for (int v : gph[u]) {
    if (c[v] <= c[u])
      s.emplace(c[v], c[u]);
    if (!seen[u])
      dfs(v, s);
  }
}

ll Solution() {
  c.resize(6);
  std::iota(c.begin(), c.end(), 0);
  c.emplace_back(0);
  int ans = 0;
  do {
    memset(seen, 0, sizeof(seen));
    set<Pii> s;
    fora (i, 0, N) {
      if (!seen[i])
        dfs(i, s);
    }
    ans = max(ans, (int)s.size());
  } while (std::next_permutation(c.begin(), c.end()));
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    gph.resize(N);
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      --u, --v;
      gph[u].emplace_back(v);
      gph[v].emplace_back(u);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
