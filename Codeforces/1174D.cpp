//
// Created by sinn on 7/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

void Solution() {
  vector<ll> ans;
  ll maxi = 1 << N;
  if (K >= maxi) {
    cout << maxi - 1 << '\n';
    fora (i, 1, maxi) {
      cout << (i ^ (i - 1)) << ' ';
    }
    cout << '\n';
  } else {
    set<ll> unavail;
    ans.emplace_back(0);
    unavail.emplace(K);
    fora (i, 1, maxi) {
      if (unavail.count(i) == 0) {
        ans.emplace_back(i);
        unavail.emplace(K ^ i);
      }
    }
    cout << ans.size() - 1 << '\n';
    fora (i, 1, ans.size()) {
      cout << (ans[i] ^ ans[i-1]) << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    Solution();
  }
  return 0;
}
