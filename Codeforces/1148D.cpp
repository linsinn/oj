//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 10;

using ll = long long;

int N, M, K;
pair<int, int> A[MAXN];
vector<pair<int, int>> fk, sk;

struct Cmp1 {
  bool operator () (const pair<int, int>& a, const pair<int, int>& b) {
    return a.first > b.first;
  }
};

struct Cmp2 {
  bool operator () (const pair<int, int>& a, const pair<int, int>& b) {
    return a.first < b.first;
  }
};

void Solution() {
  fk.clear();
  sk.clear();
  fora (i, 0, N) {
    if (A[i].first < A[i].second)
      fk.emplace_back(make_pair(A[i].second, i+1));
    else
      sk.emplace_back(make_pair(A[i].second, i+1));
  }
  if (fk.size() > sk.size()) {
    sort(fk.begin(), fk.end(), Cmp1());
    cout << fk.size() << '\n';
    fora (i, 0, fk.size()) {
      cout << fk[i].second << ' ';
    }
    cout << '\n';
  } else {
    sort(sk.begin(), sk.end(), Cmp2());
    cout << sk.size() << '\n';
    fora (i, 0, sk.size()) {
      cout << sk[i].second << ' ';
    }
    cout << '\n';

  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i].first >> A[i].second;
    }
    Solution();
  }
  return 0;
}