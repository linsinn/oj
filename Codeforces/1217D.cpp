#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<vector<Pii>> gph;
vector<int> cols, seen;
int used_cols;

void dfs(int u) {
  seen[u] = 1;
  for (auto &p : gph[u]) {
    if (seen[p.first] == 1) {
      cols[p.second] = 1;
      used_cols = 2;
    } else if (seen[p.first] == 0) {
      dfs(p.first);
    }
  }
  seen[u] = 2;
}

void Solution() {
  used_cols = 1;
  cols.assign(M, 0);
  seen.assign(N+1, 0);
  fora (i, 1, N+1) {
    if (!seen[i]) {
      dfs(i);
    }
  }
  cout << used_cols << '\n';
  fora (i, 0, M) {
    cout << cols[i] + 1 << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    gph.assign(N+1, vector<Pii>());
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      gph[u].emplace_back(v, i);
    }
    Solution();
  }
  return 0;
}
