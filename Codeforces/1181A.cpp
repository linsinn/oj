//
// Created by sinn on 6/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
ll x, y, z;

void Solution() {
  ll a = x % z;
  ll b = y % z;
  ll ans0 = x / z + y / z + (a + b) / z;
  ll ans1 = 0;
  if (a + b >= z) {
    ans1 = min(z-a, z-b);
  }
  cout << ans0 << ' ' << ans1 << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> x >> y >> z) {
    Solution();
  }
  return 0;
}
