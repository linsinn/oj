#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<ll> A;
vector<Vec> gph;
vector<bool> seen;
vector<map<ll, int>> mps;

void dfs(int u, ll& ans, map<ll, int>& mp) {
  seen[u] = true;
  map<ll, int> nmp;
  nmp[A[u]]++;
  ans += A[u];
  ans %= MOD;
  for (auto &p : mp) {
    ll g = __gcd(p.first, A[u]);
    ans += g % MOD * p.second;
    ans %= MOD;
    nmp[g] += p.second;
  }
  for (int v : gph[u]) {
    if (!seen[v]) {
      dfs(v, ans, nmp);
    }
  }
}

ll Solution() {
  seen.assign(N, false);
  mps.assign(N, map<ll, int>());
  ll ans = 0;
  map<ll, int> mp;
  dfs(0, ans, mp);
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    gph.assign(N, Vec());
    int u, v;
    fora (i, 0, N-1) {
      cin >> u >> v;
      --u, --v;
      gph[u].emplace_back(v);
      gph[v].emplace_back(u);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
