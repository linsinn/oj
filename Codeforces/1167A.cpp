//
// Created by sinn on 5/16/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s;

string Solution() {
  int eight = 0;
  while (eight < s.size() && s[eight] != '8') {
    eight++;
  }
  if (s.size() - eight >= 11)
    return "YES";
  return "NO";
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int t;
  cin >> t;
  while (t--) {
    cin >> N;
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}
