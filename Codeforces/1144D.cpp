//
// Created by sinn on 4/13/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

void Solution() {
    map<int, int> mps;
    for (int i = 0; i < N; i++)
        mps[A[i]]++;
    int most_val = A[0], most_cnt = 1;
    for (auto p : mps) {
        if (p.second > most_cnt) {
            most_cnt = p.second;
            most_val = p.first;
        }
    }
    queue<int> que;
    for (int i = 0; i < N; i++) {
        if (A[i] == most_val)
            que.emplace(i);
    }
    cout << N - most_cnt << '\n';
    while (!que.empty()) {
        int idx = que.front();
        que.pop();
        if (idx > 0) {
            if (A[idx-1] < most_val) {
                cout << 1 << ' ' << idx << ' ' << idx+1 << '\n';
                que.emplace(idx-1);
            } else if (A[idx-1] > most_val) {
                cout << 2 << ' ' << idx << ' ' << idx+1 << '\n';
                que.emplace(idx-1);
            }
            A[idx-1] = most_val;
        }
        if (idx < N-1) {
            if (A[idx+1] < most_val) {
                cout << 1 << ' ' << idx+2 << ' ' << idx+1 << '\n';
                que.emplace(idx+1);
            } else if (A[idx+1] > most_val){
                cout << 2 << ' ' << idx+2 << ' ' << idx+1 << '\n';
                que.emplace(idx+1);
            }
            A[idx+1] = most_val;
        }
    }
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++) {
            cin >> A[i];
        }
        Solution();
    }
    return 0;
}

