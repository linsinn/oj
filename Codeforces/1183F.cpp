//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int A[MAXN];

ll Solution() {
  set<int> s;
  fora (i, 0, N) s.emplace(A[i]);
  int maxi = *s.rbegin();
  int ans = maxi;
  if (maxi % 30 == 0) {
    if (s.count(maxi/2) && s.count(maxi/3) && s.count(maxi/5)) {
      ans = max(ans, maxi/2 + maxi/3 + maxi/5);
    }
  }
  vector<int> res;
  while (!s.empty() && res.size() < 3) {
    int g = *s.rbegin();
    s.erase(prev(s.end()));
    bool ok = true;
    for (int r : res)
      ok &= (r % g) != 0;
    if (ok)
      res.emplace_back(g);
  }
  ans = max(ans, accumulate(res.begin(), res.end(), 0));
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      fora (i, 0, N) cin >> A[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}
