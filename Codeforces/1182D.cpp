//
// Created by sinn on 6/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
vector<int> g[MAXN];
int vis[MAXN];
int sz[MAXN];
int st[MAXN];

void dfs(int u) {
  vis[u] = true;
  sz[u] = 1;
  for (int v : g[u]) {
    if (!vis[v]) {
      dfs(v);
      sz[u] += sz[v];
    }
  }
}

int centr(int u) {
  vis[u] = 1;
  ll sm = 0;
  ll mx = 0;
  ll ver = -1;
  for (int v : g[u]) {
    if (!vis[v]) {
      sm += sz[v];
      if (mx < sz[v]) {
        ver = v;
        mx = sz[v];
      }
    }
  }
  if (ver == -1) return u;
  if (sm >= N / 2 && mx <= N / 2)
    return u;
  else return centr(ver);
}

bool check(int u) {
  queue<pair<int, int>> que;
  que.emplace(u, 0);
  while (!que.empty()) {
    auto p = que.front();
    que.pop();
    vis[p.first] = true;
    if (st[p.second] && g[p.first].size() != st[p.second])
      return false;
    st[p.second] = g[p.first].size();
    for (int q : g[p.first]) {
      if (!vis[q])
        que.emplace(q, p.second+1);
    }
  }
  return true;
}

pair<int, int> dfs2(int u, int kol) {
  vis[u] = true;
  if (g[u].size() > 2)
    return {-1, -1};
  for (int v : g[u]) {
    if (!vis[v])
      return dfs2(v, kol + 1);
  }
  return {u, kol};
}

int Solution() {
  memset(sz, 0, sizeof(sz));
  memset(vis, 0, sizeof(vis));
  dfs(0);
  memset(vis, 0, sizeof(vis));
  int c = centr(0);
  memset(vis, 0, sizeof(vis));
  int kek = -1;
  vis[c] = true;
  map<int, pair<int, int>> pis;
  for (int v : g[c]) {
    pair<int, int> mem = dfs2(v, 0);
    if (mem.first != -1) {
      pis[mem.second].first = mem.first;
      pis[mem.second].second++;
    }
  }
  for (auto& p : pis) {
    if (p.second.second == 1) {
      kek = p.second.first;
    }
  }
  if (kek != -1) {
    c = kek;
  }
  memset(vis, 0, sizeof(vis));
  if (check(c))
    return c + 1;
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int u, v;
    fora (i, 1, N) {
      cin >> u >> v;
      --u, --v;
      g[u].emplace_back(v);
      g[v].emplace_back(u);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
