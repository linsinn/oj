//
// Created by sinn on 7/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string board[MAXN];

ll Solution() {
  vector<int> r(N, 0), c(M, 0);
  fora (i, 0, N) {
    fora (j, 0, M) {
      if (board[i][j] == '*') {
        ++r[i];
        ++c[j];
      }
    }
  }
  ll ans = 1e8;
  fora (i, 0, N) {
    fora (j, 0, M) {
      ll g = M - r[i] + N - c[j];
      if (board[i][j] == '.')
        --g;
      ans = min(ans, g);
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      fora (i, 0, N) cin >> board[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}

