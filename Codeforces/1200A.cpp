#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

void Solution() {
  Vec rs(10, 0);
  for (char ch : s) {
    if (ch == 'L') {
      fora (i, 0, 10) {
        if (rs[i] == 0) {
          rs[i] = 1;
          break;
        }
      }
    } else if (ch == 'R') {
      ford (i, 9, 0) {
        if (rs[i] == 0) {
          rs[i] = 1;
          break;
        }
      }
    } else {
      int c = ch - '0';
      rs[c] = 0;
    }
  }
  fora (i, 0, 10) 
    cout << rs[i];
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    Solution();
  }
  return 0;
}
