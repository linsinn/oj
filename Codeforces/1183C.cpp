//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll k, n, a, b;

bool Check(ll mid) {
  ll g = mid * a + (n - mid) * b;
  return g < k;
}

ll Solution() {
  if ((k - 1) / b < n) {
    return -1;
  }
  ll lb = 0, rb = min((k - 1) / a, n);
  ll ans = 0;
  while (lb <= rb) {
    ll mid = lb + ((rb - lb) >> 1);
    if (Check(mid)) {
      ans = mid;
      lb = mid + 1;
    } else {
      rb = mid - 1;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> k >> n >> a >> b;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
