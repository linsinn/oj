//
// Created by sinn on 7/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
vector<int> gph[MAXN];
ll su[MAXN], tot[MAXN];
bool vis[MAXN];


void dfs1(int u) {
  vis[u] = true;
  su[u] = 1;
  for (int v : gph[u]) {
    if (!vis[v]) {
      dfs1(v);
      su[u] += su[v];
      tot[u] += tot[v];
    }
  }
  tot[u] += su[u];
}

ll dfs2(int u, ll fp, ll &ans) {
  vis[u] = true;
  vector<ll> ch;
  ll maxi = 0;
  for (int v : gph[u]) {
    if (!vis[v]) {
      ll k = fp + tot[u] - tot[v] - su[u] + N - su[v];
      ll g = dfs2(v, k, ans);
      maxi = max(maxi, g + k);
    }
  }
  if (ch.empty()) {
    ans = max(ans, fp + N);
    return N;
  } else {
    ans = max(ans, maxi);
    return maxi;
  }
}

ll Solution() {
  memset(vis, 0, sizeof(vis));
  dfs1(1);
  memset(vis, 0, sizeof(vis));
  ll ans = 0;
  dfs2(1, 0, ans);
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int u, v;
    fora (i, 0, N-1) {
      cin >> u >> v;
      gph[u].emplace_back(v);
      gph[v].emplace_back(u);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
