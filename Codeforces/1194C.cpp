//
// Created by sinn on 7/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string s, t, p;

ll Solution() {
  vector<bool> vis(t.size(), false);
  int i = 0, j = 0;
  while (i < s.size() && j < t.size()) {
    if (s[i] == t[j]) {
      ++i;
      vis[j] = true;
    }
    ++j;
  }
  if (i < s.size())
    return false;
  map<char, int> mp;
  for (char q : p) mp[q]++;
  fora (k, 0, t.size()) {
    if (!vis[k]) {
      auto it = mp.find(t[k]);
      if (it == mp.end()) return false;
      if (it->second == 1)
        mp.erase(it);
      else
        mp[it->first]--;
    }
  }
  return true;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> s >> t >> p;
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
