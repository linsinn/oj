#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
ll sx, sy, ex, ey;

bool Solution() {
  ll n = N / K, m = M / K;
  ll b[2];
  if (sx == 1) {
    b[0] = (sy - 1) / n;
  } else {
    b[0] = (sy - 1) / m;
  }
  if (ex == 1) {
    b[1] = (ey - 1) / n;
  } else {
    b[1] = (ey - 1) / m;
  }
  return b[0] == b[1];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> T) {
    K = __gcd(N, M);
    while (T--) {
      cin >> sx >> sy >> ex >> ey;
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
