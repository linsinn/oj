//
// Created by sinn on 7/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int A[MAXN];

ll Solution() {
  map<int, int> mp;
  fora (i, 0, N) {
    mp[A[i]]++;
  }
  map<int, int> cnt;
  for (auto &p : mp) {
    cnt[p.second]++;
  }
  ll ans = N;
  while (ans > 0) {
    if (cnt.size() == 1 && (cnt.begin()->first == 1 || cnt.begin()->first == ans))
      return ans;
    if (cnt.size() == 2) {
      if (cnt.begin()->first + 1 == cnt.rbegin()->first && cnt.rbegin()->second == 1)
        return ans;
      if (cnt.begin()->first == 1 && cnt.begin()->second == 1)
        return ans;
    }
    int g = mp[A[ans-1]];
    cnt[g]--;
    if (cnt[g] == 0)
      cnt.erase(g);
    if (g - 1 != 0)
      cnt[g-1]++;
    mp[A[ans-1]]--;
    --ans;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
