#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> v;

bool Cmp(const Pii& a, const Pii& b) {
  return a.first + a.second > b.first + b.second;
}

ll Solution() {
  vector<Pii> pos, neg;
  fora (i, 0, N) {
    if (v[i].second >= 0) pos.emplace_back(v[i]);
    else {
      v[i].first = max(v[i].first, -v[i].second);
      neg.emplace_back(v[i]);
    }
  }
  sort(pos.begin(), pos.end());
  sort(neg.begin(), neg.end(), Cmp);
  int taken = 0;
  fora (i, 0, pos.size()) {
    if (K >= pos[i].first) {
      ++taken;
      K += pos[i].second;
    }
  }
  vector<Vec> dp(neg.size() + 1, Vec(K+1, 0));
  dp[0][K] = taken;
  fora (i, 0, neg.size()) {
    fora (j, 0, K+1) {
      if (j >= neg[i].first && j + neg[i].second >= 0) {
        Max(dp[i+1][j+neg[i].second], dp[i][j] + 1);
      }
      Max(dp[i+1][j], dp[i][j]);
    }
  }
  int ans = 0;
  fora (j, 0, K+1)
    Max(ans, dp[neg.size()][j]);
  return ans == N;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    v.resize(N);
    fora (i, 0, N) {
      cin >> v[i].first >> v[i].second;
    }
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}
