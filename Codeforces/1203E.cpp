#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  map<int, int> cnt;
  set<int> ss;
  for (int a : A) {
    cnt[a]++;
  }
  for (auto &p : cnt) {
    if (p.second >= 3) {
      ss.emplace(p.first - 1);
      ss.emplace(p.first);
      ss.emplace(p.first + 1);
    } else if (p.second == 2) {
      ss.emplace(p.first);
      if (ss.count(p.first - 1) || p.first - 1 == 0) {
        ss.emplace(p.first + 1);
      } else {
        ss.emplace(p.first - 1);
      }
    } else {
      if (ss.count(p.first - 1) || p.first - 1 == 0) {
        if (ss.count(p.first)) 
          ss.emplace(p.first + 1);
        else
          ss.emplace(p.first);
      } else {
        ss.emplace(p.first - 1);
      }
    }
  }
  if (*ss.begin() == 0) 
    ss.emplace(1);
  return ss.size() - (*ss.begin() == 0);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
