#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
struct Node {
  int x, y;
  int cost;
};
vector<Node> A;
vector<Pii> ev[MAXN];
Vec vals;
Pll t[4 * MAXN];
ll add[4 * MAXN];

void Build(int v, int l, int r) {
  if (l == r) {
    t[v] = make_pair(-vals[l], l);
    add[v] = 0;
    return ;
  }
  int m = (l + r) >> 1;
  Build(v * 2, l, m);
  Build(v * 2 + 1, m+1, r);
  t[v] = max(t[v * 2], t[v * 2 + 1]);
}

void Push(int v, int l, int r) {
  if (add[v] == 0) return ;
  t[v].first += add[v];
  if (l != r) {
    add[v * 2] += add[v];
    add[v * 2 + 1] += add[v];
  }
  add[v] = 0;
}

void Upd(int v, int l, int r, int L, int R, int val) {
  Push(v, l, r);
  if (L > R) return ;
  if (l == L && r == R) {
    add[v] += val;
    Push(v, l, r);
    return ;
  }
  int m = (l + r) >> 1;
  Upd(v * 2, l, m, L, min(m, R), val);
  Upd(v * 2 + 1, m+1, r, max(m+1, L), R, val);
  t[v] = max(t[v*2], t[v*2+1]);
}

Pll Get(int v, int l, int r, int L, int R) {
  Push(v, l, r);
  if (L > R) return make_pair(-1e18, 0);
  if (l == L && r == R) return t[v];
  int m = (l + r) >> 1;
  auto a = Get(v * 2, l, m, L, min(m, R));
  auto b = Get(v * 2 + 1, m+1, r, max(m+1, L), R);
  return max(a, b);
}

void Solution() {
  fora (i, 0, N) {
    vals.emplace_back(A[i].x);
    vals.emplace_back(A[i].y);
  }
  vals.emplace_back(0);
  sort(vals.begin(), vals.end());
  vals.emplace_back(vals.back() + 1);
  vals.resize(unique(vals.begin(), vals.end()) - vals.begin());

  fora (i, 0, N) {
    int x = lower_bound(vals.begin(), vals.end(), A[i].x) - vals.begin();
    int y = lower_bound(vals.begin(), vals.end(), A[i].y) - vals.begin();
    ev[min(x, y)].emplace_back(max(x, y), A[i].cost);
  }
  N = vals.size();
  Build(1, 0, N-1);
  ll ans = -1;
  ll ansl = -1, ansr = -1;
  ford (i, N-1, 0) {
    for (auto it : ev[i])
      Upd(1, 0, N-1, it.first, N-1, it.second);
    auto cur = Get(1, 0, N-1, i, N-1);
    if (cur.first + vals[i] > ans) {
      ans = cur.first + vals[i];
      ansl = vals[i];
      ansr = vals[cur.second];
    }
  }
  cout << ans << '\n' << ansl << ' ' << ansl << ' ' << ansr << ' ' << ansr << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) {
      cin >> A[i].x >> A[i].y >> A[i].cost;
    }
    Solution();
  }
  return 0;
}
