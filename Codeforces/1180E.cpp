//
// Created by sinn on 6/22/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e6 + 10;
//constexpr int MAXN = 3;

using ll = long long;

int N, M, K, T;
int A[MAXN], B[MAXN];
int Q[MAXN][3];
struct Node {
  bool lazy = false;
  int sum = 0;
  int mini = 0;
} nodes[MAXN * 4];

void PushDown(int root, int lb, int rb) {
  if (!nodes[root].lazy || nodes[root].sum == 0) return ;
  nodes[root].lazy = false;
  int tmp = nodes[root].sum;
  nodes[root].sum = 0;
  if (lb == rb) return ;
  int lc = root << 1, rc = root << 1 | 1;
  nodes[lc].sum += tmp;
  nodes[rc].sum += tmp;
  nodes[lc].mini += tmp;
  nodes[rc].mini += tmp;
  nodes[lc].lazy = true;
  nodes[rc].lazy = true;
}

void Change(int root, int lb, int rb, int l, int r, int chg) {
  if (rb < l || r < lb) return ;
  if (l <= lb && rb <= r) {
    nodes[root].sum += chg;
    nodes[root].mini += chg;
    nodes[root].lazy = true;
    return ;
  }
  PushDown(root, lb, rb);
  int mid = lb + ((rb - lb) >> 1);
  int lc = root << 1, rc = root << 1 | 1;
  Change(lc, lb, mid, l, r, chg);
  Change(rc, mid+1, rb, l, r, chg);
  nodes[root].mini = min(nodes[lc].mini, nodes[rc].mini);
}

int Query(int root, int lb, int rb) {
  PushDown(root, lb, rb);
  if (lb == rb)
    return lb;
  int mid = lb + ((rb - lb) >> 1);
  int lc = root << 1, rc = root << 1 | 1;
  if (nodes[rc].mini < 0)
    return Query(rc, mid+1, rb);
  else if (nodes[lc].mini < 0)
    return Query(lc, lb, mid);
  else return -1;
}

void Solution() {
  multiset<int> msa;
  fora (i, 0, N) {
    msa.emplace(A[i]);
  }
  for (auto it = msa.rbegin(); it != msa.rend(); ++it) {
    Change(1, 1, MAXN, 1, *it, -1);
  }
  fora (i, 0, M) {
    Change(1, 1, MAXN, 1, B[i], 1);
  }
  fora (i, 0, K) {
    int tp = Q[i][0], idx = Q[i][1]-1, val = Q[i][2];
    if (tp == 1) {
      Change(1, 1, MAXN, 1, A[idx], 1);
      A[idx] = val;
      Change(1, 1, MAXN, 1, A[idx], -1);
    } else {
      Change(1, 1, MAXN, 1, B[idx], -1);
      B[idx] = val;
      Change(1, 1, MAXN, 1, B[idx], 1);
    }
    cout << Query(1, 1, MAXN) << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) cin >> A[i];
    fora (i, 0, M) cin >> B[i];
    cin >> K;
    fora (i, 0, K) cin >> Q[i][0] >> Q[i][1] >> Q[i][2];
    Solution();
  }
  return 0;
}