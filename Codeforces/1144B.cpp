//
// Created by sinn on 4/13/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int a[MAXN];

int Solution() {
    multiset<int> ss[2];
    for (int i = 0; i < N; i++) {
        if (a[i] & 1) {
            ss[0].emplace(a[i]);
        } else {
            ss[1].emplace(a[i]);
        }
    }
    int cur = ss[0].size() > ss[1].size() ? 0 : 1;
    while (!ss[cur].empty()) {
        ss[cur].erase(--ss[cur].end());
        cur ^= 1;
    }
    int ans = 0;
    for (int e : ss[cur^1])
        ans += e;
    return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++) {
            cin >> a[i];
        }
        cout << Solution() << '\n';
    }
    return 0;
}