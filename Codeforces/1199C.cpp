#include <bits/stdc++.h>
using namespace std;
 
#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)
 
constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
 
using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;
 
using ll = long long;
 
ll N, M, K, T;
Vec A;
 
int CntErase(const map<int, int>& mp) {
  int k = M * 8 / N;
  if (k >= 31)
    return 0;
  return mp.size() - min((1 << k), (int)mp.size());
}
 
ll Solution() {
  map<int, int> cnts;
  fora (i, 0, N) {
    cnts[A[i]]++;
  }
  Vec v;
  for (auto &p : cnts) {
    v.emplace_back(p.second);
  }
  vector<ll> su(v.size() + 1, 0);
  fora (i, 0, v.size()) {
    su[i+1] = su[i] + v[i];
  }
  int k = CntErase(cnts);
  ll ans = INT64_MAX;
  fora (i, 0, k+1) {
    ans = min(ans, su[i] + su[v.size()] - su[v.size() - (k - i)]);
  }
  return ans;
}
 
int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    A.resize(N);
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
