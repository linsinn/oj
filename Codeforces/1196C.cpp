#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;

struct Robot {
  int x, y;
  bool foo[4];
};
vector<Robot> rbts;

void Solution() {
  int bt = -1e5, tp = 1e5;
  int lf = -1e5, rg = 1e5;
  fora (i, 0, N) {
    auto &p = rbts[i];
    if (!p.foo[0]) {
      bt = max(bt, p.x);
    }
    if (!p.foo[1]) {
      rg = min(rg, p.y);
    }
    if (!p.foo[2]) {
      tp = min(tp, p.x);
    }
    if (!p.foo[3]) {
      lf = max(lf, p.y);
    }
  }
  if (bt > tp || lf > rg) {
    cout << "0\n";
  } else {
    cout << 1 << ' ' << bt << ' ' << lf << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      rbts.resize(N);
      fora (i, 0, N) {
        cin >> rbts[i].x >> rbts[i].y;
        fora (k, 0, 4) {
          cin >> rbts[i].foo[k];
        }
      }
      Solution();
    }
  }
  return 0;
}
