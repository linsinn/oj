//
// Created by sinn on 7/17/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll h[2][MAXN];
ll memos[4][MAXN];

ll Solution() {
  memos[0][0] = h[0][0];
  memos[1][0] = h[1][0];
  memos[2][0] = memos[3][0] = 0;
  fora (i, 1, N) {
    memos[0][i] = h[0][i] + max(memos[1][i-1], memos[3][i-1]);
    memos[1][i] = h[1][i] + max(memos[0][i-1], memos[2][i-1]);
    memos[2][i] = max(memos[0][i-1], memos[2][i-1]);
    memos[3][i] = max(memos[1][i-1], memos[3][i-1]);
  }
  ll ans = 0;
  fora (i, 0, 4) {
    ans = max(ans, memos[i][N-1]);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> h[0][i];
    fora (i, 0, N) cin >> h[1][i];
    cout << Solution() << '\n';
  }
  return 0;
}