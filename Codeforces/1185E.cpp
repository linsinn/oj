//
// Created by sinn on 6/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e3 + 10;

using ll = long long;

int N, M, K, T;
string sheet[MAXN];

void Solution() {
  map<char, map<int, vector<int>>> row;
  map<char, map<int, vector<int>>> col;
  fora (i, 0, N) {
    fora (j, 0, M) {
      char ch = sheet[i][j];
      row[ch][i].emplace_back(j);
      col[ch][j].emplace_back(i);
    }
  }
  vector<vector<int>> ans;
  fora (ch, 'a', 'z'+1) {
    if (row.find(ch) == row.end()) {
      auto it = row.upper_bound(ch);
      if (it == row.end()) continue;
      auto& t = *(it->second.begin());
      vector<int> v = {t.first, t.second[0], t.first, t.second[0]};
      ans.emplace_back(v);
      continue;
    }
    auto& r = row.find(ch)->second;
    auto& c = col.find(ch)->second;
    if (r.size() == 1) {
      auto& v = r.begin()->second;
      fora (k, v[0], v.back()+1) {
        if (sheet[r.begin()->first][k] < ch) {
          cout << "NO\n";
          return ;
        }
      }
      vector<int> g = {r.begin()->first, v[0], r.begin()->first, v.back()};
      ans.emplace_back(g);
    } else if (c.size() == 1) {
      auto& v = c.begin()->second;
      fora (k, v[0], v.back()+1) {
        if (sheet[k][c.begin()->first] < ch) {
          cout << "NO\n";
          return ;
        }
      }
      vector<int> g = {v[0], c.begin()->first, v.back(), c.begin()->first};
      ans.emplace_back(g);
    } else {
      cout << "NO\n";
      return ;
    }
  }
  cout << "YES\n";
  cout << ans.size() << '\n';
  for (auto& v : ans) {
    for (auto& e : v) {
      cout << e+1 << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      fora (i, 0, N) {
        cin >> sheet[i];
      }
      Solution();
    }
  }
  return 0;
}