#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<string> gph;
vector<Vec> dist;
Vec A;

void CalcDist(int u) {
  vector<bool> seen(N+1, false);
  queue<Pii> que;
  que.emplace(u, 0);
  seen[u] = true;
  while (!que.empty()) {
    auto cur = que.front(); que.pop();
    dist[u][cur.first] = cur.second;
    for (int i = 0; i < N; ++i) {
      if (gph[cur.first][i] == '0') continue;
      int v = i;
      if (!seen[v]) {
        seen[v] = true;
        que.emplace(v, cur.second + 1);
      }
    }
  }
}

void Solution() {
  dist.assign(N+1, Vec(N+1, -1));
  fora (i, 0, M) --A[i];
  fora (i, 0, N) CalcDist(i);
  Vec ans;
  ans.emplace_back(0);
  fora (i, 1, M) {
    while (ans.size() > 1 && dist[A[ans[ans.size() - 2]]][A[i]] == i - ans[ans.size() - 2])
      ans.pop_back();
    ans.emplace_back(i);
  }
  cout << ans.size() << '\n';
  for (int i : ans) {
    cout << A[i] + 1 << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    gph.resize(N);
    fora (i, 0, N) cin >> gph[i];
    cin >> M;
    A.resize(M);
    fora (i, 0, M) cin >> A[i];
    Solution();
  }
  return 0;
}
