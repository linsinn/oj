#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A, B;

void Solution() {
  vector<char> ans(N+1, 0);
  vector<int> am(N+1);
  vector<int> bm(N+1);
  fora (i, 0, N) {
    am[A[i]] = i;
    bm[B[i]] = i;
  }
  map<int, char> mp;
  int bnd = 0;
  fora (i, 0, N) {
    int a = A[i];
    int b = bm[a];
    if (b >= bnd) {
      auto it = mp.lower_bound(i);
      char ch;
      if (it == mp.end()) {
        ch = mp.empty() ? 'a' : min('z', char(mp.rbegin()->second + 1));
      } else {
        ch = it->second;
      }
      fora (j, bnd, b+1) {
        mp[am[B[j]]] = ch;
        ans[B[j]] = ch;
      }
      bnd = b + 1;
    }
  }
  if (mp.rbegin()->second - 'a' + 1 >= K) {
    cout << "YES\n";
    fora (i, 1, N+1) 
      cout << ans[i];
    cout << '\n';
  } else {
    cout << "NO\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    A.resize(N);
    B.resize(N);
    fora (i, 0, N) cin >> A[i];
    fora (i, 0, N) cin >> B[i];
    Solution();
  }
  return 0;
}
