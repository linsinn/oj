//
// Created by sinn on 10/17/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD =  998244353;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
vector<Vec> dp;

ll dfs(int n, int p) {
  if (dp[n][p] != -1)
    return dp[n][p];
  int k = 0;
  while ((1 << (k + 1)) - 1 <= n) {
    ++k;
  }
  k -= 1;
  int h = (1 << k) - 1;
  int hf = 1 << k;
  int rem = n - h * 2 - 1;
  int st = h + 1 + max(0, rem - hf);
  int ed = h + hf + 1 - max(0, hf - rem);
  ll ret = 0;
  fora (i, st, ed + 1) {
    if (i % 2 == p) {
      ll a = dfs(i - 1, i % 2 ^ 1);
      ll b = dfs(n - i, 0);
      ret += a * b % MOD;
      ret %= MOD;
    }
  }
  dp[n][p] = ret;
  return ret;
}

ll Solution() {
  dp.resize(N+10, Vec(2, -1));
  dp[1][0] = 0;
  dp[1][1] = 1;
  dp[2][0] = 1;
  dp[2][1] = 0;
  dp[3][0] = dp[3][1] = 0;
  dp[4][0] = 1;
  dp[4][1] = 0;
  ll a = dfs(N, 0);
  ll b = dfs(N, 1);
  return (a + b) % MOD;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}