//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];

void Solution() {
  sort(A, A+N);
  deque<int> dq;
  int ty = 0;
  fora (i, 0, N) {
    if (ty == 0) dq.emplace_back(A[i]);
    else dq.emplace_front(A[i]);
    ty ^= 1;
  }
  bool ok = true;
  fora (i, 0, N) {
    int a = dq.at((i-1+N) % N);
    int b = dq.at(i);
    int c = dq.at((i+1) % N);
    if (b >= a + c) {
      ok = false;
      break;
    }
  }
  if (!ok) {
    cout << "NO\n";
  } else {
    cout << "YES\n";
    for (int elem : dq) {
      cout << elem << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
