#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;
vector<Vec> bits;

void Add(int idx, int p, int val) {
  while (p <= N) {
    bits[idx][p] += val;
    p += p & -p;
  }
}

int Sum(int idx, int p) {
  int ret = 0;
  while (p > 0) {
    ret += bits[idx][p];
    p -= p & -p;
  }
  return ret;
}

void Init() {
  N = s.size();
  M = 26;
  bits.assign(M, Vec(N + 1));
  fora (i, 0, N) {
    Add(s[i] - 'a', i+1, 1);
  }
}

int Query(int l, int r) {
  int ans = 0;
  fora (i, 0, M) {
    int c = Sum(i, r) - Sum(i, l-1);
    if (c)
      ++ans;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    Init();
    cin >> T;
    int op, p, l, r;
    char ch;
    while (T--) {
      cin >> op;
      if (op == 2) {
        cin >> l >> r;
        cout << Query(l, r) << '\n';
      } else {
        cin >> p >> ch;
        Add(s[p-1] - 'a', p, -1);
        Add(ch - 'a', p, 1);
        s[p-1] = ch;
      }
    }
  }
  return 0;
}
