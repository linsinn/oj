//
// Created by sinn on 6/22/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
ll A[MAXN], Q[MAXN];

int Solution() {
  vector<pair<int, int>> ans;
  int maxi = *max_element(A, A+N);
  deque<int> deq(A, A+N);
  while (deq.front() != maxi) {
    int a = deq.front(); deq.pop_front();
    int b = deq.front(); deq.pop_front();
    ans.emplace_back(a, b);
    deq.emplace_front(max(a, b));
    deq.emplace_back(min(a, b));
  }
  vector<int> arr(deq.begin(), deq.end());
  fora (i, 0, M) {
    ll q = Q[i] - 1;
    if (q < ans.size())
      cout << ans[q].first << ' ' << ans[q].second << '\n';
    else {
      int g = (q - ans.size()) % (N - 1);
      cout << maxi << ' ' << arr[g+1] << '\n';
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) cin >> A[i];
    fora (i, 0, M) cin >> Q[i];
    Solution();
  }
  return 0;
}
