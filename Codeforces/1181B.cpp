//
// Created by sinn on 6/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
string num;

string GetSum(string& a, string& b) {
  if (a.empty()) return b;
  if (b.empty()) return a;
  int i = a.size() - 1, j = b.size() - 1;
  int cy = 0;
  string ret;
  while (cy > 0 || i >= 0 || j >= 0) {
    int n = i >= 0 ? a[i] - '0' : 0;
    int m = j >= 0 ? b[j] - '0' : 0;
    int k = n + m + cy;
    cy = k / 10;
    ret += k % 10 + '0';
    --i;
    --j;
  }
  reverse(ret.begin(), ret.end());
  return ret;
}

pair<int, int> NonZero(int x) {
  int pre = x, nxt = x;
  while (nxt < N && num[nxt] == '0') {
    ++nxt;
  }
  while (pre >= 0 && num[pre] == '0') {
    --pre;
  }
  return {pre, nxt};
}

struct Cmp {
  bool operator () (const string& lhs, const string& rhs) const{
    if (lhs.size() < rhs.size()) return true;
    if (rhs.size() < lhs.size()) return false;
    return lhs < rhs;
  }
};

string Solution() {
  set<string, Cmp> mp;
  if (N % 2 == 0) {
    int mid = N / 2;
    auto z = NonZero(mid);
    string l = num.substr(0, z.first);
    string r = num.substr(z.first, N - z.first);
    mp.emplace(GetSum(l, r));
    l = num.substr(0, z.second);
    r = num.substr(z.second, N - z.second);
    mp.emplace(GetSum(l, r));
  } else {
    int mid = N / 2;
    auto z = NonZero(mid);
    string l = num.substr(0, z.first);
    string r = num.substr(z.first, N - z.first);
    mp.emplace(GetSum(l, r));
    l = num.substr(0, z.second);
    r = num.substr(z.second, N - z.second);
    mp.emplace(GetSum(l, r));
    z = NonZero(mid+1);
    l = num.substr(0, z.first);
    r = num.substr(z.first, N - z.first);
    mp.emplace(GetSum(l, r));
    l = num.substr(0, z.second);
    r = num.substr(z.second, N - z.second);
    mp.emplace(GetSum(l, r));
  }
  return *mp.begin();
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> num;
    cout << Solution() << '\n';
  }
  return 0;
}
