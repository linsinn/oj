#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int a[2], k[2], n;

void Solution() {
  if (k[0] > k[1]) {
    swap(a[0], a[1]);
    swap(k[0], k[1]);
  }
  int m = a[0] * (k[0] - 1) + a[1] * (k[1] - 1);
  int mi = max(0, n - m);
  int ma = min(a[0], n / k[0]);
  ma += max(0, n - ma * k[0]) / k[1];
  cout << mi << ' ' << ma << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> a[0] >> a[1] >> k[0] >> k[1] >> n) {
    Solution();
  }
  return 0;
}
