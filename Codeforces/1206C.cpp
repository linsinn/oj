#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
  if (N % 2 == 0) {
    cout << "NO\n";
    return ;
  }
  Vec ans(N*2);
  fora (i, 0, N) {
    if (i % 2 == 0) {
      ans[i] = i * 2 + 1;
      ans[i+N] = i * 2 + 2;
    } else {
      ans[i] = i * 2 + 2;
      ans[i+N] = i * 2 + 1;
    }
  }
  cout << "YES\n";
  for (int v : ans)
    cout << v << ' ';
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}
