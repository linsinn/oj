//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
pair<double, int> A[MAXN];
int B[MAXN], ans[MAXN];

void Solution() {
  sort(A, A+N);
  ll su = 0;
  int pos = N-1;
  ford (i, N-1, 0) {
    if (A[i].first < 0) {
      B[i] = int(floor(A[i].first));
    } else if (A[i].first > 0) {
      B[i] = int(ceil(A[i].first));
      pos = i;
    } else {
      B[i] = 0;
    }
    su += B[i];
  }
  int i = 0;
  while (su < 0 && i < pos) {
    if (A[i].first != B[i]) {
      B[i] += 1;
      su += 1;
    }
    ++i;
  }
  i = N-1;
  while (su > 0 && i >= pos) {
    if (A[i].first != B[i]) {
      B[i] -= 1;
      su -= 1;
    }
    --i;
  }
  fora (k, 0, N) {
    ans[A[k].second] = B[k];
  }
  fora (k, 0, N) {
    cout << ans[k] << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i].first;
      A[i].second = i;
    }
    Solution();
  }
  return 0;
}
