#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<ll> A;
vector<vector<int>> gph;

int Found(int i, int j) {
  queue<Pii> que;
  vector<bool> seen(N, false);
  seen[i] = true;
  que.emplace(i, 0);
  while (!que.empty()) {
    auto cur = que.front();
    que.pop();
    if (cur.first == j && cur.second != 0) {
      return cur.second + 1;
    }
    for (int v : gph[cur.first]) {
      if ((cur.first == i && v == j) || (cur.first == j && v == i))
        continue;
      if (!seen[v]) {
        que.emplace(v, cur.second + 1);
        seen[v] = true;
      }
    }
  }
  return INF;
}

ll Solution() {
  M = 63;
  vector<vector<int>> bit_cnts(70, vector<int>());
  fora (i, 0, N) {
    fora (j, 0, M) {
      if ((1LL << j) & A[i]) {
        bit_cnts[j].emplace_back(i);
      }
    }
  }
  fora (i, 0, M) 
    if (bit_cnts[i].size() > 2)
      return 3;
  gph.assign(N, vector<int>());
  fora (i, 0, M) {
    if (bit_cnts[i].size() == 2) {
      gph[bit_cnts[i].front()].emplace_back(bit_cnts[i].back());
      gph[bit_cnts[i].back()].emplace_back(bit_cnts[i].front());
    }
  }
  int ans = INF;
  fora (i, 0, N) {
    for (int j : gph[i]) {
      ans = min(ans, Found(i, j));
    }
  }
  return ans == INF ? -1 : ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
