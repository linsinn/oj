#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
string s[2];

void Solution() {
  sort(s, s+2);
  cout << "YES\n";
  if (s[0] == "aa") {
    if (s[1] == "aa") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "ab") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "ac") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "ba") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "bb") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "bc") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "ca") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "cb") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "abc";
    }
  } else if (s[0] == "ab") {
    if (s[1] == "ab") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "ac") {
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "c";
      fora (i, 0, N) cout << "a";
    } else if (s[1] == "ba") {
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "c";
      fora (i, 0, N) cout << "a";
    } else if (s[1] == "bb") {
      fora (i, 0, N) cout << "bc";
      fora (i, 0, N) cout << "a";
    } else if (s[1] == "bc") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "ca") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "cb") {
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "ac";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "cba";
    }
  } else if (s[0] == "ac") {
    if (s[1] == "ac") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "ba") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "bb") {
      fora (i, 0, N) cout << "bca";
    } else if (s[1] == "bc") {
      fora (i, 0, N) cout << "c";
      fora (i, 0, N) cout << "ab";
    } else if (s[1] == "ca") {
      fora (i, 0, N) cout << "c";
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "a";
    } else if (s[1] == "cb") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "abc";
    }
  } else if (s[0] == "ba") {
    if (s[1] == "ba") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "bb") {
      fora (i, 0, N) cout << "bca";
    } else if (s[1] == "bc") {
      fora (i, 0, N) cout << "ac";
      fora (i, 0, N) cout << "b";
    } else if (s[1] == "ca") {
      fora (i, 0, N) cout << "a";
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "c";
    } else if (s[1] == "cb") {
      fora (i, 0, N) cout << "abc";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "abc";
    }
  } else if (s[0] == "bb") {
    if (s[1] == "bb") {
      fora (i, 0, N) cout << "bca";
    } else if (s[1] == "bc") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "ca") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "cb") {
      fora (i, 0, N) cout << "cab";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "abc";
    }
  } else if (s[0] == "bc") {
    if (s[1] == "bc") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "ca") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "cb") {
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "a";
      fora (i, 0, N) cout << "c";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "acb";
    }
  } else if (s[0] == "ca") {
    if (s[1] == "ca") {
      fora (i, 0, N) cout << "acb";
    } else if (s[1] == "cb") {
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "a";
      fora (i, 0, N) cout << "c";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "acb";
    }
  } else if (s[0] == "cb") {
    if (s[1] == "cb") {
      fora (i, 0, N) cout << "b";
      fora (i, 0, N) cout << "a";
      fora (i, 0, N) cout << "c";
    } else if (s[1] == "cc") {
      fora (i, 0, N) cout << "bca";
    }
  } else if (s[0] == "cc") {
    fora (i, 0, N) cout << "abc";
  }
  cout << "\n";
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> s[0] >> s[1]) {
    Solution();
  }
  return 0;
}
