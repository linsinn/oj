//
// Created by sinn on 5/15/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int B[MAXN], G[MAXN];
int R[MAXN];

ll Solution() {
  sort(B, B+N);

  int C = 0;

  ll ans = 0;
  int maxi = B[0];
  for (int i = 0; i < N; i++)
    ans += 1LL * B[i] * M;

  for (int i = 0; i < M; i++) {
    if (G[i] < B[N-1])
      return -1;
    else if (G[i] > B[N-1]) {
      if (C < M - 1)
        ans += G[i] - B[N - 1];
      else {
        if (N < 2)
          return -1;
        else
          ans += G[i] - B[N - 2];
      }
      C++;
    }
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    for (int i = 0; i < N; ++i) {
      cin >> B[i];
    }
    for (int j = 0; j < M; ++j) {
      cin >> G[j];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
