#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int l, r;

ll Solution() {
  fora (i, l, r+1) {
    map<int, int> cnt;
    int k = i;
    while (k != 0) {
      cnt[k % 10]++;
      k /= 10;
    }
    bool f = true;
    for (auto &p : cnt) {
      if (p.second > 1)
        f = false;
    }
    if (f) return i;
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> l >> r) {
    cout << Solution() << '\n';
  }
  return 0;
}
