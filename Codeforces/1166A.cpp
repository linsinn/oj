//
// Created by sinn on 5/22/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string names[MAXN];

int Solution() {
  map<char, int> cl[2];
  for (int g = 0; g < N; g++) {
    char ch = names[g][0];
    auto i = cl[0].find(ch);
    auto j = cl[1].find(ch);
    if (i == cl[0].end()) {
      cl[0][ch] = 1;
    } else {
      if (j == cl[1].end()) {
        cl[1][ch] = 1;
      } else {
        if (i->second < j->second) {
          cl[0][ch]++;
        } else {
          cl[1][ch]++;
        }
      }
    }
  }
  int ans = 0;
  for (int i = 0; i < 2; i++) {
    for (auto p : cl[i]) {
      ans += p.second * (p.second - 1) / 2;
    }
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    for (int i = 0; i < N; ++i) {
      cin >> names[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}