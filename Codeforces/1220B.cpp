#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> A;

void Solution() {
  vector<ll> ans(N, 0);
  ans[0] = 1LL * A[2][0] * A[0][1] / A[2][1];
  ans[0] = sqrt(ans[0]);
  cout << ans[0] << ' ';
  fora (i, 1, N) {
    ans[i] = A[0][i] / ans[0];
    cout << ans[i] << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.assign(N, Vec(N, 0));
    fora (i, 0, N) {
      fora (j, 0, N) {
        cin >> A[i][j];
      }
    }
    Solution();
  }
  return 0;
}
