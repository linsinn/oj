//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
string s;

ll Solution() {
  set<string> vis;
  queue<string> que;
  que.emplace(s);
  vis.emplace(s);
  int k = 0;
  ll ans = 0;
  while (!que.empty()) {
    auto n = que.front();
    que.pop();
    ans += s.size() - n.size();
    k++;
    if (k == K)
      return ans;
    fora (i, 0, n.size()) {
      string t = n.substr(0, i);
      if (i + 1 != n.size())
        t += n.substr(i+1, n.size()-i-1);
      if (vis.count(t) == 0) {
        vis.emplace(t);
        que.emplace(t);
      }
    }
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}
