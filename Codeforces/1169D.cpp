//
// Created by sinn on 5/28/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string bins;

ll Solution() {
  ll ans = 0;
  N = bins.size();
  vector<int> vl(N+1, N);
  for (int i = N-1; i >= 0; --i) {
    vl[i] = vl[i+1];
    for (int k = 1; i + 2 * k < vl[i]; ++k) {
      if (bins[i] == bins[i+k] && bins[i] == bins[i + 2*k]) {
        vl[i] = i + 2 * k;
      }
    }
    ans += N - vl[i];
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> bins) {
    cout << Solution() << '\n';
  }
  return 0;
}
