//
// Created by sinn on 1/4/20.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> A;

ll Solution() {
	vector<Pii> maxi;
	vector<int> is_ascents;
	ll ans = 0;
	fora (i, 0, N) {
		int isAscent = 0;
		int mini = 1e9;
		fora (j, 0, A[i].size()) {
			if (A[i][j] > mini) {
				isAscent = 1;
				break;
			}
			Min(mini, A[i][j]);
		}
		is_ascents.emplace_back(isAscent);
		maxi.emplace_back(*max_element(A[i].begin(), A[i].end()), isAscent);
	}
	sort(maxi.begin(), maxi.end());
	vector<int> aux(maxi.size() + 1, 0);
	fora (i, 0, N) {
		aux[i+1] = aux[i] + maxi[i].second;
	}
	fora (i, 0, N) {
		int a = *min_element(A[i].begin(), A[i].end());
		int p = distance(maxi.begin(), upper_bound(maxi.begin(), maxi.end(), make_pair(a, 1)));
		if (is_ascents[i]) {
			ans += N;
		} else {
			ans += N - p;
			ans += aux[p];
		}
	}
	return ans;
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
  	A.resize(N);
  	int l;
  	fora (i, 0, N) {
  		cin >> l;
  		A[i].resize(l);
  		fora (j, 0, l) {
  			cin >> A[i][j];
  		}
  	}
  	cout << Solution() << '\n';
  }
  return 0;
}