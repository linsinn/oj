//
// Created by sinn on 7/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string code, s[2];
int kmp[2][60], nxt[2][60][30];
int memos[1020][60][60];
constexpr int INF = MOD;

void Init() {
  fora (k, 0, 2) {
    const string &str = s[k];
    int* arr = kmp[k];
    arr[0] = 0;
    fora (i, 1, str.size()) {
      int j = arr[i-1];
      while (j > 0 && str[j] != str[i]) {
        j = arr[j-1];
      }
      arr[i] = j + (str[i] == str[j]);
    }
  }
  fora (k, 0, 2) {
    const string &str = s[k];
    int (*arr)[30] = nxt[k];
    fora (i, 0, str.size()+1) {
      fora (ch, 'a', 'z'+1) {
        int c = ch - 'a';
        int j = i;
        while (j > 0 && (str[j] != ch || j >= str.size())) {
          j = kmp[k][j-1];
        }
        arr[i][c] = j + (str[j] == ch);
      }
    }
  }
}

int dp(int pos, int a, int b) {
  if (pos >= code.size())
    return 0;
  int &x = memos[pos][a][b];
  if (x != -INF)
    return x;
  int lo = 'a', hi = 'z';
  if (code[pos] != '*') lo = hi = code[pos];
  fora (ch, lo, hi+1) {
    int c = ch - 'a';
    int ma = nxt[0][a][c];
    int mb = nxt[1][b][c];
    int g = (ma == s[0].size()) -  (mb == s[1].size()) + dp(pos+1, ma, mb);
    x = max(x, g);
  }
  return x;
}

ll Solution() {
  Init();
  fora (i, 0, code.size() + 1)
    fora (j, 0, s[0].size() + 1)
      fora (k, 0, s[1].size() + 1)
        memos[i][j][k] = -INF;
  return dp(0, 0, 0);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> code >> s[0] >> s[1]) {
    cout << Solution();
  }
  return 0;
}
