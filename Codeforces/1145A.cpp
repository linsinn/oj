//
// Created by sinn on 4/3/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

int IsSorted(int a, int b) {
    for (int i = a+1; i < b; i++) {
        if (A[i] < A[i-1])
            return false;
    }
    return true;
}

int Solution() {
    int len = N;
    while (len != 0) {
        for (int i = 0; i < N; i += len) {
            if (IsSorted(i, i+len)) {
                return len;
            }
        }
        len /= 2;
    }
    return 1;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++)
            cin >> A[i];
        cout << Solution() << '\n';
    }
    return 0;
}