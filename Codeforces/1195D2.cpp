//
// Created by sinn on 7/17/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int MAXN = 1e5 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];
ll memos[2][MAXN][15];

inline ll Add(ll a, ll b) {
  return (a + b) % MOD;
}

void Helper(ll base, ll i, ll j) {
  int g = base == 1 ? 0 : 1;
  ll x = 0;
  ll num = A[i];
  ll k = g;
  while (num != 0) {
    ll dig = num % 10;
    x = Add(x, dig * base % MOD);
    base *= (k < j ? 100 : 10);
    base %= MOD;
    num /= 10;
    ++k;
  }
  memos[g][i][j] = x;
}

ll Solution() {
  fora (i, 0, N) {
    fora (j, 1, 11) {
      Helper(1, i, j);
      Helper(10, i, j);
    }
  }
  map<int, ll> cnts;
  fora (i, 0, N) {
    int k = 0;
    ll num = A[i];
    while (num != 0) {
      ++k;
      num /= 10;
    }
    cnts[k]++;
  }
  ll ans = 0;
  fora (i, 0, N) {
    for (auto &p : cnts) {
      ans = Add(ans, memos[0][i][p.first] * p.second % MOD);
      ans = Add(ans, memos[1][i][p.first] * p.second % MOD);
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
