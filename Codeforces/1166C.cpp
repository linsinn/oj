//
// Created by sinn on 5/22/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

ll Solution() {
  ll ans = 0;
  sort(A, A+N);
  for (int i = 0; i < N; i++) {
    if (A[i] >= 0) {
      int *p = upper_bound(A, A+N, 2 * A[i]);
      ans += p - A - i - 1;
    } else {
      int *zero_pos = lower_bound(A, A+N, 0);
      int *p;
      if (A[i] % 2 == 0) { // y < 0;
        p = upper_bound(A, zero_pos, A[i] / 2);
      } else {
        p = lower_bound(A, zero_pos, A[i] / 2);
      }
      ans += p - A - i - 1;
      int *mx = upper_bound(A, A+N, -A[i]);
      if (A[i] % 2 == 0) { // y >= 0, abs(x) > abs(y);
        p = lower_bound(A, A+N, -A[i] / 2);
      } else {
        p = upper_bound(A, A+N, -A[i] / 2);
      }
      ans += mx - p;
      p = upper_bound(A, A+N, -2 * A[i]);
      ans += p - mx;
    }
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    for (int i = 0; i < N; ++i)
      cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}