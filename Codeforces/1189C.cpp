//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 10;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];
ll lb, rb;
pair<ll, ll> memos[20][MAXN];

void Init() {
  fora (j, 0, N) {
    memos[0][j].first = A[j];
    memos[0][j].second = 0;
  }
  fora (i, 1, 20) {
    fora (j, 0, N) {
      if (j + (1 << i) - 1 >= N) break;
      int nxt = j + (1 << (i - 1));
      auto p = memos[i-1][j], q = memos[i-1][nxt];
      memos[i][j].first = (p.first + q.first) % 10;
      memos[i][j].second = (p.first + q.first >= 10) + p.second + q.second;
    }
  }
}

ll Solution() {
  int k = 0;
  while (lb + (1 << k)  - 1 < rb) {
    ++k;
  }
  return memos[k][lb].second;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    Init();
    cin >> T;
    while (T--) {
      cin >> lb >> rb;
      --lb, --rb;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
