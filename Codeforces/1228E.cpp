#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec fac, rfac;

int BinPow(ll base, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1) 
      ret = ret * base % MOD;
    base = (ll)base * base % MOD;
    exp >>= 1;
  }
  return ret;
}

void Init() {
  int n = N+1;
  fac.resize(n);
  rfac.resize(n);
  fac[0] = fac[1] = 1;
  fora (i, 2, n) 
    fac[i] = (ll)fac[i-1] * i % MOD;
  rfac[n-1] = BinPow(fac[n-1], MOD-2);
  ford (i, n-2, 0) {
    rfac[i] = (ll)rfac[i+1] * (i + 1) % MOD;
  }
}

int C(int n, int k) {
  return 1LL * fac[n] * rfac[k] % MOD * rfac[n-k] % MOD;
}

ll Solution() {
  Init();
  vector<vector<ll>> dp(N+1, vector<ll>(N+1, 0));
  fora (i, 1, N+1) {
    dp[1][i] = 1LL * C(N, i) * BinPow(K-1, N-i) % MOD;
  }
  vector<vector<ll>> pows(2, vector<ll>(N+1, 0));
  pows[0][0] = pows[1][0] = 1;
  fora (i, 1, N+1) {
    pows[0][i] = pows[0][i-1] * K % MOD;
    pows[1][i] = pows[1][i-1] * (K - 1) % MOD;
  }
  fora (i, 2, N+1) {
    fora (j, 1, N+1) {
      fora (k, 1, j+1) {
        ll t = 0;
        if (j - k == 0) {
          ll g = (1LL * pows[0][j] - pows[1][j]) % MOD;
          g += MOD;
          g %= MOD;
          t = 1LL * pows[1][N-j] * g % MOD;
        } else {
          t = 1LL * C(N-k, j-k) * pows[1][N-j] % MOD * pows[0][k] % MOD;
        }
        dp[i][j] += dp[i-1][k] * t % MOD;
        dp[i][j] %= MOD;
      }
    }
  }
  return dp[N][N];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cout << Solution() << '\n';
  }
  return 0;
}
