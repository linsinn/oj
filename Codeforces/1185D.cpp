//
// Created by sinn on 6/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
pair<int, int> A[MAXN];

bool Check(int ign) {
  if (N <= 3) return true;
  if (ign == 0) {
    fora (i, 3, N) {
      if (A[i].first - A[i-1].first != A[i-1].first - A[i-2].first)
        return false;
    }
    return true;
  } else if (ign == N-1) {
    fora (i, 2, N-1) {
      if (A[i].first - A[i-1].first != A[i-1].first - A[i-2].first)
        return false;
    }
    return true;
  } else {
    int diff = A[ign+1].first - A[ign-1].first;
    fora (i, 1, ign) {
      if (A[i].first - A[i-1].first != diff)
        return false;
    }
    fora (i, ign+2, N) {
      if (A[i].first - A[i-1].first != diff)
        return false;
    }
    return true;
  }
}

int Solution() {
  sort(A, A+N);
  map<int, set<int>> mp;
  fora (i, 1, N) {
    mp[A[i].first - A[i-1].first].emplace(i);
    mp[A[i].first - A[i-1].first].emplace(i-1);
  }
  if (mp.size() > 3) return -1;
  if (mp.size() == 1) return A[0].second + 1;
  for (auto& p : mp) {
    if (p.second.size() <= 3) {
      for (int ign : p.second) {
        if (Check(ign)) {
          return A[ign].second + 1;
        }
      }
    }
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i].first;
      A[i].second = i;
    }
    cout << Solution() << '\n';
  }
  return 0;
}