//
// Created by sinn on 6/15/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e6 + 10;

using ll = long long;

int N, M, K;
int A[MAXN], B[MAXN];
bool vis[MAXN];
bool primes[MAXN];
int P[MAXN];

void Init() {
  memset(primes, 1, sizeof(primes));
  fora (i, 2, MAXN) {
    if (primes[i]) {
      int k = 2;
      while (i * k < MAXN) {
        primes[i * k] = false;
        ++k;
      }
    }
  }
  int k = 0;
  fora (i, 2, MAXN) {
    if (primes[i]) {
      P[k++] = i;
    }
  }
}

int GreatestPrime(int x) {
  if (x == 2) return 0;
  if (x % 2 == 0) {
    return x / 2;
  }
  int div = 3;
  while (div <= sqrt(x)) {
    if (x % div == 0) return x / div;
    div += 2;
  }
  return 0;
}

void Solution() {
  Init();
  memset(vis, 0, sizeof(vis));
  map<int, vector<int>> idx;
  sort(B, B+2*N);
  fora (i, 0, 2*N) {
    idx[B[i]].emplace_back(i);
  }
  int k = 0;
  ford (i, 2*N-1, 0) {
    if (vis[i]) continue;
    int g = GreatestPrime(B[i]);
    if (g != 0 && !idx[g].empty()) {
      int p = idx[g].back();
      idx[g].pop_back();
      A[k++] = B[i];
      vis[i] = true;
      vis[p] = true;
    }
  }
  multiset<int> remain;
  for (auto& p : idx) {
    for (int i : p.second) {
      if (!vis[i]) {
        remain.emplace(B[i]);
      }
    }
  }

  while (!remain.empty()) {
    int x = *remain.begin();
    int y = P[x-1];
    A[k++] = x;
    remain.erase(remain.find(y));
    remain.erase(remain.begin());
  }

  fora (i, 0, N) {
    cout << A[i] << " \n"[i == N-1];
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, 2*N) {
      cin >> B[i];
    }
    Solution();
  }
  return 0;
}
