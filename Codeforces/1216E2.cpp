#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

inline bool Cmp(int leading, ll len, int bits, ll k) {
  if (bits >= 9) return true;
  if (leading * len > k) return true;
  return (len + 1) * len * bits > (k - len * leading) * 2;
}

int calc(ll k) {
  fora (i, 1, 10) {
    ll len = 1;
    fora (j, 0, i-1)
      len = len * 10;
    if (k < len * 9 * i) {
      ll t = k / i;
      k -= t * i;
      string g = to_string(len + t);
      return g[k] - '0';
    } else {
      k -= len * 9 * i;
    }
  }
  return '0';
}

int dfs(ll leading, int bits, ll k) {
  assert(k >= 0);
  ll len = 1;
  fora (i, 0, bits-1)
    len = len * 10;
  len *= 9;
  if (Cmp(leading, len, bits, k)) {
    ll lb = 1, rb = len;
    ll t = 0, tot;
    while (lb <= rb) {
      ll mid = lb + (rb - lb) / 2;
      tot = leading * mid + (1 + mid) * mid * bits / 2;
      if (tot <= k) 
        lb = mid + 1;
      else {
        t = mid;
        rb = mid - 1;
      }
    }
    t -= 1;
    tot = leading * t + (t + 1) * t * bits / 2;
    k -= tot;
    return calc(k);
  } else {
    ll tot = leading * len + (len + 1) * len * bits / 2;
    leading += len * bits;
    return dfs(leading, bits + 1, k - tot);
  }
}

ll Solution() {
  return dfs(0, 1, K-1);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> K;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
