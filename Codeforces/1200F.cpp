#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int SZ = 2520;
Vec ks;
vector<Vec> gph;
int dp[1050][2600];
int seen[1050][2600];
int x, y;

pair<int, int> dfs(int u, int i, int t, vector<int>& v) {
  if (dp[u][i] != -1) {
    return {INF, dp[u][i]};
  }
  if (seen[u][i] != -1) {
    set<int> s;
    fora (j, seen[u][i], v.size()) {
      s.emplace(v[j]);
    }
    dp[u][i] = s.size();
    return {seen[u][i], s.size()};
  } else {
    int e = (i + ks[u]) % SZ;
    e = (e + SZ) % SZ;
    int to = gph[u][e % gph[u].size()];
    v.emplace_back(u);
    seen[u][i] = t;
    auto res = dfs(to, e, t+1, v);
    dp[u][i] = res.second;
    return res;
  }
}

ll Solution() {
  vector<int> v;
  y = y % SZ;
  y = (y + SZ) % SZ;
  auto ans = dfs(x, y, 0, v);
  return ans.second;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    memset(dp, -1, sizeof(dp));
    memset(seen, -1, sizeof(seen));
    ks.resize(N+1);
    fora (i, 0, N) cin >> ks[i+1];
    gph.resize(N+1);
    int m;
    SZ = 1;
    fora (i, 1, N+1) {
      cin >> m;
      int g = __gcd(SZ, m);
      SZ = SZ * m / g;
      gph[i].resize(m);
      fora (j, 0, m) {
        cin >> gph[i][j];
      }
    }
    cin >> T;
    while (T--) {
      cin >> x >> y;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
