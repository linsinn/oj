#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

ll Solution() {
  vector<int> pre_router(N+1, -1);
  vector<ll> dp(N+1, 1LL << 60);
  int rt = 0;
  fora (i, 0, N+1) {
    while (rt < N) {
      if (s[rt] == '0') ++rt;
      else if (rt < i) {
        if (rt + K < i - 1) 
          ++rt;
        else break;
      } else {
        break;
      }
    }
    if (rt < i)
      pre_router[i] = rt;
    if (rt < N && rt - K <= i - 1)
      pre_router[i] = rt;
  }
  dp[N] = 0;
  ford (i, N, 1) {
    Min(dp[i-1], dp[i] + i);
    if (pre_router[i] != -1) {
      int k = max(0LL, pre_router[i] - K);
      Min(dp[k], dp[i] + pre_router[i] + 1);
    }
  }
  return dp[0];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}
