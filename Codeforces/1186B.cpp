//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;

ll Helper(ll n, ll m) {
  ll col = (m + 1) / 2;
  ll row = n / 3;
  ll ret = row * col;
  if (n % 3 == 1) {
    ret += m / 3 + (m % 3 >= 2);
  } else if (n % 3 == 2) {
    ret += col;
  }
  return ret;
}

ll Solution() {
  return max(Helper(N, M), Helper(M, N));
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    cout << Solution() << '\n';
  }
  return 0;
}
