//
// Created by sinn on 7/13/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll p[MAXN];

int Rnd(int &idx, ll x) {
  int es = 0;
  while (idx < M && p[idx] <= x) {
    ++idx;
    ++es;
  }
  return es;
}

ll Solution() {
  ll pg = 1;
  int idx = 0;
  ll es = 0;
  int ans = 0;
  while (idx < M) {
    ll t = (p[idx] - es - 1) / K;
    es += Rnd(idx, (t + 1 ) * K + es);
    ++ans;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) {
    fora (i, 0, M)  {
      cin >> p[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
