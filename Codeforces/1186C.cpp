//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
string a, b;

ll Solution() {
  int x[2] = {0, 0};
  fora (i, 0, b.size()) {
    x[1] ^= b[i] - '0';
  }
  ll ans = 0;
  int i = 0, j = 0;
  while (j < a.size()) {
    x[0] ^= a[j++] - '0';
    if (j - i > b.size()) {
      x[0] ^= a[i++] - '0';
    }
    if (j - i == b.size()) {
      if ((x[0] ^ x[1]) == 0)
        ++ans;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> a >> b) {
    cout << Solution() << '\n';
  }
  return 0;
}
