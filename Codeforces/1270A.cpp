//
// Created by sinn on 12/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int a, b;
Vec A, B;

bool Solution() {
	int ma = *max_element(A.begin(), A.end());
	int mb = *max_element(B.begin(), B.end());
	return ma > mb;
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
  	while (T--) {
			cin >> N >> a >> b;
			A.resize(a);
			B.resize(b);
			fora (i, 0, a) cin >> A[i];
			fora (i, 0, b) cin >> B[i];
			if (Solution()) cout << "YES\n";
			else cout << "NO\n";
		}
  }
  return 0;
}
