//
// Created by sinn on 5/21/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M;
int K[MAXN], G[MAXN];
int D[MAXN], T[MAXN], lst[MAXN];
map<int, vector<int>> sp;

bool Check(int day) {
  memcpy(G, K, sizeof(G));
  memset(lst, -1, sizeof(lst));
  for (int i = 0; i < M; i++) {
    if (D[i] <= day)
      lst[T[i]] = max(lst[T[i]], D[i]);
  }
  sp.clear();
  for (int i = 0; i < N; i++) {
    if (lst[i] != -1)
      sp[lst[i]].emplace_back(i);
  }
  int burble = 0;
  for (int d = 0; d <= day; ++d) {
    ++burble;
    for (int t : sp[d]) {
      if (burble >= G[t]) {
        burble -= G[t];
        G[t] = 0;
      } else {
        G[t] -= burble;
        burble = 0;
        break ;
      }
    }
  }
  return accumulate(G, G+N, 0) * 2 <= burble;
}

int Solution() {
  int l = 0, r = MAXN * 2;
  while (l < r) {
    int m = l + (r - l) / 2;
    if (Check(m)) {
      r = m;
    } else {
      l = m + 1;
    }
  }
  return r + 1;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    for (int i = 0; i < N; i++) {
      cin >> K[i];
    }
    for (int i = 0; i < M; i++) {
      cin >> D[i] >> T[i];
      --D[i], --T[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}