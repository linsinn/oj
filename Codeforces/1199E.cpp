#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 3e5 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

using ll = long long;

ll N, M, K, T;
Vec A;
vector<Pii> gph[MAXN];
Vec vis;

void Solution() {
  Vec edges;
  vis.assign(3*N, 0);
  fora (i, 0, 3*N) {
    if (!vis[i]) {
      for (auto &p : gph[i]) {
        if (!vis[p.first]) {
          vis[i] = true;
          vis[p.first] = true;
          edges.emplace_back(p.second);
          break;
        }
      }
    }
  }
  if (edges.size() >= N) {
    cout << "Matching\n";
    fora (i, 0, N) {
      cout << edges[i] << ' ';
    }
    cout << '\n';
  } else {
    Vec verts;
    fora (i, 0, 3*N) {
      if (!vis[i]) {
        verts.emplace_back(i+1);
      }
    }
    if (verts.size() >= N) {
      cout << "IndSet\n";
      fora (i, 0, N) {
        cout << verts[i] << ' ';
      }
      cout << '\n';
    } else {
      cout << "Impossible\n";
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      fora (i, 0, 3*N+1) gph[i].clear();
      int u, v;
      fora (i, 0, M) {
        cin >> u >> v;
        --u, --v;
        gph[u].emplace_back(v, i+1);
        gph[v].emplace_back(u, i+1);
      }
      Solution();
    }
  }
  return 0;
}
