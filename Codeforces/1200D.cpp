#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<string> B;

ll Solution() {
  vector<Pii> br(N, make_pair(INF, -1)), bc(N, make_pair(INF, -1));
  fora (i, 0, N) {
    fora (j, 0, N) {
      if (B[i][j] == 'B') {
        Min(br[i].first, j);
        Max(br[i].second, j);
        Min(bc[j].first, i);
        Max(bc[j].second, i);
      }
    }
  }
  vector<Vec> ar(N, Vec(N-K+1, 0)), ac(N, Vec(N-K+1, 0));
  vector<Vec> sar(N+1, Vec(N-K+1, 0)), sac(N+1, Vec(N-K+1, 0));
  fora (i, 0, N) {
    fora (j, 0, N-K+1) {
      int k = j + K - 1;
      if (j <= br[i].first && br[i].second <= k)
        ar[i][j] = 1;
      if (j <= bc[i].first && bc[i].second <= k)
        ac[i][j] = 1;
    }
  }
  fora (i, 0, N) {
    fora (j, 0, N-K+1) {
      sar[i+1][j] = sar[i][j] + ar[i][j];
      sac[i+1][j] = sac[i][j] + ac[i][j];
    }
  }
  int g = sar[K][0] - sar[0][0] + sac[K][0] - sac[0][0];
  fora (i, K, N) {
    if (br[i].first > N) ++g;
    if (bc[i].first > N) ++g;
  }
  int ans = g;
  fora (i, 0, N-K+1) {
    int k = g;
    ans = max(ans, k);
    fora (j, 1, N-K+1) {
      k -= sar[i+K][j-1] - sar[i][j-1];
      k += sar[i+K][j] - sar[i][j];
      if (bc[j-1].first < N && i <= bc[j-1].first && bc[j-1].second <= i + K - 1) 
        --k;
      if (bc[j-1+K].first < N && i <= bc[j-1+K].first && bc[j-1+K].second <= i + K -1)
        ++k;
      ans = max(ans, k);
    }
    if (i < N - K) {
      g -= sac[K][i] - sac[0][i];
      g += sac[K][i+1] - sac[0][i+1];
      if (br[i].first < N && 0 <= br[i].first && br[i].second <= K-1)
        --g;
      if (br[i+K].first < N && 0 <= br[i+K].first && br[i+K].second <= K-1)
        ++g;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    B.resize(N);
    fora (i, 0, N) cin >> B[i];
    cout << Solution() << '\n';
  }
  return 0;
}
