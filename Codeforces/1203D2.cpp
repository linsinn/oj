#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s, t;
int nxt[MAXN][30], pre[MAXN][30];

ll Solution() {
  int n = s.size();
  int m = t.size();
  fora (j, 0, 26) 
    nxt[s.size()-1][j] = n;
  ford (i, n-2, 0) {
    fora (j, 0, 26) {
      nxt[i][j] = nxt[i+1][j];
    }
    int c = s[i+1] - 'a';
    nxt[i][c] = i + 1;
  }
  Vec idx(m + 2);
  int i = 0, j = 0;
  while (j < t.size()) {
    if (s[i] == t[j]) {
      idx[j+1] = i;
      ++j;
    }
    ++i;
  }
  idx[0] = -1;
  idx.back() = n;
  int ans = 0;
  for (i = 1; i <= m; ++i) {
    ans = max(ans, idx[i] - idx[i-1] - 1);
    ans = max(ans, idx[i+1] - idx[i] - 1);
  }
  for (i = m; i >= 1; --i) {
    int c = t[i-1] - 'a';
    int k = idx[i];
    while (nxt[k][c] < idx[i+1]) {
      k = nxt[k][c];
    }
    idx[i] = k;
    ans = max(ans, idx[i] - idx[i-1] - 1);
    ans = max(ans, idx[i+1] - idx[i] - 1);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s >> t) {
    cout << Solution() << '\n';
  }
  return 0;
}
