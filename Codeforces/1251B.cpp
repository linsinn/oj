//
// Created by sinn on 10/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<string> vs;

int Solution() {
  Vec aux(2, 0);
  for (string &s : vs) {
    for (char ch : s) {
      aux[ch - '0']++;
    }
  }
  int ans = 0;
  for (string &s : vs) {
    int g = aux[0] / 2 + aux[1] / 2;
    int a = s.size() / 2;
    int n = aux[0] % 2 + aux[1] % 2;
    int m = s.size() % 2;
    if (g > a || (g == a && n >= m)) {
      ++ans;
      int b = aux[0] / 2;
      int c = aux[1] / 2;
      aux[0] -= min(a, b) * 2;
      a = max(a - b, 0);
      aux[1] -= min(a, c) * 2;
      if (s.size() % 2 == 1) {
        if (aux[0] % 2 == 1)
          --aux[0];
        else
          --aux[1];
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      vs.resize(N);
      fora (i, 0, N) cin >> vs[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}