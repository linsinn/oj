#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string x, y;

ll Solution() {
  reverse(x.begin(), x.end());
  reverse(y.begin(), y.end());
  int a = 0, b = 0;
  while (a < x.size() && x[a] != '1')
    ++a;
  while (b < y.size() && y[b] != '1')
    ++b;
  if (b <= a)
    return a - b;
  a = b;
  while (a < x.size() && x[a] != '1')
    ++a;
  return a - b;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> x >> y;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
