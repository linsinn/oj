//
// Created by sinn on 10/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<string> vs;

void Solution() {
  for (string &s : vs) {
    set<char> ans;
    for (int i = 0; i < s.size(); ) {
      int j = i + 1;
      while (j < s.size() && s[j] == s[i])
        ++j;
      if ((j - i) % 2 == 1)
        ans.emplace(s[i]);
      i = j;
    }
    for (char ch : ans)
      cout << ch;
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    vs.resize(N);
    fora (i, 0, N) cin >> vs[i];
    Solution();
  }
  return 0;
}
