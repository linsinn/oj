#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> A;
ll B1[MAXN], B2[MAXN];

void Add(ll arr[], int idx, ll val) {
  while (idx <= M) {
    arr[idx] += val;
    idx += idx & -idx;
  }
}

void RangeAdd(int l, int r, ll val) {
  Add(B1, l, val);
  Add(B1, r+1, -val);
  Add(B2, l, val * (l - 1));
  Add(B2, r+1, -val * r);
}

ll Sum(ll arr[], int idx) {
  ll ret = 0;
  while (idx > 0) {
    ret += arr[idx];
    idx -= idx & -idx;
  }
  return ret;
}

ll PreSum(int idx) {
  return Sum(B1, idx) * idx - Sum(B2, idx);
}

ll RangeSum(int l, int r) {
  return PreSum(r) - PreSum(l - 1); 
}

void Solution() {
  memset(B1, 0, sizeof(B1));
  memset(B2, 0, sizeof(B2));
  fora (i, 0, N) {
    priority_queue<Pii> pq;
    K = A[i].size();
    fora (j, 0, K) {
      pq.emplace(A[i][j], j);
      while (!pq.empty()) {
        auto p = pq.top();
        if (p.first < 0 && M - K > j) {
          break;
        }
        if (j - p.second > M - K) {
          pq.pop();
        } else {
          RangeAdd(j+1, j+1, p.first);
          break;
        }
      }
    }
    int r = K;
    while (!pq.empty()) {
      auto p = pq.top();
      if (p.first < 0) 
        break;
      if (r - p.second > M - K) {
        pq.pop();
        continue;
      } else {
        int t = min(M-1, p.second + M - K);
        RangeAdd(r+1, t+1, p.first);
        r = t + 1;
      }
    }
  }
  fora (i, 0, M) {
    cout << RangeSum(i+1, i+1) << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    A.resize(N);
    fora (i, 0, N) {
      cin >> K;
      A[i].resize(K);
      fora (j, 0, K) {
        cin >> A[i][j];
      }
    }
    Solution();
  }
  return 0;
}
