//
// Created by sinn on 7/14/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
Pll points[MAXN];
ll BIT[MAXN];

void Upd (ll x) {
  while (x <= M) {
    BIT[x] += 1;
    x += x & -x;
  }
}

ll Qry(ll x) {
  ll ret = 0;
  while (x > 0) {
    ret += BIT[x];
    x -= x & -x;
  }
  return ret;
}

void Init() {
  set<ll> ss;
  fora (i, 0, N) {
    ss.emplace(points[i].first);
  }
  map<ll, ll> comp;
  int k = 1;
  for (int s : ss) {
    comp[s] = k++;
  }
  fora (i, 0, N) {
    points[i].first = comp[points[i].first];
  }
  M = k - 1;
}

ll Solution() {
  Init();
  map<ll, vector<ll>> mp;
  fora (i, 0, N) {
    mp[points[i].second].emplace_back(points[i].first);
  }
  set<ll> seen;
  ll ans = 0;
  for (auto it = mp.rbegin(); it != mp.rend(); ++it) {
    auto &vec = it->second;
    sort(vec.begin(), vec.end());
    for (ll x : vec) {
      if (seen.find(x) == seen.end()) {
        seen.emplace(x);
        Upd(x);
      }
    }
    ll pre = 0;
    for (ll x : vec) {
      ans += (Qry(x) - Qry(pre)) * (Qry(M) - Qry(x - 1));
      pre = x;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> points[i].first >> points[i].second;
    }
    cout << Solution() << '\n';
  }
  return 0;
}

