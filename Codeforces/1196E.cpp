#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
ll B, W;

void Solution() {
  ll base = B < W ? 3 : 2;
  ll y = 2;
  ll ma = max(B, W), mi = min(B, W);
  if (ma - 1 > mi * 3) {
    cout << "NO\n";
    return ;
  }
  cout << "YES\n";
  cout << y << ' ' << base - 1<< '\n';
  --ma;
  while (ma > 0 || mi > 0) {
    cout << y << ' ' << base << '\n';
    mi -= 1;
    if (ma > 0) {
      cout << y << ' ' << base+1 << '\n';
      --ma;
    }
    if (ma > mi) {
      cout << y-1 << ' ' << base << '\n';
      --ma;
    }
    if (ma > mi * 3) {
      cout << y+1 << ' ' << base << '\n';
      --ma;
    }
    base += 2;
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> B >> W;
      Solution();
    }
  }
  return 0;
}
