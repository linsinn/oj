//
// Created by sinn on 1/4/20.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> A, B;

struct Node {
	int val = 0;
	int lazy = 0;
};

struct SegTree {
	vector<Node> trees;
	SegTree() = default;
	SegTree(int N) {
		trees.resize(N * 4 + 10);
	}
	void clear() {
		fill(trees.begin(), trees.end(), Node());
	}
	void Push(int rt, int ll, int rr) {
		if (ll == rr) return ;
		int lc = rt << 1, rc = rt << 1 | 1;
		if (trees[rt].lazy != 0) {
			int tmp = trees[rt].lazy;
			trees[rt].lazy = 0;
			trees[lc].val += tmp;
			trees[lc].lazy += tmp;
			trees[rc].val += tmp;
			trees[rc].lazy += tmp;
		}
	}
	void Upd(int rt, int ll, int rr, int l, int r, int val) {
		if (rr < l || ll > r)
			return ;
		if (l <= ll && rr <= r) {
			trees[rt].val += val;
			trees[rt].lazy += val;
			return ;
		}
		Push(rt, ll, rr);
		int m = ll + ((rr - ll) >> 1);
		int lc = rt << 1, rc = rt << 1 | 1;
		Upd(lc, ll, m, l, r, val);
		Upd(rc, m+1, rr, l, r, val);
		trees[rt].val = max(trees[lc].val, trees[rc].val);
	}
	int Query(int rt, int ll, int rr, int l, int r) {
		if (rr < l || ll > r)
			return 0;
		if (l <= ll && rr <= r) {
			return trees[rt].val;
		}
		Push(rt, ll, rr);
		int m = ll + ((rr - ll) >> 1);
		int lc = rt << 1, rc = rt << 1 | 1;
		int a = Query(lc, ll, m, l, r);
		int b = Query(rc, m+1, rr, l, r);
		return max(a, b);
	}
};

bool Solution() {
	map<int, int> ss;
	fora (i, 0, N) {
		ss[A[i].first] = 0;
		ss[A[i].second] = 0;
		ss[B[i].first] = 0;
		ss[B[i].second] = 0;
	}
	int i = 1;
	for (auto & s : ss) {
		s.second = i++;
	}
	fora (k, 0, N) {
		A[k].first = ss[A[k].first];
		A[k].second = ss[A[k].second];
		B[k].first = ss[B[k].first];
		B[k].second = ss[B[k].second];
	}
	auto seed = std::chrono::system_clock::now().time_since_epoch().count();
	vector<int> idx(N);
	std::iota(idx.begin(), idx.end(), 0);

	M = ss.size();

	SegTree a(ss.size()), b(ss.size());
	fora (t, 0, 5) {
		shuffle(idx.begin(), idx.end(), std::default_random_engine(seed));
		a.clear();
		b.clear();
		fora (k, 0, N) {
			int p = a.Query(1, 1, M, A[idx[k]].first, A[idx[k]].second);
			int q = b.Query(1, 1, M, B[idx[k]].first, B[idx[k]].second);
			if (p != q)
				return false;
			a.Upd(1, 1, M, A[idx[k]].first, A[idx[k]].second, 1);
			b.Upd(1, 1, M, B[idx[k]].first, B[idx[k]].second, 1);
		}
	}
	return true;
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
  	A.resize(N);
		B.resize(N);
  	fora (i, 0, N) {
  		cin >> A[i].first >> A[i].second;
			cin >> B[i].first >> B[i].second;
  	}
  	if (Solution()) cout << "YES\n";
  	else cout << "NO\n";
  }
  return 0;
}