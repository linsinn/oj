//
// Created by sinn on 7/21/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

ll FastPow(ll x, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1) {
      ret = ret * x % MOD;
    }
    x = x * x % MOD;
    exp >>= 1;
  }
  return ret;
}

ll Solution() {
  return FastPow(2, N+M);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    cout << Solution() << '\n';
  }
  return 0;
}
