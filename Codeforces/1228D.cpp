#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<set<int>> gph;

void Solution() {
  vector<int> col(N, 0);
  set<int> ss[4];
  fora (i, 0, N) {
    if (!gph[0].count(i)) {
      ss[1].emplace(i);
      col[i] = 1;
    }
  }
  int t = *gph[0].begin();
  fora (i, 0, N) {
    if (!gph[t].count(i)) {
      ss[2].emplace(i);
      col[i] = 2;
    }
  }
  fora (i, 0, N) {
    if (!ss[1].count(i) && !ss[2].count(i)) {
      ss[3].emplace(i);
      col[i] = 3;
    }
  }
  if (ss[3].empty() || ss[1].size() + ss[2].size() + ss[3].size() != N) {
    cout << "-1\n";
    return ;
  }
  bool f = true;
  fora (i, 0, N) {
    int c[4] = {0, 0, 0};
    for (int v : gph[i]) {
      if (col[i] == col[v]) {
        f = false;
        break;
      }
      c[col[v]]++;
    }
    fora (k, 1, 4) {
      if (k != col[i] && c[k] != ss[k].size())
        f = false;
    }
    if (!f)
      break;
  }
  if (f) {
    fora (i, 0, N) {
      cout << col[i] << ' ';
    }
    cout << '\n';
  } else {
    cout << "-1\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    gph.assign(N, set<int>());
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      --u, --v;
      gph[u].emplace(v);
      gph[v].emplace(u);
    }
    Solution();
  }
  return 0;
}
