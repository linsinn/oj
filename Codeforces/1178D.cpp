//
// Created by sinn on 7/21/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

vector<ll> primes;
bool is_prime[MAXN];

void SievePrimes() {
  memset(is_prime, 1, sizeof(is_prime));
  is_prime[0] = is_prime[1] = false;
  fora (i, 2, sqrt(MAXN) + 1) {
    if (is_prime[i]) {
      int k = 0;
      while (i * i + i * k < MAXN) {
        is_prime[i*i + i*k] = false;
        ++k;
      }
    }
  }
  fora (i, 2, MAXN) {
    if (is_prime[i]) {
      primes.emplace_back(i);
    }
  }
}


void Solution() {
  SievePrimes();
  ll p = *lower_bound(primes.begin(), primes.end(), N);
  ll g = p * 2;
  ll b = *prev(upper_bound(primes.begin(), primes.end(), N));
  if (g > b * N) {
    cout << "-1\n";
    return ;
  }
  g -= N * 2;
  vector<Pii> degs(N);
  fora (i, 0, N) {
    degs[i].first = 2;
    degs[i].second = i;
  }
  for (int i = 0; i < N && g > 0; ++i) {
    ll t = *prev(upper_bound(primes.begin(), primes.end(), min(b, 2 + g)));
    degs[i].first = t;
    g -= t - 2;
  }
  vector<Pii> ans;
  fora (i, 0, N) {
    sort(degs.rbegin(), degs.rend());
    int j = 1;
    while (degs[0].first > 0) {
      ans.emplace_back(degs[0].second+1, degs[j].second+1);
      degs[0].first--;
      degs[j].first--;
      ++j;
    }
  }
  cout << ans.size() << '\n';
  for (auto& v : ans) {
    cout << v.first << ' ' << v.second << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}
