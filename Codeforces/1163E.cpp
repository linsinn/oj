//
// Created by sinn on 7/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int A[MAXN];
vector<int> basis, vec;

void Add(int u) {
  int tmp = u;
  for (int v : basis)
    u = min(u, u ^ v);
  if (u > 0) {
    basis.emplace_back(u);
    vec.emplace_back(tmp);
    for (int i = basis.size() - 1; i > 0; --i) {
      if (basis[i] > basis[i-1])
        swap(basis[i], basis[i-1]);
      else
        break;
    }
  }
}

void GrayCode(int sz) {
  vector<int> ans = {0};
  fora (i, 0, sz) {
    for (int j = (1 << i) - 1; j >= 0; --j)
      ans.emplace_back(ans[j] ^ vec[i]);
  }
  for (int v : ans)
    cout << v << ' ';
}

void Solution() {
  sort(A, A+N);
  int pt = 0, x = 0;
  fora (i, 1, 20) {
    while (pt < N && A[pt] < (1 << i)) {
      Add(A[pt]);
      ++pt;
    }
    if (basis.size() == i)
      x = i;
  }
  basis.clear();
  vec.clear();
  for (int i = 0; i < N && A[i] < (1 << x); ++i) {
    Add(A[i]);
  }
  cout << x << '\n';
  GrayCode(x);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
