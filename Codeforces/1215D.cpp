#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

ll Solution() {
  int su[2] = {0, 0};
  int msk[2] = {0, 0};
  fora (i, 0, N / 2) {
    if (s[i] == '?') msk[0]++;
    else su[0] += s[i] - '0';
  }
  fora (i, N/2, N) {
    if (s[i] == '?') msk[1]++;
    else su[1] += s[i] - '0';
  }
  if (su[0] > su[1]) {
    swap(su[0], su[1]);
    swap(msk[0], msk[1]);
  }
  if (su[0] == su[1]) {
    return msk[0] == msk[1];
  }
  if (msk[0] <= msk[1]) return false;
  if ((msk[0] - msk[1]) / 2 * 9 == su[1] - su[0])
    return true;
  return false;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    if (Solution()) cout << "Bicarp\n";
    else cout << "Monocarp\n";
  }
  return 0;
}
