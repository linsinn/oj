#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
constexpr int LOGN = 9;

struct Node {
  int bst;
  int mi[LOGN];
  Node() {
    bst = INF;
    fora (i, 0, LOGN) mi[i] = INF;
  }
  int &operator [] (int idx) {
    return mi[idx];
  }
} t[4 * MAXN];

void Upd(Node &t, int val) {
  int x = val;
  fora (i, 0, LOGN) {
    if (x % 10 != 0) 
      t[i] = min(t[i], val);
    x /= 10;
  }
}

Node Merge(Node &a, Node& b) {
  Node c;
  c.bst = min(a.bst, b.bst);
  fora (i, 0, LOGN) {
    c[i] = min(a[i], b[i]);
    if (a[i] < INF && b[i] < INF)
      c.bst = min(c.bst, a[i] + b[i]);
  }
  return c;
}

void Build(int v, int l, int r) {
  if (l == r - 1) {
    t[v] = Node();
    Upd(t[v], A[l]);
    return ;
  }
  int m = (l + r) / 2;
  Build(v * 2, l, m);
  Build(v * 2 + 1, m, r);
  t[v] = Merge(t[v*2], t[v*2+1]);
}

void Upd(int v, int l, int r, int pos, int val) {
  if (l == r - 1) {
    t[v] = Node();
    Upd(t[v], val);
    return ;
  }
  int m = (l + r) / 2;
  if (pos < m)
    Upd(v * 2, l, m, pos, val);
  else
    Upd(v * 2 + 1, m, r, pos, val);
  t[v] = Merge(t[v*2], t[v*2+1]);
}

Node Get(int v, int l, int r, int L, int R) {
  if (l == L && r == R)
    return t[v];
  int m = (l + r) / 2;
  if (R <= m)
    return Get(v*2, l, m, L, R);
  if (L >= m)
    return Get(v*2+1, m, r, L, R);
  Node ln = Get(v*2, l, m, L, m);
  Node rn = Get(v*2+1, m, r, m, R);
  return Merge(ln, rn);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> T) {
    A.resize(N);
    fora (i, 0, N) {
      cin >> A[i];
    }
    Build(1, 0, N);
    int a, b, c;
    while (T--) {
      cin >> a >> b >> c;
      --b;
      if (a == 1) {
        Upd(1, 0, N, b, c);
      } else {
        Node ans = Get(1, 0, N, b, c);
        cout << (ans.bst == INF ? -1 : ans.bst) << '\n';
      }
    }
  }
  return 0;
}
