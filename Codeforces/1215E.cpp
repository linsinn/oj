#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
constexpr int Q = 3;
ll dp[1 << Q], cnt[Q][Q];
vector<int> col[Q];

ll Solution() {
  fora (i, 0, N) {
    col[A[i]-1].emplace_back(i);
  }
  fora (i, 0, Q) {
    fora (j, 0, Q) {
      if (i == j) continue;
      if (col[i].empty() || col[j].empty()) continue;
      int pos2 = 0;
      for (int pos1 = 0; pos1 < col[i].size(); ++pos1) {
        while (true) {
          if (pos2 == col[j].size() - 1 || col[j][pos2+1] > col[i][pos1])
            break;
          ++pos2;
        }
        if (col[i][pos1] > col[j][pos2])
          cnt[i][j] += pos2 + 1;
      }
    }
  }
  memset(dp, 0x7f, sizeof(dp));
  dp[0] = 0;
  fora (mask, 0, (1 << Q)) {
    vector<int> was;
    fora (i, 0, Q) {
      if (mask & (1 << i)) {
        was.emplace_back(i);
      }
    }
    fora (i, 0, Q) {
      if (~mask & (1 << i)) {
        ll su = 0;
        fora (j, 0, was.size()) {
          su += cnt[was[j]][i];
        }
        int nmask = mask | (1 << i);
        dp[nmask] = min(dp[nmask], dp[mask] + su);
      }
    }
  }
  return dp[(1 << Q) - 1];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
