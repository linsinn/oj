//
// Created by sinn on 7/16/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll t[MAXN], fac[MAXN], rfac[MAXN];
ll r2[MAXN];

ll FastPow(ll x, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1) ret = ret * x % MOD;
    x = x * x % MOD;
    exp >>= 1;
  }
  return ret;
}

void Init() {
  fac[0] = fac[1] = 1;
  fora (i, 1, MAXN) {
    fac[i] = fac[i-1] * i % MOD;
  }
  rfac[MAXN-1] = FastPow(fac[MAXN-1], MOD-2);
  ford (i, MAXN-2, 0) {
    rfac[i] = rfac[i+1] * (i + 1) % MOD;
  }
  r2[0] = 1;
  r2[1] = FastPow(2, MOD-2);
  fora (i, 2, MAXN) {
    r2[i] = r2[i-1] * r2[1] % MOD;
  }
}

inline ll C(ll n, ll x) {
  return fac[n] * rfac[x] % MOD * rfac[n-x] % MOD;
}

ll Solution() {
  Init();
  ll tot = t[0], cnt = 1;
  ll ans = 0;
  while (tot <= T && cnt <= N) {
    if (cnt < N && tot + t[cnt] + cnt + 1 <= T) {}
    else {
      if (cnt == N) {
        ll rem = T - tot;
        fora (i, 0, rem+1) {
          ans += cnt * C(cnt, i) % MOD * r2[cnt] % MOD;
          ans %= MOD;
        }
      } else {
        ll g = tot + t[cnt];
        ll rem = T - g;
        ll lb = max(0LL, rem + 1), rb = min(cnt, T - tot);
        fora (i, lb, rb+1) {
          ans += cnt * C(cnt, i) % MOD * r2[cnt+1] % MOD;
          ans %= MOD;
        }
        rem = T - (g + 1);
        lb = max(0LL, rem + 1), rb = min(cnt, T - tot);
        fora (i, lb, rb+1) {
          ans += cnt * C(cnt, i) % MOD * r2[cnt+1] % MOD;
          ans %= MOD;
        }
      }
    }
    tot += t[cnt++];
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> T) {
    fora (i, 0, N) cin >> t[i];
    cout << Solution() << '\n';
  }
  return 0;
}
