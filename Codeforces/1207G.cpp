#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 4e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
const int AL = 26;

struct Node {
  int nxt[AL];
  Node() {
    memset(nxt, -1, sizeof(nxt));
  }
  int& operator [](int i) {
    return nxt[i];
  }
};

struct NodeAt {
  int nxt[AL];
  int p;
  char pch;
  int link;
  int go[AL];
  NodeAt() {
    memset(nxt, -1, sizeof(nxt));
    memset(go, -1, sizeof(go));
    link = p = -1;
  }
  int& operator [](int i) {
    return nxt[i];
  }
};

int cntnm;
Node trienm[MAXN];
int cntqr;
NodeAt trieqr[MAXN];

int AddString(string s) {
  int v = 0;
  for (auto ch : s) {
    int c = ch - 'a';
    if (trieqr[v][c] == -1) {
      trieqr[cntqr] = NodeAt();
      trieqr[cntqr].p = v;
      trieqr[cntqr].pch = c;
      trieqr[v][c] = cntqr;
      ++cntqr;
    }
    v = trieqr[v][c];
  }
  return v;
}

int Go(int v, int c);

int GetLink(int v) {
  if (trieqr[v].link == -1) {
    if (v == 0 || trieqr[v].p == 0)
      trieqr[v].link = 0;
    else
      trieqr[v].link = Go(GetLink(trieqr[v].p), trieqr[v].pch);
  }
  return trieqr[v].link;
}

int Go(int v, int c) {
  if (trieqr[v].go[c] == -1) {
    if (trieqr[v][c] != -1) {
      trieqr[v].go[c] = trieqr[v][c];
    } else {
      trieqr[v].go[c] = (v == 0 ? 0 : Go(GetLink(v), c));
    }
  }
  return trieqr[v].go[c];
}

int AddLetter(int v, int c) {
  if (trienm[v][c] == -1) {
    trienm[cntnm] = Node();
    trienm[v][c] = cntnm;
    ++cntnm;
  }
  return trienm[v][c];
}

Vec g[MAXN];
int tin[MAXN], tout[MAXN];

void DfsInit(int v) {
  tin[v] = T++;
  for (auto u : g[v]) {
    DfsInit(u);
  }
  tout[v] = T;
}

int f[MAXN];

void Upd(int v, int val) {
  for (int i = tin[v]; i < MAXN; i |= i + 1) {
    f[i] += val;
  }
}

int Get(int x) {
  int res = 0;
  for (int i = x; i >= 0; i = (i & (i + 1)) - 1)
    res += f[i];
  return res;
}

int Sum(int v) {
  return Get(tout[v] - 1) - Get(tin[v] - 1);
}

int nm[MAXN], qr[MAXN];
Vec nms[MAXN], reqs[MAXN];
int ans[MAXN];

void Dfs(int v, int cur) {
  Upd(cur, 1);
  for (auto it : nms[v]) 
    for (auto q : reqs[it])
      ans[q] = Sum(qr[q]);
  fora (i, 0, AL) 
    if (trienm[v][i] != -1)
      Dfs(trienm[v][i], Go(cur, i));
  Upd(cur, -1);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cntqr = 0;
    trieqr[cntqr++] = NodeAt();
    cntnm = 0;
    trienm[cntnm++] = Node();
    int tp, p; 
    string s;
    fora (i, 0, N) {
      cin >> tp;
      if (tp == 1) {
        cin >> s;
        nm[i] = AddLetter(0, s[0] - 'a');
      } else {
        cin >> p >> s;
        nm[i] = AddLetter(nm[p-1], s[0] - 'a');
      }
      nms[nm[i]].emplace_back(i);
    }
    cin >> M;
    fora (i, 0, M) {
      cin >> p >> s;
      reqs[p-1].emplace_back(i);
      qr[i] = AddString(s);
    }

    fora (v, 1, cntqr) {
      g[GetLink(v)].emplace_back(v);
    }
    T = 0;
    DfsInit(0);
    Dfs(0, 0);

    fora (i, 0, M) {
      cout << ans[i] << '\n';
    }
  }
  return 0;
}
