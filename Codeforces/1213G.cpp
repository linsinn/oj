#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
struct Edge {
  int u, v, w;
  bool operator < (const Edge& rhs) const {
    return w < rhs.w;
  }
};
vector<Edge> es;
vector<Pii> q;
Vec p, rk;
ll res;

int FindPa(int x) {
  if (x == p[x]) return x;
  return p[x] = FindPa(p[x]);
}

ll Calc(int cnt) {
  return 1LL * cnt * (cnt - 1) / 2;
}

void Merge(int u, int v) {
  u = FindPa(u);
  v = FindPa(v);
  if (rk[u] < rk[v]) swap(u, v);
  res -= Calc(rk[u]);
  res -= Calc(rk[v]);
  rk[u] += rk[v];
  res += Calc(rk[u]);
  p[v] = u;
}

void Solution() {
  res = 0;
  p = rk = Vec(N, 1);
  iota(p.begin(), p.end(), 0);

  vector<ll> ans(M);
  sort(es.begin(), es.end());
  sort(q.begin(), q.end());
  int pos = 0;
  fora (i, 0, M) {
    while (pos < N-1 && es[pos].w <= q[i].first) {
      int u = es[pos].u;
      int v = es[pos].v;
      Merge(u, v);
      ++pos;
    }
    ans[q[i].second] = res;
  }
  fora (i, 0, M) cout << ans[i] << ' ';
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    es.resize(N-1);
    fora (i, 0, N-1) {
      cin >> es[i].u >> es[i].v >> es[i].w;
      --es[i].u, --es[i].v;
    }
    q.resize(M);
    fora (i, 0, M) {
      cin >> q[i].first;
      q[i].second = i;
    }
    Solution();
  }
  return 0;
}
