//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int A[MAXN], F[MAXN];

void Solution() {
  vector<int> cnt(N, 0);
  vector<int> cnt_good(N, 0);
  fora (i, 0, N) {
    cnt[A[i]-1]++;
    if (F[i])
      cnt_good[A[i]-1]++;
  }
  vector<vector<int>> types(N+1);
  fora (i, 0, N) types[cnt[i]].emplace_back(cnt_good[i]);
  int ans1 = 0, ans2 = 0;
  multiset<int> cur;
  ford (i, N, 1) {
    for (int x : types[i]) {
      cur.emplace(x);
    }
    if (!cur.empty()) {
      int z = *cur.rbegin();
      ans1 += i;
      ans2 += min(i, z);
      cur.erase(cur.find(z));
    }
  }
  cout << ans1 << ' ' << ans2 << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      fora (i, 0, N) {
        cin >> A[i] >> F[i];
      }
      Solution();
    }
  }
  return 0;
}