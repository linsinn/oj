#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

void Solution() {
  N = s.size();
  vector<Vec> suf(s.size(), Vec(2, 0));
  vector<Vec> pre(s.size(), Vec(2, 0));
  suf[N-1][s.back() - '0'] = 1;
  ford (i, N-2, 0) {
    int k = s[i] - '0';
    if (k == 0) {
      suf[i][0] = 1 + max(suf[i+1][0], suf[i+1][1]);
      suf[i][1] = suf[i+1][1];
    } else {
      suf[i][0] = suf[i+1][0];
      suf[i][1] = 1 + suf[i+1][1];
    }
  }
  int ma = max(suf[0][0], suf[0][1]);
  string ans = s;
  fora (i, 0, N) {
    int a = (i == 0) ? 0 : pre[i-1][0];
    int b = (i == 0) ? 0 : pre[i-1][1];
    int c = (i == N-1) ? 0 : suf[i+1][0];
    int d = (i == N-1) ? 0 : suf[i+1][1];
    if (s[i] == '1') {
      if (ma == max(b + d, a + 1 + max(c, d))) {
        ans[i] = '0';
        pre[i][0] = a + 1;
        pre[i][1] = b;
      } else {
        pre[i][0] = a;
        pre[i][1] = 1 + max(a, b);
      }
    } else {
      pre[i][0] = a + 1;
      pre[i][1] = b;
    }
  }
  cout << ans << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    Solution();
  }
  return 0;
}
