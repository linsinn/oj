//
// Created by sinn on 7/17/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];

ll Solution() {
  map<int, int> mp;
  fora (i, 0, N) {
    mp[A[i]]++;
  }
  int k = 0;
  for (auto &p : mp) {
    if (p.second % 2 == 1)
      ++k;
  }
  return N - k / 2;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
