//
// Created by sinn on 5/30/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MAXN = 2e5 + 1;
constexpr int MAGIC = 1;

using ll = long long;
using Arr = array<int, MAGIC>;

int N, M, K;
string s;
int x[MAXN], y[MAXN], len[MAXN];
Arr MOD, BASE;

inline int norm(int a, const int &mod) {
  while (a >= mod)
    a -= mod;
  while (a < 0)
    a += mod;
  return a;
}

inline int mul(int a, int b, const int& mod) {
  return int(1LL * a * b % mod);
}

inline Arr operator +(const Arr& a, const Arr& b) {
  Arr ret;
  fora(i, 0, ret.size()) {
    ret[i] = norm(a[i] + b[i], MOD[i]);
  }
  return ret;
}

inline Arr operator -(const Arr& a, const Arr& b) {
  Arr ret;
  fora(i, 0, ret.size()) {
    ret[i] = norm(a[i] - b[i], MOD[i]);
  }
  return ret;
}

inline Arr operator *(const Arr& a, const Arr& b) {
  Arr ret;
  fora(i, 0, ret.size()) {
    ret[i] = mul(a[i], b[i], MOD[i]);
  }
  return ret;
}

int CMODS[] = {int(1e9) + 7, int(1e9) + 9, int(1e9) + 21, int(1e9) + 33, int(1e9) + 87, int(1e9) + 93, int(1e9) + 97, int(1e9) + 103};
int CBASE[] = {1009, 1013, 1019, 1021};

Arr pBase[MAXN];
Arr ph[27][MAXN];
vector<int> ord[MAXN];

void Init() {
  mt19937 rnd;
  unsigned int seed = N;
  fora(i, 0, N) {
    seed = (seed * 3) + s[i];
  }
  fora(i, 0, M) {
    seed = (seed * 3) + x[i];
    seed = (seed * 3) + y[i];
    seed = (seed * 3) + len[i];
  }
  rnd.seed(seed);
  set<int> mids;
  while (mids.size() < MOD.size()) {
    mids.emplace(rnd() % 8);
  }
  vector<int> vmids(mids.begin(), mids.end());
  fora(i, 0, MOD.size()) {
    MOD[i] = CMODS[vmids[i]];
    BASE[i] = CBASE[i];
  }

  fill(pBase[0].begin(), pBase[1].end(), 1);

  fora(i, 1, MAXN) {
    pBase[i] = pBase[i-1] * BASE;
  }

  fora(c, 0, 26) {
    fill(ph[c][0].begin(), ph[c][0].end(), 0);
    fora(i, 0, N) {
      int val = (s[i] - 'a' == c);
      auto arr = Arr();
      fill(arr.begin(), arr.end(), val);
      ph[c][i+1] = ph[c][i] * BASE + arr;
    }
  }

  vector<int> cur_ord(26, 0);
  iota(cur_ord.begin(), cur_ord.end(), 0);

  ford(i, N-1, 0) {
    ord[i] = cur_ord;
    auto it = find(ord[i].begin(), ord[i].end(), int(s[i]-'a'));
    ord[i].erase(it);
    ord[i].emplace(ord[i].begin(), int(s[i]-'a'));
    cur_ord = ord[i];
  }
}


Arr GetHash(int id, int l, int r) {
  return ph[id][r] - ph[id][l] * pBase[r-l];
}

void Solution() {
  Init();
  fora(q, 0, M) {
    int s1 = x[q], s2 = y[q];
    bool ok = true;
    fora(i, 0, 26) {
      if (GetHash(ord[s1][i], s1, s1+len[q]) != GetHash(ord[s2][i], s2, s2+len[q])) {
        ok = false;
        break;
      }
    }
    cout << (ok ? "YES" : "NO") << '\n';
  }
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    cin >> s;
    for (int i = 0; i < M; ++i) {
      cin >> x[i] >> y[i] >> len[i];
      --x[i], --y[i];
    }
    Solution();
  }
  return 0;
}