#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<ll> A, B;
vector<vector<ll>> dp;

ll Solution() {
  M = 4;
  dp.assign(N, vector<ll>(M, 0));
  fora (i, 0, M) dp[0][i] = B[0] * i;
  fora (i, 1, N) {
    fora (j, 0, M) {
      dp[i][j] = - 1;
      fora (k, 0, M) {
        if (A[i] + j != A[i-1] + k) {
          if (dp[i][j] == -1 || dp[i][j] > dp[i-1][k] + B[i] * j)
            dp[i][j] = dp[i-1][k] + B[i] * j;
        }
      }
    }
  }
  return *min_element(dp[N-1].begin(), dp[N-1].end());
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      A.resize(N);
      B.resize(N);
      fora (i, 0, N) cin >> A[i] >> B[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}
