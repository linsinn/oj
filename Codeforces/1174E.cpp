//
// Created by sinn on 7/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int memos[2][30][30];

ll FastPow(ll base, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1) {
      ret = ret * base % MOD;
    }
    base = base * base % MOD;
    exp >>= 1;
  }
  return ret;
}

ll F(int x, int y) {
  if (x < 0 || y < 0) return 0;
  ll t = 1 << x;
  if (t > N) return 0;
  t *= FastPow(3, y);
  return N / t;
}

ll Solution() {
  memset(memos, 0, sizeof(memos));
  ll M = log2(N);
  ll K = 0;
  ll b = 1;
  while (b * 3 <= N) {
    ++K;
    b *= 3;
  }
  memos[0][M][0] = 1;
  if ((1LL << (M - 1))  * 3LL <= N)
    memos[0][M-1][1] = 1;
  int cur = 0;
  fora (k, 1, N) {
    int nxt = cur ^ 1;
    fora (i, 0, M+1) {
      fora (j, 0, K+1) {
        if (memos[cur][i][j] != 0) {
          ll g = F(i, j) - k;
          if (g > 0) {
            memos[nxt][i][j] += memos[cur][i][j] * g % MOD;
            memos[nxt][i][j] %= MOD;
          }
          g = F(i-1, j) - F(i, j);
          if (g > 0) {
            memos[nxt][i-1][j] += memos[cur][i][j] * g % MOD;
            memos[nxt][i-1][j] %= MOD;
          }
          g = F(i, j-1) - F(i, j);
          if (g > 0) {
            memos[nxt][i][j-1] += memos[cur][i][j] * g % MOD;
            memos[nxt][i][j-1] %= MOD;
          }
        }
      }
    }
    memset(memos[cur], 0, sizeof(memos[cur]));
    cur ^= 1;
  }
  return memos[cur][0][0];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}
