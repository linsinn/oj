#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr long long INF = 1e18;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
ll a, b;
string s;
Vec A;

ll Solution() {
  s.push_back('0');
  vector<vector<ll>> dp(N+1, vector<ll>(3, INF));
  dp[0][1] = b;
  fora (i, 1, N+1) {
    if (s[i] == '0' && s[i-1] == '0') {
      dp[i][1] = min(dp[i-1][1], dp[i-1][2] + a) + a + b;
    } else {
      dp[i][1] = INF;
    }
    dp[i][2] = min(dp[i-1][2], dp[i-1][1] + a) + a + b * 2;
  }
  return dp[N][1];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> a >> b;
      cin >> s;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
