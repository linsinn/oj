//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
vector<vector<int>> ans;
vector<Pii> gph[MAXN];
vector<int> leaves[MAXN];
bool vis[MAXN];
int pa[MAXN];
int root;

void dfs1(int u) {
  vis[u] = true;
  for (auto &it : gph[u]) {
    if (!vis[it.first]) {
      pa[it.first] = u;
      dfs1(it.first);
      leaves[u].emplace_back(leaves[it.first][0]);
    }
  }
  if (leaves[u].empty())
    leaves[u].emplace_back(u);
}

void add_path(int v, int x) {
  if (leaves[v].size() == 1) {
    ans.push_back({root, v, x});
    return;
  }
  ans.push_back({root, leaves[v][0], x / 2});
  ans.push_back({root, leaves[v][1], x / 2});
  ans.push_back({leaves[v][0], leaves[v][1], -x / 2});
}

void add_edge(int v, int x) {
  if (pa[v] == root) {
    add_path(v, x);
    return;
  }
  add_path(v, x);
  add_path(pa[v], -x);
}

void dfs2(int u) {
  vis[u] = true;
  for (auto &it : gph[u]) {
    if (!vis[it.first]) {
      add_edge(it.first, it.second);
      dfs2(it.first);
    }
  }
}

void Solution() {
  fora (i, 1, N+1) {
    if (gph[i].size() == 2) {
      cout << "NO\n";
      return ;
    }
  }
  root = 1;
  while (gph[root].size() != 1)
    ++root;
  dfs1(root);
  memset(vis, 0, sizeof(vis));
  dfs2(root);
  cout << "YES\n";
  cout << ans.size() << '\n';
  for (auto &v : ans) {
    for (auto &elem : v) {
      cout << elem << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int u, v, x;
    fora (i, 0, N-1) {
      cin >> u >> v >> x;
      gph[u].emplace_back(v, x);
      gph[v].emplace_back(u, x);
    }
    Solution();
  }
  return 0;
}
