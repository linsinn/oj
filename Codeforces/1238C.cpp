//
// Created by sinn on 10/11/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  int ans = 0;
  int p = 0;
  while (p + 1 < A.size()) {
    if (p + 2 >= A.size()) {
      if (A[p + 1] != 1)
        ++ans;
      break;
    } else {
      if (A[p + 2] == A[p + 1] - 1) {
        p = p + 2;
      } else {
        p = p + 1;
        ++ans;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> K >> N;
      A.resize(N);
      fora (i, 0, N) cin >> A[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}