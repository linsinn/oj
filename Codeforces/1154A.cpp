//
// Created by sinn on 4/25/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int x[4];

void Solution() {
    int maxi = *max_element(x, x + 4);
    for (int i = 0; i < 4; i++) {
        if (x[i] != maxi) {
            cout << maxi - x[i] << ' ';
        }
    }
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> x[0] >> x[1] >> x[2] >> x[3]) {
        Solution();
    }
    return 0;
}