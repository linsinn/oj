//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e6 + 10;

using ll = long long;

ll N, M, K, T;
vector<int> g[MAXN];
int deg[MAXN];

void Solution() {
  deg[0] = 0;
  fora (i, 1, N+1) {
    if (deg[i] % 2 == 1) {
      g[0].emplace_back(i);
      g[i].emplace_back(0);
      ++deg[i];
      ++deg[0];
    }
  }
  vector<set<int>> e(N+1);
  fora (i, 0, N+1) {
    for (int v : g[i]) {
      e[i].emplace(v);
    }
  }
  stack<int> stk;
  vector<int> ep;
  int cur_v = 1;
  while (true) {
    if (e[cur_v].empty()) {
      ep.emplace_back(cur_v);
      if (stk.empty()) {
        break;
      } else {
        cur_v = stk.top();
        stk.pop();
      }
    } else {
      int u = *e[cur_v].begin();
      e[cur_v].erase(e[cur_v].begin());
      e[u].erase(cur_v);
      stk.emplace(cur_v);
      cur_v = u;
    }
  }
  fora (i, 0, N+1) {
    for (int v : g[i]) {
      e[i].emplace(v);
    }
  }

  for (int i = 1; i < ep.size(); i += 2) {
    if (i == ep.size() - 1) break;
    int u = ep[i], v = ep[i+1];
    if (u != 0 && v != 0) {
      int p = ep[i-1];
      int q;
      if (i == ep.size() - 2)
        q = ep[0];
      else
        q = ep[i + 2];
      if (p != 0 && q != 0) {
        e[u].erase(v);
        e[v].erase(u);
      }
    }
  }
  vector<pair<int, int>> edges;
  fora (i, 1, N+1) {
    for (int v : e[i]) {
      if (v != 0) {
        edges.emplace_back(i, v);
        e[v].erase(i);
      }
    }
  }
  cout << edges.size() << '\n';
  for (auto &p : edges) {
    cout << p.first << ' ' << p.second << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      g[u].emplace_back(v);
      g[v].emplace_back(u);
      ++deg[u];
      ++deg[v];
    }
    Solution();
  }
  return 0;
}