#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
  int p, q;
  cout << "? ";
  fora (i, 1, 101)
    cout << i << " ";
  cout << endl;
  cin >> p;
  cout << "? ";
  fora (i, 1, 101)
    cout << (i + 1) * 133 << " ";
  cout << endl;
  cin >> q;
  int x = p ^ q;
  int ans = 0;
  for (int i = 1; i <= 100; i++) {
    for (int k = 1; k <= 100; k++) {
      if ((i ^ ((k+1) * 133)) == x) {
        ans = p ^ i;
        break;
      }
    }
  }
  cout << "! " << ans << endl;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  Solution();
  return 0;
}
