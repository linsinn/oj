#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];

ll Solution() {
  std::vector<ll> v(N);
  fora (i, 1, N) {
    v[i-1] = A[i-1] - A[i];
  }
  sort(v.begin(), v.end());
  ll ans = A[N-1] - A[0];
  fora (i, 0, K-1) {
    ans += v[i];
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) 
      cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
