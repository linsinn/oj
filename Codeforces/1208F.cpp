#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Pii dp[1 << 21];
Vec A;

void Add(int mask, int w) {
  if (dp[mask].first == -1) 
    dp[mask].first = w;
  else if (dp[mask].second == -1) {
    if (dp[mask].first == w) 
      return ;
    dp[mask].second = w;
    if (dp[mask].first > dp[mask].second)
      swap(dp[mask].first, dp[mask].second);
  } else {
    if (dp[mask].second < w) {
      dp[mask].first = dp[mask].second;
      dp[mask].second = w;
    } else if (dp[mask].first < w && dp[mask].second != w)
      dp[mask].first = w;
  }
}

void Merge(int a, int b) {
  if (dp[b].first != -1)
    Add(a, dp[b].first);
  if (dp[b].second != -1)
    Add(a, dp[b].second);
}

ll Solution() {
  memset(dp, -1, sizeof(dp));
  int bits = 0;
  fora (i, 0, N) {
    Add(A[i], i);
    bits = max((int)log2(A[i]), bits);
  }
  ++bits;
  fora (i, 0, bits) {
    fora (mask, 0, 1 << bits) {
      if (mask & (1 << i)) {
        Merge(mask ^ (1 << i), mask);
      }
    }
  }
  int ans = 0;
  fora (i, 0, N) {
    int cur = (1 << bits) - 1 - A[i];
    int opt = 0;
    for (int j = bits - 1; j >= 0; --j) {
      if ((cur >> j) & 1) {
        if (dp[opt ^ (1 << j)].second != -1 && dp[opt ^ (1 << j)].first > i) {
          opt ^= (1 << j);
        }
      }
    }
    if (dp[opt].second != -1 && dp[opt].first > i) {
      ans = max(ans, A[i] ^ opt);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
