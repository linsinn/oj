//
// Created by sinn on 6/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
ll a, b, c, d;

ll Solution() {
  vector<ll> pos = {a, b, c};
  sort(pos.begin(), pos.end());
  ll ans = 0;
  ans += max(0LL, d - (pos[1] - pos[0]));
  ans += max(0LL, d - (pos[2] - pos[1]));
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> a >> b >> c >> d) {
    cout << Solution() << '\n';
  }
  return 0;
}
