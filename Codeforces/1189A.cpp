//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
string s;

void Solution() {
  int idx = 0;
  vector<int> p;
  while (idx < N) {
    int a = 0, b = 0;
    int maxi = 0;
    while (idx < N) {
      if (s[idx] == '0') ++a;
      else ++b;
      if (a != b)
        maxi = idx;
      ++idx;
    }
    p.emplace_back(maxi);
    idx = maxi + 1;
  }
  cout << p.size() << '\n';
  fora (i, 0, p.size()) {
    int st = i == 0 ? 0 : p[i-1] + 1;
    int ed = p[i];
    cout << s.substr(st, ed - st + 1) << ' ';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    Solution();
  }
  return 0;
}
