//
// Created by sinn on 5/19/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 998244353;
constexpr int MAXN = 1e6 + 1;

using ll = long long;

int N, M, K;
ll A[MAXN], B[MAXN];

int Solution() {
  for (int i = 0; i < N; i++) {
    A[i] = A[i] * (N-i) * (i+1);
  }
  sort(A, A + N);
  sort(B, B + N);

  ll ans = 0;
  for (int i = 0; i < N; i++) {
    ans += A[i] % MOD * B[N-1-i];
    ans %= MOD;
  }

  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    for (int i = 0; i < N; ++i)
      cin >> A[i];
    for (int j = 0; j < N; ++j)
      cin >> B[j];
    cout << Solution() << '\n';
  }
  return 0;
}
