#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
int mem[MAXN][3];
string s;

ll Solution() {
  int mp[256];
  mp['R'] = 0, mp['G'] = 1, mp['B'] = 2;
  fora (i, 0, 3) mem[0][i] = 1;
  mem[0][mp[s[0]]] = 0;
  fora (i, 1, N) {
    fora (j, 0, 3) {
      mem[i][j] = mem[i-1][(j+2)%3] + (mp[s[i]] != j);
    }
  }
  int ans = INT32_MAX;
  fora (i, K-1, N) {
    fora (j, 0, 3) {
      int g = ((j - 3 - K % 3) % 3 + 3) % 3;
      ans = min(ans, mem[i][j] - (i - K >= 0 ? mem[i-K][g] : 0));
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      cin >> s;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
