//
// Created by sinn on 10/16/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A, B;

ll Solution() {
  set<Pii> ent, ext;
  set<int> seen;
  fora (i, 0, N) {
    ent.emplace(i, A[i]);
    ext.emplace(i, B[i]);
  }
  int ans = 0;
  while (!ent.empty() && !ent.empty()) {
    int a = ent.begin()->second, b = ext.begin()->second;
    if (a != b) {
      if (seen.count(a)) {
        ent.erase(ent.begin());
      } else {
        ans += 1;
        ext.erase(ext.begin());
        seen.emplace(b);
      }
    } else {
      ent.erase(ent.begin());
      ext.erase(ext.begin());
      seen.emplace(b);
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    B.resize(N);
    fora (i, 0, N) cin >> A[i];
    fora (i, 0, N) cin >> B[i];
    cout << Solution() << '\n';
  }
  return 0;
}