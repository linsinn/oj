#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<ll> A;

void Solution() {
  M = 70;
  vector<vector<ll>> cnt(M, vector<ll>());
  fora (i, 0, N) {
    int k = 0;
    while (((1LL << k) & A[i]) == 0) {
      ++k;
    }
    cnt[k].emplace_back(A[i]);
  }
  int idx = 0;
  int su = 0;
  fora (i, 0, M) {
    su += cnt[i].size();
    if (cnt[i].size() > cnt[idx].size()) 
      idx = i;
  }
  cout << su - cnt[idx].size() << '\n';
  fora (i, 0, M) {
    if (i != idx) {
      for (ll e : cnt[i]) {
        cout << e << ' ';
      }
    }
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    Solution();

  }
  return 0;
}
