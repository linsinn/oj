//
// Created by sinn on 7/17/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e3 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int A, B, x, y, z;
int g[MAXN*MAXN];
int hei[MAXN][MAXN];
int mini[MAXN][MAXN];

struct MiniStk {
  stack<Pii> s1, s2;
  MiniStk() = default;
  void emplace(int elem) {
    int mi = s1.empty() ? elem : min(elem, s1.top().second);
    s1.push({elem, mi});
  }
  int get_mini() {
    if (s1.empty() || s2.empty())
      return s1.empty() ? s2.top().second : s1.top().second;
    return min(s1.top().second, s2.top().second);
  }
  void remove() {
    if (s2.empty()) {
      while (!s1.empty()) {
        int elem = s1.top().first;
        s1.pop();
        int mi = s2.empty() ? elem : min(elem, s2.top().second);
        s2.push({elem, mi});
      }
    }
    s2.pop();
  }
  void clear() {
    while (!s1.empty())
      s1.pop();
    while (!s2.empty())
      s2.pop();
  }
};

ll Solution() {
  fora (i, 1, N*M+1) g[i] = (1LL * g[i-1] * x + y) % z;
  fora (i, 1, N+1) {
    fora (j, 1, M+1) {
      hei[i][j] = g[(i-1) * M + j - 1];
    }
  }
  MiniStk s;
  fora (j, 1, M+1) {
    int i = 1;
    fora (k, 0, A-1) {
      s.emplace(hei[i+k][j]);
    }
    while (i + A <= N + 1) {
      s.emplace(hei[i+A-1][j]);
      mini[i][j] = s.get_mini();
      s.remove();
      ++i;
    }
    s.clear();
  }
  ll ans = 0;
  fora (i, 1, N+1-A+1) {
    int j = 1;
    fora (k, 0, B-1) {
      s.emplace(mini[i][j+k]);
    }
    while (j + B <= M + 1) {
      s.emplace(mini[i][j+B-1]);
      ans += s.get_mini();
      s.remove();
      ++j;
    }
    s.clear();
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> A >> B) {
    cin >> g[0] >> x >> y >> z;
    cout << Solution() << '\n';
  }
  return 0;
}
