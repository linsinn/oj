//
// Created by sinn on 5/29/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 1;
constexpr int BITLEN = 20;

using ll = long long;

int N, M, K, Q;
int A[MAXN], x, y;
int go[MAXN][BITLEN], last[BITLEN];

bool Solution() {
  --x, --y;
  bool good = false;
  for (int j = 0; j < BITLEN; ++j) {
    good |= (((A[y] >> j) & 1) && go[x][j] <= y);
  }
  return good;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> Q) {
    for (int i = 0; i < N; ++i) {
      cin >> A[i];
    }
    for (int i = 0; i < BITLEN; ++i) {
      go[N][i] = N;
      last[i] = N;
    }
    for (int i = N-1; i >= 0; --i) {
      for (int j = 0; j < BITLEN; ++j) go[i][j] = N;
      for (int j = 0; j < BITLEN; ++j) {
        if ((A[i] >> j) & 1) {
          for (int k = 0; k < BITLEN; ++k) {
            go[i][k] = min(go[i][k], go[last[j]][k]);
          }
          last[j] = i;
          go[i][j] = i;
        }
      }
    }
    for (int j = 0; j < Q; ++j) {
      cin >> x >> y;
      if (!Solution()) cout << "Fou\n";
      else cout << "Shi\n";
    }
  }
  return 0;
}
