#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  int b = M % 10;
  Vec r = {b};
  while ((r.back() + b) % 10 != r.front()) {
    r.emplace_back((r.back() + b) % 10);
  }
  ll tot = std::accumulate(r.begin(), r.end(), 0LL);
  ll num = N / M;
  ll ans = tot * (num / r.size());
  int rem = num % r.size();
  fora (i, 0, rem) {
    ans += r[i];
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
