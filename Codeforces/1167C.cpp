//
// Created by sinn on 5/16/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 5e5 + 1;

using ll = long long;

int N, M, K;
vector<int> G[MAXN];
int pa[MAXN], si[MAXN];

int find_pa(int x) {
  if (pa[x] == 0)
    return x;
  return pa[x] = find_pa(pa[x]);
}

void merge(int u, int v) {
  u = find_pa(u), v = find_pa(v);
  if (u == v)
    return ;
  si[u] += si[v];
  pa[v] = u;
}

void Solution() {
  memset(pa, 0, sizeof(pa));
  for (int i = 0; i <= N; i++) si[i] = 1;
  for (int i = 0; i < M; i++) {
    for (int j = 1; j < G[i].size(); j++) {
      merge(G[i][j], G[i][j-1]);
    }
  }
  for (int i = 1; i <= N; i++) {
    cout << si[find_pa(i)] << ' ';
  }
  cout << '\n';
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    for (int i = 0; i < M; i++) {
      cin >> K;
      G[i].resize(K);
      for (int j = 0; j < K; j++) {
        cin >> G[i][j];
      }
    }
    Solution();
  }
  return 0;
}