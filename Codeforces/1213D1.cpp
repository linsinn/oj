#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  int mx = *std::max_element(A.begin(), A.end());
  M = 22;
  vector<Vec> dp(mx+1, Vec(M, 0));
  fora (i, 0, N) {
    dp[A[i]][0] += 1;
  }
  int ans = INF;
  ford (i, mx, 0) { 
    int cur = 0;
    int w = 0;
    fora (j, 0, M-1) {
      if (cur + dp[i][j] >= K) {
        ans = min(ans, int(w + (K - cur) * j));
        break;
      }
      cur += dp[i][j];
      w += j * dp[i][j];
    }
    fora (j, 0, M-1)
      dp[i / 2][j+1] += dp[i][j];
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
