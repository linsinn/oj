//
// Created by sinn on 7/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 10;

using ll = long long;

ll N, M, K, T;
int A[MAXN], B[MAXN];

int nodes[MAXN * 4];

void Change(int rt, int lb, int rb, int p, int v) {
  if (rb < p || lb > p) return ;
  if (lb == rb) {
    nodes[rt] = v;
    return ;
  }
  int mid = lb + (rb - lb) / 2;
  int lc = rt << 1, rc = rt << 1 | 1;
  Change(lc, lb, mid, p, v);
  Change(rc, mid+1, rb, p, v);
  nodes[rt] = min(nodes[lc], nodes[rc]);
}

int Query(int rt, int lb, int rb, int l, int r) {
  if (l > rb || r < lb) return 1e8;
  if (l <= lb && rb <= r) {
    return nodes[rt];
  }
  int mid = lb + (rb - lb) / 2;
  int lc = rt << 1, rc = rt << 1 | 1;
  return min(Query(lc, lb, mid, l, r), Query(rc, mid+1, rb, l, r));
}

string Solution() {
  fora (i, 0, N*4) nodes[i] = 1e8;
  map<int, set<int>> pos;
  set<pair<int, int>> ord;
  fora (i, 0, N) {
    pos[A[i]].emplace(i);
    Change(1, 0, N-1, i, A[i]);
    ord.emplace(i, A[i]);
  }
  fora (i, 0, N) {
    auto t = *ord.begin();
    int a = t.first, b = t.second;
    if (b < B[i]) {
      return "NO";
    } else if (b > B[i]) {
      if (pos.count(B[i]) == 0)
        return "NO";
      int p = *pos[B[i]].begin();
      int mini = Query(1, 0, N-1, 0, p);
      if (mini < B[i])
        return "NO";
      pos[B[i]].erase(pos[B[i]].begin());
      if (pos[B[i]].empty())
        pos.erase(B[i]);
      ord.erase({p, A[p]});
      Change(1, 0, N-1, p, 1e8);
    } else {
      pos[b].erase(a);
      if (pos[b].empty())
        pos.erase(b);
      ord.erase(ord.begin());
      Change(1, 0, N-1, a, 1e8);
    }
  }
  return "YES";
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      fora (i, 0, N) cin >> A[i];
      fora (i, 0, N) cin >> B[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}
