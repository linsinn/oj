#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
  vector<Vec> ans(N, Vec(N));
  int k = 0;

  for (int i = 0; i < N; i += 2) {
    for (int j = 0; j < N; j += 2) {
      ans[i][j] = k++;
      ans[i][j+1] = k++;
      ans[i+1][j] = k++;
      ans[i+1][j+1] = k++;
    }
  }

  fora (i, 0, N) {
    fora (j, 0, N) {
      cout << ans[i][j] << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}
