//
// Created by sinn on 6/15/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
int good[] = {4, 8, 15, 16, 23, 42};
int A[MAXN];

int Solution() {
  int t[10];
  memset(t, 0, sizeof(t));
  t[0] = N;
  int ans = 0;
  fora (i, 0, N) {
    auto p = lower_bound(good, good+6, A[i]) - good;
    if (t[p] == 0)
      ++ans;
    else {
      t[p] -= 1;
      t[p+1]++;
    }
  }
  fora (i, 1, 6) {
    ans += t[i] * i;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
