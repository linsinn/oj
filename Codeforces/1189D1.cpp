//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 10;

using ll = long long;

ll N, M, K, T;
vector<int> gph[MAXN];

bool Solution() {
  fora (i, 0, N) {
    if (gph[i].size() == 2)
      return false;
  }
  return true;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int u, v;
    fora (i, 0, N-1) {
      cin >> u >> v;
      --u, --v;
      gph[u].emplace_back(v);
      gph[v].emplace_back(u);
    }
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}
