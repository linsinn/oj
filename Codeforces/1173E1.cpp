//
// Created by sinn on 7/3/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int MAXN = 3e5 + 3;

using ll = long long;

ll N, M, K, T;
int is_like[MAXN];
ll w[MAXN];
ll dp[3003][3003];

inline ll FastPow(ll x, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1) {
      ret = ret * x % MOD;
    }
    x = x * x % MOD;
    exp >>= 1;
  }
  return ret;
}

inline ll Inv(ll x) {
  x %= MOD;
  return FastPow(x, MOD - 2);
}

inline ll Mul(ll &a, ll b) {
  a = a * b % MOD;
  return a;
}

inline ll Add(ll &a, ll b) {
  a = (a + b + MOD) % MOD;
  return a;
}

void Solution() {
  ll su[2] = {0, 0};
  fora (i, 0, N) {
    su[is_like[i]] += w[i];
  }
  ll f[2] = {su[0], su[1]};
  memset(dp, 0, sizeof(dp));
  dp[1][0] = 1;
  fora (i, 1, M+1) {
    fora (j, 0, i+1) {
      ll n1 = su[1] + j, n2 = max(0LL, su[0] - (i - 1 - j));
      if (!dp[i][j])
        continue;
      ll bk = Inv(n1 + n2);
      Mul(n1, bk);
      Mul(n2, bk);
      Add(dp[i+1][j], n2 * dp[i][j] % MOD);
      Add(dp[i+1][j+1], n1 * dp[i][j] % MOD);
    }
  }
  fora (j, 0, M+2) {
    Add(f[1], dp[M+1][j] * j % MOD);
    Add(f[0], -(dp[M+1][j] * (M - j) % MOD));
  }
  su[0] = Inv(su[0]);
  su[1] = Inv(su[1]);
  fora (i, 0, N) {
    ll ns = f[is_like[i]] * su[is_like[i]] % MOD * w[i] % MOD;
    cout << ns << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) {
      cin >> is_like[i];
    }
    fora (i, 0, N) {
      cin >> w[i];
    }
    Solution();
  }
  return 0;
}
