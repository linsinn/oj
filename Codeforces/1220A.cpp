#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

void Solution() {
  vector<int> mp(256, 0);
  for (char ch : s) mp[ch]++;
  while (true) {
    if (mp['o'] > 0 && mp['n'] > 0 && mp['e'] > 0) {
      mp['o']--;
      mp['n']--;
      mp['e']--;
      cout << "1 ";
    }
    else if (mp['z'] > 0 && mp['e'] > 0 && mp['r'] > 0 && mp['o'] > 0) {
      mp['z']--;
      mp['e']--;
      mp['r']--;
      mp['o']--;
      cout << "0 ";
    }
    else 
      break;
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    Solution();
  }
  return 0;
}
