#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s, t;

void Solution() {
  queue<int> ab, ba;
  fora (i, 0, N) {
    if (s[i] == 'a' && t[i] == 'b')
      ab.emplace(i);
    if (s[i] == 'b' && t[i] == 'a')
      ba.emplace(i);
  }
  if ((ab.size() + ba.size()) % 2 == 1) {
    cout << "-1\n";
    return ;
  }
  vector<Pii> ans;
  while (ab.size() >= 2) {
    int p = ab.front(); ab.pop();
    int q = ab.front(); ab.pop();
    ans.emplace_back(p, q);
  }
  while (ba.size() >= 2) {
    int p = ba.front(); ba.pop();
    int q = ba.front(); ba.pop();
    ans.emplace_back(p, q);
  }
  if (!ab.empty()) {
    int p = ab.front(), q = ba.front();
    ans.emplace_back(p, p);
    ans.emplace_back(p, q);
  }
  cout << ans.size() << '\n';
  for (auto &p : ans) {
    cout << p.first + 1 << ' ' << p.second + 1 << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s >> t;
    Solution();
  }
  return 0;
}
