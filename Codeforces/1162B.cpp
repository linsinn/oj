//
// Created by sinn on 5/8/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[55][55], B[55][55];

bool Solution() {
  for (int i = 0 ; i < N; i++) {
    for (int j = 0; j < M; j++) {
      if (A[i][j] > B[i][j])
        swap(A[i][j], B[i][j]);
    }
  }
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < M; j++) {
      if (j < M-1 && (A[i][j] >= A[i][j+1] || B[i][j] >= B[i][j+1]))
        return false;
      if (i < N-1 && (A[i][j] >= A[i+1][j] || B[i][j] >= B[i+1][j]))
        return false;
    }
  }
  return true;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    for (int i = 0; i < N; i++)
      for (int j = 0; j < M; j++)
        cin >> A[i][j];
    for (int i = 0; i < N; i++)
      for (int j = 0; j < M; j++)
        cin >> B[i][j];
    if (Solution()) cout << "Possible\n";
    else cout << "Impossible\n";
  }
  return 0;
}