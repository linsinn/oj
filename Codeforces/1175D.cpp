//
// Created by sinn on 7/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];

ll Solution() {
  ll su = 0;
  vector<ll> v;
  ford (i, N-1, 0) {
    su += A[i];
    if (i > 0)
      v.emplace_back(su);
  }
  ll ans = su;
  sort(v.rbegin(), v.rend());
  fora (i, 0, K-1) ans += v[i];
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
