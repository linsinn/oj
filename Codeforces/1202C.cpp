#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

struct Rob {
  ll x, y;
  Rob(int _x, int _y) : x{_x}, y{_y} {}
  void Move(char m) {
    if (m == 'W') {
      x -= 1;
    } else if (m == 'A') {
      y -= 1;
    } else if (m == 'D') {
      y += 1;
    } else {
      x += 1;
    }
  }
};

ll Solution() {
  Rob rob(0, 0);
  ll lf = 0, ri = 0, tp = 0, bt = 0;
  for (char ch : s) {
    rob.Move(ch);
    lf = min(lf, rob.y);
    ri = max(ri, rob.y);
    tp = min(tp, rob.x); 
    bt = max(bt, rob.x);
  }
  ll w = ri - lf + 1;
  ll h = bt - tp + 1;
  ll ans = w * h;
  if (w > 2) {
    lf = 0, ri = 0, tp = 0, bt = 0;
    rob.x = rob.y = 0;
    int i = 0;
    for (; i < s.size(); ++i) {
      rob.Move(s[i]);
      lf = min(lf, rob.y);
      ri = max(ri, rob.y);
      if (ri - lf + 1 == w) {
        i += 1;
        break;
      }
    }
    bool f = true;
    rob.x = rob.y = 0;
    lf = 0, ri = 0, tp = 0, bt = 0;
    for (; i < s.size(); ++i) {
      rob.Move(s[i]);
      lf = min(lf, rob.y);
      ri = max(ri, rob.y);
      if (ri - lf + 1 == w) {
        f = false;
        break;
      }
    }
    if (f)
      ans = min(ans, (w - 1) * h);
  }
  if (h > 2) {
    lf = 0, ri = 0, tp = 0, bt = 0;
    rob.x = rob.y = 0;
    int i = 0;
    for (; i < s.size(); ++i) {
      rob.Move(s[i]);
      tp = min(tp, rob.x); 
      bt = max(bt, rob.x);
      if (bt - tp + 1 == h) {
        i += 1;
        break;
      }
    }
    bool f = true;
    rob.x = rob.y = 0;
    lf = 0, ri = 0, tp = 0, bt = 0;
    for (; i < s.size(); ++i) {
      rob.Move(s[i]);
      tp = min(tp, rob.x); 
      bt = max(bt, rob.x);
      if (bt - tp + 1 == h) {
        f = false;
        break;
      }
    }
    if (f)
      ans = min(ans, w * (h - 1));
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> s;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
