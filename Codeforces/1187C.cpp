//
// Created by sinn on 7/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e3 + 10;

using ll = long long;

ll N, M, K, T;
int t[MAXN], l[MAXN], r[MAXN];

bool Check(int idx, vector<int>& v) {
  if (t[idx] == 1) {
    fora (i, l[idx]+1, r[idx]+1) {
      if (v[i] < v[i-1])
        return false;
    }
    return true;
  } else {
    fora (i, l[idx]+1, r[idx]+1) {
      if (v[i] < v[i-1])
        return true;
    }
    return false;
  }
}

void Solution() {
  int g[2] = {0, 0};
  vector<tuple<int, int, int>> vec;
  fora (i, 0, M) {
    vec.emplace_back(l[i], t[i], 1);
    vec.emplace_back(r[i], t[i], -1);
  }
  sort(vec.begin(), vec.end());
  vector<int> ans(N+1);
  fora (i, 1, N+1) {
    bool f = false;
    fora (j, 0, M) {
      if (t[j] == 1 && l[j]+1 <= i && i <= r[j]) {
        ans[i] = ans[i - 1];
        f = true;
      }
    }
    if (!f) ans[i] = ans[i-1] - 1;
  }
  fora (i, 0, M) {
    if (!Check(i, ans)) {
      cout << "NO\n";
      return ;
    }
  }
  int diff = 1 - *min_element(ans.begin(), ans.end());
  cout << "YES\n";
  fora (i, 1, N+1) {
    cout << ans[i] + diff << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, M) {
      cin >> t[i] >> l[i] >> r[i];
    }
    Solution();
  }
  return 0;
}
