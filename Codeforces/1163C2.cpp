//
// Created by sinn on 7/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
Pii pts[MAXN];
constexpr ll INF = 1e9;

pair<double, double> Calc(int i, int j) {
  int g = pts[i].first - pts[j].first;
  if (g == 0) {
    return {INF, pts[i].first};
  }
  double s = double(pts[i].second - pts[j].second) / double(g);
  double x = pts[i].second - s * pts[i].first;
  return {s, x};
}

ll Solution() {
  map<double, set<double>> mp;
  fora (i, 0, N) {
    fora (j, i+1, N) {
      auto p = Calc(i, j);
      mp[p.first].emplace(p.second);
    }
  }
  ll tot = 0;
  for (auto &p : mp) {
    tot += p.second.size();
  }
  ll ans = 0;
  for (auto &p : mp) {
    ans += (tot - p.second.size()) * p.second.size();
    tot -= p.second.size();
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> pts[i].first >> pts[i].second;
    cout << Solution() << '\n';
  }
  return 0;
}
