#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec pre_max, pre_min;
Vec nxt_max, nxt_min;
Vec A;
Vec c; 
int t[MAXN * 4], add[MAXN * 4];

void Build(int v, int l, int r) {
  if (l == r) {
    t[v] = c[l];
    return ;
  }
  int m = (l + r) >> 1;
  Build(v * 2, l, m);
  Build(v * 2 + 1, m + 1, r);
  t[v] = max(t[v*2], t[v*2 + 1]);
}

void Push(int v, int l, int r) {
  if (l == r) return ;
  if (add[v] != 0) {
    add[v * 2] += add[v];
    add[v * 2 + 1] += add[v];
    t[v * 2] += add[v];
    t[v * 2 + 1] += add[v];
    add[v] = 0;
  }
}

void Upd(int v, int l, int r, int L, int R, int val) {
  Push(v, l, r);
  if (l > R || r < L) return ;
  if (L <= l && r <= R) {
    t[v] += val;
    add[v] += val;
    return ;
  }
  int m = (l + r) / 2;
  Upd(v * 2, l, m, L, R, val);
  Upd(v * 2 + 1, m+1, r, L, R, val);
  t[v] = max(t[v*2], t[v*2+1]);
}

int Qry(int v, int l, int r, int p) {
  Push(v, l, r);
  if (l == p && r == p) return t[v];
  int m = (l + r) / 2;
  if (p <= m) {
    return Qry(v * 2, l, m, p);
  } else {
    return Qry(v * 2 + 1, m+1, r, p);
  }
}


void Solution() {
  fora (i, 0, N) A.emplace_back(A[i]);
  pre_max.assign(N, -1);
  pre_min.assign(N, -1);
  nxt_max.assign(N, -1);
  nxt_min.assign(N, -1);
  fora (i, 1, 2 * N) {
    if (A[i-1] < A[i]) {
      pre_min[i%N] = (i-1) % N;
    } else {
      int j = pre_min[(i-1)%N];
      while (j != -1 && A[j] > A[i]) {
        j = pre_min[j];
      }
      if (j == -1 || A[i] == A[j]) pre_min[i%N] = -1;
      else pre_min[i%N] = j;
    }
  }
  ford (i, 2*N - 2, 0) {
    if (A[i+1] < A[i]) {
      nxt_min[i%N] = (i+1) % N;
    } else {
      int j = nxt_min[(i+1) % N];
      while (j != -1 && A[j] > A[i]) {
        j = nxt_min[j];
      }
      if (j == -1 || A[i] == A[j]) nxt_min[i%N] = -1;
      else nxt_min[i%N] = j;
    }
  }
  c.assign(N, 0);
  Vec a(N, 0), b(N, 0);
  fora (i, 0, N) {
    int j = pre_min[i];
    if (j == -1 || j == i) continue;
    if (j < i) {
      a[i] = a[j] + 1;
    }
  }
  ford (i, N-1, 0) {
    int j = nxt_min[i];
    if (j == -1 || j == i) continue;
    if (j > i) {
      b[i] = b[j] + 1;
    }
  }
  fora (i, 0, N) c[i] = a[i] + b[i];
  Build(1, 0, N-1);
  int ans = t[1];
  int shift = 0;
  fora (i, 1, N) {
    int j = nxt_min[i-1];
    if (j == -1 || j == i - 1) {
      continue;
    } else { 
      if (j > i - 1)
        Upd(1, 0, N-1, i, j-1, -1);
      else {
        Upd(1, 0, N-1, i, N-1, -1);
        Upd(1, 0, N-1, 0, j-1, -1);
      }
    }
    j = pre_min[i-1];
    if (j < i - 1) {
      Upd(1, 0, N-1, j+1, i-2, 1);
    } else {
      Upd(1, 0, N-1, 0, i-2, 1);
      Upd(1, 0, N-1, j+1, N-1, 1);
    }
    int v1 = Qry(1, 0, N-1, j);
    int v2 = Qry(1, 0, N-1, i-1);
    Upd(1, 0, N-1, i-1, i-1, v1 + 1 - v2);
    if (ans > t[1]) {
      ans = t[1];
      shift = i;
    }
  }
  cout << ans + 1 << ' ' << shift << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
