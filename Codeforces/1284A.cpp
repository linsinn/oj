//
// Created by sinn on 1/4/20.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<string> A, B, C;

void Init() {
	int i = 0, j = 0;
	do {
		C.emplace_back(A[i] + B[j]);
		i = (i + 1) % N;
		j = (j + 1) % M;
	} while (i != 0 || j != 0);
}

string Solution() {
	return C[(K-1) % C.size()];
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
  	A.resize(N);
  	B.resize(M);
  	fora (i, 0, N) cin >> A[i];
  	fora (i, 0, M) cin >> B[i];
  	Init();
  	cin >> T;
  	while (T--) {
  		cin >> K;
  		cout << Solution() << '\n';
  	}
  }
  return 0;
}