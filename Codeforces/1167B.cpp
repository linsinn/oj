//
// Created by sinn on 5/16/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[6] = {4, 8, 15, 16, 23, 42};
map<int, pair<int, int>> mps;

void handle(int r0, int r1) {
  int a = mps[r0].first, b = mps[r0].second;
  int c = mps[r1].first, d = mps[r1].second;
  if (a == c || a == d)
    cout << b << ' ' << a << ' ' << (a == c ? d : c) << ' ';
  if (b == c || b == d)
    cout << a << ' ' << b << ' ' << (b == c ? d : c) << ' ';
}

void Solution() {
  for (int i = 0; i < 6; i++) {
    for (int j = i+1; j < 6; j++)
      mps[A[i] * A[j]] = make_pair(A[i], A[j]);
  }
  int ret[4];
  cout << "? 1 2" << endl;
  cin >> ret[0];
  cout << "? 2 3" << endl;
  cin >> ret[1];
  cout << "? 4 5" << endl;
  cin >> ret[2];
  cout << "? 5 6" << endl;
  cin >> ret[3];

  cout << "! ";
  handle(ret[0], ret[1]);
  handle(ret[2], ret[3]);
  cout << endl;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  Solution();
  return 0;
}