//
// Created by sinn on 5/16/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s;

string Solution() {
  char cols[] = {'0', '1'};
  int layer = 0;
  stack<int> stk;
  stk.emplace(0);
  string ans;
  for (char ch : s) {
    if (ch == '(') {
      stk.emplace(stk.top() + 1);
      ans += cols[stk.top()%2];
    } else {
      ans += cols[stk.top()%2];
      stk.pop();
    }
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}