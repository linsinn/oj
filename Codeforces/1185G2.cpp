//
// Created by sinn on 6/21/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 55;
constexpr int MAXS = 2505;

using ll = long long;

int N, M, K, T;
int a[MAXN][MAXS];
int bc[MAXN][MAXN][MAXS];
int ways[MAXN][MAXN][MAXN][4];
int t[MAXN], g[MAXN];

void Inc(int& x, int y) {
  x += y;
  if (x >= MOD) x -= MOD;
}

int Solution() {
  memset(a, 0, sizeof(a));
  memset(bc, 0, sizeof(bc));
  memset(ways, 0, sizeof(ways));
  a[0][0] = bc[0][0][0] = 1;
  vector<int> cnts(4, 0);
  vector<int> durs(4, 0);
  fora (i, 0, N) {
    int dur = t[i], type = g[i] - 1;
    if (type == 0) {
      ford (cnts0, cnts[0], 0) {
        fora (durs0, 0, durs[0]+1) {
          Inc(a[cnts0+1][durs0+dur], a[cnts0][durs0]);
        }
      }
    } else {
      ford (cnts1, cnts[1], 0) {
        ford (cnts2, cnts[2], 0) {
          fora (durs12, 0, durs[1] + durs[2] + 1) {
            Inc(bc[cnts1+(type==1)][cnts2+(type==2)][durs12+dur], bc[cnts1][cnts2][durs12]);
          }
        }
      }
    }
    cnts[type]++;
    durs[type] += dur;
  }

  ways[0][0][0][3] = 1;
  vector<int> c(3);
  for (c[0] = 0; c[0] <= cnts[0]; ++c[0]) {
    for (c[1] = 0; c[1] <= cnts[1]; ++c[1]) {
      for (c[2] = 0; c[2] <= cnts[2]; ++c[2]) {
        fora (lst, 0, 4) {
          if (ways[c[0]][c[1]][c[2]][lst] != 0) {
            fora (nxt, 0, 3) {
              if (nxt != lst && c[nxt] + 1 <= cnts[nxt]) {
                vector<int> cn(c);
                ++cn[nxt];
                Inc(ways[cn[0]][cn[1]][cn[2]][nxt], ways[c[0]][c[1]][c[2]][lst]);
              }
            }
          }
        }
      }
    }
  }
  vector<int> f(N+1, 1);
  fora (i, 0, N) {
    f[i+1] = 1LL * f[i] * (i+1) % MOD;
  }
  int ans = 0;
  for (c[0] = 0; c[0] <= cnts[0]; ++c[0]) {
    fora (durs0, 0, durs[0] + 1) {
      if (T - durs0 >= 0) {
        for (c[1] = 0; c[1] <= cnts[1]; ++c[1]) {
          for (c[2] = 0; c[2] <= cnts[2]; ++c[2]) {
            ll extra = 1LL * a[c[0]][durs0] * bc[c[1]][c[2]][T-durs0] % MOD;
            fora (i, 0, 3) extra = extra * f[c[i]] % MOD;
            fora (lst, 0, 3) {
              if (c[lst] > 0) {
                Inc(ans, extra * ways[c[0]][c[1]][c[2]][lst] % MOD);
              }
            }
          }
        }
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> T) {
    fora (i, 0, N) {
      cin >> t[i] >> g[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
