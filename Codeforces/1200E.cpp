#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<string> ss;

struct StringHash {
  using ull = unsigned long long;
  vector<vector<ull>> H;
  vector<ull> base = {63}, mod = {1000000009ull};
  vector<vector<ull>> pows;
  int SZ;
  inline int ToInt(char ch) {
    if ('a' <= ch && ch <= 'z') return ch - 'a' + 1;
    else if ('A' <= ch && ch <= 'Z') return ch - 'A' + 1 + 26;
    else return ch - '0' + 1 + 52;
  }
  StringHash(const string &s) {
    SZ = base.size();
    int k = ToInt(s[0]);
    H.emplace_back(vector<ull>(SZ, k));
    pows.emplace_back(vector<ull>());
    fora (i, 0, SZ) {
      pows.back().emplace_back(1);
    }
    fora (i, 1, s.size()) {
      vector<ull> h(SZ);
      vector<ull> p(SZ);
      k = ToInt(s[i]);
      fora (j, 0, SZ) {
        ull g = H.back()[j];
        h[j] = (g * base[j] + k) % mod[j];
        p[j] = pows.back()[j] * base[j] % mod[j];
      }
      H.emplace_back(h);
      pows.emplace_back(p);
    }
  }
  void Append(char ch) {
    vector<ull> h(SZ);
    vector<ull> p(SZ);
    int k = ToInt(ch);
    fora (j, 0, SZ) {
      ull g = H.back()[j];
      h[j] = (g * base[j] + k) % mod[j];
      p[j] = pows.back()[j] * base[j] % mod[j];
    }
    H.emplace_back(h);
    pows.emplace_back(p);
  }
  int Compare(const string &s) {
    vector<ull> ha(SZ, ToInt(s[0]));
    int res = -1;
    fora (i, 0, s.size()) {
      if (i+1 > H.size()) break;
      int k = ToInt(s[i]);
      bool found = true;
      fora (j, 0, SZ) {
        ull g = i == 0 ? 0 : ha[j];
        ha[j] = (g * base[j] + k) % mod[j];
        if (H.size() - 1 == i) 
          g = H.back()[j];
        else {
          g = H[H.size() - 1 - i - 1][j];
          g = g * pows[i+1][j] % mod[j];
          g = (H.back()[j] - g + mod[j]) % mod[j];
        }
        if (g != ha[j]) {
          found = false;
          break;
        }
      }
      if (found) 
        res = i;
    }
    fora (i, res+1, s.size()) {
      Append(s[i]);
    }
    return res + 1;
  }
};

void Solution() {
  StringHash sh(ss[0]);
  vector<int> ans = {0};
  fora (i, 1, N) {
    ans.emplace_back(sh.Compare(ss[i]));
  }
  fora (i, 0, N) {
    cout << ss[i].substr(ans[i]);
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    ss.resize(N);
    fora (i, 0, N) cin >> ss[i];
    Solution();
  }
  return 0;
}
