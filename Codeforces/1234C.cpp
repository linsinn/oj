#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s[2];

ll Solution() {
  vector<int> tp(256, 0);
  fora (i, '1', '3') tp[i] = 0;
  fora (i, '3', '7') tp[i] = 1;
  int i = 0, j = 0, k = 0;
  bool f = true;
  while (j < N) {
    if (i == 0) {
      if (k == 0) {
        if (tp[s[i][j]] == 0) {
          ++j;
        } else {
          i = 1;
          k = 1;
        }
      } else {
        if (tp[s[i][j]] == 0) {
          f = false;
          break;
        } else {
          ++j;
          k = 0;
        }
      }
    } else {
      if (k == 0) {
        if (tp[s[i][j]] == 0) {
          ++j;
        } else {
          i = 0;
          k = 1;
        }
      } else {
        if (tp[s[i][j]] == 0) {
          f = false;
          break;
        } else {
          ++j;
          k = 0;
        }
      }
    }
  }
  if (!f) return f;
  return i == 1 && j == N;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      cin >> s[0] >> s[1];
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
