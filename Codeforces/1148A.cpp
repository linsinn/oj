//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int a, b, c;

ll Solution() {
  ll ans = 0;
  int mini = min(a, b);
  ans += 1LL * mini * 2;
  ans += 1LL * c * 2;
  a -= mini;
  b -= mini;
  if (a != 0 || b != 0)
    ++ans;
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> a >> b >> c) {
    cout << Solution() << '\n';
  }
  return 0;
}
