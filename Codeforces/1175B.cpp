//
// Created by sinn on 7/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
ll maxi = 1e12;

ll N, M, K, T;
string cmds[MAXN];

ll Handle(int st, int ed, map<int, int> &match) {
  if (st > ed) return 0;
  int i = st;
  ll v = 0;
  while (i <= ed) {
    if (cmds[i][0] == 'a') {
      ++v;
      ++i;
    } else {
      auto p = cmds[i].find(' ') + 1;
      int lt = stoi(cmds[i].substr(p, cmds[i].size() - p));
      ll g = lt * Handle(i+1, match[i]-1, match);
      if (maxi - g <= v)
        return maxi;
      v = v + g;
      i = match[i] + 1;
    }
    if (v >= maxi)
      return maxi;
  }
  return v;
}

void Solution() {
  map<int, int> mp;
  stack<int> stk;
  fora (i, 0, N) {
    if (cmds[i][0] == 'f') {
      stk.emplace(i);
    } else if (cmds[i][0] == 'e') {
      mp[stk.top()] = i;
      stk.pop();
    }
  }
  ll v = Handle(0, N-1, mp);
  if (v > UINT32_MAX)
    cout << "OVERFLOW!!!\n";
  else
    cout << v << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    getline(cin, cmds[0]);
    fora (i, 0, N) {
      getline(cin, cmds[i]);
    }
    Solution();
  }
  return 0;
}
