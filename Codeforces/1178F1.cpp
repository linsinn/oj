//
// Created by sinn on 7/22/19.
//

#include <bits/stdc++.h>
#include <iostream>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int MAXN = 505;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int C[MAXN];

ll Solution() {
  vector<vector<ll>> D(N+1, vector<ll>(N+1, 1LL));
  fora (l, 1, M+1) {
    fora (a, 0, M-l+1) {
      Pll lo = {C[a], 0};
      fora (i, 0, l) {
        lo = min(lo, {C[a+i], i});
      }
      int j = lo.second;
      if (j < 0 || j >= l) {
        D[a][l] = 0;
        continue;
      }
      ll left = 0, right = 0;
      fora (u, 0, j+1) {
        left += D[a][u] * D[a+u][j-u] % MOD;
        left %= MOD;
      }
      fora (v, j+1, l+1) {
        right += D[a+j+1][v-(j+1)] * D[a+v][l-v];
        right %= MOD;
      }
      D[a][l] = left * right % MOD;
    }
  }
  return D[0][N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) {
      cin >> C[i];
      --C[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
