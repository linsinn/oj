//
// Created by sinn on 10/11/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

ll Solution() {
  Vec vec(1, 0);
  for (int i = 0; i < s.size(); ) {
    int j = i + 1;
    while (j < s.size() && s[j] == s[i])
      ++j;
    vec.emplace_back(j - i);
    i = j;
  }
  vec.emplace_back(0);
  ll ans = 1LL * s.size() * (s.size() - 1) / 2;
  fora (i, 1, vec.size() - 1) {
    ans -= vec[i - 1];
    ans -= vec[i + 1];
  }
  return ans + vec.size() - 3;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}
