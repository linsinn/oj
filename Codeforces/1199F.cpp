#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
string grid[MAXN];;

ll Solution() {
  int dp[N+2][N+2][N+2][N+2];
  memset(dp, 0, sizeof dp);
  fora (i, 1, N+1) {
    fora (j, 1, N+1) {
      dp[1][1][i][j] = (grid[i-1][j-1] == '#');
    }
  }
  fora (h, 1, N+1) {
    fora (w, 1, N+1) {
      fora (i, 1, N-h+2) {
        fora (j, 1, N-w+2) {
          if (h == 1 && w == 1) continue;
          dp[h][w][i][j] = max(h, w);
          fora (k, 0, h+1) {
            Min(dp[h][w][i][j], dp[k][w][i][j] + dp[h-k][w][i+k][j]);
          }
          fora (k, 0, w+1) {
            Min(dp[h][w][i][j], dp[h][k][i][j] + dp[h][w-k][i][j+k]);
          }
        }
      }
    }
  }
  return dp[N][N][1][1];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> grid[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
