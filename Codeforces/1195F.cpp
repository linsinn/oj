//
// Created by sinn on 7/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

struct Point {
  int x, y;
  Point(int _x, int _y) : x{_x}, y{_y} {}
  Point operator - (const Point& rhs) const {
    return Point(x - rhs.x, y - rhs.y);
  }
  Point& operator /= (int num) {
    x /= num;
    y /= num;
    return *this;
  }
  bool operator < (const Point& rhs) const {
    return x < rhs.x || (x == rhs.x && y < rhs.y);
  }
};

vector<int> segtree;

void SegtreeSet(int pos, int val) {
  pos += M;
  segtree[pos] =val;
  pos /= 2;
  while (pos) {
    segtree[pos] = segtree[pos*2] + segtree[pos*2+1];
    pos /= 2;
  }
}

int SegtreeGet(int v, int vl, int vr, int l, int r) {
  if (vr <= l || r <= vl)
    return 0;
  if (l <= vl && vr <= r)
    return segtree[v];
  int vm = (vl + vr) / 2;
  return SegtreeGet(v*2, vl, vm, l, r) + SegtreeGet(v*2+1, vm, vr, l, r);
}

vector<vector<Point>> polys;
vector<Point> vecs;
vector<int> borders;

void Solution() {
  borders.emplace_back(0);
  for (auto &poly : polys) {
    int k = poly.size();
    borders.emplace_back(borders.back() + k);
    fora (i, 0, k) {
      int nxt = (i + 1) % k;
      Point v = poly[nxt] - poly[i];
      int tmp = __gcd(abs(v.x), abs(v.y));
      v /= tmp;
      vecs.emplace_back(v);
    }
  }
  vector<int> arr;
  map<Point, int> inds;
  for (auto v : vecs) {
    if (!inds.count(v)) {
      int tmp = inds.size();
      inds[v] = tmp;
    }
    arr.emplace_back(inds[v]);
  }
  vector<int> nxt(vecs.size());
  vector<int> lst(inds.size(), INF);
  ford (i, vecs.size()-1, 0) {
    nxt[i] = lst[arr[i]];
    lst[arr[i]] = i;
  }
  vector<vector<Pii>> here(vecs.size());
  cin >> T;
  vector<int> ans(T);
  fora (i, 0, T) {
    int l, r;
    cin >> l >> r;
    l = borders[l-1];
    r = borders[r];
    here[l].emplace_back(r, i);
  }
  M = 1;
  while (M < vecs.size()) {
    M *= 2;
  }
  segtree.resize(M * 2);
  fora (i, 0, inds.size()) 
    SegtreeSet(lst[i], 1);
  fora (i, 0, vecs.size()) {
    for (auto p : here[i]) {
      ans[p.second] = SegtreeGet(1, 0, M, i, p.first);
    }
    SegtreeSet(i, 0);
    if (nxt[i] != INF)
      SegtreeSet(nxt[i], 1);
  }
  for (int num : ans)
    cout << num << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> K;
      polys.emplace_back(vector<Point>());
      fora (j, 0, K) {
        int x, y;
        cin >> x >> y;
        polys.back().emplace_back(Point(x, y));
      }
    }
    Solution();
  }
  return 0;
}
