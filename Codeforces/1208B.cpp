#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  map<int, int> ss;
  fora (i, 0, N) {
    ss[A[i]]++;
  }
  if (ss.size() == N)
    return 0;
  int ans = N;
  map<int, int> tmp;
  fora (i, 0, N) {
    tmp = ss;
    fora (j, i, N) {
      tmp[A[j]]--;
      if (tmp[A[j]] == 0)
        tmp.erase(A[j]);
      if (tmp.size() == N - (j - i + 1))
        ans = min(ans, j - i + 1);
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
