//
// Created by sinn on 6/15/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
bool vis[MAXN];
int vs[2][MAXN];

void Solution(vector<vector<int>>& g) {
  memset(vis, 0, sizeof(int) * (N+1));
  queue<pair<int, int>> que;
  que.emplace(1, 1);
  vis[1] = true;
  int s[2] = {0, 0};
  while (!que.empty()) {
    auto n = que.front();
    que.pop();
    vs[n.second][s[n.second]++] = n.first;
    for (int v : g[n.first]) {
      if (!vis[v]) {
        vis[v] = true;
        que.emplace(v, n.second ^ 1);
      }
    }
  }
  int t = s[0] < s[1] ? 0 : 1;
  cout << s[t] << '\n';
  fora (i, 0, s[t]) {
    cout << vs[t][i] << " \n"[i == s[t]-1];
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      vector<vector<int>> g(N+1);
      int u, v;
      fora (i, 0, M) {
        cin >> u >> v;
        g[u].emplace_back(v);
        g[v].emplace_back(u);
      }
      Solution(g);
    }
  }
  return 0;
}