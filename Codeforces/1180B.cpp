//
// Created by sinn on 6/22/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
int A[MAXN];

void Solution() {
  vector<int> ans(N);
  vector<pair<int, int>> less0, more0;
  fora (i, 0, N) {
    if (A[i] < 0) less0.emplace_back(A[i], i);
    else more0.emplace_back(A[i], i);
  }
  sort(more0.begin(), more0.end());
  sort(less0.begin(), less0.end());
  int n = less0.size(), m = more0.size();
  fora (i, 0, m) {
     more0[i].first = -more0[i].first - 1;
  }
  if (n % 2 != m % 2) {
    if (less0.empty() || (!more0.empty() && abs(less0.front().first) < abs(more0.back().first)))
      more0[m-1].first = -more0[m-1].first - 1;
    else
      less0[0].first = -less0[0].first - 1;
  }
  for (auto& p : less0)
    ans[p.second] = p.first;
  for (auto& p : more0)
    ans[p.second] = p.first;
  fora (i, 0, N) cout << ans[i] << " \n"[i==N-1];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}