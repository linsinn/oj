#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  if (K == 1) return 3;
  vector<ll> phi(N+1);
  std::iota(phi.begin(), phi.end(), 0);
  for (int i = 2; i <= N; ++i) {
    if (phi[i] == i) {
      phi[i]--;
      for (int j = i * 2; j <= N; j += i) {
        phi[j] = (phi[j] / i) * (i - 1);
      }
    }
  }
  K += 2;
  sort(phi.begin(), phi.end());
  return std::accumulate(phi.begin(), phi.begin() + K + 1, 0LL);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cout << Solution() << '\n';
  }
  return 0;
}
