#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string t, s[MAXN];

constexpr int A = 27;

struct Node {
  Node *to[A];
  int cnt;
} nodes[MAXN * 3];
int szn = 0;

Node* GetNode() {
  assert(szn < MAXN * 3);
  fora (i, 0, A)
    nodes[szn].to[i] = nullptr;
  nodes[szn].cnt = 0;
  return &nodes[szn++];
}

void AddWord(Node *v, const string& x) {
  fora (i, 0, x.size()) {
    int c = x[i] - 'a';
    if (v->to[c] == nullptr)
      v->to[c] = GetNode();
    v = v->to[c];
  }
  v->cnt++;
}

int CalcCnt(Node *v, const string& x, int pos) {
  int res = 0;
  while (pos < x.size()) {
    int c = x[pos] - 'a';
    if (v->to[c] == nullptr)
      break;
    v = v->to[c];
    res += v->cnt;
    ++pos;
  }
  return res;
}

Vec ZFun(string x) {
  Vec z(x.size(), 0);
  for (int i = 1, l = 0, r = 0; i < x.size(); ++i) {
    if (i <= r)
      z[i] = min(r - i + 1, z[i - l]);
    while (i + z[i] < x.size() && x[z[i]] == x[i + z[i]])
      ++z[i];
    if (i + z[i] - 1 > r)
      l = i, r = i + z[i] - 1;
  }
  return z;
}

ll Solution() {
  Vec cnt[2];
  M = sqrt(t.size());
  fora (k, 0, 2) {
    cnt[k].assign(t.size() + 1, 0);
    szn = 0;
    Node* root = GetNode();
    fora (i, 0, N) {
      if (s[i].size() >= M) {
        auto z = ZFun(s[i] + t);
        fora (j, 0, t.size())
          cnt[k][j] += z[s[i].size() + j] >= s[i].size();
      } else {
        AddWord(root, s[i]);
      }
    }
    fora (i, 0, t.size())
      cnt[k][i] += CalcCnt(root, t, i);
    reverse(t.begin(), t.end());
    fora (i, 0, N)
      reverse(s[i].begin(), s[i].end());
  }
  ll ans = 0;
  fora (i, 0, t.size() + 1)
    ans += 1LL * cnt[0][i] * cnt[1][t.size() - i];
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> t) {
    cin >> N;
    fora (i, 0, N) cin >> s[i];
    cout << Solution() << '\n';
  }
  return 0;
}
