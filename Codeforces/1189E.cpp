//
// Created by sinn on 7/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];

ll Calc(ll x) {
  ll ret = 1;
  fora (i, 0, 4) {
    ret = ret * x % M;
  }
  (ret -= K * x) %= M;
  return (ret + M) % M;
}

ll Solution() {
  map<ll, int> cnts;
  fora (i, 0, N) {
    cnts[Calc(A[i])]++;
  }
  ll ans = 0;
  for (auto &p : cnts) {
    int t = p.second;
    ans = ans + (t * (t - 1)) / 2;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) {
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
