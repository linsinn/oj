//
// Created by sinn on 6/22/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;

struct Node {
  int x, y;
  int dx, dy;
  Node() = default;
  Node(int a, int b, int c, int d): x{a}, y{b}, dx{c}, dy{d} {}
  void Move() {
    if (y + dy > M ) {
      x += dx;
      y = M;
      dy = -1;
    } else if (y + dy < 1) {
      x += dx;
      y = 1;
      dy = 1;
    } else {
      y += dy;
    }
  }
};

void Solution() {
  Node nodes[2] = {Node(1, 1, 1, 1), Node(N, M, -1, -1)};
  int curr = 0;
  fora (i, 0, N * M ) {
    cout << nodes[curr].x << ' ' << nodes[curr].y << '\n';
    nodes[curr].Move();
    curr ^= 1;
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    Solution();
  }
  return 0;
}