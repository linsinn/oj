//
// Created by sinn on 7/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll l[MAXN], r[MAXN];
ll p[MAXN], q[MAXN], e[MAXN];

ll Inv(ll x) {
  ll ret = 1;
  ll exp = MOD - 2;
  x %= MOD;
  while (exp != 0) {
    if (exp & 1) {
      ret = ret * x % MOD;
    }
    x = x * x % MOD;
    exp >>= 1;
  }
  return ret;
}

void Add(ll &a, ll b) {
  (a += b) %= MOD;
}

ll Solution() {
  fora (i, 0, N) ++r[i];
  p[0] = 1, q[0] = 0;
  ll sp = 1;
  fora (i, 1, N) {
    ll g = min(r[i], r[i-1]) - max(l[i], l[i-1]);
    if (g <= 0) q[i] = 0;
    else {
      q[i] = g % MOD * Inv((r[i]-l[i]) * (r[i-1]-l[i-1])) % MOD;
    }
    p[i] = (1 - q[i] + MOD) % MOD;
    (sp += p[i]) %= MOD;
  }
  fora (i, 0, N) {
    e[i] = (1 - q[i] + MOD) % MOD;
    if (i < N-1)
      e[i] = (e[i] - q[i+1] + MOD) % MOD;
    if (i > 0 && i < N-1) {
      ll g = min({r[i-1], r[i], r[i+1]}) - max({l[i-1], l[i], l[i+1]});
      if (g > 0) {
        e[i] += g % MOD * Inv((r[i-1]-l[i-1]) * (r[i]-l[i]) % MOD * (r[i+1]-l[i+1])) % MOD;
        e[i] %= MOD;
      }
    }
  }
  ll ans = 0;
  fora (i, 0, N) {
    Add(ans, p[i]);
    ll g = p[i];
    if (i > 0) Add(g, p[i-1]);
    if (i < N-1) Add(g, p[i+1]);
    Add(ans, ((sp - g + MOD) % MOD * p[i]) % MOD);
    if (i > 0)
      Add(ans, e[i-1]);
    if (i < N-1)
      Add(ans, e[i]);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> l[i];
    fora (i, 0, N) cin >> r[i];
    cout << Solution() << '\n';
  }
  return 0;
}
