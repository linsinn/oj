#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
  fora (i, 0, N) {
    if (A[i] < 0) A[i] = -1;
    else A[i] = 1;
  }
  ll ans[2] = {0, 0};
  ll dp[2][2];
  memset(dp, 0, sizeof(dp));
  int cur = 0;
  fora (i, 0, N) {
    memset(dp[cur^1], 0, sizeof(dp[cur^1]));
    if (A[i] < 0) {
      dp[cur^1][0] += 1;
      dp[cur^1][0] += dp[cur][1];
      dp[cur^1][1] += dp[cur][0];
    } else {
      dp[cur^1][1] += 1;
      dp[cur^1][0] += dp[cur][0];
      dp[cur^1][1] += dp[cur][1];
    }
    cur ^= 1;
    fora (i, 0, 2) ans[i] += dp[cur][i];
  }
  cout << ans[0] << ' ' << ans[1] << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
