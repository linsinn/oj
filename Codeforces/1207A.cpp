#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
int b, p, f, h, c;

ll Solution() {
  ll ans = 0;
  if (h > c) {
    int t = min(b / 2, p);
    ans += t * h;
    b -= t * 2;
    ans += min(b/2, f) * c;
  } else {
    int t = min(b / 2, f);
    ans += t * c;
    b -= t * 2;
    ans += min(b/2, p) * h;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> b >> p >> f;
      cin >> h >> c;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
