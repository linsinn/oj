//
// Created by sinn on 5/30/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
bool is[100];

void Solution() {
  is[1] = is[7] = is[9] = 1;
  if (((N > 9 && N < 30) || is[N/10] || is[N%10]) && N != 12)
    cout << "NO\n";
  else
    cout << "YES\n";
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}
