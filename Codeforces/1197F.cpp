#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e3 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;
using Vec = vector<int>;
using Mat = vector<Vec>;

ll N, M, K, T;
int f[3][3];
int len[MAXN];
int dp[MAXN][4];
vector<Pii> colored[MAXN];
Mat color_matrices[3];
Mat full_matrix;
Mat full_pows[31];

int Add(int x, int y) {
  return (x + y) % MOD;
}

int Mul(int x, int y) {
  return 1LL * x * y % MOD;
}

Vec Mul(const Mat& a, const Vec& b) {
  int n = a.size();
  int m = b.size();
  Vec c(m);
  fora (i, 0, n)
    fora (j, 0, m)
      c[i] = Add(c[i], Mul(b[j], a[i][j]));
  return c;
}

Mat Add(const Mat& a, const Mat& b) {
  int n = a.size();
  int m = a[0].size();
  Mat c(n, Vec(m, 0));
  fora (i, 0, n)
    fora (j, 0, m)
      c[i][j] = Add(a[i][j], b[i][j]);
  return c;
}

Mat Mul(const Mat& a, const Mat& b) {
  int x = a.size();
  int y = b.size();
  int z = b[0].size();
  Mat c(x, Vec(z, 0));
  fora (i, 0, x)
    fora (j, 0, y)
      fora (k, 0, z)
        c[i][k] = Add(c[i][k], Mul(a[i][j], b[j][k]));
  return c;
}

Mat Binpow(Mat a, int exp) {
  int n = a.size();
  Mat c = Mat(n, Vec(n, 0));
  fora (i, 0, n) c[i][i] = 1;
  while (exp) {
    if (exp & 1) c = Mul(c, a);
    a = Mul(a, a);
    exp >>= 1;
  }
  return c;
}

int Extend(int color, const Vec& last_numbers) {
  Vec used(4, 0);
  fora (i, 0, 3) 
    if (f[color][i])
      used[last_numbers[i]] = 1;
  fora (i, 0, 4)
    if (used[i] == 0)
      return i;
  return 3;
}

Vec ExtendState(int color, Vec last_numbers) {
  int z = Extend(color, last_numbers);
  last_numbers.emplace(last_numbers.begin(), z);
  last_numbers.pop_back();
  return last_numbers;
}

Vec Int2State(int x) {
  Vec res;
  fora (i, 0, 3) {
    res.emplace_back(x % 4);
    x /= 4;
  }
  return res;
}

int State2Int(const Vec& x) {
  int res = 0;
  int deg = 1;
  for (auto y : x) {
    res += deg * y;
    deg *= 4;
  }
  return res;
}

Mat FormMatrix(int color) {
  Mat res(64, Vec(64, 0));
  fora (i, 0, 64) {
    int j = State2Int(ExtendState(color, Int2State(i)));
    res[j][i] = Add(res[j][i], 1);
  }
  return res;
}

void PrecalcPows() {
  full_pows[0] = full_matrix;
  fora (i, 0, 31)
    full_pows[i+1] = Mul(full_pows[i], full_pows[i]);
}

Vec PowMul(int exp, Vec b) {
  fora (i, 0, 31) {
    if (exp % 2 == 1)
      b = Mul(full_pows[i], b);
    exp >>= 1;
  }
  return b;
}

ll Solution() {
  fora (i, 0, N) 
    sort(colored[i].begin(), colored[i].end());
  fora (i, 0, 3)
    color_matrices[i] = FormMatrix(i);
  full_matrix = color_matrices[0];
  fora (i, 1, 3) {
    full_matrix = Add(full_matrix, color_matrices[i]);
  }
  PrecalcPows();
  dp[0][0] = 1;
  fora (i, 0, N) {
    Vec cur(64);
    cur[State2Int({3, 3, 3})] = 1;
    int lst = 0;
    for (auto x : colored[i]) {
      cur = PowMul(x.first - lst, cur);
      cur = Mul(color_matrices[x.second], cur);
      lst = x.first + 1;
    }
    cur = PowMul(len[i] - lst, cur);
    fora (j, 0, 4) {
      fora (k, 0, 64) {
        Vec s = Int2State(k);
        dp[i + 1][j ^ s[0]] = Add(dp[i + 1][j ^ s[0]], Mul(dp[i][j], cur[k]));
      }
    }
  }
  return dp[N][0];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> len[i];
    }
    cin >> M;
    fora (i, 0, M) {
      int x, y, c;
      cin >> x >> y >> c;
      colored[x-1].emplace_back(y-1, c-1);
    }
    fora (i, 0, 3) 
      fora (j, 0, 3)
        cin >> f[i][j];
    cout << Solution() << '\n';
  }
  return 0;
}
