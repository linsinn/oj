//
// Created by sinn on 4/13/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string str;
int h[256];

bool Solution() {
    memset(h, 0, sizeof(h));
    for (char ch : str) {
        h[ch] += 1;
    }
    bool start = false, end = false;
    for (int i : h) {
        if (i > 1)
            return false;
        else if (i == 1) {
            start = true;
            if (end) {
                return false;
            }
        } else {
            if (start)
                end = true;
        }
    }
    return true;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++) {
            cin >> str;
            if (Solution())
                cout << "Yes\n";
            else
                cout << "No\n";
        }
    }
    return 0;
}