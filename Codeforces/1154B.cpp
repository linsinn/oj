//
// Created by sinn on 4/25/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int a[MAXN];

int Solution() {
    set<int> ss;
    for (int i = 0; i < N; i++) {
        ss.emplace(a[i]);
    }
    if (ss.size() == 1)
        return 0;
    else if (ss.size() == 2) {
        int a = *ss.begin(), b = *ss.rbegin();
        if ((b-a) % 2 == 0)
            return (b - a) >> 1;
        else
            return b - a;
    } else if (ss.size() == 3) {
        int a = *ss.begin(), b = *++ss.begin();
        int c = *ss.rbegin();
        if (b - a == c - b)
            return b - a;
        else
            return -1;
    } else
        return -1;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++)
            cin >> a[i];
        cout << Solution() << '\n';
    }
    return 0;
}
