//
// Created by sinn on 12/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
	ll a = 0, b = 0;
	fora (i, 0, N) {
		a += A[i];
		b ^= A[i];
	}
	b <<= 1;
	ll ans = 0;
	fora (i, 0, 63) {
		int p = (a >> i) & 1;
		int q = (b >> i) & 1;
		if (p != q) {
			ans += (1LL << i);
			a += (1LL << i);
			b ^= (1LL << (i + 1));
		}
	}
	if (ans == 0) cout << "0\n\n";
	else {
		cout << "1\n";
		cout << ans << '\n';
	}
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
  	while (T--) {
  		cin >> N;
  		A.resize(N);
  		fora (i, 0, N) cin >> A[i];
  		Solution();
  	}
  }
  return 0;
}