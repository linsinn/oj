#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<ll> A;

void Solution() {
  ll base = 0;
  map<ll, vector<ll>> pos;
  fora (i, 0, M) 
    pos[A[i]].emplace_back(i);
  vector<Pii> vec;
  fora (i, 1, M) {
    base += abs(A[i] - A[i-1]);
    if (A[i] > A[i-1])
      vec.emplace_back(A[i-1], A[i]);
    else if (A[i] < A[i-1])
      vec.emplace_back(A[i], A[i-1]);
  }
  multiset<Pll> aux;
  sort(vec.begin(), vec.end());
  int p = 0;
  fora (i, 1, N+1) {
    while (p < vec.size() && vec[p].first < i) {
      aux.emplace(vec[p].second, vec[p].first);
      ++p;
    }
    while (!aux.empty() && aux.begin()->first <= i)
      aux.erase(aux.begin());
    ll ans = base - aux.size();
    if (pos.count(i)) {
      for (int v : pos[i]) {
        if (v != 0 && A[v-1] != A[v]) {
          ans -=  abs(A[v-1] - A[v]);
          ans += A[v-1] - (A[v-1] > A[v]);
        }
        if (v != M-1 && A[v+1] != A[v]) {
          ans -=  abs(A[v+1] - A[v]);
          ans += A[v+1] - (A[v+1] > A[v]);
        }
      }
    }
    cout << ans << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    A.resize(M);
    fora (i, 0, M) cin >> A[i];
    Solution();
  }
  return 0;
}
