//
// Created by sinn on 7/2/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;

void Solution() {
  vector<pair<int, int>> ans;
  ans.emplace_back(1, 1);
  int dx = 0, dy = 1;
  fora (i, 0, N-1) {
    auto p = ans.back();
    ans.emplace_back(p.first+dx, p.second+dy);
    swap(dx, dy);
  }
  cout << max(ans.back().first, ans.back().second) << '\n';
  for (auto &p : ans) {
    cout << p.first << ' ' << p.second << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}