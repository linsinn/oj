#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> A, B;

bool Check(int x, int y) {
  if (x < 0 || y < 0 || x == N-1 || y == M-1)
    return false;
  fora (i, 0, 2) {
    fora (j, 0, 2) {
      if (A[x+i][y+j] != 1)
        return false;
    }
  }
  return true;
}

void Solution() {
  B.assign(N, Vec(M, 0));
  vector<Pii> ans;
  fora (i, 0, N) {
    fora (j, 0, M) {
      if (A[i][j] == 0 && B[i][j] == 1) {
        cout << "-1\n";
        return ;
      }
      if (A[i][j] == 1 && B[i][j] == 0) {
        int a, b;
        if (Check(i-1, j-1)) {
          a = i - 1, b = j-1;
        } else if (Check(i-1, j)) {
          a = i - 1, b = j;
        } else if (Check(i, j-1)) {
          a = i, b = j-1;
        } else if (Check(i, j)) {
          a = i, b = j;
        } else {
          cout << "-1\n";
          return ;
        }
        ans.emplace_back(a, b);
        B[a][b] = B[a][b+1] = B[a+1][b] = B[a+1][b+1] = 1;
      }
    }
  }

  cout << ans.size() << '\n';
  for (auto &p : ans) {
    cout << p.first + 1 << ' ' << p.second + 1 << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    A.assign(N, Vec(M, 0));
    fora (i, 0, N) {
      fora (j, 0, M) {
        cin >> A[i][j];
      }
    }
    Solution();
  }
  return 0;
}
