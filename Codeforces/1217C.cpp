#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
string s;

ll Solution() {
  N = s.size();
  vector<int> pre_one(N);
  pre_one[0] = -1;
  fora (i, 1, N) {
    pre_one[i] = pre_one[i-1];
    if (s[i-1] == '1') pre_one[i] = i-1;
  }
  ll ans = 0;
  fora (i, 0, N) {
    int j = i;
    int g = s[i] - '0';
    do {
      int t = pre_one[j];
      if (i - t >= g && g != 0) {
        ans += 1;
      } 
      j = t;
      g += (1 << (i - t));
    } while (j != -1 && (i - j <= 30));
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> s;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
