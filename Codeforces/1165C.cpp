//
// Created by sinn on 5/19/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s;

void Solution() {
  string ans;
  int i = 0, j = 1;
  while (i < s.size()) {
    while (j < s.size() && s[j] == s[i])
      ++j;
    if (j < s.size()) {
      ans += s[i];
      ans += s[j];
    }
    i = j + 1;
    j = i + 1;
  }
  cout << s.size() - ans.size() << '\n';
  cout << ans << '\n';
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    Solution();
  }
  return 0;
}
