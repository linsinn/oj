//
// Created by sinn on 7/17/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int MAXN = 1e2 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string s;
int digs[MAXN];
int numPos[10], denPos[10];
int num, den;

struct HalfState {
  int carry;
  int mask;
  bool comp;
  HalfState() = default;
  HalfState(int c, int m, bool comp): carry(c), mask(m), comp(comp) {};
  bool operator < (const HalfState& rhs) const {
    if (carry != rhs.carry) return carry < rhs.carry;
    if (mask != rhs.mask) return mask < rhs.mask;
    return comp < rhs.comp;
  }
  bool operator == (const HalfState& rhs) const {
    return carry == rhs.carry && mask == rhs.mask && comp == rhs.comp;
  }
  bool operator != (const HalfState& rhs) const {
    return !(*this == rhs);
  }
};

HalfState Go(const HalfState& hs, int pos, int digit, bool isNum) {
  int newDigit = digit * (isNum ? num : den) + hs.carry;
  int newCarry = newDigit / 10;
  newDigit %= 10;
  int newMask = hs.mask;
  int maskPos = (isNum ? numPos[newDigit] : denPos[newDigit]);
  if (maskPos != -1)
    newMask |= (1 << maskPos);
  bool newComp = (newDigit < digs[pos]) || (newDigit == digs[pos] && hs.comp);
  return {newCarry, newMask, newComp};
}

struct State {
  HalfState numState, denState;
  State() = default;
  State(HalfState n, HalfState d) : numState{n}, denState{d} {}
  void Norm() {
    if (numState.mask & denState.mask)
      numState.mask = denState.mask = 1;
  }
  bool Valid() {
    return bool(numState.mask & denState.mask) && numState.comp && denState.comp && numState.carry == 0 && denState.carry == 0;
  }
  bool operator < (const State& rhs) const {
    if (numState != rhs.numState)
      return numState < rhs.numState;
    return denState < rhs.denState;
  }
};

State Go(State& st, int pos, int digit) {
  HalfState newNum = Go(st.numState, pos, digit, true);
  HalfState newDen = Go(st.denState, pos, digit, false);
  State ret(newNum, newDen);
  ret.Norm();
  return ret;
}

inline int Add(int a, int b) {
  return (0LL + a + b) % MOD;
}

ll CalcFixed(int x, int y) {
  num = x, den = y;
  fora (i, 0, 10) {
    numPos[i] = denPos[i] = -1;
  }
  int cnt = 0;
  fora (i, 1, 10) {
    fora (j, 1, 10) {
      if (i * y == j * x) {
        numPos[i] = denPos[j] = cnt++;
      }
    }
  }
  vector<map<State, int>> memos(102);
  memos[0][State(HalfState(0, 0, true), HalfState(0, 0, true))] = 1;
  fora (i, 0, 101) {
    for (auto &mp : memos[i]) {
      State curState = mp.first;
      int curCount = mp.second;
      fora (j, 0, 10) {
        State newState = Go(curState, i, j);
        memos[i+1][newState] = Add(memos[i+1][newState], curCount);
      }
    }
  }
  ll ret = 0;
  for (auto &mp : memos[101]) {
    State curState = mp.first;
    int curCount = mp.second;
    if (curState.Valid()) {
      ret = Add(ret, curCount);
    }
  }
  return ret;
}

ll Solution() {
  N = s.size();
  reverse(s.begin(), s.end());
  fora (i, 0, N) {
    digs[i] = s[i] - '0';
  }
  ll ans = 0;
  fora (i, 1, 10) {
    fora (j, 1, 10) {
      if (__gcd(i, j) == 1) {
        ans = Add(ans, CalcFixed(i, j));
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    cout << Solution() << '\n';
  }
  return 0;
}
