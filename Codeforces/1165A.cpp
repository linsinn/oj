//
// Created by sinn on 5/19/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K, x, y;
string s;

int Solution() {
  int ans = 0;
  for (int i = 0; i < y; i++) {
    if (s[N-1-i] != '0')
      ++ans;
  }
  if (s[N-1-y] != '1')
    ++ans;
  for (int i = y+1; i < x; i++) {
    if (s[N-1-i] != '0')
      ++ans;
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> x >> y) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}