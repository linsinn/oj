//
// Created by sinn on 5/23/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = unsigned long long;

int N, M, K;
ll a, b, m;

void Solution() {
  if (a == b) {
    cout << 1 << ' ' << a << '\n';
    return ;
  }
  int lst_idx = -1;
  unsigned long long base = 1;
  for (int i = 0; i <= 50; ++i) {
    ll lb = (base << i) * a + (base << i);
    ll rb = (base << i) * a + (base << i) * m;
    if (lb <= b && b <= rb) {
      lst_idx = i;
      break;
    }
  }
  if (lst_idx == -1) {
    cout << "-1\n";
    return ;
  }
  ll left = b - (base << lst_idx) * a;
  vector<ll> r, mul;
  while (lst_idx > 0) {
    mul.emplace_back(base << (lst_idx - 1));
    r.emplace_back(1);
    left -= mul.back() * r.back();
    --lst_idx;
  }
  mul.emplace_back(1);
  r.emplace_back(1);
  --left;

  int i = 0;
  while (left != 0) {
    ll g = min(left / mul[i], m - r[i]);
    r[i] += g;
    left -= g * mul[i];
    i++;
  }

  cout << r.size() + 1 << ' ';
  cout << a << ' ';
  ll pre = a;
  for (i = 0; i < r.size(); i++) {
    cout << pre + r[i] << ' ';
    pre += pre + r[i];
  }
  cout << '\n';
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> K) {
    while (K--) {
      cin >> a >> b >> m;
      Solution();
    }
  }
  return 0;
}
