//
// Created by sinn on 7/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e3 + 10;

using ll = long long;

ll N, M, K, T;
int r[MAXN], c[MAXN];
int p[MAXN], q[MAXN];

void Solution() {
  fora (i, 1, N+1) {
    p[r[i]] = i;
    q[c[i]] = i;
  }
  vector<vector<int>> ans;
  fora (i, 1, N+1) {
    if (p[i] == i && q[i] == i)
      continue;
    vector<int> t = {i, q[i], p[i], i};
    ans.emplace_back(t);
    r[p[i]] = r[i];
    p[r[i]] = p[i];
    c[q[i]] = c[i];
    q[c[i]] = q[i];
  }
  cout << ans.size() << '\n';
  for (auto &vec : ans) {
    for (int elem : vec) {
      cout << elem << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 1, N+1) cin >> r[i];
    fora (i, 1, N+1) cin >> c[i];
    Solution();
  }
  return 0;
}
