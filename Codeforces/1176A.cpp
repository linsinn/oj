//
// Created by sinn on 6/15/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K;

int Solution() {
  int ans = 0;
  while (N != 1) {
    if (N % 2 == 0) {
      N /= 2;
      ++ans;
    }
    else if (N % 3 == 0) {
      N = N / 3 * 2;
      ++ans;
    }
    else if (N % 5 == 0) {
      N = N / 5 * 4;
      ++ans;
    } else {
      return -1;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N;
    cout << Solution() << '\n';
  }
  return 0;
}