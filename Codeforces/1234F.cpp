#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

ll Solution() {
  M = 20;
  vector<int> dp(1 << M, 0);
  fora (i, 0, s.size()) {
    int c = 0;
    int k = 0;
    fora (j, 0, M) {
      if (i + j >= s.size()) break;
      int b = 1 << (s[i+j] - 'a');
      if (k & b) {
        break;
      } else {
        ++c;
        k |= b;
        dp[k] = c;
      }
    }
  }
  fora (i, 0, 1 << M) {
    if (dp[i] != 0) {
      fora (j, 0, M) {
        if (((1 << j) & i) == 0) {
          Max(dp[(1 << j) | i], dp[i]);
        }
      }
    }
  }
  int ans = 0;
  int f = (1 << M) - 1;
  fora (i, 0, 1 << M) {
    Max(ans, dp[i] + dp[f ^ i]);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    cout << Solution() << '\n';
  }
  return 0;
}
