//
// Created by sinn on 10/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string str;

void Solution() {
  string odd, even;
  for (char ch : str) {
    if ((ch - '0') % 2 == 1)
      odd.push_back(ch);
    else
      even.push_back(ch);
  }
  int i = 0, j = 0;
  string ans;
  while (i < odd.size() || j < even.size()) {
    char a = i < odd.size() ? odd[i] : '9' + 1;
    char b = j < even.size() ? even[j] : '9' + 1;
    if (a < b) {
      ans.push_back(a);
      ++i;
    } else {
      ans.push_back(b);
      ++j;
    }
  }
  cout << ans << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> str;
      Solution();
    }
  }
  return 0;
}