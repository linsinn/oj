//
// Created by sinn on 5/30/19.
//


#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K, Q;
int A[MAXN], B[MAXN], l[MAXN], r[MAXN];
vector<int> st[MAXN], ed[MAXN];

void Solution() {
  for (int i = 1; i <= Q; ++i)
    l[i] = N+1, r[i] = 0;
  for (int i = 1; i <= N; ++i) {
    if (A[i]) {
      l[A[i]] = min(l[A[i]], i);
      r[A[i]] = max(r[A[i]], i);
    }
  }
  if (!r[Q]) {
    int k;
    for (k = 1; k <= N; ++k) {
      if (!A[k])
        break;
    }
    if (k > N) {
      cout << "NO\n";
      return ;
    }
    l[Q] = r[Q] = k;
  }
  for (int i = 1; i < Q; ++i) {
    if (!r[i]) {
      l[i] = l[i+1];
      r[i] = r[i+1];
    }
  }
  l[1] = 1, r[1] = N;
  for (int i = 1; i <= Q; ++i) {
    st[l[i]].emplace_back(i);
    ed[r[i]].emplace_back(i);
  }
  set<int> S;
  for (int i = 1; i <= N; ++i) {
    for (auto u : st[i])
      S.emplace(u);
    B[i] = *S.rbegin();
    if (A[i] && A[i] != B[i]) {
      cout << "NO\n";
      return ;
    }
    for (auto u : ed[i])
      S.erase(u);
  }
  cout << "YES\n";
  for (int i = 1; i <= N; ++i)
    cout << B[i] << ' ';
  cout << '\n';
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> Q) {
    for (int i = 1; i <= N; ++i) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}