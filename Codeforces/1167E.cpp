//
// Created by sinn on 5/16/19.
//


#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e6 + 100;

using ll = long long;

int N, M, K;
vector<int> pos[MAXN];
int prefMax[MAXN];
int A[MAXN];

ll Solution() {
  memset(prefMax, 0, sizeof(prefMax));
  for (int i = 0; i <= K; i++) pos[i].clear();

  for (int i = 0; i < N; ++i) {
    pos[A[i]].emplace_back(i);
    prefMax[i] = max(A[i], (i > 0 ? prefMax[i-1] : A[i]));
  }

  int p = 1;
  int lst = N + 5;

  for (int i = K; i >= 1; --i) {
    if (pos[i].empty()) {
      p = i;
      continue;
    }
    if (pos[i].back() > lst)
      break;
    p = i;
    lst = pos[i][0];
  }

  ll ans = 0;
  lst = -1;
  for (int l = 1; l <= K; ++l) {
    int r = max(l, p-1);
    if (lst != -1)
      r = max(r, prefMax[lst]);
    ans += K - r + 1;
    if (!pos[l].empty()) {
      if (pos[l][0] < lst)
        break;
      lst = pos[l].back();
    }
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    for (int i = 0; i < N; ++i) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}