#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
struct Rec {
  int x1, y1;
  int x2, y2;
  Rec() = default;
  Rec(int a, int b, int c, int d) {
    x1 = a, y1 = b, x2 = c, y2 = d;
  }
} A[3];

bool Cover(Rec a, Rec b) {
  if (a.x1 == a.x2) return true;
  if (a.y1 == a.y2) return true;
  return b.x1 <= a.x1 && a.x2 <= b.x2 && b.y1 <= a.y1 && a.y2 <= b.y2;
}

ll Solution() {
  vector<Rec> v;
  if (A[0].x2 <= A[1].x1) {
    v.emplace_back(A[0]);
  } else if (A[0].x1 < A[1].x1) {
    v.emplace_back(A[0].x1, A[0].y1, A[1].x1, A[0].y2);
    if (A[1].x2 < A[0].x2) {
      v.emplace_back(A[1].x2, A[0].y1, A[0].x2, A[0].y2);
    }
    if (A[0].y1 < A[1].y1) {
      v.emplace_back(A[1].x1, A[0].y1, min(A[0].x2, A[1].x2), min(A[0].y2, A[1].y1));
    }
    if (A[0].y2 > A[1].y2) {
      v.emplace_back(A[1].x1, max(A[0].y1, A[1].y2), min(A[0].x2, A[1].x2), A[0].y2);
    }
  } else {
    if (A[1].x2 <= A[0].x1) 
      v.emplace_back(A[0]);
    else {
      v.emplace_back(min(A[0].x2, A[1].x2), A[0].y1, A[0].x2, A[0].y2);
      if (A[0].y1 < A[1].y1) {
        v.emplace_back(A[0].x1, A[0].y1, min(A[0].x2, A[1].x2), min(A[0].y2, A[1].y1));
      }
      if (A[0].y2 > A[1].y2) {
        v.emplace_back(A[0].x1, max(A[0].y1, A[1].y2), min(A[0].x2, A[1].x2), A[0].y2);
      }
    }
  }
  for (auto &r : v) {
    if (!Cover(r, A[2]))
      return true;
  }
  return false;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> A[0].x1 >> A[0].y1 >> A[0].x2 >> A[0].y2) {
    fora (i, 1, 3) {
      cin >> A[i].x1 >> A[i].y1 >> A[i].x2 >> A[i].y2;
    }
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}
