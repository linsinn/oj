//
// Created by sinn on 5/8/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K, H;
int restrictions[100];

int Solution() {
  int ans = 0;
  for (int i = 1; i <= N; i++) {
    if (restrictions[i] != -1)
      ans += restrictions[i] * restrictions[i];
    else
      ans += H * H;
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> H >> M) {
    memset(restrictions, -1, sizeof(restrictions));
    int l, r, h;
    for (int i = 0; i < M; i++) {
      cin >> l >> r >> h;
      for (int j = l; j <= r; j++)
        restrictions[j] = restrictions[j] == -1 ? h : min(restrictions[j], h);
    }
    cout << Solution() << '\n';
  }
  return 0;
}