#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];

bool Solution() {
  int p = std::max_element(A, A+N) - A;
  int i = p - 1, j = p + 1;
  int cur = A[p];
  while (i >= 0 || j < N) {
    if (i >= 0 && j < N) {
      if (A[i] >= cur || A[j] >= cur)
        return false;
      if (A[i] > A[j])
        cur = A[i--];
      else if (A[i] < A[j])
        cur = A[j++];
      else
        return false;
    } else if (i >= 0) {
      if (A[i] < cur)
        cur = A[i--];
      else
        return false;
    } else if (j < N) {
      if (A[j] < cur)
        cur = A[j++];
      else 
        return false;
    }
  }
  return true;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N)
      cin >> A[i];
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}
