//
// Created by sinn on 6/21/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
int t[MAXN], g[MAXN];
ll memos[1 << 16][4];

ll Solution() {
  memset(memos, 0, sizeof(memos));
  memos[0][0] = 1;
  fora (i, 0, 1 << N) {
    fora (b, 0, N) {
      if ((1 << b) & i) continue;
      int k = i + (1 << b);
      fora (j, 0, 4) {
        if (g[b] != j)
          (memos[k][g[b]] += memos[i][j]) %= MOD;
      }
    }
  }
  ll ans = 0;
  fora (i, 0, 1 << N) {
    int su = 0;
    fora (k, 0, 16) {
      if ((1 << k) & i) {
        su += t[k];
      }
    }
    if (su == T) {
      fora (j, 1, 4) {
        (ans += memos[i][j]) %= MOD;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> T) {
    fora (i, 0, N) {
      cin >> t[i] >> g[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
