//
// Created by sinn on 4/29/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
ll foods[3], tmp[3];
ll f[3] = {3, 2, 2};
ll w[] = {0, 1, 2, 0, 2, 1, 0};

ll Solution() {
    ll d = foods[0];
    for (int i = 0; i < 3; i++) {
        d = min(d, foods[i] / f[i]);
    }
    for (int i = 0; i < 3; i++) {
        foods[i] -= f[i] * d;
    }
    d *= 7;
    ll g = 0;
    for (int i = 0; i < 7; i++) {
        memcpy(tmp, foods, sizeof(tmp));
        ll k = 0;
        while (tmp[w[(k+i) % 7]] > 0) {
            tmp[w[(k+i) % 7]]--;
            k++;
        }
        g = max(g, k);
    }
    return d + g;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> foods[0] >> foods[1] >> foods[2]) {
        cout << Solution() << '\n';
    }
    return 0;
}