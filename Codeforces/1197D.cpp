#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];

ll S(vector<ll>& v, int l, int r) {
  l = max(l, 0);
  return v[r] - (l == 0 ? 0 : v[l-1]);
}


ll Solution() {
  std::vector<ll> psum(N, 0), bst(N, 0);
  fora (i, 0, N) {
    psum[i] = A[i] + (i == 0 ? 0 : psum[i-1]);
  }
  ll ans = 0;
  fora (len, 1, min(M, N)+1) {
    ans = max(ans, S(psum, 0, len-1) - K);
  }
  fora (i, 0, N) {
    if (i + 1 >= M) {
      ll nbst = S(psum, i-M+1, i) - K;
      if (i - M >= 0) {
        nbst += bst[i-M];
      }
      bst[i] = max(0LL, nbst);
    }
    fora (len, 0, min(M, N-i)) {
      ans = max(ans, bst[i] + S(psum, i+1, i+len) - K * (len > 0));
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) {
    fora (i, 0, N)
      cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
