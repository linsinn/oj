//
// Created by sinn on 7/13/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
string s;
int A[MAXN];
int p[MAXN][2], n[MAXN][2];
int f[2], l[2];

bool Check(int idx, int chg) {
  int a[2], b[2];
  fora (i, 0, 2) {
    a[i] = f[i];
    b[i] = l[i];
  }
  if (chg == 0) {
    if (idx <= a[0]) {
      a[0] = idx + K + n[idx + K -1][0];
    }
    a[1] = min(a[1], idx);
    if (idx + K >= N - b[0]) {
      b[0] = N - (idx - p[idx][0]);
    }
    b[1] = min(b[1], int(N - (idx + K)));
  } else {
    if (idx <= a[1]) {
      a[1] = idx + K + n[idx + K -1][1];
    }
    a[0] = min(a[0], idx);
    if (idx + K >= N - b[1]) {
      b[1] = N - (idx - p[idx][1]);
    }
    b[0] = min(b[0], int(N - (idx + K)));
  }
  return a[0] + b[0] + K >= N ||  a[1] + b[1] + K >= N;
}

void Solution() {
  fora (i, 1, N) {
    p[i][0] = p[i-1][0];
    p[i][1] = p[i-1][1];
    if (A[i-1] == 0) {
      ++p[i][0];
      p[i][1] = 0;
    }
    if (A[i-1] == 1) {
      ++p[i][1];
      p[i][0] = 0;
    }
  }
  ford (i, N-2, 0) {
    n[i][0] = n[i+1][0];
    n[i][1] = n[i+1][1];
    if (A[i+1] == 0) {
      ++n[i][0];
      n[i][1] = 0;
    }
    if (A[i+1] == 1) {
      ++n[i][1];
      n[i][0] = 0;
    }
  }
  if (A[0] == 0) {
    f[0] = n[0][0] + 1;
  } else {
    f[1] = n[0][1] + 1;
  }
  if (A[N-1] == 0) {
    l[0] = p[N-1][0] + 1;
  } else {
    l[1] = p[N-1][1] + 1;
  }
  if (f[0] + l[0] + K >= N ||  f[1] + l[1] + K >= N) {
    cout << "tokitsukaze\n";
    return ;
  }
  bool w = true;
  fora (i, 0, N-K+1) {
    if (!Check(i, 0) || !Check(i, 1)) {
      w = false;
      break;
    }
  }
  if (w) {
    cout << "quailty\n";
  } else {
    cout << "once again\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cin >> s;
    fora (i, 0, N) {
      A[i] = s[i] - '0';
    }
    Solution();
  }
  return 0;
}
