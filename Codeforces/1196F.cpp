#include <bits/stdc++.h>
using namespace std;
 
#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)
 
constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 10;
 
using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
 
using ll = long long;
 
ll N, M, K, T;
vector<Pii> gph[MAXN];
 
struct P {
  int u, v;
  ll w;
  void Init(int _u, int _v, ll _w) {
    u = min(_u, _v);
    v = max(_u, _v);
    w = _w;
  }
  bool operator < (const P& rhs) const {
    return w > rhs.w;
  }
};
std::priority_queue<P> pq;
map<Pii, ll> mp;

bool Cmp(const Pii& a, const Pii& b) {
  return a.second < b.second;
}
 
ll Solution() {
  fora (i, 1, N+1) {
    sort(gph[i].begin(), gph[i].end(), Cmp);
  }
  int k = 1;
  while (k < K) {
    auto n = pq.top();
    pq.pop();
    auto iw = mp.find({n.u, n.v});
    if (iw->second < n.w) {
      continue;
    }
    ++k;
    mp[{n.u, n.v}] = -1;
    P p;
    for (int i = 0; i < min((int)K, (int)gph[n.u].size()); ++i) {
      auto &x = gph[n.u][i];
      if (x.first != n.v) {
        p.Init(n.v, x.first, n.w + x.second);
        auto it = mp.find({p.u, p.v});
        if (it == mp.end() || it->second > p.w) {
          pq.push(p);
          mp[{p.u, p.v}] = p.w;
        }
      }
    }
    for (int i = 0; i < min((int)K, (int)gph[n.v].size()); ++i) {
      auto &x = gph[n.v][i];
      if (x.first != n.u) {
        p.Init(n.u, x.first, n.w + x.second);
        auto it = mp.find({p.u, p.v});
        if (it == mp.end() || it->second > p.w) {
          pq.push(p);
          mp[{p.u, p.v}] = p.w;
        }
      }
    }
  }
  return pq.top().w;
}
 
int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) { 
    int u, v, w;
    P p;
    fora (i, 0, M) {
      cin >> u >> v >> w;
      gph[u].emplace_back(v, w);
      gph[v].emplace_back(u, w);
      p.Init(u, v, w);
      auto it = mp.find({p.u, p.v});
      if (it == mp.end() || it->second > p.w) {
        pq.push(p);
        mp[{p.u, p.v}] = p.w;
      }
    }
    cout << Solution() << '\n';
  }
  return 0;
}
