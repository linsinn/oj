#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD =  998244353;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> A;
Vec fac, rfac;

int BinPow(int base, int exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1) 
      ret = ret * base % MOD;
    base = (ll)base * base % MOD;
    exp >>= 1;
  }
  return ret;
}

void Init() {
  int n = N+1;
  fac.resize(n);
  rfac.resize(n);
  fac[0] = fac[1] = 1;
  fora (i, 2, n) 
    fac[i] = (ll)fac[i-1] * i % MOD;
  rfac[n-1] = BinPow(fac[n-1], MOD-2);
  ford (i, n-2, 0) {
    rfac[i] = (ll)rfac[i+1] * (i + 1) % MOD;
  }
}

ll Add(ll x, ll y) {
  x = (x + y) % MOD;
  x = (x + MOD) % MOD;
  return x;
}

ll Solution() {
  Init();
  ll ans = fac[N];
  map<int, int> fi, se;
  map<Pii, int> p;
  for (auto &a : A) {
    fi[a.first]++;
    se[a.second]++;
    p[a]++;
  }
  ll t = 1;
  for (auto &v : fi) {
    t = t * fac[v.second] % MOD;
  }
  ans = Add(ans, -t);
  t = 1;
  for (auto &v : se) {
    t = t * fac[v.second] % MOD;
  }
  ans = Add(ans, -t);
  sort(A.begin(), A.end());
  bool f = true;
  fora (i, 1, N) {
    if (A[i].second < A[i-1].second) {
      f = false;
      break;
    }
  }
  t = 1;
  for (auto &v : p) {
    t = t * fac[v.second] % MOD;
  }
  if (f)
    ans = Add(ans, t);
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i].first >> A[i].second;;
    cout << Solution() << '\n';
  }
  return 0;
}
