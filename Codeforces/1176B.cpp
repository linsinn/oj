//
// Created by sinn on 6/15/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
ll A[MAXN];

int Solution() {
  int mods[3];
  memset(mods, 0, sizeof(mods));
  fora (i, 0, N) {
    mods[A[i] % 3]++;
  }
  int ans = mods[0];
  int g = min(mods[1], mods[2]);
  ans += g;
  mods[1] -= g, mods[2] -= g;
  ans += mods[1] / 3;
  ans += mods[2] / 3;
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N;
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}