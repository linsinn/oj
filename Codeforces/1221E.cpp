#include <bits/stdc++.h>
using namespace std;
 
#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)
 
constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
 
using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;
 
template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}
 
ll N, M, K, T;
int a, b;
string s;
 
bool Solution() {
  s.push_back('X');
  int ba = 0, a2b = 0, l2b = 0;
  int p = 0, q = 0;
  int pre = 0;
  int len = 0;
  fora (i, 0, s.size()) {
    if (s[i] == 'X') {
      int k = i - pre;
      if (k >= b && k < a) ba++;
      if (a <= k && k < 2 * b) a2b++;
      if (k >= 2 * b) {
        l2b++;
        len = k;
      }
      pre = i + 1;
    }
  }
  
  if (l2b >= 2 || ba) return false;
  if (l2b == 1) {
    int g = len - a;
    fora (i, 0, g/2 + 1) {
      int j = g - i;
      if (i >= b && i < a) continue;
      if (j >= b && j < a) continue;
      if (i >= 2 * b || j >= 2 * b) continue;
      int k = 0;
      if (a <= i && i < 2 * b) ++k;
      if (a <= j && j < 2 * b) ++k;
      if ((a2b + k) % 2 == 0) return true; 
    }
    return false;
  } else {
    return a2b % 2 == 1;
  }
}
 
int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> a >> b;
      cin >> s;
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
