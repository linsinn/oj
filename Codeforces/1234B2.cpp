#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
  list<int> v;
  set<int> ss;
  fora (i, 0, N) {
    if (!ss.count(A[i])) {
      ss.emplace(A[i]);
      v.push_front(A[i]);
      if (v.size() > K) {
        ss.erase(v.back());
        v.pop_back();
      }
    }
  }
  cout << v.size() << '\n';
  for (int a : v) {
    cout << a << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
