//
// Created by sinn on 7/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int ans[MAXN];
bool is_prime[MAXN];

int SievePrimes() {
  memset(is_prime, 1, sizeof(is_prime));
  int ret = 1;
  is_prime[1] = is_prime[1] = false;
  ans[1] = 1;
  fora (i, 2, sqrt(MAXN) + 1) {
    if (is_prime[i]) {
      ans[i] = ret;
      int k = 0;
      while (i * i + i * k < MAXN) {
        ans[i*i + i*k] = ret;
        is_prime[i*i + i*k] = false;
        ++k;
      }
      ++ret;
    }
  }
  return ret;
}

void Solution() {
  int g = SievePrimes();
  fora (i, 2, N+1) {
    if (ans[i] == 0) {
      cout << g++ << ' ';
    } else {
      cout << ans[i] << ' ';
    }
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}
