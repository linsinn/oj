#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll BinPow(ll base, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1) 
      ret = ret * base % MOD;
    base = (ll)base * base % MOD;
    exp >>= 1;
  }
  return ret;
}

ll Solution() {
  int bnd = sqrt(K) + 1;
  Vec p;
  fora (i, 2, bnd) {
    if (K % i == 0) {
      p.emplace_back(i);
      while (K % i == 0)
        K /= i;
    }
  }
  if (K != 1)
    p.emplace_back(K);
  ll ans = 1;
  for (int v : p) {
    ll k = 0;
    ll g = 1;
    ll num = v;
    while (N / num != 0) {
      k = N / num;
      g = g * BinPow(v, k) % MOD;
      if (N / v / num != 0)
        num *= v;
      else
        break;
    }
    ans = ans * g % MOD;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> K >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}
