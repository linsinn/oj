#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
ll A[MAXN];

void Solution() {
  ll su = 0;
  ll k = 0;
  vector<int> ans;
  fora (i, 0, N) {
    su += A[i];
    if (su % 2 == 1 && k < K-1) {
      ans.emplace_back(i+1);
      su = 0;
      ++k;
    }
  }
  if (su % 2 == 0) {
    cout << "NO\n";
  } else {
    cout << "YES\n";
    ans.emplace_back(N);
    for (int v : ans) {
      cout << v << ' ';
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      fora (i, 0, N) cin >> A[i];
      Solution();
    }
  }
  return 0;
}
