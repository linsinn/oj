//
// Created by sinn on 7/16/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e4 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
vector<Pii> segs[MAXN];
vector<Pii> hor[MAXN], ver[MAXN];
vector<int> h[MAXN];
int BIT[MAXN];
constexpr int D = 5001;

void Upd(int pos, int x) {
  while (pos <= MAXN) {
    BIT[pos] += x;
    pos += pos & -pos;
  }
}

int Get(int pos) {
  int ret = 0;
  while (pos > 0) {
    ret += BIT[pos];
    pos -= pos & -pos;
  }
  return ret;
}

int Get(int l, int r) {
  return Get(r) - Get(l - 1);
}


ll Solution() {
  fora (i, 0, N) {
    int x1 = segs[i][0].first, y1 = segs[i][0].second;
    int x2 = segs[i][1].first, y2 = segs[i][1].second;
    x1 += D, y1 += D, x2 += D, y2 += D;
    if (y1 == y2)
      hor[y1].emplace_back(min(x1, x2), max(x1, x2));
    else
      ver[x1].emplace_back(min(y1, y2), max(y1, y2));
  }
  ll ans = 0;
  fora (y, 0, MAXN) {
    for (auto s : hor[y]) {
      int l = s.first, r = s.second;
      fora (x, l, r+1) {
        for (auto s2 : ver[x]) {
          if (s2.first <= y && s2.second > y) {
            h[s2.second].emplace_back(x);
            Upd(x, 1);
          }
        }
      }
      fora (y2, y+1, MAXN) {
        for (auto s2 : hor[y2]) {
          ll cnt = Get(s2.first, s2.second);
          ans += cnt * (cnt - 1) / 2;
        }
        for (int x : h[y2]) {
          Upd(x, -1);
        }
        h[y2].clear();
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int x1, y1, x2, y2;
    fora (i, 0, N) {
      cin >> x1 >> y1 >> x2 >> y2;
      segs[i].emplace_back(x1, y1);
      segs[i].emplace_back(x2, y2);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
