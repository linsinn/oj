#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
ll INF = INT64_MAX;
Pll dolls[MAXN];

ll Norm(ll a, ll b) {
  return (a + b) % MOD;
}

Pll Combine(const Pll& a, const Pll& b) {
  if (a.first < b.first) 
    return a;
  if (b.first < a.first)
    return b;
  return {a.first, Norm(a.second, b.second)};
}

vector<Pll> segt;

void SetVal(int pos, const Pll& val) {
  segt[pos += N] = val;
  for (pos >>= 1; pos > 0; pos >>= 1)
    segt[pos] = Combine(segt[2*pos], segt[2*pos+1]);
}

Pll GetMin(int l, int r) {
  Pll ans = {INF, 0};
  for (l += N, r += N; l < r; l >>= 1, r >>= 1) {
    if (l & 1)
      ans = Combine(segt[l++], ans);
    if (r & 1)
      ans = Combine(segt[--r], ans);
  }
  return ans;
}


ll Solution() {
  sort(dolls, dolls+N);
  segt.assign(2 * N, {INF, 0});
  ford (i, N-1, 0) {
    auto pos = std::lower_bound(dolls, dolls+N, make_pair(dolls[i].second, 0LL)) - dolls;
    if (pos >= N) {
      SetVal(i, {dolls[i].first, 1LL});
      continue;
    }
    Pll bst = GetMin(pos, N);
    SetVal(i, {bst.first - (dolls[i].second - dolls[i].first), bst.second});
  }
  return GetMin(0, N).second;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    for (int i = 0; i < N; ++i) {
      cin >> dolls[i].first >> dolls[i].second;
      swap(dolls[i].first, dolls[i].second);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
