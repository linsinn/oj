//
// Created by sinn on 7/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];

void Solution() {
  int g[2] = {0, 0};
  fora (i, 0, N) {
    g[A[i] % 2]++;
  }
  if (g[0] != 0 && g[1] != 0) {
    sort(A, A+N);
  }
  fora (i, 0, N) {
    cout << A[i] << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}
