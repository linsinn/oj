//
// Created by sinn on 6/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
int A[MAXN];
pair<ll, int> qrys[MAXN];
int BIT[MAXN];

void Insert(int x) {
  while (x <= M) {
    BIT[x]++;
    x += x & -x;
  }
}

int GetSum(int x) {
  int ret = 0;
  while (x > 0) {
    ret += BIT[x];
    x -= x & -x;
  }
  return ret;
}

int GetCity(int p) {
  ++p;
  int lb = 1, rb = M;
  while (lb <= rb) {
    int mid = lb + ((rb - lb) >> 1);
    int s = GetSum(mid);
    if (s < p) {
      lb = mid + 1;
    } else {
      rb = mid - 1;
    }
  }
  return rb + 1;
}

void Solution() {
  map<int, int> t;
  fora (i, 0, N) {
    t[A[i]]++;
  }
  vector<pair<int, int>> vc;
  for (auto& p : t) {
    vc.emplace_back(p.second, p.first);
  }
  sort(vc.begin(), vc.end());
  int cur_sz = 0;
  ll pre_times = 0;
  int nxt_city = 0;
  fora (i, 1, M+1) {
    if (t.find(i) == t.end()) {
      Insert(i);
      ++cur_sz;
    }
  }
  if (cur_sz == 0) {
    cur_sz = 1;
    pre_times = vc[0].first;
    Insert(vc[0].second);
    nxt_city = 1;
  }
  vector<int> ans(K);
  ll last_day = N;
  sort(qrys, qrys+K);
  fora (i, 0, K) {
    auto q = qrys[i];
    ll e = q.first - last_day;
    ll g = vc[nxt_city].first - pre_times;
    while (nxt_city < vc.size() && g * cur_sz < e) {
      last_day += g * cur_sz;
      ++cur_sz;
      pre_times = vc[nxt_city].first;
      Insert(vc[nxt_city++].second);
      g = vc[nxt_city].first - pre_times;
      e = q.first - last_day;
    }
    ll sh = (e-1) % cur_sz;
    ans[q.second] = GetCity(sh);
  }
  fora (i, 0, K) {
    cout << ans[i] << "\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    ll d;
    fora (k, 0, K) {
      cin >> qrys[k].first;
      qrys[k].second = k;
    }
    Solution();
  }
  return 0;
}

