//
// Created by sinn on 7/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];
int first[MAXN], last[MAXN];

ll Solution() {
  memset(first, -1, sizeof(first));
  memset(last, -1, sizeof(last));
  fora (i, 0, K) {
    last[A[i]] = i;
  }
  ford (i, K-1, 0) {
    first[A[i]] = i;
  }
  ll ans = 0;
  fora (i, 1, N+1) {
    fora (j, -1, 2) {
      int a = i, b = i + j;
      if (b >= 1 && b <= N) {
        if (first[a] == -1 || last[b] == -1 || last[b] < first[a]) {
          ++ans;
        }
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, K) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}