//
// Created by sinn on 7/2/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 998244353;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll perm[MAXN];
vector<int> gph[MAXN];

ll Solution() {
  perm[0] = 1;
  fora (i, 1, N+1) {
    perm[i] = perm[i-1] * i % MOD;
  }
  ll ans = 1;
  fora (i, 1, N+1) {
    ans = ans * perm[gph[i].size()] % MOD;
  }
  ans = ans * N % MOD;
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int u, v;
    fora (i, 0, N-1) {
      cin >> u >> v;
      gph[u].emplace_back(v);
      gph[v].emplace_back(u);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
