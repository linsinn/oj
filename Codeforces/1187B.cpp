//
// Created by sinn on 7/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
string s;
string t;
vector<int> pos[30];

void Init() {
  fora (i, 0, s.size()) {
    pos[s[i]-'a'].emplace_back(i);
  }
}

ll Solution() {
  vector<int> cnt(30, 0);
  fora (i, 0, t.size()) {
    cnt[t[i]-'a']++;
  }
  int ans = 0;
  fora (i, 0, 30) {
    if (cnt[i] != 0) {
      ans = max(ans, pos[i][cnt[i]-1]);
    }
  }
  return ans + 1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    Init();
    cin >> M;
    fora (i, 0, M) {
      cin >> t;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
