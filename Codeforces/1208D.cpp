#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
ll A[MAXN];
ll BIT[MAXN];

void SetVal(int x) {
  int v = x;
  while (x <= N) {
    BIT[x] += v;
    x += x & -x;
  }
}

ll GetSum(int x) {
  ll ret = 0;
  while (x > 0) {
    ret += BIT[x];
    x -= x & -x;
  }
  return ret;
}

void Solution() {
  memset(BIT, 0, sizeof(BIT));
  vector<ll> ans(N);
  set<int> s;
  fora (i, 0, N) s.emplace(i+1);
  ford (i, N-1, 0) {
    int l = 1, r = N;
    while (l <= r) {
      ll m = (l + r) / 2;
      ll t = A[i] + GetSum(m-1);
      ll g = (m - 1) * m / 2;
      if (t < g) {
        r = m - 1;
      } else if (t > g){
        l = m + 1;
      } else {
        int k = *s.lower_bound(m);
        s.erase(k);
        ans[i] = k;
        SetVal(k);
        break;
      }
    }
  }
  fora (i, 0, N) {
    cout << ans[i] << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}
