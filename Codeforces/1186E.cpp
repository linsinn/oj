//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e3 + 10;

using ll = long long;

ll N, M, K, T;
string mat[MAXN];
ll x[2], y[2];
ll su[MAXN][MAXN];

bool is_reversed(ll j) {
  if (j == 0) return false;
  ll l = 1;
  while (l * 2 <= j)
    l *= 2;
  return !is_reversed(j - l);
}

ll GetSum(ll i, ll j) {
  if (i == 0 || j == 0)
    return 0;
  ll N2 = N * 2, M2 = M * 2;
  --i, --j;
  ll p = i / N2;
  ll q = j / M2;
  ll ret = p * q * su[N2 - 1][M2 - 1];
  ll a = su[N2-1][j % M2] * p;
  bool r0 = is_reversed(j / M);
  int k0 = (j / M) % 2;
  if ((k0 == 0 && r0) || (k0 == 1 && !r0)) {
    a = (j % M2 + 1) * N2 * p - a;
  }
  ret += a;
  a = su[i % N2][M2 - 1] * q;
  int k1 = (i / N) % 2;
  bool r1 = is_reversed(i / N);
  if ((k1 == 0 && r1) || (k1 == 1 && !r1)) {
    a = (i % N2 + 1) * M2 * q - a;
  }
  ret += a;
  a = su[i % N2][j % M2];
  if ((k1 == 0 && k0 == 1 && !(r0 ^ r1)) || (k1 == 1 && k0 == 0 && !(r0 ^ r1)) ||
          (k1 == 0 && k0 == 0 && (r0 ^ r1)) || (k1 == 1 && k0 == 1 && (r0 ^ r1))) {
    a = (i % N2 + 1) * (j % M2 + 1) - a;
  }
  ret += a;
  return ret;
}

void Init() {
  fora (i, 0, N) {
    string s;
    fora (j, 0, M) {
      if (mat[i][j] == '0') s += '1';
      else s += '0';
    }
    mat[i] += s;
  }
  fora (i, N, 2 * N) {
    string s;
    fora (j, 0, 2 * M) {
      if (mat[i-N][j] == '0') s += '1';
      else s += '0';
    }
    mat[i] = s;
  }
  fora (i, 0, 2 * N) {
    fora (j, 0, 2 * M) {
      su[i][j] = mat[i][j] - '0' + (j == 0 ? 0 : su[i][j-1]);
    }
  }
  fora (i, 1, 2 * N) {
    fora (j, 0, 2 * M) {
      su[i][j] += su[i-1][j];
    }
  }
}

ll Solution() {
  return GetSum(x[1], y[1]) - GetSum(x[1], y[0]-1) - GetSum(x[0]-1, y[1]) + GetSum(x[0]-1, y[0]-1);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> T) {
    fora (i, 0, N) cin >> mat[i];
    Init();
    fora (i, 0, T) {
      cin >> x[0] >> y[0] >> x[1] >> y[1];
      cout << Solution() << '\n';
    }
  }
  return 0;
}
