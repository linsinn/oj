//
// Created by sinn on 10/16/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
struct Pt {
  int x, y, z;
  Pt() = default;
  Pt(int _x, int _y, int _z) {
    x = _x;
    y = _y;
    z = _z;
  }
  bool operator < (const Pt &b) const {
    if (x != b.x)
      return x < b.x;
    if (y != b.y)
      return y < b.y;
    return z < b.z;
  }
};
vector<Pt> pts;

void Solution() {
  map<int, map<int, set<Pii>>> mp;
  fora (i, 0, N) {
    mp[pts[i].x][pts[i].y].emplace(pts[i].z, i+1);
  }
  Vec left;
  for (auto i = mp.begin(); i != mp.end(); ++i) {
    auto &t = i->second;
    Vec tmp;
    for (auto j = t.begin(); j != t.end(); ++j) {
      auto &p = j->second;
      auto g = p.begin();
      while (g != p.end() && next(g) != p.end()) {
        cout << g->second  << ' ' << next(g)->second << '\n';
        advance(g, 2);
      }
      if (g != p.end())
        tmp.emplace_back(p.rbegin()->second);
    }
    int k = 0;
    while (k < tmp.size() && k + 1 < tmp.size()) {
      cout << tmp[k] << ' ' << tmp[k+1] << '\n';
      k += 2;
    }
    if (k + 1 == tmp.size())
      left.emplace_back(tmp[k]);
  }
  for (int i = 0; i < left.size(); i += 2) {
    cout << left[i] << ' ' << left[i+1] << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    pts.resize(N);
    fora (i, 0, N) {
      cin >> pts[i].x >> pts[i].y >> pts[i].z;
    }
    Solution();
  }
  return 0;
}