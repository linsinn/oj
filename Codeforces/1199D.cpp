#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

using ll = long long;

ll N, M, K, T;
Vec A;
vector<Vec> eve;

void Solution() {
  Vec nxt_larget(eve.size()+1, T);
  ford (i, T-2, 0) {
    nxt_larget[i] = nxt_larget[i+1];
    if (eve[i+1][0] == 2) {
      if (nxt_larget[i] == T || (eve[i+1][1] > eve[nxt_larget[i]][1])) {
        nxt_larget[i] = i+1;
      }
    }
  }
  int nxt = nxt_larget[0];
  if (eve[0][0] == 2 && (nxt == T || eve[0][1] > eve[nxt][1]))
    nxt = 0;
  fora (i, 0, nxt) {
    if (eve[i][0] == 1) 
      A[eve[i][1]] = eve[i][2];
  }
  if (nxt != T)
    fora (i, 0, N)
      A[i] = max(A[i], eve[nxt][1]);
  int i = nxt + 1;
  nxt = nxt_larget[nxt];
  while (i < nxt) {
    stack<int> chged;
    fora (j, i, nxt) {
      if (eve[j][0] == 1) {
        A[eve[j][1]] = eve[j][2];
        chged.emplace(eve[j][1]);
      }
    }
    while (nxt != T && !chged.empty()) {
      int k = chged.top();
      chged.pop();
      A[k] = max(A[k], eve[nxt][1]);
    }
    i = nxt + 1;
    nxt = nxt_larget[nxt];
  }
  fora (i, 0, N) 
    cout << A[i] << ' ';
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cin >> T;
    int x, y, z;
    eve.clear();
    fora (i, 0, T) {
      cin >> x;
      if (x == 1) {
        cin >> y >> z;
        eve.emplace_back(Vec({x, y-1, z}));
      } else {
        cin >> y;
        eve.emplace_back(Vec({x, y}));
      }
    }
    Solution();
  }
  return 0;
}
