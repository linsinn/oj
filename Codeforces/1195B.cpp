//
// Created by sinn on 7/17/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

ll Solution() {
  ll lb = 0, rb = N;
  ll ans = 0;
  while (lb <= rb) {
    ll mid = lb + (rb - lb) / 2;
    ll g = N - mid;
    ll k = (g + 1) * g / 2 - mid;
    if (k == K)
      return mid;
    else if (k < K)
      rb = mid - 1;
    else
      lb = mid + 1;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cout << Solution() << '\n';
  }
  return 0;
}
