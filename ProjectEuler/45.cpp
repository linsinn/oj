//
// Created by sinn on 6/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int a, b;

void Solution() {
  unordered_set<ll> s;
  vector<ll> ans;
  ll idx = 1;
  if (a == 3) {
    while (true) {
      ll x = idx * (idx + 1) / 2;
      if (x >= N)
        break;
      ll d = sqrt(x * 2 / 3);
      fora (i, d-2, d+3) {
        if (1LL * i * (3 * i - 1) / 2 == x) {
          ans.emplace_back(x);
          break;
        }
      }
      ++idx;
    }
  } else {
    while (true) {
      ll x = idx * (3 * idx - 1) / 2;
      if (x >= N)
        break;
      ll d = sqrt(x / 2);
      fora (i, d-2, d+3) {
        if (1LL * i * (2 * i - 1) == x) {
          ans.emplace_back(x);
          break;
        }
      }
      ++idx;
    }
  }
  for (ll x : ans) {
    cout << x << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> a >> b) {
    Solution();
  }
  return 0;
}