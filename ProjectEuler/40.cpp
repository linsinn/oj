//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll d[10];
string s;

int dfs(int dig_num, ll k) {
  int t = 0;
  ll lb = 1;
  while (t < dig_num - 1) {
    lb *= 10;
    ++t;
  }
  ll tot = lb * 9;
  if (k >= tot * dig_num)
    return dfs(dig_num + 1, k - tot * dig_num);
  ll num = lb + k / dig_num;
  ll left = k - k / dig_num * dig_num;
  return to_string(num)[left] - '0';
}

int FindDig(ll k) {
  if (k < s.size())
    return s[k] - '0';
  return dfs(3, k - s.size());
}

ll Solution() {
  ll ret = 1;
  fora (i, 0, 7) {
    int t = dfs(1, d[i] - 1);
    ret *= t;
  }
  return ret;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      fora (i, 0, 7) {
        cin >> d[i];
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}
