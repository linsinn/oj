//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e3 + 10;

using ll = long long;

ll N, M, K, T;
ll grid[MAXN][MAXN];

void Init() {
  memset(grid, 0, sizeof(grid));
  fora (i, 0, MAXN) {
    fora (j, 0, MAXN) {
      if (i == 0 || j == 0) {
        grid[i][j] = 1;
      } else {
        grid[i][j] = (grid[i-1][j] + grid[i][j-1]) % MOD;
      }
    }
  }
}

ll Solution() {
  return grid[N][M];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N >> M;
      cout << Solution() << '\n';
    }
  }
  return 0;
}