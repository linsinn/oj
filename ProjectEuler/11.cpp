//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int grid[25][25];

ll Solution() {
  ll ans = 0;
  fora (i, 0, 20) {
    fora (j, 0, 20) {
      if (i + 3 < 20) {
        ll cur = 1;
        fora (k, 0, 4) {
          cur *= grid[i+k][j];
        }
        ans = max(ans, cur);
      }
      if (j + 3 < 20) {
        ll cur = 1;
        fora (k, 0, 4) {
          cur *= grid[i][j+k];
        }
        ans = max(ans, cur);
      }
      if (i + 3 < 20 && j + 3 < 20) {
        ll cur = 1;
        fora (k, 0, 4) {
          cur *= grid[i+k][j+k];
        }
        ans = max(ans, cur);
      }
      if (i + 3 < 20 && j - 3 >= 0)  {
        ll cur = 1;
        fora (k, 0, 4) {
          cur *= grid[i+k][j-k];
        }
        ans = max(ans, cur);
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  fora (i, 0, 20) {
    fora (j, 0, 20) {
      cin >> grid[i][j];
    }
  }
  cout << Solution() << '\n';
  return 0;
}
