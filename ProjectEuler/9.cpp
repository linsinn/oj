//
// Created by sinn on 6/24/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e3 + 10;

using ll = long long;

ll N, M, K, T;
int ans[MAXN];

void Init() {
  memset(ans, -1, sizeof(ans));
  vector<ll> sqr(MAXN+1);
  fora (i, 1, MAXN) {
    sqr[i] = i * i;
  }
  fora (i, 1, MAXN) {
    fora (j, 1, MAXN) {
      int g = i * i + j * j;
      auto it = lower_bound(sqr.begin(), sqr.end(), g);
      if (it != sqr.end() && *it == g) {
        int k = distance(sqr.begin(), it);
        if (i + j + k < MAXN)
          ans[i+j+k] = max(ans[i+j+k], i * j * k);
      }
    }
  }
}

ll Solution() {
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
