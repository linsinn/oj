#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
vector<vector<int>> sel_ways;
bool primes[MAXN];

void GenSel(vector<int>& cand, vector<int>& v, int idx) {
  if (v.size() == K) {
    sel_ways.emplace_back(v);
    return ;
  }
  if (idx >= cand.size())
    return;
  fora (k, 0, K - v.size() + 1) {
    fora (p, 0, k) {
      v.emplace_back(cand[idx]);
    }
    GenSel(cand, v, idx + 1);
    fora (p, 0, k) {
      v.pop_back();
    }
  }
}

void Init() {
  vector<int> cand(9);
  fora (i, 0, 9) cand[i] = i + 1;
  vector<int> v;
  GenSel(cand, v, 0);
  memset(primes, 1, sizeof(primes));
  fora (i, 2, MAXN) {
    if (primes[i]) {
      int k = 2;
      while (i * k < MAXN) {
        primes[i * k++] = false;
      }
    }
  }
}

void GenNumsHelper(vector<int>& sel, vector<bool>& vis, int cnt, vector<int>& digs, int idx, set<int>& nums, int num) {
  if (cnt == sel.size() && idx == digs.size()) {
    if (!primes[num])
      nums.emplace(num);
    return ;
  }
  fora (i, 0, sel.size()) {
    if (!vis[i]) {
      vis[i] = true;
      GenNumsHelper(sel, vis, cnt + 1, digs, idx, nums, num * 10 + sel[i]);
      vis[i] = false;
    }
  }
  if (idx < digs.size()) {
    if (digs[idx] != 0 || num != 0)
      GenNumsHelper(sel, vis, cnt, digs, idx + 1, nums, num * 10 + digs[idx]);
  }
}

void GenNums(vector<int>& sel, int x, set<int>& nums) {
  vector<int> digs(M);
  int k = 0;
  while (x != 0) {
    digs[k++] = x % 10;
    x /= 10;
  }
  while (digs.size() + sel.size() < N) {
    digs.emplace_back(0);
  }
  reverse(digs.begin(), digs.end());
  vector<bool> vis(sel.size());
  GenNumsHelper(sel, vis, 0, digs, 0, nums, 0);
}

bool CanGen(int g, int x, vector<int> &sel) {
  vector<int> s, q;
  while (g != 0) {
    s.emplace_back(g % 10);
    g /= 10;
  }
  if (s.size() != N) return false;
  while (x != 0) {
    q.emplace_back(x % 10);
    x /= 10;
  }
  int k = -1;
  fora (i, 0, s.size()) {
    if (s[i] == 0) {
      k = i;
    }
  }
  multiset<int> m(sel.begin(), sel.end());
  vector<int> v(s.size(), 0);
  int i = 0, j = 0;
  int w = -1;
  while (i < s.size()) {
    if (j < q.size() && s[i] == q[j]) {
      v[i] = 1;
      ++j;
      w = i;
    }
    ++i;
  }
  if (j != q.size()) return false;

  fora (o, 0, v.size()) {
    if (v[o] == 0) {
      if (s[o] != 0) {
        if (m.count(s[o])) {
          m.erase(m.find(s[o]));
        } else {
          return false;
        }
      } else if (w > o)
        return false;
    }
  }
  return m.empty();
}

void Solution() {
  M = N - K;
  Init();
  int lb = 1;
  fora (i, 0, M-1) lb *= 10;
  int rb = lb * 10;
  int ans[2] = {0, 0};
  set<pair<int, int>> vis;
  fora (i, 1, rb) {
    for (auto &sel : sel_ways) {
      set<int> denos;
      GenNums(sel, i, denos);
      for (int d : denos) {
        fora (j, 1, i) {
          if (d * j % i == 0) {
            int g = d * j / i;
            if (CanGen(g, j, sel)) {
              vis.emplace(g, d);
            }
          }
        }
      }
    }
  }
  for (auto& p : vis) {
    ans[0] += p.first;
    ans[1] += p.second;
  }
  cout << ans[0] << ' ' << ans[1] << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
  freopen("a.txt", "w", stdout);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    Solution();
  }
  return 0;
}
