//
// Created by sinn on 6/24/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
vector<ll> vec;

bool IsPalindrome(ll x) {
  vector<int> b;
  while (x != 0) {
    b.emplace_back(x % 10);
    x /= 10;
  }
  int i = 0, j = b.size() - 1;
  while (i < j) {
    if (b[i++] != b[j--]) return false;
  }
  return true;
}

void Init() {
  fora (i, 100, 1000) {
    fora (j, 100, 1000) {
      if (IsPalindrome(i * j)) {
        vec.emplace_back(i * j);
      }
    }
  }
  sort(vec.begin(), vec.end());
}

ll Solution() {
  auto it = lower_bound(vec.begin(), vec.end(), N);
  return *--it;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
