//
// Created by sinn on 6/27/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e6 + 10;

using ll = long long;

ll N, M, K, T;
bool is_prime[MAXN];
ll ans[MAXN];

bool IsCP(int x) {
  if (!is_prime[x]) return false;
  vector<int> digs;
  while (x != 0) {
    digs.emplace_back(x % 10);
    x /= 10;
  }
  reverse(digs.begin(), digs.end());
  set<int> s;
  bool is_cp = true;
  fora (i, 0, digs.size()) {
    x = 0;
    fora (j, 0, digs.size()) {
      x = x * 10 + digs[(i+j) % digs.size()];
    }

    if (x < MAXN && !is_prime[x])
      is_cp = false;
    s.emplace(x);
  }
  return is_cp;
}

ll Solution() {
  memset(is_prime, 1, sizeof(is_prime));
  memset(ans, -1, sizeof(ans));
  fora (i, 2, MAXN) {
    if (is_prime[i]) {
      int k = 2;
      while (k * i < MAXN) {
        is_prime[i * k++] = false;
      }
    }
  }
  fora (i, 2, MAXN) {
//    if (ans[i] == -1)
    ans[i] = IsCP(i);
  }
  ans[0] = ans[1] = 0;
  fora (i, 2, MAXN) {
    ans[i] = ans[i] == 1 ? ans[i-1] + i : ans[i-1];
  }
  return ans[N-1];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}
