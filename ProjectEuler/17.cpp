//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
map<ll, string> bases, nums;

void Init() {
  nums[1] = "One";
  nums[2] = "Two";
  nums[3] = "Three";
  nums[4] = "Four";
  nums[5] = "Five";
  nums[6] = "Six";
  nums[7] = "Seven";
  nums[8] = "Eight";
  nums[9] = "Nine";
  nums[10] = "Ten";
  nums[11] = "Eleven";
  nums[12] = "Twelve";
  nums[13] = "Thirteen";
  nums[14] = "Fourteen";
  nums[15] = "Fifteen";
  nums[16] = "Sixteen";
  nums[17] = "Seventeen";
  nums[18] = "Eighteen";
  nums[19] = "Nineteen";
  nums[20] = "Twenty";
  nums[30] = "Thirty";
  nums[40] = "Forty";
  nums[50] = "Fifty";
  nums[60] = "Sixty";
  nums[70] = "Seventy";
  nums[80] = "Eighty";
  nums[90] = "Ninety";
  bases[ll(1e12)] = "Trillion";
  bases[ll(1e9)] = "Billion";
  bases[ll(1e6)] = "Million";
  bases[ll(1e3)] = "Thousand";
}

string Helper(int num) {
  string ret;
  if (num >= 100) {
    ret += nums[num / 100] + " " + "Hundred";
    num %= 100;
    if (num != 0) {
      ret += " ";
    }
  }
  if (num <= 20) {
    ret += nums[num];
  } else {
    ret += nums[num / 10 * 10];
    if (num % 10 != 0)
      ret += " " + nums[num % 10];
  }
  return ret + " ";
}

string Solution() {
  ll bs[] = {(ll)1e12, (ll)1e9, (ll)1e6, (ll)1e3};
  string ans;
  fora (i, 0, 4) {
    if (N >= bs[i]) {
      ans += Helper(N / bs[i]) + bases[bs[i]] + " ";
      N %= bs[i];
    }
  }
  ans += Helper(N);
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}