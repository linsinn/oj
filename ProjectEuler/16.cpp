//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e4 + 10;

using ll = long long;

ll N, M, K, T;
int ans[MAXN];


void Init() {
  string s = "1";
  int c = 0;
  fora (i, 1, MAXN) {
    int tot = 0;
    for (char& ch : s) {
      c = (ch - '0') * 2 + c;
      ch = c % 10 + '0';
      tot += ch - '0';
      c /= 10;
    }
    while (c != 0) {
      s.push_back(c % 10 + '0');
      tot += c % 10;
      c /= 10;
    }
    ans[i] = tot;
  }
}

ll Solution() {
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
