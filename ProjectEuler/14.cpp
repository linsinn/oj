//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 5e6 + 10;

using ll = long long;

ll N, M, K, T;
int memos[MAXN];
vector<int> ans(MAXN);

int dfs(ll num) {
  if (num == 1) return 1;
  if (num < MAXN && memos[num] != 0) return memos[num];
  int g = 0;
  if (num % 2 == 0) {
    g = 1 + dfs(num / 2);
  } else {
    g = 1 + dfs(3 * num + 1);
  }
  if (num < MAXN)
    memos[num] = g;
  return g;
}

void Init() {
  memset(memos, 0, sizeof(memos));
  memos[1] = 1;
  fora (i, 1, MAXN) {
    dfs(i);
  }
  fora (i, 1, MAXN) {
    ans[i] = i;
    if (memos[i] < memos[ans[i-1]])
      ans[i] = ans[i-1];
  }
}

ll Solution() {
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
