//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
bool is_prime[MAXN];

void Init() {
  memset(is_prime, 1, sizeof(is_prime));
  is_prime[0] = is_prime[1] = false;
  fora (i, 2, MAXN) {
    if (is_prime[i]) {
      int k = 2;
      while (i * k < MAXN) {
        is_prime[i*k] = false;
        ++k;
      }
    }
  }
}

void Solution() {
  Init();
  int maxi = 0;
  int ans[2];
  fora (a, -N, N+1) {
    fora (b, -N, N+1) {
      ll n = 0;
      while (true) {
        ll g = n * n + a * n + b;
        if (0 <= g && is_prime[g])
          ++n;
        else
          break;
      }
      if (n > maxi) {
        maxi = n;
        ans[0] = a, ans[1] = b;
      }
    }
  }
  cout << ans[0] << ' ' << ans[1] << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}