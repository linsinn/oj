//
// Created by sinn on 6/24/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)
#define PrintArr(arr) for (auto& elm : arr) cout << elm << ' ';

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
vector<ll> fib, evens;
map<ll, ll> memos;

ll Solution() {
  while (fib.back() < N) {
    fib.emplace_back(fib[fib.size()-1] + fib[fib.size()-2]);
    if (fib.back() % 2 == 0)
      evens.emplace_back(fib.back());
  }
  ll su = 0;
  fora (i, 0, evens.size()) {
    if (evens[i] > N) break;
    su += evens[i];
  }
  return su;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    fib.emplace_back(1);
    fib.emplace_back(2);
    evens.emplace_back(2);
    memos[0] = 0;
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}