//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e4 + 10;

using ll = long long;

ll N, M, K, T;
bool ans[MAXN];

ll DivisorSum(ll x) {
  if (x == 1) return 0;
  ll g = sqrt(x);
  if (g * g < x)
    ++g;
  ll ret = 1;
  fora (i, 2, g) {
    if (x % i == 0) {
      ret += i;
      ret += x / i;
    }
  }
  if (g * g == x)
    ret += g;
  return ret;
}

void Init() {
  memset(ans, 0, sizeof(ans));
  vector<int> vec;
  fora (i, 1, MAXN) {
    if (DivisorSum(i) > i) {
      vec.emplace_back(i);
    }
  }
  fora (i, 0, vec.size()) {
    fora (j, i, vec.size()) {
      ll k = vec[i] + vec[j];
      if (k < MAXN)
        ans[k] = true;
    }
  }
}

bool Solution() {
  if (N >= MAXN) return true;
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
