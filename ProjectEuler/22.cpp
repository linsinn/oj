//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
string ns[MAXN], name;

ll Solution() {
  sort(ns, ns + N);
  auto it = lower_bound(ns, ns + N, name);
  ll k = distance(ns, it) + 1;
  ll g = 0;
  for (char ch : *it) {
    g += ch - 'A' + 1;
  }
  return g * k;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> ns[i];
    }
    cin >> T;
    while (T--) {
      cin >> name;
      cout << Solution() << '\n';
    }
  }
  return 0;
}