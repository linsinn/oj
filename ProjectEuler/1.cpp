//
// Created by sinn on 6/23/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;

ll Solution() {
  ll k = (N-1) / 3;
  ll ans = 3LL * k * (k + 1) / 2;
  k = (N-1) / 5;
  ans += 5LL * k * (k + 1) / 2;
  k = (N-1) / 15;
  ans -= 15LL * k * (k + 1) / 2;
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (cin >> N) {
      cout << Solution() << '\n';
    }
  }
  return 0;
}
