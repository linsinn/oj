//
// Created by sinn on 6/24/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e4 + 10;

using ll = long long;

int N, M, K, T;
ll pre_sum[MAXN];

void Init() {
  pre_sum[0] = 0;
  fora (i, 1, MAXN) {
    pre_sum[i] = pre_sum[i-1] + (ll)i * i;
  }
}

ll Solution() {
  ll g = N * (N + 1) / 2;
  g *= g;
  return abs(g - pre_sum[N]);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
