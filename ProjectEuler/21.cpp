//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

ll N, M, K, T;
bool is_amicable[MAXN];
ll divs_sum[MAXN];
ll ans[MAXN];

ll DivisorSum(ll x) {
  if (x < MAXN && divs_sum[x] != -1)
    return divs_sum[x];
  ll g = sqrt(x);
  if (g * g < x)
    ++g;
  ll ret = 1;
  fora (i, 2, g) {
    if (x % i == 0) {
      ret += i;
      ret += x / i;
    }
  }
  if (g * g == x)
    ret += g;
  return ret;
}

void Init() {
  memset(is_amicable, 0, sizeof(is_amicable));
  memset(divs_sum, -1, sizeof(divs_sum));
  divs_sum[1] = 0;
  memset(ans, 0, sizeof(ans));
  map<ll, vector<int>> mp;
  fora (i, 2, MAXN) {
    if (!is_amicable[i]) {
      ll g = DivisorSum(i);
      divs_sum[i] = g;
      ll k = DivisorSum(g);
      if (k == i && i != g) {
        is_amicable[i] = true;
        if (g < MAXN)
          is_amicable[g] = true;
      }
      if (g < MAXN)
        divs_sum[g] = k;
    }
  }
  fora (i, 1, MAXN) {
    ans[i] = ans[i-1];
    if (is_amicable[i])
      ans[i] += i;
  }
}

ll Solution() {
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}