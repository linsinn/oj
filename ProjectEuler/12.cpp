//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

ll N, M, K, T;
vector<int> divs;
vector<pair<ll, ll>> ans;
bool sieve[MAXN];
map<int, int> cnts[MAXN];

int CountDivs(int a, int b) {
  map<int, int> mp;
  for (auto &p : cnts[a]) {
    mp[p.first] += p.second;
  }
  for (auto &p : cnts[b]) {
    mp[p.first] += p.second;
  }
  int ret = 1;
  for (auto &p : mp) {
    ret *= p.second + 1;
  }
  return ret;
}

void Init() {
  memset(sieve, 1, sizeof(sieve));
  fora (i, 2, MAXN) {
    if (sieve[i]) {
      int k = 1;
      while (i * k < MAXN) {
        sieve[i * k] = false;
        int g = i * k;
        int c = 0;
        while (g % i == 0) {
          g /= i;
          ++c;
        }
        cnts[i*k][i] = c;
        ++k;
      }
    }
  }
  fora (i, 1, MAXN) {
    ll a = i, b = i + 1;
    if (a % 2 == 0) a >>= 1;
    else b >>= 1;
    ans.emplace_back(CountDivs(a, b), a * b);
  }
  fora (i, 1, ans.size()) {
    if (ans[i].first < ans[i-1].first) {
      ans[i] = ans[i-1];
    }
  }
}

ll Solution() {
  auto it = lower_bound(ans.begin(), ans.end(), make_pair(N+1, 0LL));
  return it->second;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}