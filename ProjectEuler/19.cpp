//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll y[2], m[2], d[2];
ll days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

bool IsLeapYear(ll x) {
  return (x % 100 != 0 && x % 4 == 0) || x % 400 == 0;
}

ll DayFrom190011(ll w) {
  ll year = y[w];
  ll ed = (year - 1900) * 365;
  ed += (year - 1897) / 4;
  ll a = (year - 1801) / 100;
  if (a <= 1) ed -= a;
  else {
    ed -= a - ((a - 2) / 4 + 1);
  }
  fora (i, 1, m[w]) {
    ed += days[i-1];
    if (i == 2 && IsLeapYear(y[w]))
      ++ed;
  }
  ed += d[w];
  return ed;
}

struct Helper {
  ll year, month, day;
  ll wd;
  Helper(ll a, ll b, ll c, ll w)
    : year{a}, month{b}, day{c}, wd{w} {}
  ll cnt = 0;
  void Advance() {
    if (wd == 6 && day == 1)
      ++cnt;
    ++day;
    ll g = days[month - 1];
    if (IsLeapYear(year) && month == 2) ++g;
    if (day > g) {
      ++month;
      day = 1;
    }
    if (month > 12) {
      ++year;
      month = 1;
    }
    (wd += 1) %= 7;
  }
  bool Exceed(ll a, ll b, ll c) {
    if (year > a) return true;
    if (year == a && month > b) return true;
    return year == a && month == b && day > c;
  }
};

bool Zeller(ll w) {
  ll q = d[w];
  ll p = m[w];
  if (p <= 2) p += 12;
  ll k = y[w] % 100;
  ll j = y[w] / 100;
  ll h = (q + 13 * (p + 1) / 5 + k + k / 4 + j / 4 + 5 * j) % 7;
  return (h - 2 + 7) % 7;
}

ll Solution() {
  if (y[0] > y[1] || (y[0] == y[1] && m[0] > m[1]) || (y[0] == y[1] && m[0] == m[1] && d[0] > d[1])) {
    swap(y[0], y[1]);
    swap(m[0], m[1]);
    swap(d[0], d[1]);
  }
  ll ed0 = DayFrom190011(0);
  ll wd = (ed0 - 1) % 7;
//  ll wd0 = Zeller(0);
//  if (wd != wd0)
//    cout << wd << ' ' << wd0 << '\n';
  Helper hlp(y[0], m[0], d[0], wd);
  while (!hlp.Exceed(y[1], m[1], d[1])) {
    hlp.Advance();
  }
  return hlp.cnt;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      fora (i, 0, 2) {
        cin >> y[i] >> m[i] >> d[i];
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}
