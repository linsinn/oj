//
// Created by sinn on 6/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
vector<ll> vec;

ll IsOk(vector<int> &dig) {
  ll x = 0;
  for (int d : dig) {
    x = x * 10 + d;
  }
  if (x == 2) return x;
  if (x % 2 == 0) return -1;
  for (int p = 3; p <= sqrt(x); p += 2) {
    if (x % p == 0)
      return -1;
  }
  return x;
}

void Init() {
  fora (i, 2, 10) {
    vector<int> digs(i);
    fora (j, 0, i) digs[j] = j+1;
    do {
      ll g = IsOk(digs);
      if (g != -1) {
        vec.emplace_back(g);
      }
    } while (next_permutation(digs.begin(), digs.end()));
  }
  sort(vec.begin(), vec.end());
}

ll Solution() {
  auto it = upper_bound(vec.begin(), vec.end(), N+1);
  if (it == vec.begin())
    return -1;
  else
    return *prev(it);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
