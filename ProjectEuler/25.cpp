//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll ans[MAXN];

void Add(string& a, string& b) {
  string s;
  int c = 0;
  int i = 0, j = 0;
  while (c != 0 || i < a.size() || j < b.size()) {
    int x = i < a.size() ? a[i] - '0': 0;
    int y = j < b.size() ? b[j] - '0': 0;
    c = x + y + c;
    s += c % 10 + '0';
    c /= 10;
    i++;
    j++;
  }
  a = b;
  b = s;
}

void Init() {
  string a = "1", b = "1";
  ans[1] = ans[2] = 1;
  K = 3;
  while (b.size() < 5000) {
    Add(a, b);
    ans[K++] = b.size();
  }
//  cout << K << '\n';
}

ll Solution() {
  return lower_bound(ans, ans + K, N) - ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}