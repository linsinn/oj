//
// Created by sinn on 6/24/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 10;

using ll = long long;

int N, M, K, T;
bool sieve[MAXN];
vector<int> primes;

void Init() {
  memset(sieve, 1, sizeof(sieve));
  fora (i, 2, MAXN) {
    if (sieve[i]) {
      primes.emplace_back(i);
      int k = 2;
      while (k * i < MAXN) {
        sieve[k * i] = false;
        ++k;
      }
    }
  }
}

int Solution() {
  return primes[N-1];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
