//
// Created by sinn on 6/27/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

ll N, M, K, T;
ll memos[MAXN][10];
ll ps[] = {1, 2, 5, 10, 20, 50, 100, 200};

ll dfs(ll target, int idx, vector<int>& vec) {
  if (memos[target][idx])
    return memos[target][idx];
  if (target == 0) {
    return 1;
  }
  if (idx >= 8) return 0;
  ll cur = 0;
  int k = 0;
  while (ps[idx] * k <= target) {
    vec.emplace_back(k);
    ll g = dfs(target - ps[idx] * k, idx + 1, vec);
    vec.pop_back();
    (cur += g) %= MOD;
    ++k;
  }
  memos[target][idx] = cur;
  return cur;
}

void Init() {
  map<int, map<int, ll>> mp;
  memset(memos, 0, sizeof(memos));
  fora (i, 0, 10) {
    mp[i][0] = 1;
  }
  fora (i, 1, MAXN) {
    ford (j, 7, 0) {
      int k = j + 1;
      auto& m = mp[k];
      ll t = mp[k][i % ps[j]];
      memos[i][j] = t;
      if (j > 0)
        (mp[j][i % ps[j-1]] += t) %= MOD;
    }
  }
}

ll Solution() {
  return memos[N][0];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
