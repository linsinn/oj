//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e6 + 10;

using ll = long long;

ll N, M, K, T;
ll ans[MAXN];

bool IsPalindrome(vector<int> &v) {
  int i = 0, j = v.size() - 1;
  while (i < j) {
    if (v[i++] != v[j--]) return false;
  }
  return true;
}

vector<int> GetDig(int x) {
  vector<int> ret;
  while (x != 0) {
    ret.emplace_back(x % 10);
    x /= 10;
  }
  return ret;
}

bool IsDoubleBasePalindromes(int x){
  vector<int> dig = GetDig(x);
  if (!IsPalindrome(dig))
    return false;
  dig.clear();
  while (x != 0) {
    dig.emplace_back(x % K);
    x /= K;
  }
  return IsPalindrome(dig);
}


ll Solution() {
  memset(ans, 0, sizeof(ans));
  fora (i, 1, N+1) {
    if (IsDoubleBasePalindromes(i))
      ans[i] = 1;
  }
  fora (i, 1, N+1) {
    ans[i] = ans[i] == 1 ? ans[i-1] + i : ans[i-1];
  }
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    cout << Solution() << '\n';
  }
  return 0;
}
