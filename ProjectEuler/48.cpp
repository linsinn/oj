//
// Created by sinn on 6/30/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = unsigned long long;
constexpr ll mod = 1e10;

ll N, M, K, T;

void Mul(ll &a, ll b) {
  ll c = 0;
  ll g = 1e8;
  ll x = b % g;
  ll y = b - x;
  c += (a * x) % mod;
  c += (a % ll(1e5) * y) % mod;
  a = c;
}

ll fast_pow(ll base, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1)
      Mul(ret, base);
    Mul(base, base);
    exp >>= 1;
  }
  return ret;
}

ll Solution() {
  ll ans = 0;
  fora (i, 1, N+1) {
    (ans += fast_pow(i, i)) %= mod;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}

