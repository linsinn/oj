//
// Created by sinn on 6/30/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
bool is_prime[MAXN];
vector<ll> primes, sqrs;

void SievePrimes() {
  memset(is_prime, 1, sizeof(is_prime));
  is_prime[0] = is_prime[1] = false;
  fora (i, 2, sqrt(MAXN) + 1) {
    if (is_prime[i]) {
      int k = 0;
      while (i * i + i * k < MAXN) {
        is_prime[i*i + i*k] = false;
        ++k;
      }
    }
  }
}

void Init() {
  SievePrimes();
  fora (i, 2, MAXN) {
    if (is_prime[i])
      primes.emplace_back(i);
  }
  fora (i, 1, sqrt(MAXN)) {
    sqrs.emplace_back(i * i);
  }
}

ll Solution() {
  int ans = 0;
  fora (i, 1, primes.size()) {
    ll g = (N - primes[i]) / 2;
    auto it = lower_bound(sqrs.begin(), sqrs.end(), g);
    if (it != sqrs.end() && *it == g)
      ++ans;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
