//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
bool is_prime[MAXN];
ll ans[MAXN];

ll Solution() {
  memset(is_prime, 1, sizeof(is_prime));
  is_prime[0] = is_prime[1] = false;
  fora (i, 2, MAXN) {
    if (is_prime[i]) {
      int k = 2;
      while (i * k < MAXN) {
        is_prime[i * k++] = false;
      }
    }
  }
  memset(ans, 0, sizeof(ans));
  ford (i, N, 10) {
    if (!is_prime[i]) continue;
    int g = i;
    int x = 1;
    bool flag = true;
    do {
      if (!is_prime[g]) {
        flag = false;
        break;
      }
      g /= 10;
      x *= 10;
    } while (g != 0);
    if (!flag) continue;
    do {
      if (!is_prime[i % x]) {
        flag = false;
        break;
      }
      x /= 10;
    } while (x != 1);
    ans[i] = flag;
  }
  fora (i, 1, N+1) {
    ans[i] = ans[i] == 0 ? ans[i-1] : ans[i-1] + i;
  }
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}
