//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
string nums[MAXN];

string Add(string& a, string& b) {
  string ret;
  int c = 0;
  int i = a.size() - 1, j = b.size() - 1;
  while (c != 0 || i >= 0 || j >= 0) {
    int x = i >= 0 ? a[i] - '0' : 0;
    int y = j >= 0 ? b[j] - '0' : 0;
    c = x + y + c;
    ret += '0' + c % 10;
    c /= 10;
    --i;
    --j;
  }
  reverse(ret.begin(), ret.end());
  return ret;
}

string Solution() {
  string ans;
  fora (i, 0, N) {
    ans = Add(ans, nums[i]);
  }
  return ans.substr(0, 10);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> nums[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}