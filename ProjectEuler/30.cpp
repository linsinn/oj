//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;

ll fast_pow(ll base, ll exp) {
  ll ret = 1;
  while (exp) {
    if (exp & 1)
      ret *= base;
    base *= base;
    exp >>= 1;
  }
  return ret;
}

pair<ll, int> digit_pow_sum(ll x) {
  ll ret = 0;
  int l = 0;
  while (x != 0) {
    ret += fast_pow(x % 10, N);
    x /= 10;
    ++l;
  }
  return {ret, l};
}

ll Solution() {
  ll ans = 0;
  ll maxi = fast_pow(9, N);
  ll i = 2;
  while (true) {
    auto p = digit_pow_sum(i);
    if (p.first == i)
      ans += i;
    ++i;
    if (i > maxi * p.second)
      break;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}
