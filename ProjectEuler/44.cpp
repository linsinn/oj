//
// Created by sinn on 6/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;

void Solution() {
  vector<ll> vec;
  fora (idx, 1, 2 * N + 1) {
    ll p = 1LL * idx * (3 * idx - 1) / 2;
    vec.emplace_back(p);
  }
  vector<ll> ans;
  fora (i, 0, N-1) {
    if (i - K >= 0) {
      ll u = vec[i], v = vec[i - K];
      ll a = u - v;
      auto it = lower_bound(vec.begin(), vec.end(), a);
      if (it != vec.end() && *it == a) {
        ans.emplace_back(u);
        continue;
      }
      a = u + v;
      it = lower_bound(vec.begin(), vec.end(), a);
      if (it != vec.end() && *it == a)
        ans.emplace_back(u);
    }
  }
  for (ll x : ans) {
    cout << x << "\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    Solution();
  }
  return 0;
}
