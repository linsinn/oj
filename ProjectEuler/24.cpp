//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll fac[20];

void Init() {
  fac[0] = 1;
  fora (i, 1, 20) {
    fac[i] = fac[i-1] * i;
  }
}

void dfs(ll kth, set<char>& ss, string& ans) {
  int n = ss.size();
  if (n == 1) {
    ans += *ss.begin();
    return ;
  }
  ll f = fac[n-1];
  auto it = ss.begin();
  while (f < kth) {
    f += fac[n-1];
    ++it;
  }
  ans += *it;
  ss.erase(it);
  dfs(kth - f + fac[n-1], ss, ans);
}

string Solution() {
  set<char> ss;
  fora (i, 0, 13) {
    ss.emplace('a' + i);
  }
  string ans;
  dfs(N, ss, ans);
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
