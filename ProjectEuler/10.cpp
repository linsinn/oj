//
// Created by sinn on 6/24/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
vector<ll> primes, pre_sum;
bool sieve[MAXN];

void Init() {
  memset(sieve, 1, sizeof(sieve));
  fora (i, 2, MAXN) {
    if (sieve[i]) {
      primes.emplace_back(i);
      int k = 2;
      while (i * k < MAXN) {
        sieve[i*k] = false;
        ++k;
      }
    }
  }
  pre_sum.emplace_back(0);
  for (int p : primes) {
    pre_sum.emplace_back(pre_sum.back() + p);
  }
}

ll Solution() {
  auto it = upper_bound(primes.begin(), primes.end(), N);
  return pre_sum[distance(primes.begin(), it)];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
