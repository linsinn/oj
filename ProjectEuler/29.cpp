//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
ll vis[MAXN];
ll memos[MAXN];

ll Solution() {
  memset(vis, 0, sizeof(vis));
  memset(memos, -1, sizeof(memos));
  ll ans = 0;
  set<int> ss;
  fora (i, 2, N+1) {
    if (!vis[i]) {
      int k = 1;
      ll g = i;
      while (g <= N) {
        vis[g] = true;
        if (memos[k] != -1) {
          ans += memos[k];
        } else {
          int t = 0;
          fora (m, 2, N+1) {
            if (ss.find(m * k) == ss.end()) {
              ++t;
              ss.emplace(m * k);
            }
          }
          ans += t;
          memos[k] = t;
        }
        ++k;
        g *= i;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}

