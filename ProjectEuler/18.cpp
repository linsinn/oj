//
// Created by sinn on 6/25/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int grid[202][202];

ll Solution() {
  ford (i, N-2, 0) {
    fora (j, 0, i+1) {
      grid[i][j] += max(grid[i+1][j], grid[i+1][j+1]);
    }
  }
  return grid[0][0];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      fora (i, 0, N) {
        fora (j, 0, i+1) {
          cin >> grid[i][j];
        }
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}
