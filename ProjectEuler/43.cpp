//
// Created by sinn on 6/29/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
int divs[] = {2, 3, 5, 7, 11, 13, 17};

ll Solution() {
  ll ans = 0;
  vector<int> digs;
  fora (i, 0, N+1) digs.emplace_back(i);
  do {
    bool ok = true;
    fora (i, 1, digs.size() - 2) {
      ll x = 0;
      fora (j, 0, 3) {
        x = x * 10 + digs[i + j];
      }
      if (x % divs[i-1] != 0) {
        ok = false;
        break;
      }
    }
    if (ok) {
      ll x = 0;
      fora (i, 0, digs.size()) {
        x = x * 10 + digs[i];
      }
      ans += x;
    }
  } while (next_permutation(digs.begin(), digs.end()));
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}