//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e4 + 10;

using ll = long long;

ll N, M, K, T;
ll rec_part[MAXN], ans[MAXN];

ll FindRec(int x) {
  map<int, int> mp;
  ll g = 1;
  int k = 0;
  while (g != 0) {
    while (g < x) {
      g *= 10;
      ++k;
    }
    if (mp.find(g) != mp.end())
      return k - mp[g];
    mp[g] = k;
    g %= x;
  }
  return 0;
}

void Init() {
  memset(rec_part, 0, sizeof(rec_part));
  memset(ans, 0, sizeof(ans));
  fora (i, 1, MAXN) {
    rec_part[i] = FindRec(i);
//    cout << rec_part[i] << '\n';
  }
  fora (i, 1, MAXN) {
    ans[i] = ans[i-1];
    if (rec_part[i] > rec_part[ans[i]])
      ans[i] = i;
  }
}

ll Solution() {
  return ans[N-1];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}

