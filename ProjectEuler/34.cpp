//
// Created by sinn on 6/27/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

ll N, M, K, T;

ll Solution() {
  ll f[10];
  f[0] = 1;
  fora (i, 1, 10) {
    f[i] = f[i-1] * i;
  }
  vector<ll> ans(MAXN, 0);
  fora (i, 0, MAXN) {
    int k = 0, g = i;
    ll t = 0;
    while (g != 0) {
      t += f[g % 10];
      g /= 10;
      k++;
    }
    if (k > 1 && t % i == 0) {
      ans[i] = 1;
    }
  }
  fora (i, 1, MAXN) {
    ans[i] = ans[i] == 1 ? ans[i-1] + i : ans[i-1];
  }
  return ans[N-1];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}