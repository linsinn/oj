//
// Created by sinn on 6/30/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
bool is_prime[MAXN];

void SievePrimes() {
  memset(is_prime, 1, sizeof(is_prime));
  is_prime[0] = is_prime[1] = false;
  fora (i, 2, sqrt(MAXN) + 1) {
    if (is_prime[i]) {
      int k = 0;
      while (i * i + i * k < MAXN) {
        is_prime[i*i + i*k] = false;
        ++k;
      }
    }
  }
}

void Solution() {
  SievePrimes();
  vector<int> primes;
  fora (i, 2, N+1) {
    if (is_prime[i])
      primes.emplace_back(i);
  }
  vector<int> f(N+4);
  fora (i, 2, N+4) {
    ll g = i;
    int cur = 0;
    fora (j, 0, primes.size()) {
      if (g == 1) break;
      if (is_prime[g]) {
        ++cur;
        break;
      }
      if (g % primes[j] == 0) {
        ++cur;
        while (g % primes[j] == 0) {
          g /= primes[j];
        }
      }
    }
    f[i] = cur;
  }
  fora (i, 2, N+1) {
    if (f[i] == K) {
      bool ok = true;
      fora (j, 1, K) {
        if (f[i+j] != f[i+j-1]) {
          ok = false;
        }
      }
      if (ok) cout << i << '\n';
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    Solution();
  }
  return 0;
}
