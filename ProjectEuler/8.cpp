//
// Created by sinn on 6/24/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
string num;

int Solution() {
  int ans = 0;
  fora (i, 0, N-K) {
    int cur = 1;
    fora (j, 0, K) {
      cur *= num[i+j] - '0';
    }
    ans = max(ans, cur);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      cin >> num;
      cout << Solution() << '\n';
    }
  }
  return 0;
}

