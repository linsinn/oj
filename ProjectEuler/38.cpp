//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;

void Solution() {
  fora (i, 2, N) {
    set<int> ss;
    fora (j, 0, K) ss.emplace(j+1);
    fora (k, 1, 10) {
      int g = i * k;
      bool flag = true;
      while (g != 0) {
        if (ss.count(g % 10) == 0) {
          flag = false;
          break;
        }
        ss.erase(g % 10);
        g /= 10;
      }
      if (!flag) break;
      if (ss.empty()) {
        cout << i << '\n';
        break ;
      }
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    Solution();
  }
  return 0;
}
