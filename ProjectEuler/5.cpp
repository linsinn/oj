//
// Created by sinn on 6/24/19.
//


#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;

ll Solution() {
  vector<int> factors(44);
  ll ans = 1;
  fora (i, 1, N+1) {
    map<int, int> cnts;
    int t = i;
    while (t != 1) {
      fora (k, 2, N+1) {
        while (t % k == 0) {
          t /= k;
          cnts[k]++;
        }
      }
    }
    for (auto& p : cnts) {
      int g = max(0, p.second - factors[p.first]);
      factors[p.first] += g;
      while (g--) {
        ans *= p.first;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}