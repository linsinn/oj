//
// Created by sinn on 6/27/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;

ll GetNum(int i, int j, vector<int>& vec) {
  ll ret = 0;
  fora (k, i, j) {
    ret = ret * 10 + vec[k];
  }
  return ret;
}

ll Solution() {
  vector<int> v(N);
  fora (i, 0, N) v[i] = i + 1;
  set<ll> ss;
  do {
    fora (i, 1, N-1) {
      if (i > N / 2) break;
      fora (j, i+1, N) {
        if (j - i > N / 2) break;
        ll a = GetNum(0, i, v);
        ll b = GetNum(i, j, v);
        ll c = GetNum(j, N, v);
        if (a * b == c)
          ss.emplace(c);
      }
    }
  } while (next_permutation(v.begin(), v.end()));
  return accumulate(ss.begin(), ss.end(), 0LL);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cout << Solution() << '\n';
  }
  return 0;
}