//
// Created by sinn on 6/28/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 5e6 + 10;

using ll = long long;

ll N, M, K, T;
ll cnts[MAXN];
ll ans[MAXN];

int gcd(int a, int b) {
  return b == 0 ? a : gcd(b, a % b);
}

void Init() {
  memset(cnts, 0, sizeof(cnts));
  memset(ans, 0, sizeof(ans));
  int bnd = sqrt(MAXN);
  ford (i, bnd, 1) {
    ford (j, i-1, 1) {
      if (gcd(i, j) == 1 && ((i & 1) & (j & 1)) == 0) {
        int a = i * i - j * j;
        int b = 2 * i * j;
        int c = i * i + j * j;
        int g = a + b + c;
        int k = 1;
        while (g * k < MAXN) {
          cnts[g * k++] += 1;
        }
      }
    }
  }
  cout << l << '\n';
  int maxi = 0;
  fora (i, 1, MAXN) {
    ans[i] = ans[i-1];
    if (cnts[i] > maxi) {
      ans[i] = i;
      maxi = cnts[i];
    }
  }
}

ll Solution() {
  return ans[N];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
