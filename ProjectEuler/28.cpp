//
// Created by sinn on 6/26/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;

ll FastPow(ll base, ll exp) {
  ll ret = 1;
  while (exp != 0) {
    if (exp & 1) {
      ret = ret * base % MOD;
    }
    base = base * base % MOD;
    exp >>= 1;
  }
  return ret;
}

ll Solution() {
  N = N / 2 + 1;
  if (N == 1) return 1;
  if (N == 2) return 25;
  ll ans = 25;
  ll inv6 = FastPow(6, MOD - 2);
  ll inv2 = FastPow(2, MOD - 2);
  (ans += 24LL * ((N - 2) % MOD) % MOD) %= MOD;
  (ans += 10LL * ((N - 2) % MOD) % MOD * ((N - 1) % MOD) % MOD) %= MOD;
  ll g = (N - 2) % MOD * ((N - 1) % MOD) % MOD * ((2LL * (N % MOD) % MOD - 3) % MOD) % MOD * inv6 % MOD;
  ll k = (N - 2) % MOD * ((N - 1) % MOD) % MOD * inv2 % MOD % MOD;
  (ans += 16LL * (g + k) % MOD) %= MOD;
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
