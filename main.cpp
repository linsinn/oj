#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

struct Node {
    int val = 0;
    ll des_cnt = 0;
    ll aes_cnt = 0;
    vector<Node* > children;
} nodes[MAXN];

void CalcVal(Node* root) {
    for (Node* child : root->children) {
        CalcVal(child);
        root->val += child->val;
    }
}

void Parse(Node* root, int aes_cnt) {
    root->aes_cnt = aes_cnt;
    for (Node* child : root->children) {
        Parse(child, aes_cnt + (root->val == K));
        root->des_cnt += child->des_cnt;
    }
    root->des_cnt += root->val == K;
}

void GetAns(Node* root, ll& ans1, ll& ans2) {
    if (root != &nodes[0]) {
        if (root->val == K) {
            ans1 += nodes[0].des_cnt - root->des_cnt - root->aes_cnt;
        }
        if (root->val == 2 * K) {
            ans2 += root->des_cnt - (root->val == K);
        }
    }
    for (auto child : root->children)
        GetAns(child, ans1, ans2);
}

ll Solution() {
    CalcVal(&nodes[0]);
    M = nodes[0].val;
    if (M % 3 != 0)
        return 0;
    K = M / 3;
    Parse(&nodes[0], 0);
    ll ans[] = {0, 0};
    GetAns(&nodes[0], ans[0], ans[1]);
    return ans[0] / 2 + ans[1];
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int T, v, p;
    cin >> T;
    while (T--) {
        cin >> N;
        for (int i = 0; i <= N; i++) {
            nodes[i].val = nodes[i].aes_cnt = nodes[i].des_cnt = 0;
            nodes[i].children.clear();
        }
        for (int i = 1; i <= N; i++) {
            cin >> v >> p;
            nodes[i].val = v;
            nodes[p].children.emplace_back(&nodes[i]);
        }
        cout << Solution() << '\n';
    }
    return 0;
}