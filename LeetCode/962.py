class Solution:
    def maxWidthRamp(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        N = len(A)
        right_largest = [-1] * N
        idx, a = N-1, A[N-1]
        for i in range(N-2, -1, -1):
            right_largest[i] = idx
            if A[i] > a:
                a = A[i]
                idx = i
        ans = 0
        for i in range(N):
            k = i
            while right_largest[k] != -1:
                if A[right_largest[k]] >= A[i]:
                    k = right_largest[k]
                else:
                    break
            ans = max(ans, k - i)
        return ans
