# -*- coding: utf-8 -*-

import collections

class Solution(object):
	def threeSumMulti(self, A, target):
		"""
		:type A: List[int]
		:type target: int
		:rtype: int
		"""
		MOD = 10 ** 9 + 7
		cnt = collections.Counter(A)
		lst = sorted(list(cnt.items()))

		def sel(n, k):
			if n < k:
				return 0
			ret = 1
			for i in xrange(k):
				ret *= (n - i)
			for i in xrange(k):
				ret /= (i + 1)
			return ret % MOD

		ans = 0
		for i in xrange(len(lst)):
			for j in xrange(i+1, len(lst)):
				t = target - lst[i][0] - lst[j][0]
				if t > lst[j][0]:
					ans = (ans + (lst[i][1] * lst[j][1] * cnt[t])) % MOD

		for i in xrange(len(lst)):
			t = target - 2 * lst[i][0]
			if t != lst[i][0]:
				ans = (ans + sel(lst[i][1], 2) * cnt[t]) % MOD

		for i in xrange(len(lst)):
			if lst[i][0] * 3 == target:
				ans = (ans + sel(lst[i][1], 3)) % MOD
		return ans

