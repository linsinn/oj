class Solution:
	def rankTeams(self, votes: List[str]) -> str:
		cnt = {}
		for vote in votes:
			for i in range(len(vote)):
				if vote[i] not in cnt:
					cnt[vote[i]] = [0] * 26
					cnt[vote[i]].append(vote[i])
				cnt[vote[i]][i] -= 1
		return ''.join(elem[-1] for elem in sorted(cnt.values()))
