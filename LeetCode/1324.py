class Solution:
	def printVertically(self, s: str):
		li = s.split(' ')
		max_len = max(map(len, li))
		ans = [[] for _ in range(max_len)]
		for i in range(max_len):
			for j in range(len(li)):
				if i < len(li[j]):
					ans[i].append(li[j][i])
				else:
					ans[i].append(' ')
		for i in range(max_len):
			ans[i] = ''.join(ans[i]).rstrip()
		return ans


s = "TO BE OR NOT TO BE"
print (Solution().printVertically(s))
