import bisect


class Solution(object):
	def findNthSmallest(self, nums1, a, b, nums2, c, d, k):
		if c == d:
			return nums1[a+k]
		mid = c + (d-c) // 2
		idx = bisect.bisect_left(nums1, nums2[mid], a, b)
		left = idx - a + mid - c
		if left == k:
			return nums2[mid]
		elif left < k:
			return self.findNthSmallest(nums1, idx, b, nums2, mid+1, d, k-left-1)
		else:
			return self.findNthSmallest(nums1, a, idx, nums2, c, mid, k)

	def findMedianSortedArrays(self, nums1, nums2):
		"""
		:type nums1: List[int]
		:type nums2: List[int]
		:rtype: float
		"""
		n, m = len(nums1), len(nums2)
		tot = n + m
		if tot % 2 == 1:
			return self.findNthSmallest(nums1, 0, n, nums2, 0, m, (n+m)//2)
		else:
			a = self.findNthSmallest(nums1, 0, n, nums2, 0, m, (n+m)//2-1)
			b = self.findNthSmallest(nums1, 0, n, nums2, 0, m, (n+m)//2)
			return (a + b) / 2.0


sol = Solution()
print sol.findMedianSortedArrays([1], [1])