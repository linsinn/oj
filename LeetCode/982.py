class Solution:
    def countTriplets(self, A: 'List[int]') -> 'int':
        N = len(A)
        f = [-1] * (1 << 16)
        ans = 0
        for i in range(N):
            for j in range(N):
                x = A[i] & A[j]
                if f[x] == -1:
                    f[x] = 0
                    for k in range(N):
                        if (x & A[k]) == 0:
                            f[x] += 1
                ans += f[x]
        return ans
