class Solution:
    def largest1BorderedSquare(self, grid) -> int:
        N, M = len(grid), len(grid[0])
        r0 = [[0] * (M+1) for _ in range(N+1)]
        r1 = [[0] * (M+1) for _ in range(N+1)]
        d0 = [[0] * (M+1) for _ in range(N+1)]
        d1 = [[0] * (M+1) for _ in range(N+1)]
        for i in range(N):
            for j in range(M-1, -1, -1):
                if grid[i][j] == 0:
                    r0[i][j] = r0[i][j+1] + 1
                    r1[i][j] = 0
                else:
                    r1[i][j] = r1[i][j+1] + 1
                    r0[i][j] = 0
        for j in range(M):
            for i in range(N-1, -1, -1):
                if grid[i][j] == 0:
                    d0[i][j] = d0[i+1][j] + 1
                    d1[i][j] = 0
                else:
                    d1[i][j] = d1[i+1][j] + 1
                    d0[i][j] = 0
        ans = 0
        w = min(N, M)
        while w > 0:
            found = False
            for i in range(0, N-w+1):
                for j in range(0, M-w+1):
                    if r1[i][j] >= w and d1[i][j] >= w:
                        if r1[i+w-1][j] >= w and d1[i][j+w-1] >= w:
                            ans = max(ans, w * w)
                            found = True
                            break
                if found:
                    break
            if found:
                break
            w -= 1
        return ans


grid = [[1, 1, 0, 0]]
print(Solution().largest1BorderedSquare(grid))