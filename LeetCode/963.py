class Solution:
    def minAreaFreeRect(self, points):
        """
        :type points: List[List[int]]
        :rtype: float
        """

        def dot(a, b, c):
            return (b[0] - a[0]) * (c[0] - b[0]) + (b[1] - a[1]) * (c[1] - b[1])

        def cross(a, b, c):
            return (b[0] - a[0]) * (c[1] - a[1]) - (b[1] - a[1]) * (c[0] - a[0])

        def area(rects):
            a, b = 0, 1
            for j in range(1, 4):
                if (rects[j][0]-rects[a][0]) ** 2 + (rects[j][1]-rects[a][1]) ** 2 > \
                   (rects[b][0]-rects[a][0]) ** 2 + (rects[b][1]-rects[a][1]) ** 2:
                    b = j
            c, d = -1, -1
            for j in range(1, 4):
                if j != a and j != b and c == -1:
                    c = j
                if j != a and j != b and j != c and d == -1:
                    d = j
            if dot(rects[a], rects[c], rects[b]) != 0 or \
                dot(rects[a], rects[d], rects[b]) != 0:
                return 0
            x = cross(rects[a], rects[b], rects[c])
            y = cross(rects[a], rects[b], rects[d])
            if x * y >= 0 or x + y != 0:
                return 0
            return abs(x)

        def dfs(idx, rects):
            if len(points) - idx < 4 - len(rects):
                return 0
            if len(rects) == 4:
                return area(rects)
            ret = float('inf')
            for i in range(idx, len(points)):
                rects.append(points[i])
                a = dfs(i+1, rects)
                if a != 0:
                    ret = min(ret, a)
                rects.pop()
            return ret

        rects = []
        ans = dfs(0, rects)
        ans = 0 if ans == float('inf') else ans
        return ans


points = [[24420,16685],[20235,25520],[14540,20845],[20525,14500],[16876,24557],[24085,23720],[25427,18964],[21036,14573],[24420,23315],[22805,24760],[21547,25304],[16139,23952],[21360,14645],[24715,17120],[19765,25520],[19388,25491],[22340,25005],[25520,19765],[25365,21320],[23124,15443],[20845,14540],[24301,16532],[16685,24420],[25100,17875],[22125,25100],[15699,23468],[14592,21131],[25460,19155],[17837,25084],[23468,24301],[25460,20845],[18453,25304],[21131,14592],[22805,15240],[19475,25500],[15443,23124],[25355,21360],[15285,22880],[20000,25525],[24085,16280],[22163,25084],[22880,15285],[14916,22163],[16280,24085],[24875,17400],[22600,24875],[14573,21036],[25427,21036],[17120,24715],[25500,19475]]
print(Solution().minAreaFreeRect(points))
