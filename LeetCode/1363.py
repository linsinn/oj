class Solution:
	def largestMultipleOfThree(self, digits) -> str:
		su = sum(digits)
		cnt = [0] * 10
		for dig in digits:
			cnt[dig] += 1
		if su % 3 == 1:
			f = False
			for i in range(10):
				if i % 3 == 1 and cnt[i] >= 1:
					cnt[i] -= 1
					f = True
					break
			if not f:
				need = 2
				for i in range(10):
					if i % 3 == 2:
						if cnt[i] >= need:
							cnt[i] -= need
							break
						elif cnt[i] >= 1:
							cnt[i] -= 1
							need = 1
		elif su % 3 == 2:
			f = False
			for i in range(10):
				if i % 3 == 2 and cnt[i] >= 1:
					cnt[i] -= 1
					f = True
					break
			if not f:
				need = 2
				for i in range(10):
					if i % 3 == 1:
						if cnt[i] >= need:
							cnt[i] -= need
							break
						elif cnt[i] >= 1:
							cnt[i] -= 1
							need = 1
		ans = ''
		for i in range(9, -1, -1):
			ans += str(i) * cnt[i]
		if ans == '':
			return ans
		if ans[0] == '0':
			return '0'
		return ans

digits = [1]
print(Solution().largestMultipleOfThree(digits))
