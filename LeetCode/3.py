class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        mps = {}
        i, j = 0, 0
        ans = 0
        while j < len(s):
            if s[j] in mps:
                i = max(i, mps[s[j]] + 1)
            mps[s[j]] = j
            ans = max(ans, j - i + 1)
            j += 1
        return ans


s = "abasdfasdba"
sol = Solution()
print sol.lengthOfLongestSubstring(s)


