class Solution:
	def minFlips(self, a: int, b: int, c: int) -> int:
		ans = 0
		while a != 0 or b != 0 or c != 0:
			x = a % 2
			a //= 2
			y = b % 2
			b //= 2
			z = c % 2
			c //= 2
			if x | y == z:
				continue
			elif z == 1:
				ans += 1
			else:
				ans += x + y
		return ans
