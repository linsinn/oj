from collections import Counter

class Solution:
    def canReorderDoubled(self, A):
        """
        :type A: List[int]
        :rtype: bool
        """
        cnt = Counter(A)
        for a in sorted(A, key=abs):
            if cnt[a] == 0:
                continue
            if cnt[a * 2] == 0:
                return False
            cnt[a] -= 1
            cnt[a * 2] -= 1
        return True
