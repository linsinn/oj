import collections

class Solution:
    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:
        N = len(grid)
        if grid[0][0] == 1 or grid[N-1][N-1] == 1:
            return -1
        que = collections.deque()
        vis = [[False] * N for _ in range(N)]
        vis[0][0] = True
        que.append((0, 0, 1))
        dirs = [[-1, -1], [-1, 0], [-1, 1],
                [0, -1,], [0, 1],
                [1, -1], [1, 0], [1, 1]]
        while len(que) != 0:
            x, y, s = que.popleft()
            if x == N-1 and y == N-1:
                return s
            for dx, dy in dirs:
                nx = x + dx
                ny = y + dy
                if 0 <= nx < N and 0 <= ny < N and not vis[nx][ny] and grid[nx][ny] == 0:
                    vis[nx][ny] = True
                    que.append((nx, ny, s+1))
        return -1
