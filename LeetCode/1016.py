class Solution:
    def queryString(self, S: str, N: int) -> bool:
        seen = set()
        X = 0
        for i in range(len(S)):
            if S[i] == '0':
                continue
            j = i
            num = 0
            while j < len(S) and num <= N:
                num = (num << 1) + (1 if S[j] == '1' else 0)
                if num <= N and num not in seen:
                    X += 1
                    seen.add(num)
                j += 1
        return X == N
