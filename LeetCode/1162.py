import collections


class Solution:
    def maxDistance(self, grid: List[List[int]]) -> int:
        n = len(grid[0])
        que = collections.deque()
        for i in range(n):
            for j in range(n):
                if grid[i][j] == 1:
                    que.append([i, j, 0])
        dist = [[-1] * (n+1) for _ in range(n+1)]
        dirs = [-1, 0, 1, 0, -1]
        while len(que) != 0:
            x, y, s = que.popleft()
            for d in range(4):
                a = x + dirs[d]
                b = y + dirs[d+1]
                if 0 <= a < n and 0 <= b < n and dist[a][b] == -1:
                    dist[a][b] = s + 1
                    que.append([a, b, s + 1])
        ans = -1
        for i in range(n):
            for j in range(n):
                if grid[i][j] == 0:
                    ans = max(ans, dist[i][j])
        return ans


