from LeetCode import Interval


class Solution(object):
    def intervalIntersection(self, A, B):
        """
        :type A: List[Interval]
        :type B: List[Interval]
        :rtype: List[Interval]
        """
        A.sort(key=lambda k: (k.start, k.end))
        B.sort(key=lambda k: (k.start, k.end))
        ans = []
        i, j = 0, 0
        while i < len(A) and j < len(B):
            if B[j].start <= A[i].end:
                if B[j].end >= A[i].start:
                    ans.append(Interval(max(A[i].start, B[j].start),
                                        min(A[i].end, B[j].end)))
                if B[j].end >= A[i].end:
                    i += 1
                else:
                    j += 1
            else:
                i += 1
        return ans


A = [[4,11]]
B = [[1,2],[8,11],[12,13],[14,15],[17,19]]
x = []
y = []
for a in A:
    x.append(Interval(a[0], a[1]))
for b in B:
    y.append(Interval(b[0], b[1]))
for i in Solution().intervalIntersection(x, y):
    print(i.start, i.end)