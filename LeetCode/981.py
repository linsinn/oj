import bisect

class TimeMap:
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.mps = {}

    def set(self, key: 'str', value: 'str', timestamp: 'int') -> 'None':
        if key not in self.mps:
            self.mps[key] = [(timestamp, value)]
        else:
            self.mps[key].append((timestamp, value))

    def get(self, key: 'str', timestamp: 'int') -> 'str':
        if key not in self.mps:
            return ''
        pos = bisect.bisect(self.mps[key], (timestamp, ''))
        if pos >= len(self.mps[key]) or self.mps[key][pos][0] > timestamp:
            return self.mps[key][pos-1][1] if pos > 0 else ''
        else:
            return self.mps[key][pos][1]


tm = TimeMap()
tm.set('foo', 'bar', 1)
print(tm.get('foo', 1))