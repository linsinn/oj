class Solution:
    def longestOnes(self, A, K: int) -> int:
        i, j = 0, 0
        cur_zero = 0
        ans = 0
        while j < len(A):
            if A[j] == 1:
                ans = max(ans, j - i + 1)
            else:
                cur_zero += 1
                if cur_zero > K:
                    while i <= j and A[i] != 0:
                        i += 1
                    if A[i] == 0:
                        i += 1
                    cur_zero -= 1
                ans = max(ans, j - i + 1)
            j += 1
        return ans


A = [0]
K = 1
print(Solution().longestOnes(A, K))