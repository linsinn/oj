class Solution:
    def numRookCaptures(self, board: List[List[str]]) -> int:
        N, M = 8, 8
        st = None
        for i in range(N):
            for j in range(N):
                if board[i][j] == 'R':
                    st = [i, j]

        ans = 0
        i, j = st[0] - 1, st[1]
        while i >= 0:
            if board[i][j] == 'p':
                ans += 1
                break
            elif board[i][j] == 'B':
                break
            i -= 1

        i, j = st[0] + 1, st[1]
        while i < N:
            if board[i][j] == 'p':
                ans += 1
                break
            elif board[i][j] == 'B':
                break
            i += 1

        i, j = st[0], st[1] - 1
        while j >= 0:
            if board[i][j] == 'p':
                ans += 1
                break
            elif board[i][j] == 'B':
                break
            j -= 1

        i, j = st[0], st[1] + 1
        while j < M:
            if board[i][j] == 'p':
                ans += 1
                break
            elif board[i][j] == 'B':
                break
            j += 1

        return ans
