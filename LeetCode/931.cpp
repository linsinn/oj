#include <bits/stdc++.h>
using namespace std;

class Solution {
	int N;
	vector<vector<int>> dp;

	int dfs(int i, int j, vector<vector<int>>& A) {
		if (i >= N)
			return 0;
		if (dp[i][j] != 0x7fffffff)
			return dp[i][j];
		int mini = 0x7fffffff;
		for (int d = -1; d <= 1 ; d++) {
			int k = j + d;
			if (0 <= k && k < N) {
				mini = min(mini, dfs(i+1, k, A));
			}
		}
		dp[i][j] = A[i][j] + mini;
		return dp[i][j];
	}
public:
	int minFallingPathSum(vector<vector<int>>& A) {
		N = A.size();
		dp.resize(N, vector<int>(N, 0x7fffffff));
		int ans = 0x7fffffff;
		for (int i = 0; i < N; i++) {
			ans = min(ans, dfs(0, i, A));
		}
		return ans;
	}
};

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    return 0;
}
