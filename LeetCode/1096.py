class Solution:
    def dfs(self, l, r, expr, match):
        cnts = 0
        comma = []
        if expr[l] == '{' and match[l] == r:
            l += 1
            r -= 1
        for i in range(l, r+1):
            if expr[i] == '{':
                cnts += 1
            elif expr[i] == '}':
                cnts -=1
            elif expr[i] == ',' and cnts == 0:
                comma.append(i)
        s = set()
        if len(comma) == 0:
            k = l
            while k <= r:
                if expr[k] == '{':
                    g = match[k]
                    ret = self.dfs(k, g, expr, match)
                    if len(s) == 0:
                        s = ret
                    else:
                        tmp = set()
                        for c in s:
                            for d in ret:
                                tmp.add(c + d)
                        s = tmp
                    k = g + 1
                else:
                    g = k
                    while g <= r and expr[g].isalpha():
                        g += 1
                    if len(s) == 0:
                        s.add(expr[k:g])
                    else:
                        tmp = set()
                        for c in s:
                            tmp.add(c + expr[k:g])
                        s = tmp
                    k = g
        else:
            for i in range(len(comma)+1):
                lb = comma[i-1]+1 if i > 0 else l
                rb = comma[i]-1 if i < len(comma) else r
                s = s.union(self.dfs(lb, rb, expr, match))
        return s

    def braceExpansionII(self, expression: str):
        stk = []
        match = {}
        for i, e in enumerate(expression):
            if e == '{':
                stk.append(i)
            elif e == '}':
                match[stk.pop()] = i
        return sorted(list(self.dfs(0, len(expression)-1, expression, match)))


s = "{a,b}c{d,e}f"
# s = "{a,b}"
print(Solution().braceExpansionII(s))