class Solution:
    def maxScoreSightseeingPair(self, A: List[int]) -> int:
        best = len(A) - 1
        ans = 0
        for i in range(len(A)-2, -1, -1):
            if A[i] + A[best] + i - best < A[i] + A[i+1] - 1:
                best = i + 1
            ans = max(ans, A[i] + A[best] + i - best)
        return ans