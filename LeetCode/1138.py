import collections

class Solution:
    def bfs(self, x, y, target):
        que = collections.deque()
        dirs = [-1, 0, 1, 0, -1]
        da = ["U", "R", "D", "L"]
        que.append([x, y, ""])
        seen = {x, y}
        while len(que) != 0:
            cx, cy, s = que.popleft()
            if self.board[cx][cy] == target:
                return cx, cy, s
            for i in range(4):
                nx = cx + dirs[i]
                ny = cy + dirs[i+1]
                if (nx, ny) not in seen and 0 <= nx < 6 and 0 <= ny < 5:
                    if nx < 5 or ny == 0:
                        que.append((nx, ny, s + da[i]))
                        seen.add((nx, ny))


    def alphabetBoardPath(self, target: str) -> str:
        self.board = ["abcde", "fghij", "klmno", "pqrst", "uvwxy", "z"]
        ans = ""
        x, y = 0, 0
        for c in target:
            x, y, s = self.bfs(x, y, c)
            ans += s + "!"
        return ans


target = "t"
print(Solution().alphabetBoardPath(target))