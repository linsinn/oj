//
// Created by sinn on 1/22/19.
//

#include <bits/stdc++.h>
using namespace std;


class Solution {
public:
    int maxTurbulenceSize(vector<int>& A) {
        int ans = 1;
        int odd = 1, even = 1;
        for (int i = 1; i < A.size(); i++) {
            if (i & 1) {
                if (A[i] > A[i-1]) {
                    odd += 1;
                    even = 1;
                } else if (A[i] < A[i-1]){
                    odd = 1;
                    even += 1;
                } else {
                    odd = 1;
                    even = 1;
                }
            } else {
                if (A[i] > A[i-1]) {
                    odd = 1;
                    even += 1;
                } else if (A[i] < A[i-1]){
                    odd += 1;
                    even = 1;
                } else {
                    odd = 1;
                    even = 1;
                }

            }
            ans = max({ans, even, odd});
        }
        return ans;
    }
};