class Solution:
    def twoCitySchedCost(self, costs: List[List[int]]) -> int:
        c = sorted(costs, key=lambda k: k[0] - k[1])
        ans = 0
        for i in range(len(costs) // 2):
            ans += c[i][0]
            ans += c[len(costs)-1-i][1]
        return ans
