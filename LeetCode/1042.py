import collections


class Solution:
    def gardenNoAdj(self, N: int, paths: List[List[int]]) -> List[int]:
        adj = collections.defaultdict(set)
        for path in paths:
            adj[path[0]].add(path[1])
            adj[path[1]].add(path[0])

        colors = [0] * N

        def dfs(node):
            if colors[node-1] != 0:
                return
            c = [0, 1, 1, 1, 1]
            for a in adj[node]:
                if colors[a-1] != 0:
                    c[colors[a-1]] = 0
            for i in range(len(c)):
                if c[i] != 0:
                    colors[node-1] = i
                    break
            for a in adj[node]:
                dfs(a)

        for i in range(1, N+1):
            if colors[i-1] == 0:
                dfs(i)

        return colors
