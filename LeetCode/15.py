class Solution:
	def threeSum(self, nums: List[int]) -> List[List[int]]:
		N = len(nums)
		mp = set()
		ans = set()
		for i in range(N):
			for j in range(i+1, N):
				t = -(nums[i] + nums[j])
				if t in mp:
					ans.add(tuple(sorted([t, nums[i], nums[j]])))
			mp.add(nums[i])
		res = []
		for arr in ans:
			res.append(list(arr))
		return res
