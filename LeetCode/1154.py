class Solution:
	def dayOfYear(self, date: str) -> int:
		g = list(map(int, date.split('-')))
		m = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
		if (g[0] % 100 != 0 and g[0] % 4 == 0) or (g[0] % 400 == 0):
			m[1] += 1
		i = 0
		ans = 0
		while i < g[1] - 1:
			ans += m[i]
			i += 1
		return ans + g[2]
