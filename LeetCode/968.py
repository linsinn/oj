from LeetCode import TreeNode

class Solution:

    def dfs(self, root, idx, vis):
        if root is None:
            return
        self.dfs(root.left, idx * 2, vis)
        self.dfs(root.right, idx * 2 + 1, vis)
        if idx in vis or idx // 2 in vis or idx * 2 in vis or idx * 2 + 1 in vis:
            return
        if idx == 1:
            vis.add(idx)
        else:
            vis.add(idx // 2)

    def minCameraCover(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        vis = set()
        self.dfs(root, 1, vis)
        return len(vis)
