class Solution(object):
    def numPermsDISequence(self, S):
        """
        :type S: str
        :rtype: int
        """
        MOD = int(1e9 + 7)
        dp = {}
        if S[-1] == 'D':
            dp[1] = 1
        else:
            dp[0] = 1
        for i in xrange(len(S)-2, -1, -1):
            tmp = {}
            for k, v in dp.iteritems():
                if S[i] == 'D':
                    for j in xrange(max(1, k+1), len(S) - i + 1):
                        t = tmp.get(j, 0)
                        tmp[j] = (t + v) % MOD
                else:
                    for j in xrange(0, min(k+1, len(S) - i)):
                        t = tmp.get(j, 0)
                        tmp[j] = (t + v) % MOD
            dp = tmp
        ans = 0
        for v in dp.itervalues():
            ans = (ans + v) % MOD
        return ans

                
            
