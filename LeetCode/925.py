class Solution(object):
    def isLongPressedName(self, name, typed):
        """
        :type name: str
        :type typed: str
        :rtype: bool
        """

        def compress(S):
            arr = []
            for i in xrange(len(S)):
                if i - 1 >= 0 and S[i] == S[i-1]:
                    arr[-1][1] += 1
                else:
                    arr.append([S[i], 1])
            return arr

        a = compress(name)
        b = compress(typed)

        if len(a) != len(b):
            return False

        for x, y in zip(a, b):
            if x[0] != y[0] or x[1] > y[1]:
                return False
        return True

