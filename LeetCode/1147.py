class Solution:
    def longestDecomposition(self, S):
        res, l, r = 0, "", ""
        for i, j in zip(S, S[::-1]):
            l, r = l + i, j + r
            if l == r:
                res, l, r = res + 1, "", ""
        return res
    # def findKMP(self, text: str, base):
    #     self.kmps[base][base] = 0
    #     for i in range(base+1, self.N):
    #         j = self.kmps[base][i-1]
    #         while j != 0 and self.text[base+j] != text[i]:
    #             j = self.kmps[base][base+j-1]
    #         self.kmps[base][i] = j + (1 if text[base+j] == text[i] else 0)
    #
    # def dfs(self, st, ed):
    #     if st == ed:
    #         return 1
    #     if st > ed:
    #         return 0
    #     if self.dp[st][ed] != 0:
    #         return self.dp[st][ed]
    #     l = self.kmps[st][ed]
    #     if l == 0:
    #         self.dp[st][ed] = 1
    #         return 1
    #     cur = 0
    #     if l <= (ed - st + 1) // 2:
    #         cur = self.dfs(st+l, ed-l) + self.dfs(st, st+l-1) * 2
    #         self.dp[st][ed] = cur
    #     else:
    #         for i in range(1, (ed-st+1)//2+1):
    #             if self.text[st:st+i] == self.text[ed+1-i:ed+1]:
    #                 cur = self.dfs(st+i, ed-i) + self.dfs(st, st+i-1) * 2
    #                 self.dp[st][ed] = cur
    #                 return cur
    #         self.dp[st][ed] = cur = 1
    #     return cur
    #
    # def longestDecomposition(self, text: str) -> int:
    #     self.text = text
    #     self.N = len(text)
    #     self.kmps = [[0] * (self.N+1) for _ in range(self.N+1)]
    #     for i in range(self.N):
    #         self.findKMP(text, i)
    #     self.dp = [[0] * (self.N+1) for _ in range(self.N+1)]
    #     return self.dfs(0, self.N-1)


text = "abba"
print(Solution().longestDecomposition(text))



