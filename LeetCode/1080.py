from LeetCode import TreeNode


class Solution:
    def dfs(self, root: TreeNode, idx, su, path):
        path.append(idx)
        if root.left is None and root.right is None:
            self.paths.append([path[:], su + root.val])
            path.pop()
            return
        if root.left is not None:
            self.dfs(root.left, idx<<1, su+root.val, path)
        if root.right is not None:
            self.dfs(root.right, idx<<1|1, su+root.val, path)
        path.pop()

    def delete(self, root, idx):
        lc = idx << 1
        rc = idx << 1 | 1
        f = True
        for path, v in self.paths:
            if lc in path and v >= self.limit:
                f = False
                break
        if f:
            root.left = None
        else:
            self.delete(root.left, lc)

        f = True
        for path, v in self.paths:
            if rc in path and v >= self.limit:
                f = False
                break
        if f:
            root.right = None
        else:
            self.delete(root.right, rc)

    def sufficientSubset(self, root: TreeNode, limit: int) -> TreeNode:
        self.paths = []
        self.limit = limit
        self.dfs(root, 1, 0, [])
        f = True
        for path, v in self.paths:
            if 1 in path and v >= self.limit:
                f = False
                break
        if f:
            return None
        self.delete(root, 1)
        return root


root = TreeNode(4)
root.left = TreeNode(10)
root.left.left = TreeNode(3)
root.right = TreeNode(15)
# root.right.left = TreeNode(4)
# root.right.left.right = TreeNode(3)
print(Solution().sufficientSubset(root, 18))
