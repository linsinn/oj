from LeetCode import TreeNode


class Solution:
    def isCousins(self, root: 'TreeNode', x: 'int', y: 'int') -> 'bool':
        def findNode(root, dep, pa, var):
            if root is None:
                return None, None
            if root.val == var:
                return dep, pa
            d, p = findNode(root.left, dep+1, root, var)
            if d is not None:
                return d, p
            else:
                return findNode(root.right, dep+1, root, var)

        dx, px = findNode(root, 0, root, x)
        dy, py = findNode(root, 0, root, y)

        return dx == dy and px != py
