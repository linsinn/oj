class Solution(object):
    def maxSubarraySumCircular(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        def kadane(gen):
            ans = cur = 0
            for x in gen:
                cur = x + max(cur, 0)
                ans = max(ans, cur)
            return ans

        S = sum(A)
        ans1 = kadane(iter(A))
        ans2 = S + kadane(-A[i] for i in xrange(1, len(A)))
        ans3 = S + kadane(-A[i] for i in xrange(0, len(A)-1))
        return max(ans1, ans2, ans3)


w = [-17,29,-12,30,15,10,27,2,-12,-1,-4,28,29,-18,0,2,-20,21,-16,21,25,-2,-6,13,-18,-25,-12,22,-27,-7]
s = Solution()
print s.maxSubarraySumCircular(w)
