class Solution:
    def isValid(self, S: str) -> bool:
        idx = S.find('abc')
        while len(S) != 0 and idx != -1:
            S = S[0:idx] + S[idx+3:]
            idx = S.find('abc')
        return len(S) == 0
