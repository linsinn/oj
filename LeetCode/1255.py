import collections


class Solution:
	def maxScoreWords(self, words: List[str], letters: List[str], score: List[int]) -> int:
		cnt = collections.Counter(letters)
		N = len(words)
		ws = [0] * len(words)
		for i, word in enumerate(words):
			s = 0
			for ch in word:
				s += score[ord(ch) - ord('a')]
			ws[i] = s
		ans = 0
		for i in range(1 << N):
			tmp = collections.defaultdict(lambda : 0)
			sc = 0
			for j in range(N):
				if ((1 << j) & i) != 0:
					sc += ws[j]
					for ch in words[j]:
						tmp[ch] += 1
			f = True
			for k, v in tmp.items():
				if v > cnt.get(k, 0):
					f = False
					break
			if f:
				ans = max(ans, sc)
		return ans



