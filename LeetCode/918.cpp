#include <bits/stdc++.h>
using namespace std;


class Solution {
public:
    int maxSubarraySumCircular(vector<int>& A) {
        int N = A.size();
        for (int i = 0; i < N; i++) {
            A.emplace_back(A[i]);
        }
        vector<int> P(A.size() + 1, 0);
        for (int i = 1; i < P.size(); i++)
            P[i] = P[i-1] + A[i-1];
        int ans = A[0];
        list<int> deque;
        deque.emplace_back(0);
        for (int i = 1; i < P.size(); i++) {
            if (deque.front() < i - N)
                deque.pop_front();

            ans = max(ans, P[i] - P[deque.front()]);
            while (!deque.empty() && P[i] <= P[deque.back()])
                deque.pop_back();
            deque.emplace_back(i);
        }
        return ans;
    }
};


int main() {
    vector<int> A = {-5,3,5};
    Solution s;
    cout << s.maxSubarraySumCircular(A) << '\n';
    return 0;
}
