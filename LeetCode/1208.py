class Solution:
  def equalSubstring(self, s: str, t: str, maxCost: int) -> int:
    N = len(s)
    arr = [0] * N
    for i in range(N):
      k = abs(ord(s[i]) - ord(t[i]))
      arr[i] = k
    cost = 0
    ans = 0
    i, j = 0, -1
    while j + 1 < N:
      cost += arr[j+1]
      j += 1
      if cost <= maxCost:
        ans = max(ans, j - i + 1)
      while cost > maxCost:
        cost -= arr[i]
        i += 1
    return ans


s = "abcd"
t = "abcd"
cost = 0
print(Solution().equalSubstring(s, t, cost))