class Solution:
    def removeDuplicates(self, S: str) -> str:
        x = [""] * 2
        x[0] = S
        cur = 0
        f = False
        while True:
            i, j = 0,  1
            while i < len(x[cur]):
                while j < len(x[cur]) and x[cur][j] == x[cur][i]:
                    j += 1
                if (j - i) % 2 == 1:
                    x[cur^1] += x[cur][i]
                if j - i > 1:
                    f = True
                i = j
                j = i+1
            x[cur] = x[cur^1]
            x[cur^1] = ""
            if not f:
                break
            f = False
        return x[cur]


S = "aaaaaaaaa"
print(Solution().removeDuplicates(S))