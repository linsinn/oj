//
// Created by sinn on 8/18/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;


template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

class Solution {
  using ll = long long;
  using Pii = pair<int, int>;
  using Pll = pair<ll, ll>;
  using Vec = vector<int>;
public:
  string lastSubstring(string s) {
    s += '$';
    Vec arr = SuffixArray(s);
    string ans = s.substr(arr.back());
    ans.pop_back();
    return ans;
  }

  Vec SuffixArray(const string &s) {
    int n = s.size();
    const int A = 256;
    Vec p(n), c(n), cnt(max(A, n), 0);
    fora (i, 0, n) cnt[s[i]]++;
    fora (i, 1, A) cnt[i] += cnt[i-1];
    fora (i, 0, n)
      p[--cnt[s[i]]] = i;
    c[p[0]] = 0;
    int classes = 1;
    fora (i, 1, n) {
      if (s[p[i-1]] != s[p[i]])
        ++classes;
      c[p[i]] = classes - 1;
    }
    Vec pn(n), cn(n);
    for (int h = 0; (1 << h) < n; ++h) {
      fora (i, 0, n) {
        pn[i] = p[i] - (1 << h);
        if (pn[i] < 0) pn[i] += n;
      }
      fill(cnt.begin(), cnt.begin() + classes, 0);
      fora (i, 0, n) ++cnt[c[i]];
      fora (i, 1, classes) cnt[i] += cnt[i-1];
      ford (i, n-1, 0)
        p[--cnt[c[pn[i]]]] = pn[i];
      cn[p[0]] = 0;
      classes = 1;
      fora (i, 1, n) {
        Pii cur = {c[p[i]], c[(p[i] + (1 << h)) % n]};
        Pii pre = {c[p[i-1]], c[(p[i-1] + (1 << h)) % n]};
        if (cur != pre)
          ++classes;
        cn[p[i]] = classes - 1;
      }
      c.swap(cn);
    }
    return p;
  }
};

void Solution() {
  auto vec = SuffixArray(S);
  fora (i, 0, S.size()) {
    cout << S[(vec[0] + i) % S.size()];
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> S) {
    Solution();
  }
  return 0;
}

