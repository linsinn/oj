//
// Created by sinn on 3/9/19.
//

#include "LeetCode.h"

class Solution {
    int K;
    int memos[33][33][33];
    int pre_sum[40];

    int dp(int n, int a, int b) {
        if (memos[n][a][b] != -1)
            return memos[n][a][b];
        int len = b - a + 1;
        if ((len - n) % (K - 1) != 0)
            return -1;
        if (a == b)
            return n == 1 ? 0 : -1;
        if (n == 1)
            return dp(K, a, b) + pre_sum[b+1] - pre_sum[a];
        int cur = -1;
        for (int mid = a; mid < b; mid += K-1) {
            int i = dp(1, a, mid);
            int j = dp(n-1, mid+1, b);
            if (i != -1 && j != -1) {
                cur = (cur == -1 ? i + j : min(cur, i + j));
            }
        }
        memos[n][a][b] = cur;
        return cur;
    }

public:
    int mergeStones(vector<int>& stones, int K) {
        int N = stones.size();
        this->K = K;
        if ((N-1) % (K-1) != 0)
            return -1;
        memset(memos, -1, sizeof(memos));
        memset(pre_sum, 0, sizeof(pre_sum));
        for (int i = 1; i <= N; i ++) {
            pre_sum[i] = pre_sum[i-1] + stones[i-1];
        }
        return dp(1, 0, N-1);
    }
};