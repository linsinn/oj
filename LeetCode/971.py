class Solution:
	def dfs(self, root, voyage, st, ed, ans):
		if root is None:
			return
		if root.val != voyage[st]:
			ans.append(-1)
			return
		l, r = -1, -1
		try:
			if root.left:
				l = voyage.index(root.left.val)
			if root.right:
				r = voyage.index(root.right.val)
		except ValueError:
			ans.append(-1)
			return
		if l == -1:
			if r == -1:
				return
			else:
				if r != st + 1:
					ans.append(-1)
					return
				self.dfs(root.right, voyage, st + 1, ed, ans)
		else:
			if r == -1:
				if l != st + 1:
					ans.append(-1)
					return
				self.dfs(root.left, voyage, st + 1, ed, ans)
			else:
				if l == st + 1:
					self.dfs(root.left, voyage, st + 1, r, ans)
					self.dfs(root.right, voyage, r, ed, ans)
				elif r == st + 1:
					ans.append(root.val)
					self.dfs(root.right, voyage, st + 1, r, ans)
					self.dfs(root.left, voyage, l, ed, ans)
				else:
					ans.append(-1)

	def flipMatchVoyage(self, root, voyage):
		"""
		:type root: TreeNode
		:type voyage: List[int]
		:rtype: List[int]
		"""
		ans = []
		self.dfs(root, voyage, 0, len(voyage), ans)
		if -1 in ans:
			return [-1]
		return ans
