//
// Created by sinn on 12/15/19.
//

#include "LeetCode.h"

struct Node {
	int x, y;
	int used;
	Node() = default;
	Node(int _x, int _y, int _u) : x{_x}, y{_y}, used{_u} {}
	bool operator < (const Node& rhs) const {
		if (used != rhs.used)
			return used < rhs.used;
		if (x != rhs.x)
			return x < rhs.x;
		return y < rhs.y;
	}
};

class Solution {
public:
	int shortestPath(vector<vector<int>>& grid, int k) {
		int N = grid.size(), M = grid[0].size();
		const int dirs[] = {-1, 0, 1, 0, -1};
		queue<pair<Node, int>> que;
		Node st(0, 0, 0);
		que.emplace(st, 0);
		set<Node> seen = {st};
		while (!que.empty()) {
			Node cur = que.front().first;
			int step = que.front().second;
			que.pop();
			if (cur.x == N-1 && cur.y == M-1)
				return step;
			for (int d = 0; d < 4; ++d) {
				int nx = cur.x + dirs[d];
				int ny = cur.y + dirs[d+1];
				if (nx < 0 || nx >= N || ny < 0 || ny >= M)
					continue;
				Node nxt(nx, ny, cur.used);
				if (grid[nx][ny] == 0) {
					if (seen.find(nxt) == seen.end()) {
						que.emplace(nxt, step + 1);;
						seen.emplace(nxt);
					}
				} else {
					if (cur.used < k) {
						nxt.used += 1;
						if (seen.find(nxt) == seen.end()) {
							que.emplace(nxt, step + 1);;
							seen.emplace(nxt);
						}
					}
				}
			}
		}
		return -1;
	}
};
