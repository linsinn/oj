class Solution:
    def maxUncrossedLines(self, A, B) -> int:
        N = len(A) + 1
        M = len(B) + 1
        f = [[0] * M for _ in range(N)]
        for i in range(1, N):
            for j in range(1, M):
                f[i][j] = max(f[i-1][j-1] + (1 if A[i-1] == B[j-1] else 0), f[i-1][j], f[i][j-1])
        return f[N-1][M-1]

A = [1,3,7,1,7,5]
B = [1,9,2,5,1]
print(Solution().maxUncrossedLines(A, B))