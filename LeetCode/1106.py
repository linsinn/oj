class Solution:
    def dfs(self, lb, rb, expr, matched):
        if expr[lb] == '!':
            return not self.dfs(lb + 2, rb-1, expr, matched)
        if expr[lb] == 'f':
            return False
        if expr[lb] == 't':
            return True
        op = expr[lb]
        lb += 2
        rb -= 1
        commas = []
        level = 0
        for i in range(lb, rb + 1):
            if expr[i] == ',' and level == 0:
                commas.append(i)
            if expr[i] == '(':
                level += 1
            if expr[i] == ')':
                level -= 1
        commas.append(rb+1)
        bs = []
        pre = lb
        for c in commas:
            bs.append(self.dfs(pre, c - 1, expr, matched))
            pre = c + 1
        if op == '&':
            ret = True
            for b in bs:
                ret = ret and b
            return ret
        if op == '|':
            ret = False
            for b in bs:
                ret = ret or b
            return ret

    def parseBoolExpr(self, expression: str) -> bool:
        matched = {}
        stk = []
        for i, p in enumerate(expression):
            if p == '(':
                stk.append(i)
            elif p == ')':
                matched[stk.pop()] = i
        return self.dfs(0, len(expression)-1, expression, matched)


expr = "|(&(t,f,t),!(t))"
print(Solution().parseBoolExpr(expr))