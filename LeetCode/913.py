import collections

class Solution(object):
	def catMouseGame(self, graph):
		N = len(graph)

		def parents(m, c, t):
			if t == 2:
				for m2 in graph[m]:
					yield m2, c, 3-t
			else:
				for c2 in graph[c]:
					if c2:
						yield m, c2, 3-t

		DRAW, MOUSE, CAT = 0, 1, 2
		color = collections.defaultdict(int)

		degree = {}
		for m in xrange(N):
			for c in xrange(N):
				degree[m, c, 1] = len(graph[m])
				degree[m, c, 2] = len(graph[c]) - (0 in graph[c])

		que = collections.deque()
		for i in xrange(N):
			for t in xrange(1, 3):
				color[0, i, t] = MOUSE
				que.append((0, i, t, MOUSE))
				if i > 0:
					color[i, i, t] = CAT
					que.append((i, i, t, CAT))

		while que:
			i, j, t, c = que.popleft()
			for i2, j2, t2, in parents(i, j, t):
				if color[i2, j2, t2] is DRAW:
					if t2 == c:
						color[i2, j2, t2] = c
						que.append((i2, j2, t2, c))
					else:
						degree[i2, j2, t2] -= 1
						if degree[i2, j2, t2] == 0:
							color[i2, j2, t2] = 3 - t2
							que.append((i2, j2, t2, 3 - t2))
		return color[1, 2, 1]
			
					
