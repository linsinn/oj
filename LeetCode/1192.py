class Solution:
  def criticalConnections(self, n: int, connections):
    gph = [[] for _ in range(n)]
    for conn in connections:
      gph[conn[0]].append(conn[1])
      gph[conn[1]].append(conn[0])

    seen = [False] * n
    tin = [0] * n
    low = [0] * n
    timer = [0]

    ans = []

    def dfs(u, p=-1):
      seen[u] = True
      timer[0] += 1
      tin[u] = low[u] = timer[0]
      for v in gph[u]:
        if v == p:
          continue
        if seen[v]:
          low[u] = min(low[u], tin[v])
        else:
          dfs(v, u)
          low[u] = min(low[u], low[v])
          if low[v] > tin[u]:
            ans.append([u, v])

    dfs(0)
    return ans


n = 4
conn = [[0,1],[1,2],[2,0],[1,3]]
print(Solution().criticalConnections(n, conn))

