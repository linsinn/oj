class Solution:
  def helper(self, arr):
    ret = 0
    mem = 0
    for i in range(len(arr)):
      ret = max(ret, mem + arr[i])
      mem = max(0, mem + arr[i])
    return ret

  def kConcatenationMaxSum(self, arr, k: int) -> int:
    self.MOD = 10 ** 9 + 7
    N = len(arr)
    p, q = 0, 0
    su = 0
    for i in range(N):
      p = min(p, su + arr[i])
      su += arr[i]
    su = 0
    for i in range(N-1, -1, -1):
      q = min(q, su + arr[i])
      su += arr[i]
    p = min(p, 0)
    q = min(q, 0)
    su = sum(arr)
    ans = 0
    if su >= 0 and k >= 2:
      ans = su * k - p - q
    if k >= 2:
      ans = max(ans, self.helper(arr + arr))
    else:
      ans = max(ans, self.helper(arr))
    return ans % self.MOD


arr = [-5,-2,0,0,3,9,-2,-5,4]
k = 5
print(Solution().kConcatenationMaxSum(arr, k))