from collections import deque


class Solution:
    def minKBitFlips(self, A: 'List[int]', K: 'int') -> 'int':
        ans = 0
        flip_points = deque()
        for i in range(len(A) - K + 1):
            while len(flip_points) != 0 and flip_points[0]+K-1 < i:
                flip_points.popleft()
            if A[i] ^ (len(flip_points) % 2) == 0:
                flip_points.append(i)
                ans += 1
        for i in range(len(A)-K+1, len(A)):
            while len(flip_points) != 0 and flip_points[0]+K-1 < i:
                flip_points.popleft()
            if A[i] ^ (len(flip_points) % 2) == 0:
                return -1
        return ans


A = [0,0,0,1,0,1,1,0]
K = 3
print(Solution().minKBitFlips(A, K))