//
// Created by sinn on 10/20/19.
//

#include "LeetCode.h"

struct Job {
  int s, e;
  int p;
  Job() = default;
  Job(int _s, int _e, int _p) {
    s = _s, e = _e, p = _p;
  }
  bool operator < (const Job& b) const {
    return e < b.e;
  }
};

class Solution {
public:
  int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
    vector<Job> jobs;
    for (int i = 0; i < startTime.size(); ++i) {
      jobs.emplace_back(startTime[i], endTime[i], profit[i]);
    }
    sort(jobs.begin(), jobs.end());
    map<int, int> mp;
    for (auto &job: jobs) {
      auto p = mp.upper_bound(job.s);
      if (p == mp.begin()) {
        if (mp.count(job.e) == 0)
          mp[job.e] = job.p;
        else
          mp[job.e] = max(mp[job.e], job.p);
        auto q = mp.lower_bound(job.e);
        if (q != mp.begin()) {
          mp[job.e] = max(mp[job.e], prev(q)->second);
        }
      } else {
        p = prev(p);
        if (mp.count(job.e) == 0)
          mp[job.e] = p->second + job.p;
        else
          mp[job.e] = max(mp[job.e], p->second + job.p);
        auto q = mp.lower_bound(job.e);
        if (q != mp.begin()) {
          mp[job.e] = max(mp[job.e], prev(q)->second);
        }
      }
    }
    int ans = 0;
    for (auto &p : mp) {
      ans = max(ans, p.second);
    }
    return ans;
  }
};

int main() {
  vector<int> s = {4,2,4,8,2};
  vector<int> e = {5,5,5,10,8};
  vector<int> p = {1,2,8,10,4};
  cout << Solution().jobScheduling(s, e, p) << '\n';
}