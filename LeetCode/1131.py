class Solution:
    def maxAbsValExpr(self, arr1, arr2) -> int:
        g = [0] * 4
        g[0] = g[1] = arr1[0] + arr2[0]
        g[2] = arr2[0] - arr1[0]
        g[3] = -g[2]
        ans = 0
        for i in range(1, len(arr1)):
            a, b = arr1[i], arr2[i]
            ans = max(ans, a + b - g[0] + i)
            ans = max(ans, g[1] - a - b + i)
            ans = max(ans, a - b + g[2] + i)
            ans = max(ans, b - a + g[3] + i)
            g[0] = min(g[0], a + i + b + i - i)
            g[1] = max(g[1], a - i + b - i + i)
            g[2] = max(g[2], b - i - (a + i) + i)
            g[3] = max(g[3], a - i - (b + i) + i)
        return ans


# arr1 = [1, -5, 0, 10]
# arr2 = [0, -1, -7, -4]
arr1 = [-5, 1, 4, -10, -7, -5, -2, -7, -2, 10, 1, -8, -8, -8, 2, -3, -5, -3, -4, 3]
arr2 = [3, 9, -6, 6, -8, -2, 8, 8, -6, 7, 5, 0, -4, 5, -10, -5, -9, -10, 8, 10]
print(Solution().maxAbsValExpr(arr1, arr2))
