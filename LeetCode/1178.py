class Solution:
  def findNumOfValidWords(self, words: List[str], puzzles: List[str]) -> List[int]:
    mp = {}
    for word in words:
      mask = 0
      for a in word:
        mask |= 1 << (ord(a) - ord('a'))
      mp[mask] = mp.get(mask, 0) + 1

    ans = [0] * len(puzzles)
    for i, puzzle in enumerate(puzzles):
      mask = 0
      for a in puzzle:
        mask |= 1 << (ord(a) - ord('a'))
      sub_mask = mask
      h = 1 << (ord(puzzle[0]) - ord('a'))
      while sub_mask != 0:
        if (sub_mask & h) != 0:
          ans[i] += mp.get(sub_mask, 0)
        sub_mask = (sub_mask - 1) & mask
    return ans


