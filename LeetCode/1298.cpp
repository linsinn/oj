//
// Created by sinn on 12/22/19.
//

#include "LeetCode.h"

class Solution {
public:
	int maxCandies(vector<int>& status, vector<int>& candies, vector<vector<int>>& keys, vector<vector<int>>& containedBoxes, vector<int>& initialBoxes) {
		int N = status.size();
		set<int> ss[2];
		int ans = 0;
		queue<int> que;
		for (int i : initialBoxes) {
			if (status[i] == 0) {
				ss[0].emplace(i);
	 		} else {
				que.emplace(i);
			}
		}
		vector<bool> seen(N, false);
		while (!que.empty()) {
			int idx = que.front();
			que.pop();
			if (seen[idx])
				continue;
			ans += candies[idx];

			for (int b : containedBoxes[idx]) {
				if (status[b] == 1) {
					que.emplace(b);
				} else {
					if (ss[1].count(b)) {
						que.emplace(b);
					} else {
						ss[0].emplace(b);
					}
				}
			}

			for (int k : keys[idx]) {
				if (ss[0].count(k)) {
					que.emplace(k);
					ss[0].erase(k);
				} else {
					ss[1].emplace(k);
				}
			}

		}
		return ans;
	}
};