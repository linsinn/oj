class Solution:
  def minCostToMoveChips(self, chips: List[int]) -> int:
    t = [0, 0]
    for c in chips:
      t[c % 2] += 1
    return min(t)