class SnapshotArray:
    def __init__(self, length: int):
        self.arr = [[] for _ in range(length)]
        self.snap_id = 0

    def set(self, index: int, val: int) -> None:
        self.arr[index].append([self.snap_id, val])

    def snap(self) -> int:
        self.snap_id += 1
        return self.snap_id - 1

    def get(self, index: int, snap_id: int) -> int:
        lb, rb = 0, len(self.arr[index])-1
        ans = 0
        while lb <= rb:
            m = (lb + rb) // 2
            if self.arr[index][m][0] <= snap_id:
                ans = self.arr[index][m][1]
                lb = m + 1
            else:
                rb = m - 1
        return ans
