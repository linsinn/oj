import collections


class Solution:
  def smallestStringWithSwaps(self, s: str, pairs) -> str:
    mp = collections.defaultdict(list)
    for a, b in pairs:
      mp[a].append(b)
      mp[b].append(a)

    N = len(s)
    seen = [0] * N

    def dfs(u, f):
      seen[u] = f
      for v in mp[u]:
        if seen[v] == 0:
          dfs(v, f)

    f = 1
    for i in range(N):
      if seen[i] == 0:
        dfs(i, f)
      f += 1

    ans = list(s)
    col = collections.defaultdict(list)
    for i in range(N):
      if seen[i] != 0:
        col[seen[i]].append(i)

    for v in col.values():
      tmp = []
      for idx in v:
        tmp.append(ans[idx])
      tmp.sort()
      for i, idx in enumerate(v):
        ans[idx] = tmp[i]
    return ''.join(ans)


s = "dcab"
pairs = [[0, 3], [1, 2]]
print(Solution().smallestStringWithSwaps(s, pairs))