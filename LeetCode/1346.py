import os


class Solution:
	def checkIfExist(self, arr: List[int]) -> bool:
		N = len(arr)
		for i in range(N):
			for j in range(N):
				if i == j:
					continue
				if arr[i] * 2 == arr[j] or arr[j] * 2 == arr[i]:
					return True
		return False
