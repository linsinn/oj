class Solution:
	def removePalindromeSub(self, s: str) -> int:
		N = len(s)
		if N == 0:
			return 0
		i, j = 0, N-1
		while i < j:
			if s[i] != s[j]:
				return 2
			i += 1
			j -= 1
		return 1
