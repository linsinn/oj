from LeetCode import ListNode
from collections import deque


class Solution:
    def nextLargerNodes(self, head: ListNode):
        def get_len(node):
            if not node:
                return 0
            return get_len(node.next) + 1
        ans = [0] * get_len(head)

        st = []
        i = 0
        while head:
            while len(st) != 0 and st[-1][1] < head.val:
                ans[st[-1][0]] = head.val
                st.pop()
            st.append((i, head.val))
            i += 1
            head = head.next
        while len(st) != 0:
            ans[st[-1][0]] = 0
            st.pop()
        return ans


head = ListNode(2)
head.next = ListNode(1)
head.next.next = ListNode(5)
print(Solution().nextLargerNodes(head))
