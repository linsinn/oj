class Solution:
    def carPooling(self, trips: List[List[int]], capacity: int) -> bool:
        arr = []
        for trip in trips:
            arr.append([trip[1], trip[0]])
            arr.append([trip[2], -trip[0]])
        arr.sort()
        cur = 0
        for _, num in arr:
            cur += num
            if cur > capacity:
                return False
        return True
