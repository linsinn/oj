class Solution:
    def numPairsDivisibleBy60(self, time) -> int:
        mps = {}
        ans = 0
        for t in time:
            ans += mps.get((60 - t % 60) % 60, 0)
            mps[t % 60] = mps.get(t % 60, 0) + 1
        return ans
