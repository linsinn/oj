class Solution:
    def distributeCandies(self, candies: int, num_people: int) -> List[int]:
        ans = [0] * num_people
        idx = 0
        cur = 1
        while candies > 0:
            k = idx % num_people
            ans[k] += min(cur, candies)
            candies -= cur
            cur += 1
            idx += 1
        return ans


