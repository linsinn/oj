class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        def check(s, l):
            if len(s) % l != 0:
                return False
            for j in range(l, len(s), l):
                if s[j-l:j] != s[j:j+l]:
                    return False
            return True

        min_len = min(len(str1), len(str2))
        for i in range(min_len, 0, -1):
            if check(str1, i) and check(str2, i) and str1[:i] == str2[:i]:
                return str1[:i]
        return ""

