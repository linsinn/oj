class Solution:
	def myAtoi(self, str: str) -> int:
		str = str.strip()
		if len(str) == 0:
			return 0
		idx = 0
		ans = []
		if str[0] == '-' or str[0] == '+':
			ans.append(str[0])
			idx += 1
		while idx < len(str):
			if not str[idx].isdigit():
				break
			ans.append(str[idx])
			idx += 1
		t = 0
		try:
			t = int(''.join(ans))
		except ValueError:
			return 0
		return min(max(t, -(1 << 31)), (1 << 31) - 1)


str = "words and 987"
print(Solution().myAtoi(str))