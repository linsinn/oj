class Solution:
	def numTimesAllBlue(self, light) -> int:
		all_open = 0
		max_open = 0
		ans = 0
		for li in light:
			all_open += 1
			max_open = max(max_open, li)
			if all_open == max_open:
				ans += 1
		return ans


light = [4, 1, 2, 3]
print(Solution().numTimesAllBlue(light))