class Solution:
  def removeSubfolders(self, folder: List[str]) -> List[str]:
    ss = set()
    for f in folder:
      ss.add(tuple(f.split('/')))

    ans = []
    for f in folder:
      t = tuple(f.split('/'))
      if len(t) == 1:
        ans.append('/'.join(t))
      else:
        b = True
        for i in range(len(t) - 1):
          if t[0:i+1] in ss:
            b = False
            break
        if b:
          ans.append('/'.join(t))
    return ans
