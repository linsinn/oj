class Solution:
    def brokenCalc(self, X: 'int', Y: 'int') -> 'int':
        if X > Y:
            return X - Y
        steps = 0
        while Y != X:
            if Y < X:
                return steps + X - Y
            if Y % 2 == 0:
                Y //= 2
                steps += 1
            else:
                Y = (Y + 1) // 2
                steps += 2
        return steps


X = 1
Y = 1000000000
print(Solution().brokenCalc(X, Y))