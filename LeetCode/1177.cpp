//
// Created by sinn on 9/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;

class Solution {
public:
  vector<bool> canMakePaliQueries(string s, vector<vector<int>>& queries) {
    int A = 26;
    int N = s.size();
    vector<vector<int>> aux(N+1, vector<int>(A, 0));
    for (int i = 0; i < N; ++i) {
      aux[i+1] = aux[i];
      aux[i+1][s[i]-'a']++;
    }
    vector<bool> ans(queries.size(), true);
    for (int i = 0; i < queries.size(); ++i) {
      int l = queries[i][0];
      int r = queries[i][1];
      int limit = queries[i][2];
      int cnt = 0;
      for (int j = 0; j < A; ++j) {
        int k = aux[r+1][j] - aux[l][j];
        if (k % 2)
          ++cnt;
      }
      if ((r - l + 1) % 2) --cnt;
      if ((cnt + 1) / 2 > limit)
        ans[i] = false;
    }
    return ans;
  }
};

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  return 0;
}
