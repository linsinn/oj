from LeetCode import TreeNode

class Solution:
    def isUnivalTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if root is None:
            return True
        l = self.isUnivalTree(root.left)
        r = self.isUnivalTree(root.right)
        if not l or not r:
            return False
        if root.left is not None and root.left.val != root.val:
            return False
        if root.right is not None and root.right.val != root.val:
            return False
        return True
