class Solution:
  def removeDuplicates(self, s: str, k: int) -> str:
    while True:
      tmp = ""
      i = 0
      while i < len(s):
        j = i + 1
        while j < len(s) and s[j] == s[i]:
          j += 1
        l = j - i
        tmp += s[i:i + l % k]
        i = j
      if len(tmp) == len(s):
        break
      s = tmp
    return s



