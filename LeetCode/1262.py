
class Solution:
	def maxSumDivThree(self, nums) -> int:
		mods = [[] for _ in range(3)]
		for n in nums:
			mods[n % 3].append(n)
		for i in range(3):
			mods[i].sort()
		ans = sum(mods[0])
		tmp = sum(mods[1]) + sum(mods[2])
		if tmp % 3 == 1:
			if len(mods[2]) >= 2 and sum(mods[2][:2]) < mods[1][0]:
				ans = ans + tmp - sum(mods[2][:2])
			else:
				ans = ans + tmp - mods[1][0]
		elif tmp % 3 == 2:
			if len(mods[1]) >= 2 and sum(mods[1][:2]) < mods[2][0]:
				ans = ans + tmp - sum(mods[1][:2])
			else:
				ans = ans + tmp - mods[2][0]
		else:
			ans += tmp
		return ans