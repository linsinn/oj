import math


class Solution:
	def isGoodArray(self, nums: List[int]) -> bool:
		g = nums[0]
		for num in nums:
			g = math.gcd(g, num)
			if g == 1:
				return True
		return False
