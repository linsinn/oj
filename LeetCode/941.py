class Solution(object):
    def validMountainArray(self, A):
        """
        :type A: List[int]
        :rtype: bool
        """
        i, j = 0, len(A)-1
        while i + 1 < len(A) and A[i] < A[i+1]:
            i += 1
        while j - 1 >= 0 and A[j-1] > A[j]:
            j -= 1
        return len(A) >= 3 and i == j and i != 0 and j != len(A) - 1


sol = Solution()
print sol.validMountainArray([0, 3, 2, 1])