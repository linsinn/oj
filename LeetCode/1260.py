class Solution:
	def shiftGrid(self, grid, k: int):
		N = len(grid)
		M = len(grid[0])
		ans = []
		k %= (N * M)
		for row in grid:
			ans.extend(row)
		ans = ans[-k:] + ans[:N*M - k]
		res = []
		for i in range(N):
			tmp = []
			for j in range(M):
				tmp.append(ans[i * M + j])
			res.append(tmp)
		return res


grid = [[3,8,1,9],[19,7,2,5],[4,6,11,10],[12,0,21,13]]
k = 4
print(Solution().shiftGrid(grid, k))