class Solution:
	def longestCommonPrefix(self, strs) -> str:
		if len(strs) == 0:
			return ""
		mi = min(map(len, strs))
		ans = 0
		for i in range(mi):
			f = True
			for j in range(1, len(strs)):
				if strs[j][:i+1] != strs[j-1][:i+1]:
					f = False
					break
			if f:
				ans = i + 1
			else:
				break
		return strs[0][:ans]


strs = ["flower","flow","flight"]
print(Solution().longestCommonPrefix(strs))
