class Solution:
	def minimumSwap(self, s1: str, s2: str) -> int:
		d = [0, 0]
		for a, b in zip(s1, s2):
			if a != b:
				if a == 'x':
					d[0] += 1
				else:
					d[1] += 1
		if (d[0] + d[1]) % 2 == 1:
			return -1
		ans = d[0] // 2 + d[1] // 2
		if d[0] % 2 == 1:
			ans += 2
		return ans
