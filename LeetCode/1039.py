class Solution:
    def minScoreTriangulation(self, A) -> int:

        memos = [[0] * 51 for _ in range(51)]

        def dp(i, j):
            if memos[i][j] != 0:
                return memos[i][j]
            if j - i <= 1:
                return 0
            cur = 1 << 31
            for k in range(i+1, j):
                cur = min(cur, dp(i, k) + A[i] * A[j] * A[k] + dp(k, j))

            memos[i][j] = cur
            return cur

        return dp(0, len(A)-1)


A = [2,5,3,8,4,7,2,5,4]
print(Solution().minScoreTriangulation(A))

