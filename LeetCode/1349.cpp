//
// Created by sinn on 2/9/20.
//

#include "LeetCode.h"

class TweetCounts {
	map<string, vector<int>> tweets;
public:
	TweetCounts() {
	}

	void recordTweet(string tweetName, int time) {
		if (tweets.find(tweetName) == tweets.end())
			tweets[tweetName] = vector<int>();
		vector<int>& arr = tweets[tweetName];
		auto it = lower_bound(arr.begin(), arr.end(), time);
		arr.insert(it, time);
	}

	vector<int> getTweetCountsPerFrequency(string freq, string tweetName, int startTime, int endTime) {
		int delta = 60;
		if (freq == "hour") delta = 60 * 60;
		if (freq == "day") delta = 24 * 60 * 60;
		vector<int> ans;
		const vector<int>& arr = tweets[tweetName];
		int lastP = lower_bound(arr.begin(), arr.end(), startTime) - arr.begin();
		int i = 1;
		while (true) {
			int p = upper_bound(arr.begin() + lastP, arr.end(), min(startTime + i * delta - 1, endTime)) - arr.begin();
			ans.emplace_back(p - lastP);
			if (startTime + i * delta - 1 >= endTime) {
				break;
			}
			lastP = p;
			++i;
		}
		return ans;
	}
};