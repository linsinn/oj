class Solution:
	def convert(self, s: str, numRows: int) -> str:
		aux = [[] for _ in range(numRows)]
		i = 0
		dirs = 0
		for ch in s:
			aux[i].append(ch)
			if dirs == 0:
				i += 1
				if i == numRows:
					i -= 2
					dirs = 1
			else:
				i -= 1
				if i < 0:
					i += 2
					dirs = 0
		ans = []
		for t in aux:
			ans.extend(t)
		return ''.join(ans)
