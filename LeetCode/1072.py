class Solution:
    def maxEqualRowsAfterFlips(self, matrix: List[List[int]]) -> int:
        ans = 0
        N, M = len(matrix), len(matrix[0])
        mask = [[0] * 2 for _ in range(N)]

        for i in range(N):
            for v in range(2):
                for j in range(M):
                    if matrix[i][j] != v:
                        mask[i][v] |= 1 << j

        for i in range(N):
            for v in range(2):
                m1 = mask[i][v]
                cur = 1
                for k in range(i+1, N):
                    m2 = mask[k][0]
                    m3 = mask[k][1]
                    if m1 == m2 or m1 == m3:
                        cur += 1
                ans = max(ans, cur)

        return ans

