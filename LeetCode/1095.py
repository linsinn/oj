# """
# This is MountainArray's API interface.
# You should not implement it, or speculate about its implementation
# """
#class MountainArray:
#    def get(self, index: int) -> int:
#    def length(self) -> int:

class Solution:
    def findInMountainArray(self, target: int, mountain_arr: 'MountainArray') -> int:
        N = mountain_arr.length()
        lb, rb = 0, N-1
        while lb < rb:
            mid = (lb + rb) // 2
            a = mountain_arr.get(mid)
            b = mountain_arr.get(mid+1)
            if b < a:
                rb = mid
            else:
                lb = mid+1
        peak = lb
        lb = 0
        rb = peak
        while lb < rb:
            mid = (lb + rb) // 2
            elem = mountain_arr.get(mid)
            if elem == target:
                return mid
            elif elem < target:
                lb = mid + 1
            else:
                rb = mid
        if mountain_arr.get(lb) == target:
            return lb
        lb = peak
        rb = N-1
        while lb < rb:
            mid = (lb + rb) // 2
            elem = mountain_arr.get(mid)
            if elem == target:
                return mid
            elif elem > target:
                lb = mid + 1
            else:
                rb = mid
        if mountain_arr.get(lb) == target:
            return lb
        return -1



