class Solution:
    def camelMatch(self, queries: List[str], pattern: str) -> List[bool]:
        ans = [True] * len(queries)
        for i, query in enumerate(queries):
            j = 0
            flag = True
            for ch in query:
                if j < len(pattern) and ch == pattern[j]:
                    j += 1
                elif ch.lower() != ch:
                    flag = False
            ans[i] = (j == len(pattern) and flag)
        return ans
