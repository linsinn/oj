class Foo:
    def __init__(self):
        self.foo = [None] * 3

    def first(self, printFirst: 'Callable[[], None]') -> None:
        # printFirst() outputs "first". Do not change or remove this line.
        printFirst()
        self.foo[0] = -1
        self.second(self.foo[1])

    def second(self, printSecond: 'Callable[[], None]') -> None:
        # printSecond() outputs "second". Do not change or remove this line.
        if self.foo[0] == -1:
            if printSecond is not None:
                printSecond()
                self.foo[1] = -1
        if self.foo[1] is None and printSecond is not None:
            self.foo[1] = printSecond
        self.third(self.foo[2])

    def third(self, printThird: 'Callable[[], None]') -> None:
        # printThird() outputs "third". Do not change or remove this line.
        if self.foo[0] == -1:
            if printThird is not None:
                printThird()
        if self.foo[2] is None and printThird is not None:
            self.foo[2] = printThird
