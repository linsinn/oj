from collections import deque


class Solution:
    def orangesRotting(self, grid: 'List[List[int]]') -> 'int':
        que = deque()
        N, M = len(grid), len(grid[0])
        for i in range(N):
            for j in range(M):
                if grid[i][j] == 2:
                    que.append([i, j, 0])
        ans = 0
        dirs = [-1, 0, 1, 0, -1]
        while len(que) != 0:
            x, y, t = que.popleft()
            ans = t
            for i in range(4):
                nx, ny = x + dirs[i], y + dirs[i+1]
                if 0 <= nx < N and 0 <= ny < M and grid[nx][ny] == 1:
                    grid[nx][ny] = 2
                    que.append([nx, ny, t+1])
        for i in range(N):
            for j in range(M):
                if grid[i][j] == 1:
                    return -1
        return ans


grid = [[2],[1],[1],[1],[2],[1],[1]]
print(Solution().orangesRotting(grid))