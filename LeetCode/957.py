class Solution:
    def prisonAfterNDays(self, cells, N):
        """
        :type cells: List[int]
        :type N: int
        :rtype: List[int]
        """
        def next_day(t):
            l = t[:]
            l[0] = l[-1] = 0
            for i in range(1, len(l)-1):
                l[i] = 1 if t[i-1] == t[i+1] else 0
            return l
        h = {}
        tmp = []
        d = 0
        while tuple(cells) not in h and d <= N:
            h[tuple(cells)] = d
            tmp.append(cells)
            cells = next_day(cells)
            d += 1
        c = h.get(tuple(cells), 0)
        return tmp[(N-c) % (d-c) + c]


cells = [0,1,0,1,1,0,0,1]
N = 7
print(Solution().prisonAfterNDays(cells, N))
