class Solution:
    def shipWithinDays(self, weights, D: int) -> int:
        def Check(capa):
            i = 0
            cur = 0
            d = 1
            while i < len(weights):
                if cur + weights[i] <= capa:
                    cur += weights[i]
                    i += 1
                else:
                    if cur == 0:
                        return False
                    cur = 0
                    d += 1
            return d <= D

        l, r = weights[0], sum(weights)
        while l < r:
            m = l + (r - l) // 2
            if Check(m):
                r = m
            else:
                l = m + 1
        return l

weights = [3,2,2,4,1,4]
D = 3
print(Solution().shipWithinDays(weights, D))
