import collections


class Solution:
  def longestSubsequence(self, arr, difference: int) -> int:
    mp = collections.defaultdict(lambda: 0)
    ans = 1
    for a in arr:
      cnt = mp[a - difference] + 1
      ans = max(ans, cnt)
      mp[a] = max(mp[a], cnt)
    return ans


arr = [1, 2, 3, 4]
print(Solution().longestSubsequence(arr, 1))