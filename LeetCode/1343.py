from LeetCode import TreeNode


class Solution:
	def calcSum(self, root: TreeNode, idx):
		if root is None:
			return
		self.calcSum(root.left, idx << 1)
		self.calcSum(root.right, idx << 1 | 1)
		self.subtree_sum[idx] = root.val + self.subtree_sum.get(idx << 1, 0) + self.subtree_sum.get(idx << 1 | 1, 0)

	def dfs(self, root: TreeNode, idx):
		if root is None:
			return
		self.ans = max(self.ans, self.subtree_sum[idx] * (self.tot - self.subtree_sum[idx]))
		self.dfs(root.left, idx << 1)
		self.dfs(root.right, idx << 1 | 1)

	def maxProduct(self, root: TreeNode) -> int:
		self.subtree_sum = {}
		self.calcSum(root, 1)
		self.tot = self.subtree_sum[1]
		self.ans = 0
		self.dfs(root, 1)
		return self.ans % (10 ** 9 + 7)


root = TreeNode(11)
root.left = TreeNode(11)
print(Solution().maxProduct(root))