class Solution:
    def isAlienSorted(self, words, order):
        """
        :type words: List[str]
        :type order: str
        :rtype: bool
        """
        order = {o: i for i, o in enumerate(order)}

        def le(pre, cur):
            for a, b in zip(pre, cur):
                if order[a] < order[b]:
                    return True
                elif order[a] > order[b]:
                    return False
            return len(pre) <= len(cur)

        for i in range(1, len(words)):
            pre = words[i-1]
            cur = words[i]
            if not le(pre, cur):
                return False
        return True


words = ["hello","leetcode"]
order = "hlabcdefgijkmnopqrstuvwxyz"
print(Solution().isAlienSorted(words, order))
