class Solution:
	def countSquares(self, matrix: List[List[int]]) -> int:
		N = len(matrix)
		M = len(matrix[0])
		do = [[0] * M for _ in range(N)]
		ri = [[0] * M for _ in range(N)]
		for i in range(N):
			for j in range(M):
				prev = 0 if i == 0 else do[i - 1][j]
				do[i][j] = 0 if matrix[i][j] == 0 else prev + 1
		for i in range(N):
			for j in range(M):
				prev = 0 if j == 0 else ri[i][j - 1]
				ri[i][j] = 0 if matrix[i][j] == 0 else prev + 1
		ans = 0
		cur = []
		nxt = []
		for i in range(N):
			for j in range(M):
				if matrix[i][j] == 1:
					ans += 1
					cur.append([i, j])

		for le in range(2, min(N, M) + 1):
			for i, j in cur:
					if i < N-1 and j < M-1:
						if do[i+1][j+1] >= le and ri[i+1][j+1] >= le:
							nxt.append([i+1, j+1])
							ans += 1
			cur = nxt[:]
			nxt = []
		return ans
