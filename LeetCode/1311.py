import collections

class Solution:
	def watchedVideosByFriends(self, watchedVideos: List[List[str]], friends: List[List[int]], id: int, level: int) -> List[str]:
		que = collections.deque()
		que.append([id, 0])
		w = collections.defaultdict(lambda: 0)
		seen = {id}
		while len(que) > 0:
			cur = que.popleft()
			if cur[1] == level:
				for v in watchedVideos[cur[0]]:
					w[v] += 1
				continue
			for f in friends[cur[0]]:
				if f not in seen:
					seen.add(f)
					que.append([f, cur[1] + 1])
		li = list([k, v] for k, v in w.items())
		li = sorted(li, key=lambda v: (v[1], v[0]))
		ans = []
		for l in li:
			ans.append(l[0])
		return ans
