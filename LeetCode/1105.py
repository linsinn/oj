class Solution:
    def minHeightShelves(self, books, shelf_width: int) -> int:
        n = len(books)
        memos = [1e12] * (n + 1)
        memos[n] = 0
        memos[n - 1] = books[n - 1][1]
        for i in range(n - 2, -1, -1):
            mini = 1e12
            maxi = 0
            cur_w = 0
            idx = i
            while idx < n and cur_w + books[idx][0] <= shelf_width:
                maxi = max(books[idx][1], maxi)
                cur_w += books[idx][0]
                idx += 1
                mini = min(maxi + memos[idx], mini)
            memos[i] = mini
        return int(memos[0])


books = [[1, 1], [2, 3], [2, 3], [1, 1], [1, 1], [1, 1], [1, 2]]
shelf_width = 4
print(Solution().minHeightShelves(books, shelf_width))
