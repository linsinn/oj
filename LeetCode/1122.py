class Solution:
    def relativeSortArray(self, arr1: List[int], arr2: List[int]) -> List[int]:
        ans = []
        tmp = []
        ss = set(arr2)
        for a in arr2:
            for b in arr1:
                if b == a:
                    ans.append(b)
        for e in arr1:
            if e not in ss:
                tmp.append(e)
        ans.extend(sorted(tmp))
        return ans
