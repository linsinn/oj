class Solution:
  def canMakePaliQueries(self, s: str, queries):
    A = 26
    N = len(s)
    aux = [[0] * A for _ in range(N+1)]
    for i in range(N):
      for j in range(A):
        aux[i+1][j] = aux[i][j]
      aux[i+1][ord(s[i]) - ord('a')] += 1

    ans = [True] * len(queries)
    for i, (l, r, limit) in enumerate(queries):
      cnt = 0
      for j in range(A):
        k = aux[r+1][j] - aux[l][j]
        if k % 2 == 1:
          cnt += 1
      if cnt // 2 > limit:
        ans[i] = False
    return ans


s = "abcda"
qrys = [[0, 3, 2], [3,3,0],[1,2,0],[0,3,1],[0,3,2],[0,4,1]]
print(Solution().canMakePaliQueries(s, qrys))