import collections


class Solution:
	def numberOfSubarrays(self, nums, k: int) -> int:
		mp = collections.defaultdict(lambda : 0)
		mp[0] = 1
		g = 0
		ans = 0
		for num in nums:
			if num % 2 == 1:
				g += 1
			ans += mp[g - k]
			mp[g] += 1
		return ans


nums = [1, 1, 2, 1, 1]
k = 1
print(Solution().numberOfSubarrays(nums, k))
