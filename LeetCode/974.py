import collections

class Solution:
    def subarraysDivByK(self, A, K):
        """
        :type A: List[int]
        :type K: int
        :rtype: int
        """
        reminders = [0] * K
        tot = 0
        for a in A:
            tot += a
            reminders[tot % K] += 1
        ans = 0
        rot = 0
        for a in A:
            ans += reminders[rot]
            rot += a % K
            rot %= K
            reminders[rot] -= 1
        return ans


A = [7, 7]
K = 7
print(Solution().subarraysDivByK(A, K))
