class Solution:
    def minDeletionSize(self, A):
        """
        :type A: List[str]
        :rtype: int
        """
        M = max(map(len, A))
        rel = [0] * len(A)
        ans = 0

        for j in range(M):
            tmp = [0] * len(A)
            for i in range(1, len(A)):
                pre = "" if j >= len(A[i-1]) else A[i-1][j]
                cur = "" if j >= len(A[i]) else A[i][j]
                if cur > pre:
                    tmp[i] = 1
                elif cur == pre:
                    tmp[i] = 0
                else:
                    tmp[i] = -1
            for i in range(1, len(A)):
                if tmp[i] < 0:
                    if rel[i] <= 0:
                        ans += 1
                        break
                    tmp[i] = 1
            else:
                rel = list(map(max, zip(rel, tmp)))

        return ans


A = ["doeeqiy","yabhbqe","twckqte"]
print(Solution().minDeletionSize(A))