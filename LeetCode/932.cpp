#include <bits/stdc++.h>
using namespace std;

class Solution {
	map<int, vector<int>> memo;
	vector<int> dfs(int N) {
		if (memo.count(N) != 0)
			return memo[N];
		vector<int> ans(N);
		if (N == 1) {
			ans[0] = 1;
		} else {
			int t = 0;
			for (int x : dfs((N + 1) / 2))
				ans[t++] = 2 * x - 1;
			for (int x : dfs(N / 2))
				ans[t++] = 2 * x;
		}
		memo[N] = ans;
		return ans;
	}
public:
	vector<int> beautifulArray(int N) {
		return dfs(N);
	}
};

int main() {
	Solution sol;
	auto vec = sol.beautifulArray(4);
	for (int v : vec) {
		cout << v << ' ';
	}
	return 0;
}
