import collections
from functools import reduce


class StreamChecker:
    def __init__(self, words):
        T = lambda : collections.defaultdict(T)
        self.trie = T()
        for w in words:
            reduce(dict.__getitem__, w[::-1], self.trie)['#'] = True
        self.S = ""
        self.W = max(map(len, words))

    def query(self, letter: str) -> bool:
        self.S = (letter + self.S)[:self.W]
        cur = self.trie
        for c in self.S:
            if c in cur:
                cur = cur[c]
                if cur['#']:
                    return True
            else:
                break
        return False


checker = StreamChecker(['cd', 'ab', 'd'])