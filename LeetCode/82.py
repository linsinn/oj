from LeetCode import ListNode

class Solution(object):
	def deleteDuplicates(self, head):
		"""
		:type head: ListNode
		:rtype: ListNode
		"""
		nil = ListNode(0)
		pre = nil
		cur = head
		while cur is not None:
			nxt = cur.next
			if nxt is None:
				pre.next = cur
				pre = pre.next
			elif nxt.val != cur.val:
				pre.next = cur
				pre = pre.next
			else:
				while nxt is not None and nxt.val == cur.val:
					nxt = nxt.next
			cur = nxt

		pre.next = None
		return nil.next
