class Solution:
    def maxSumAfterPartitioning(self, A, K: int) -> int:
        N = len(A)
        subs = [[0] * (N+1) for _ in range(N+1)]
        memos = [[0] * (N+1) for _ in range(N+1)]

        for i in range(N):
            su, maxi = 0, 0
            for j in range(min(K, N-i)):
                su += A[i+j]
                maxi = max(maxi, A[i+j])
                subs[i][j+1] = maxi * (j+1)

        def dp(i, j):
            if j == 1:
                return A[i]
            if memos[i][j] != 0:
                return memos[i][j]
            ret = 0
            for a in range(1, min(j, K+1)+1):
                ret = max(ret, subs[i][a] + dp(i+a, j-a))
            memos[i][j] = ret
            return ret

        return dp(0, N)


A = [1,4,1,5,7,3,6,1,9,9,3]
K = 4
print(Solution().maxSumAfterPartitioning(A, K))