//
// Created by sinn on 11/17/19.
//

#include <bits/stdc++.h>
using namespace std;

int tot = 0;


class Solution {
  const int dirs[5] = {-1, 0, 1, 0, -1};
  struct Node {
    int x, y;
    int i, j;
    int step;
    Node() = default;
  };
  int N, M;
  bool can_reach(int a, int b, int c, int d, int e, int f, const vector<vector<char>>& grid) {
    if (a == e && b == f) return true;
    bool seen[N+1][M+1];
    memset(seen, 0, sizeof(seen));
    queue<pair<int, int>> que;
    que.emplace(a, b);
    seen[a][b] = true;
    while (!que.empty()) {
      auto cur = que.front();
      que.pop();
      if (cur.first == e && cur.second == f) {
        return true;
      }
      for (int di = 0; di < 4; ++di) {
        int nx = cur.first + dirs[di], ny = cur.second + dirs[di+1];
        if (nx < 0 || nx >= N || ny < 0 || ny >= M || grid[nx][ny] == '#' || (nx == c && ny == d))
          continue;
        if (!seen[nx][ny]) {
          seen[nx][ny] = true;
          que.emplace(nx, ny);
//          cout << tot++ << '\n';
        }
      }
    }
    return false;
  }
public:
  int minPushBox(vector<vector<char>>& grid) {
    N = grid.size();
    M = grid[0].size();
    Node st, ed, box;
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < M; ++j) {
        if (grid[i][j] == 'S') {
          st.x = i, st.y = j;
        }
        if (grid[i][j] == 'T') {
          ed.x = i, ed.y = j;
        }
        if (grid[i][j] == 'B') {
          box.x = i, box.y = j;
        }
      }
    }
    bool seen[N+1][M+1][N+1][M+1];
    memset(seen, 0, sizeof(seen));
    queue<Node> qq;
    box.step = 0;
    box.i = st.x, box.j = st.y;
    qq.emplace(box);
    seen[box.x][box.y][box.i][box.j] = true;
    while (!qq.empty()) {
      auto cur = qq.front();
      qq.pop();
      if (cur.x == ed.x && cur.y == ed.y) {
        return cur.step;
      }
      for (int d = 0; d < 4; ++d) {
        int nx = cur.x + dirs[d], ny = cur.y + dirs[d+1];
        if (nx < 0 || nx >= N || ny < 0 || ny >= M || grid[nx][ny] == '#')
          continue;
        int px = cur.x - dirs[d], py = cur.y - dirs[d+1];
        if (px >= 0 && px < N && py >=0 && py < M) {
          if (!seen[nx][ny][cur.x][cur.y] && can_reach(cur.i, cur.j, cur.x, cur.y, px, py, grid)) {
            Node nxt;
            nxt.x = nx, nxt.y = ny, nxt.step = cur.step + 1;
            nxt.i = cur.x, nxt.j = cur.y;
            seen[nx][ny][cur.x][cur.y] = true;
            qq.emplace(nxt);
          }
        }
      }
    }
    return -1;
  }
};


int main() {
//  vector<string> grid = {"######", "#T..##", "#.#B.#", "#....#", "#...S#", "######"};
  vector<vector<string>> grid = {{"#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"},{"#",".",".",".",""
                                                                                                                   ".",".",".",".",".",".",".",""
                                                                                                                  ".","#","#","#","#"},{"#",".","#","#","#","#",".","#","#","#","#",".","#","#","#","."},{"#",".",".",".",".",".",".","#","T","#",".",".","#","#","#","."},{"#",".",".",".","#",".",".",".",".",".",".",".","#","#","#","."},{"#",".",".",".",".",".","B",".",".",".",".",".","#","#","#","."},{"#",".","#","#","#","#","#","#","#","#","#",".","#","#","#","."},{"#",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."},{"#",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."},{"#",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."},{"#",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."},{"#",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."},{"#",".",".",".",".",".",".",".",".",".",".",".",".",".",".","."},{"#",".",".",".",".",".",".",".","S",".",".",".",".",".",".","."},{"#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"}};
  vector<vector<char>> g(grid.size());
  for (int i = 0; i < grid.size(); ++i) {
    for (auto ch : grid[i]) {
      g[i].emplace_back((ch[0]));
    }
  }
  Solution sol;
  cout << sol.minPushBox(g) << '\n';
  return 0;
}