class Solution:
    def isBoomerang(self, points: List[List[int]]) -> bool:
        points.sort()
        lines = [[points[1][0]-points[0][0], points[1][1]-points[0][1]],
                 [points[2][0]-points[0][0], points[2][1]-points[0][1]]]
        if lines[0][0] == 0 and lines[1][0] == 0:
            return False
        return lines[0][1] * lines[1][0] != lines[0][0] * lines[1][1]
