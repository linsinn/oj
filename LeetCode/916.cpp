#include "LeetCode.h"

class Solution {
public:
    vector<string> wordSubsets(vector<string>& A, vector<string>& B) {
        int cnts[256];
        memset(cnts, 0, sizeof cnts);
        int tmp[256];
        for (const string& b : B) {
            memset(tmp, 0, sizeof tmp);
            for (char c : b) {
                tmp[c] += 1;
                cnts[c] = max(cnts[c], tmp[c]);
            }
        }
        vector<string> ans;
        for (const string& a : A) {
            memset(tmp, 0, sizeof tmp);
            for (char c : a) {
                tmp[c] += 1;
            }
            bool f = true;
            for (int i = 'a'; i <= 'z'; i++) {
                if (tmp[i] < cnts[i]) {
                    f = false;
                    break;
                }
            }
            if (f)
                ans.emplace_back(a);
        }
        return ans;
    }
};
