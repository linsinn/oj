class Solution:
    def sampleStats(self, count):
        arr = []
        tot = 0
        su = 0
        mini, maxi = -1, -1
        mode = -1
        mode_cnt = 0
        for i, cnt in enumerate(count):
            su += i * cnt
            tot += cnt
            if cnt != 0:
                if mini == -1:
                    mini = i
                maxi = i
            if cnt > mode_cnt:
                mode_cnt = cnt
                mode = i
        pre_tot = 0
        last_non_zero = -1
        median = -1
        for i, cnt in enumerate(count):
            if cnt != 0:
                if pre_tot + cnt > tot // 2 and tot - pre_tot >= tot // 2:
                    if tot % 2 == 0 and tot - pre_tot == tot // 2:
                        median = (i + last_non_zero) / 2
                    else:
                        median = i
                    break
                last_non_zero = i
                pre_tot += cnt
        return [float(mini), float(maxi), su / tot, float(median), float(mode)]


l = [0,1,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
print(Solution().sampleStats(l))


