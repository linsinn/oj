from collections import Counter

class Solution:
    def repeatedNTimes(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        cnts = Counter(A)
        for k, v in cnts.items():
            if v >= len(A) // 2:
                return k
