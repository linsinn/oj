//
// Created by sinn on 11/24/19.
//

#include "LeetCode.h"

class Solution {
public:
  int numWays(int steps, int arrLen) {
    using ll = long long;
    constexpr int MOD = 1e9 + 7;
    constexpr int N = 505;
    vector<vector<ll>> dp(2, vector<ll>(N, 0));
    int cur = 0;
    dp[0][0] = 1;
    for (int i = 0; i < steps; i++) {
      for (int j = 0; j < N; j++) {
        dp[cur ^ 1][j] = dp[cur][j];
        if (j > 0) {
          dp[cur ^ 1][j] += dp[cur][j - 1];
        }
        if (j < min(N-1, arrLen-1)) {
          dp[cur ^ 1][j] += dp[cur][j+1];
        }
        dp[cur ^ 1][j] %= MOD;
      }
      fill(dp[cur].begin(), dp[cur].end(), 0);
      cur ^= 1;
    }
    return dp[cur][0];
  }
};