class Solution:
	def reverse(self, x: int) -> int:
		tmp = 0
		if x < 0:
			tmp = -int(str(x)[:0:-1])
		else:
			tmp = int(str(x)[::-1])
		if tmp >= (1 << 31) or tmp < - (1 << 31):
			return 0
		else:
			return tmp

