class Solution:
  def dieSimulator(self, n: int, rollMax) -> int:
    MOD = 10 ** 9 + 7
    mx = max(rollMax)
    dp = [[[0] * (mx + 1) for _ in range(7)] for _ in range(2)]
    for i in range(1, 7):
      dp[0][i][1] = 1
    cur = 0
    for _ in range(2, n + 1):
      nxt = cur ^ 1
      for i in range(1, 7):
        dp[nxt][i][1] = 0
        for j in range(2, rollMax[i-1]+1):
          dp[nxt][i][j] = dp[cur][i][j - 1]
        for j in range(1, 7):
          if i != j:
            dp[nxt][i][1] += sum(dp[cur][j][1:rollMax[j - 1] + 1])
            dp[nxt][i][1] %= MOD
      cur ^= 1
    ans = 0
    for i in range(1, 7):
      ans += sum(dp[cur][i][1:rollMax[i - 1] + 1])
      ans %= MOD
    return ans


n = 4
rolls = [2, 1, 1, 3, 3, 2]
print(Solution().dieSimulator(n, rolls))
