class Solution:
  def getMaximumGold(self, grid) -> int:
    N = len(grid)
    M = len(grid[0])

    seen = [[False] * M for _ in range(N)]
    ans = [0]

    def dfs(i, j, tot):
      ans[0] = max(ans[0], tot)
      dirs = [-1, 0, 1, 0, -1]
      for d in range(4):
        x = i + dirs[d]
        y = j + dirs[d+1]
        if 0 <= x < N and 0 <= y < M and not seen[x][y] and grid[x][y] != 0:
          seen[x][y] = True
          dfs(x, y, tot + grid[x][y])
          seen[x][y] = False

    for i in range(N):
      for j in range(M):
        if grid[i][j] != 0:
          seen[i][j] = True
          dfs(i, j, grid[i][j])
          seen[i][j] = False

    return ans[0]


grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
print(Solution().getMaximumGold(grid))