//
// Created by sinn on 12/9/18.
//

#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    int tallestBillboard(vector<int>& rods) {
        int n = static_cast<int>(rods.size());
        if (n == 0)
            return 0;
        vector<vector<int>> dp(static_cast<unsigned long>(n), vector<int>(10010, -1));
        dp[0][5000] = dp[0][5000 - rods[0]] = 0;
        dp[0][5000 + rods[0]] = rods[0];
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < 10010; j++) {
                dp[i][j] = dp[i-1][j];
                int r = rods[i];
                if (j - r >= 0 && dp[i-1][j-r] != -1)
                    dp[i][j] = max(dp[i][j], dp[i-1][j-r] + r);
                if (j + r < 10010 && dp[i-1][j+r] != -1)
                    dp[i][j] = max(dp[i][j], dp[i-1][j+r]);
            }
        }
        return max(0, dp[n-1][5000]);
    }
};

int main() {
    vector<int> rods = {1, 2, 3, 4, 5, 6};
    cout << Solution().tallestBillboard(rods) << '\n';
    return 0;
}