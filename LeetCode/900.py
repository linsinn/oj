class RLEIterator(object):
    def __init__(self, A):
        """
        :type A: List[int]
        """
        self.A = A
        
    def next(self, n):
        """
        :type n: int
        :rtype: int
        """
        while len(self.A) != 0 and self.A[0] < n:
            n -= self.A[0]
            self.A.pop(0)
            self.A.pop(0)
        if len(self.A) == 0:
            return -1
        self.A[0] -= n
        return self.A[1]

