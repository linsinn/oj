from LeetCode import TreeNode


class Solution:
  def twoSumBSTs(self, root1: TreeNode, root2: TreeNode, target: int) -> bool:
    def dfs(root, ss):
      if root is None:
        return
      ss.add(root.val)
      dfs(root.left, ss)
      dfs(root.right, ss)

    ss1 = set()
    ss2 = set()
    dfs(root1, ss1)
    dfs(root2, ss2)

    for num in ss1:
      if target - num in ss2:
        return True
    return False

