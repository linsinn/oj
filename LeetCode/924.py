# -*- coding: utf-8 -*-

class DSU(object):
	def __init__(self, N):
		super(DSU, self).__init__()
		self.p = range(N)
		self.sz = [1] * N

	def find(self, x):
		if self.p[x] != x:
			self.p[x] = self.find(self.p[x])
		return self.p[x]

	def union(self, x, y):
		xr = self.find(x)
		yr = self.find(y)
		self.p[xr] = yr
		self.sz[yr] += self.sz[xr]

	def size(self, x):
		return self.sz[self.find(x)]
		
import collections

class Solution(object):
	def minMalwareSpread(self, graph, initial):
		"""
		:type graph: List[List[int]]
		:type initial: List[int]
		:rtype: int
		"""
		dsu = DSU(len(graph))
		for j, row in enumerate(graph):
			for i in xrange(j):
				if row[i]:
					dsu.union(i, j)

		count = collections.Counter(dsu.find(u) for u in initial)
		ans = (-1, min(initial))
		for node in sorted(initial):
			root = dsu.find(node)
			if count[root] == 1:
				if dsu.size(root) > ans[0]:
					ans = dsu.size(root), node
		return ans[1]
