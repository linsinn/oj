class Solution:
    def dfs(self, idx, take, piles):
        take = min(take, len(piles))
        if idx >= len(piles):
            return 0
        if self.memos[idx][take] != 0:
            return self.memos[idx][take]
        cur = 0
        tot = self.su[len(piles)] - self.su[idx]
        for i in range(1, 2*take+1):
            ret = self.dfs(idx + i, max(take, i), piles)
            cur = max(cur, tot - ret)
        self.memos[idx][take] = cur
        return cur

    def stoneGameII(self, piles) -> int:
        self.memos = [[0] * 101 for _ in range(101)]
        self.su = [0] * (len(piles) + 1)
        for i in range(len(piles)):
            self.su[i+1] = self.su[i] + piles[i]
        return self.dfs(0, 1, piles)


