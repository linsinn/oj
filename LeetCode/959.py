class Solution:
    def regionsBySlashes(self, grid):
        """
        :type grid: List[str]
        :rtype: int
        """
        N = len(grid)
        M = len(grid[0])
        vis = [[False] * M * 2 for _ in range(N)]
        dirs = [-1, 0, 1, 0, 1]

        ws = {
            ('/', 0): 1,
            ('/', 1): 1,
            ('/', 2): 0,
            ('/', 3): 0,
            ('\\', 0): 0,
            ('\\', 1): 1,
            ('\\', 2): 1,
            ('\\', 3): 0,
            (' ', 0): 0,
            (' ', 1): 0,
            (' ', 2): 0,
            (' ', 3): 0,
        }

        def dfs(x, y, w):
            if x < 0 or x >= N:
                return
            if y < 0 or y >= M:
                return
            w = ws[(grid[x][y], w)]
            if vis[x][y*2+w]:
                return
            if grid[x][y] == ' ':
                vis[x][y*2] = True
                vis[x][y*2+1] = True
            else:
                vis[x][y*2+w] = True
            if grid[x][y] == '/':
                if w == 0:
                    dfs(x-1, y, 0)
                    dfs(x, y-1, 1)
                else:
                    dfs(x+1, y, 2)
                    dfs(x, y+1, 3)
            elif grid[x][y] == '\\':
                if w == 0:
                    dfs(x, y-1, 1)
                    dfs(x+1, y, 2)
                else:
                    dfs(x-1, y, 0)
                    dfs(x, y+1, 3)
            else:
                dfs(x-1, y, 0)
                dfs(x, y-1, 1)
                dfs(x+1, y, 2)
                dfs(x, y+1, 3)

        ans = 0
        for i in range(N):
            for j in range(2*M):
                if not vis[i][j]:
                    ans += 1
                    if grid[i][j//2] == '/':
                        dfs(i, j//2, 2 if j % 2 == 0 else 1)
                    else:
                        dfs(i, j//2, 0 if j % 2 == 0 else 1)
        return ans



grid = [" /","/ "]
print(Solution().regionsBySlashes(grid))