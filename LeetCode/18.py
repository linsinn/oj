import collections


class Solution:
	def fourSum(self, nums, target: int):
		N = len(nums)
		mp = collections.defaultdict(list)
		for i in range(N):
			for j in range(i+1, N):
				mp[nums[i] + nums[j]].append([i, j])
		ans = set()
		for k, val in mp.items():
			t = target - k
			for u in mp.get(t, []):
				for v in val:
					s = {v[0], v[1], u[0], u[1]}
					if len(s) == 4:
						arr = sorted([nums[a] for a in s])
						ans.add(tuple(arr))
		res = [list(v) for v in ans]
		return res


nums = [1, 1]
target = 2
print(Solution().fourSum(nums, target))