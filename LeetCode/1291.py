import bisect


class Solution:
	def sequentialDigits(self, low: int, high: int) -> List[int]:
		cand = []
		prev = [0] * 9
		for i in range(len(prev)):
			prev[i] = i + 1
		nxt = []
		for i in range(2, 10):
			for num in prev:
				b = num % 10 + 1
				if b < 10:
					d = num * 10 + b
					cand.append(d)
					nxt.append(d)
			prev = nxt
			nxt = []
		l = bisect.bisect_left(cand, low)
		r = bisect.bisect_right(cand, high)
		return cand[l:r]


