class Solution:
	def helper(self, height):
		stk = []
		ans = 0
		for i, h in enumerate(height):
			if len(stk) == 0:
				stk.append(i)
			else:
				if h > height[stk[-1]]:
					stk.append(i)
				else:
					l, r = 0, len(stk) - 1
					idx = -1
					while l <= r:
						m = (l + r) // 2
						if height[stk[m]] < height[i]:
							l = m + 1
						else:
							idx = m
							r = m - 1
					ans = max(ans, (i - stk[idx]) * height[i])
		return ans

	def maxArea(self, height) -> int:
		ans = self.helper(height)
		ans = max(ans, self.helper(height[::-1]))
		return ans


arr = [1,8,6,2,5,4,8,3,7]
print(Solution().maxArea(arr))