class Solution:
    def subarraysWithKDistinct(self, A: 'List[int]', K: 'int') -> 'int':
        return self.longestWithK(A, K) - self.longestWithK(A, K-1)

    def longestWithK(self, A, K):
        i, j = 0, 0
        s = {}
        ret = 0
        while j < len(A):
            s[A[j]] = s.get(A[j], 0) + 1
            while len(s) > K:
                s[A[i]] -= 1
                if s[A[i]] == 0:
                    s.pop(A[i])
                i += 1
            ret += j - i + 1
            j += 1
        return ret



A = [1, 2, 1, 2, 3]
K = 2
print(Solution().subarraysWithKDistinct(A, K))
