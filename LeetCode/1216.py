class Solution:
  def isValidPalindrome(self, s: str, k: int) -> bool:
    t = s[::-1]
    N = len(s)
    dp = [[0] * (N+1) for _ in range(N+1)]
    for i in range(1, N+1):
      for j in range(1, N+1):
        dp[i][j] = max(dp[i-1][j], dp[i][j-1])
        if s[i-1] == t[j-1]:
          dp[i][j] = max(dp[i][j], dp[i-1][j-1] + 1)
    return k >= N - dp[N][N]
