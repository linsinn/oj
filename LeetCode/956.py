class Solution:
    def tallestBillboard(self, rods):
        """
        :type rods: List[int]
        :rtype: int
        """

        if len(rods) == 0:
            return 0
        SHIFT = sum(rods)
        dp = [[-1] * 10010 for _ in range(len(rods))]
        dp[0][SHIFT-rods[0]] = 0
        dp[0][SHIFT] = 0
        dp[0][SHIFT+rods[0]] = rods[0]
        for i in range(1, len(rods)):
            for j in range(0, 2 * SHIFT + 1):
                dp[i][j] = dp[i-1][j]
                r = rods[i]
                if j-r >= 0 and dp[i-1][j-r] != -1:
                    dp[i][j] = max(dp[i][j], dp[i-1][j-r] + r)
                if j+r < len(dp[0]) and dp[i-1][j+r] != -1:
                    dp[i][j] = max(dp[i][j], dp[i-1][j+r])
        return 0 if dp[len(rods)-1][SHIFT] < 0 else dp[len(rods)-1][SHIFT]


rods = [124,121,107,127,156,146,135,153,137,150,141,138,129,142,124,144,126,900,900,900]
print(Solution().tallestBillboard(rods))