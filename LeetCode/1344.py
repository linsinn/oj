class Solution:

	def dfs(self, idx):
		if self.memo[idx][0] == 0:
			for i in range(1, self.d+1):
				if idx-i >= 0 and self.arr[idx] > self.arr[idx-i]:
					self.memo[idx][0] = max(self.memo[idx][0], self.dfs(idx-i))
				else:
					break
			self.memo[idx][0] += 1
		if self.memo[idx][1] == 0:
			for i in range(1, self.d+1):
				if idx+i < self.N and self.arr[idx] > self.arr[idx+i]:
					self.memo[idx][1] = max(self.memo[idx][1], self.dfs(idx+i))
				else:
					break
			self.memo[idx][1] += 1
		return max(self.memo[idx])

	def maxJumps(self, arr, d: int) -> int:
		self.arr = arr
		self.d = d
		self.N = len(arr)
		self.memo = [[0] * 2 for _ in range(self.N)]
		ans = 0
		for i in range(self.N):
			ans = max(ans, self.dfs(i))
		return ans


arr = [1, 7, 1]
print(Solution().maxJumps(arr, 1))
