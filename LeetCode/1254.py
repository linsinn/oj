class Solution:
	def dfs(self, x, y, grid):
		self.seen[x][y] = True
		dirs = [-1, 0, 1, 0, -1]
		for d in range(4):
			i = x + dirs[d]
			j = y + dirs[d+1]
			if i < 0 or j < 0 or i >= self.N or j >= self.M:
				self.cur_closed = False
			else:
				if grid[i][j] == 0 and not self.seen[i][j]:
					self.seen[i][j] = True
					self.dfs(i, j, grid)

	def closedIsland(self, grid: List[List[int]]) -> int:
		self.N = len(grid)
		self.M = len(grid[0])
		self.seen = [[False] * self.M for _ in range(self.N)]
		self.cur_closed = True
		ans = 0
		for i in range(self.N):
			for j in range(self.M):
				if grid[i][j] == 0 and not self.seen[i][j]:
					self.cur_closed = True
					self.dfs(i, j, grid)
					if self.cur_closed:
						ans += 1
		return ans
