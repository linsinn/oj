import bisect

class Solution:
    def numMovesStonesII(self, stones):
        N = len(stones)
        stones.sort()
        diff_sum = 0
        for i in range(1, N):
            diff_sum += stones[i] - stones[i-1] - 1
        maxi = diff_sum - min(stones[1] - stones[0] - 1, stones[-1] - stones[-2] - 1)
        mini_space, pos = N, 0
        for i in range(N):
            rb = bisect.bisect_right(stones, stones[i]+N-1)
            s = N - (rb - i)
            if s < mini_space:
                mini_space = s
                pos = i
        if mini_space == 1 and stones[pos] + N - 1 not in stones and stones[pos] + N not in stones:
            return [2, maxi]
        return [mini_space, maxi]


stones = [8,7,6,5,10]
print(Solution().numMovesStonesII(stones))