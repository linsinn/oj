class Solution:
	def filterRestaurants(self, restaurants, veganFriendly: int, maxPrice: int, maxDistance: int):
		arr = []
		for r in restaurants:
			if (veganFriendly == 0 or r[2] == 1) and r[-2] <= maxPrice and r[-1] <= maxDistance:
				arr.append([-r[1], -r[0]])
		arr.sort()
		return [-v for u, v in arr]


restaurants = [[1,4,1,40,10],[2,8,0,50,5],[3,8,1,30,4],[4,10,0,10,3],[5,1,1,15,1]]
veganFriendly = 0
maxPrice = 30
maxDistance = 3
print(Solution().filterRestaurants(restaurants, veganFriendly, maxPrice, maxDistance))