class Solution:
	def maxStudents(self, seats) -> int:
		N = len(seats)
		M = len(seats[0])
		memo = []
		for i in range(N):
			memo.append({})

		def avail(layer, curMask, preMask):
			t = curMask | preMask
			for i in range(M - 1):
				if (t & (1 << i)) and (t & (1 << (i + 1))):
					return False
			for i in range(M):
				if (curMask & (1 << i)) and seats[layer][i] == '#':
					return False
			return True

		def bits(mask):
			cnt = 0
			for i in range(M):
				if mask & (1 << i):
					cnt += 1
			return cnt

		def dfs(layer, preMask):
			if layer == N:
				return 0
			t = memo[layer].get(preMask, -1)
			if t != -1:
				return t
			cnt = 0
			for i in range(1 << M):
				if avail(layer, i, preMask):
					ret = dfs(layer + 1, i)
					cnt = max(cnt, bits(i) + ret)
			memo[layer][preMask] = cnt
			return cnt

		return dfs(0, 0)


seats = []
for i in range(8):
	a = []
	for j in range(8):
		a.append('.')
	seats.append(a)

print(Solution().maxStudents(seats))
