class Solution:
    def prefixesDivBy5(self, A: List[int]) -> List[bool]:
        ans = []
        rem = 0
        for a in A:
            rem *= 2
            rem += a
            rem %= 5
            ans.append(True if rem == 0 else False)
        return ans
