class Solution:
    def addNegabinary(self, arr1, arr2):
        arr1 = arr1[::-1]
        arr2 = arr2[::-1]
        i, j = 0, 0
        c = 0
        ans = []
        while i < len(arr1) or j < len(arr2):
            b1 = arr1[i] if i < len(arr1) else 0
            b2 = arr2[i] if i < len(arr2) else 0
            b = b1 + b2 + c
            if b >= 2:
                ans.append(b - 2)
                c = -1
            elif 0 <= b < 2:
                ans.append(b)
                c = 0
            else:
                ans.append(1)
                c = 1
            i += 1
            j += 1
        while c != 0:
            b = c
            if b >= 2:
                ans.append(b - 2)
                c = -1
            elif 0 <= b < 2:
                ans.append(b)
                c = 0
            else:
                ans.append(1)
                c = 1
        while ans[-1] == 0 and len(ans) > 1:
            ans.pop()
        return ans[::-1]


arr1 = [0]
arr2 = [0]
print(Solution().addNegabinary(arr1, arr2))