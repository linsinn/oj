class Solution:
	def cnt_change(self, st, ed, s):
		cnt = 0
		while st < ed:
			if s[st] != s[ed]:
				cnt += 1
			st += 1
			ed -= 1
		return cnt

	def palindromePartition(self, s: str, k: int) -> int:
		N = len(s)
		min_change = [[0] * (N+1) for _ in range(N+1)]
		for i in range(N):
			for j in range(i+1, N):
				min_change[i+1][j+1] = self.cnt_change(i, j, s)

		dp = [[-1] * (N + 1) for _ in range(k+1)]
		for i in range(1, N+1):
			dp[1][i] = min_change[1][i]
		for x in range(2, k+1):
			for y in range(x, N+1):
				for z in range(x-1, y):
					if dp[x][y] == -1:
						dp[x][y] = dp[x-1][z] + min_change[z+1][y]
					else:
						dp[x][y] = min(dp[x][y], dp[x-1][z] + min_change[z+1][y])
		return dp[k][N]


s = "tcymekt"
k = 4
print(Solution().palindromePartition(s, k))


