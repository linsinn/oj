from collections import deque

class Solution(object):
    def deckRevealedIncreasing(self, deck):
        """
        :type deck: List[int]
        :rtype: List[int]
        """
        N = len(deck)

        def dfs(n):
            if n == 1:
                return deque([N-1])
            tail = dfs(n-1)
            tail.rotate(1)
            tail.appendleft(N-n)
            return tail

        idx = dfs(N)
        deck.sort()
        return [deck[i] for i in idx]


deck = [1, 2, 3, 4]
print Solution().deckRevealedIncreasing(deck)