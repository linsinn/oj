class Solution(object):
    def totalFruit(self, tree):
        """
        :type tree: List[int]
        :rtype: int
        """
        i = 0
        diff = {tree[0]: 1}
        ans = 1
        for j in xrange(1, len(tree)):
            if len(diff) < 2:
                diff[tree[j]] = diff.get(tree[j], 0) + 1
            else:
                if tree[j] in diff:
                    diff[tree[j]] += 1
                else:
                    while i < j:
                        diff[tree[i]] -= 1
                        if diff[tree[i]] == 0:
                            break
                        i += 1
                    i += 1
                    diff = {k:v for k, v in diff.iteritems() if v != 0}
                    diff[tree[j]] = 1
            ans = max(ans, j - i + 1)
        return ans
