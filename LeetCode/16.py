class Solution:
	def threeSumClosest(self, nums: List[int], target: int) -> int:
		N = len(nums)
		nums = sorted(nums)
		ans = sum(nums[:3])
		for i in range(N):
			j, k = i - 1, i + 1
			while j >= 0 and k < N:
				t = nums[i] + nums[j] + nums[k]
				if t == target:
					return t
				if t < target:
					k += 1
				else:
					j -= 1
				if abs(ans - target) > abs(t - target):
					ans = t
		return ans
