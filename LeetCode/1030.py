from collections import deque

class Solution:
    def allCellsDistOrder(self, R: int, C: int, r0: int, c0: int) -> List[List[int]]:
        ans = [[r0, c0]]
        que = deque(ans)
        dirs = [-1, 0, 1, 0, -1]
        vis = set()
        vis.add((r0, c0))
        while len(que) != 0:
            cur = que.popleft()
            for i in range(4):
                nx = cur[0] + dirs[i]
                ny = cur[1] + dirs[i+1]
                if 0 <= nx < R and 0 <= ny < C and (nx, ny) not in vis:
                    ans.append([nx, ny])
                    vis.add((nx, ny))
                    que.append([nx, ny])
        return ans
