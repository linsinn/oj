class Solution(object):
    def diStringMatch(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        i, d = 0, len(S)
        ans = []
        for ch in S:
            if ch == 'I':
                ans.append(i)
                i += 1
            else:
                ans.append(d)
                d -= 1
        ans.append(i)
        return ans
