class Solution:
  def numPrimeArrangements(self, n: int) -> int:
    MOD = 10 ** 9 + 7

    num_primes = 0

    def is_prime(x):
      if x == 1: return False
      if x == 2: return True
      if x % 2 == 0: return False
      for k in range(3, x, 2):
        if x % k == 0:
          return False
      return True

    for i in range(1, n+1):
      if is_prime(i):
        num_primes += 1

    fac = [0] * (n + 1)
    fac[0] = fac[1] = 1
    for i in range(2, n+1):
      fac[i] = fac[i-1] * i % MOD
    return fac[num_primes] * fac[n-num_primes] % MOD
