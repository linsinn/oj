class Solution:
	def tictactoe(self, moves: List[List[int]]) -> str:
		grid = [['.'] * 3 for _ in range(3)]
		ch = ['X', 'O']

		def check():
			for i in range(3):
				if grid[i][0] == grid[i][1] == grid[i][2] and grid[i][0] != '.':
					return True
				if grid[0][i] == grid[1][i] == grid[2][i] and grid[0][i] != '.':
					return True
			if grid[0][0] == grid[1][1] == grid[2][2] and grid[1][1] != '.':
				return True
			if grid[0][2] == grid[1][1] == grid[2][0] and grid[1][1] != '.':
				return True
			return False

		cur = 0
		for move in moves:
			grid[move[0]][move[1]] = ch[cur]
			if check():
				return "A" if cur == 0 else "B"
			cur ^= 1
		return "Pending" if len(moves) < 9 else "Draw"
