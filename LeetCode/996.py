import math

class Solution:
    def numSquarefulPerms(self, A: 'List[int]') -> 'int':
        N = len(A)
        T = [[0] * N for _ in range(N)]
        for i in range(N):
            for j in range(i+1, N):
                s = A[i] + A[j]
                sqr = int(math.sqrt(s))
                if sqr * sqr == s:
                    T[i][j] = T[j][i] = 1

        f = [[-1] * (1<<N) for _ in range(N)]

        def dp(cur, selected):
            if (selected ^ (1 << cur)) == 0:
                return 1
            if f[cur][selected] != -1:
                return f[cur][selected]
            ret = 0
            vis = set()
            for i in range(N):
                if cur != i and T[cur][i] == 1 and ((1 << i) & selected != 0)\
                        and (A[cur], A[i]) not in vis:
                    vis.add((A[cur], A[i]))
                    ret += dp(i, selected ^ (1 << cur))
            f[cur][selected] = ret
            return ret

        ans = 0
        vis = set()
        for i in range(N):
            for j in range(N):
                if T[i][j] == 1 and (A[i], A[j]) not in vis:
                    vis.add((A[i], A[j]))
                    ans += dp(j, ((1<<N)-1) ^ (1 << i))
        return ans


A = [0, 0, 0, 1, 1, 1]
print(Solution().numSquarefulPerms(A))


