from LeetCode import TreeNode, ListNode

from collections import deque


class Solution:
	def isSubPath(self, head: ListNode, root: TreeNode) -> bool:
		cand = []
		que = deque([root])
		while len(que) != 0:
			node = que.popleft()
			if node.val == head.val:
				cand.append(node)
			if node.left:
				que.append(node.left)
			if node.right:
				que.append(node.right)

		cur_iter = head.next
		next_cand = []
		while len(cand) != 0:
			for node in cand:
				if cur_iter is None:
					return len(cand) != 0
				if node.left and node.left.val == cur_iter.val:
					next_cand.append(node.left)
				if node.right and node.right.val == cur_iter.val:
					next_cand.append(node.right)
			cur_iter = cur_iter.next
			cand, next_cand = next_cand, []
		return False


head = ListNode(4)
head.next = ListNode(2)
head.next.next = ListNode(8)
root = TreeNode(1)
root.left = TreeNode(4)
root.left.right = TreeNode(2)
root.left.right.left = TreeNode(1)
root.right = TreeNode(4)
root.right.left = TreeNode(2)
root.right.left.left = TreeNode(6)
root.right.left.right = TreeNode(8)

print(Solution().isSubPath(head, root))