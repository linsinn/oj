from LeetCode import TreeNode


class Solution:
    def dfs(self, root: TreeNode, to_del, pa, is_left, ans):
        if root is None:
            return
        self.dfs(root.left, to_del, root, True, ans)
        self.dfs(root.right, to_del, root, False, ans)
        if root.val in to_del:
            if root.left is not None:
                ans.append(root.left)
            if root.right is not None:
                ans.append(root.right)
            root.left = root.right = None
            if is_left and pa is not None:
                pa.left = None
            if not is_left and pa is not None:
                pa.right = None

    def delNodes(self, root: TreeNode, to_delete: List[int]) -> List[TreeNode]:
        ans = []
        self.dfs(root, set(to_delete), None, True, ans)
        if root.val not in to_delete:
            ans.append(root)
        return ans
