from LeetCode import TreeNode

class Solution(object):
    def flipEquiv(self, root1, root2):
        """
        :type root1: TreeNode
        :type root2: TreeNode
        :rtype: bool
        """
        if root1 is not None and root2 is not None:
            if root1.val == root2.val:
                return self.flipEquiv(root1.left, root2.right) and self.flipEquiv(root1.right, root2.left) or \
                    self.flipEquiv(root1.left, root2.left) and self.flipEquiv(root1.right, root2.right)
            return False
        return root1 is None and root2 is None
