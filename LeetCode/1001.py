from collections import defaultdict

class Solution:
    def gridIllumination(self, N: int, lamps, queries):
        rows = defaultdict(set)
        cols = defaultdict(set)
        dias = defaultdict(set)
        rdias = defaultdict(set)

        def f1(x, y):
            return x - y

        def f2(x, y):
            return x + y

        for lamp in lamps:
            rows[lamp[0]].add(lamp[1])
            cols[lamp[1]].add(lamp[0])
            dias[f1(lamp[0], lamp[1])].add(lamp[0])
            rdias[f2(lamp[0], lamp[1])].add(lamp[0])

        ans = []
        for x, y in queries:
            if x in rows or y in cols or f1(x, y) in dias or f2(x, y) in rdias:
                ans.append(1)
            else:
                ans.append(0)
            rows.get(x, set()).discard(y-1)
            rows.get(x, set()).discard(y)
            rows.get(x, set()).discard(y+1)
            if x not in rows or len(rows[x]) == 0:
                rows.pop(x, None)
            rows.get(x-1, set()).discard(y-1)
            rows.get(x-1, set()).discard(y)
            rows.get(x-1, set()).discard(y+1)
            if x-1 not in rows or len(rows[x-1]) == 0:
                rows.pop(x-1, None)
            rows.get(x+1, set()).discard(y-1)
            rows.get(x+1, set()).discard(y)
            rows.get(x+1, set()).discard(y+1)
            if x+1 not in rows or len(rows[x+1]) == 0:
                rows.pop(x+1, None)

            cols.get(y, set()).discard(x-1)
            cols.get(y, set()).discard(x)
            cols.get(y, set()).discard(x+1)
            if y not in cols or len(cols[y]) == 0:
                cols.pop(y, None)
            cols.get(y-1, set()).discard(x-1)
            cols.get(y-1, set()).discard(x)
            cols.get(y-1, set()).discard(x+1)
            if y-1 not in cols or len(cols[y-1]) == 0:
                cols.pop(y-1, None)
            cols.get(y+1, set()).discard(x-1)
            cols.get(y+1, set()).discard(x)
            cols.get(y+1, set()).discard(x+1)
            if y+1 not in cols or len(cols[y+1]) == 0:
                cols.pop(y+1, None)

            dias.get(f1(x-1, y+1), set()).discard(x-1)
            if f1(x-1, y+1) not in dias or len(dias[f1(x-1, y+1)]) == 0:
                dias.pop(f1(x-1, y+1), None)
            dias.get(f1(x-1, y), set()).discard(x-1)
            dias.get(f1(x-1, y), set()).discard(x)
            if f1(x-1, y) not in dias or len(dias[f1(x-1, y)]) == 0:
                dias.pop(f1(x-1, y), None)
            dias.get(f1(x, y), set()).discard(x-1)
            dias.get(f1(x, y), set()).discard(x)
            dias.get(f1(x, y), set()).discard(x+1)
            if f1(x, y) not in dias or len(dias[f1(x, y)]) == 0:
                dias.pop(f1(x, y), None)
            dias.get(f1(x+1, y), set()).discard(x)
            dias.get(f1(x+1, y), set()).discard(x+1)
            if f1(x+1, y) not in dias or len(dias[f1(x+1, y)]) == 0:
                dias.pop(f1(x+1, y), None)
            dias.get(f1(x+1, y-1), set()).discard(x+1)
            if f1(x+1, y-1) not in dias or len(dias[f1(x+1, y-1)]) == 0:
                dias.pop(f1(x+1, y-1), None)

            rdias.get(f2(x-1, y-1), set()).discard(x-1)
            if f2(x-1, y-1) not in rdias or len(rdias[f2(x-1, y-1)]) == 0:
                rdias.pop(f2(x-1, y-1), None)
            rdias.get(f2(x-1, y), set()).discard(x-1)
            rdias.get(f2(x-1, y), set()).discard(x)
            if f2(x-1, y) not in rdias or len(rdias[f2(x-1, y)]) == 0:
                rdias.pop(f2(x-1, y), None)
            rdias.get(f2(x, y), set()).discard(x-1)
            rdias.get(f2(x, y), set()).discard(x)
            rdias.get(f2(x, y), set()).discard(x+1)
            if f2(x, y) not in rdias or len(rdias[f2(x, y)]) == 0:
                rdias.pop(f2(x, y), None)
            rdias.get(f2(x+1, y), set()).discard(x)
            rdias.get(f2(x+1, y), set()).discard(x+1)
            if f2(x+1, y) not in rdias or len(rdias[f2(x+1, y)]) == 0:
                rdias.pop(f2(x+1, y), None)
            rdias.get(f2(x+1, y+1), set()).discard(x+1)
            if f2(x+1, y+1) not in rdias or len(rdias[f2(x+1, y+1)]) == 0:
                rdias.pop(f2(x+1, y+1), None)

        return ans


N = 5
l = [[0,0],[1,0]]
q = [[1,1],[1,1]]
print(Solution().gridIllumination(N, l, q))