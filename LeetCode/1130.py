from collections import deque
from collections import defaultdict


class Solution:
    def shortestAlternatingPaths(self, n: int, red_edges: List[List[int]], blue_edges: List[List[int]]) -> List[int]:
        que = deque()
        seen = set()
        seen.add((0, -1))
        que.append((0, 0, -1))
        ans = [-1] * n
        red = defaultdict(list)
        blue = defaultdict(list)
        for e in red_edges:
            red[e[0]].append(e[1])
        for e in blue_edges:
            blue[e[0]].append(e[1])
        while len(que) != 0:
            node, k, co = que.popleft()
            if ans[node] == -1:
                ans[node] = k
            if co == -1 or co == 1:
                for v in red[node]:
                    if (v, 0) not in seen:
                        seen.add((v, 0))
                        que.append((v, k+1, 0))
            if co == -1 or co == 0:
                for v in blue[node]:
                    if (v, 1) not in seen:
                        seen.add((v, 1))
                        que.append((v, k+1, 1))
        return ans
