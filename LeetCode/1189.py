import collections


class Solution:
  def maxNumberOfBalloons(self, text: str) -> int:
    cnt = collections.Counter(text)
    ans = len(text)
    ans = min(ans, cnt.get('b', 0))
    ans = min(ans, cnt.get('a', 0))
    ans = min(ans, cnt.get('l', 0) // 2)
    ans = min(ans, cnt.get('o', 0) // 2)
    ans = min(ans, cnt.get('n', 0))
    return ans
