class Solution(object):
    def smallestRangeI(self, A, K):
        """
        :type A: List[int]
        :type K: int
        :rtype: int
        """
        mini = min(A)
        maxi = max(A)
        return max(0, maxi - mini - 2 * K)
