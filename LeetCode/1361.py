
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution:
	def construct(self, idx, leftChild, rightChild):
		if not self.ans:
			return None
		if self.seen[idx]:
			self.ans = False
			return None
		node = TreeNode(idx)
		self.seen[idx] = True
		if leftChild[idx] != -1:
			node.left = self.construct(leftChild[idx], leftChild, rightChild)
		if rightChild[idx] != -1:
			node.right = self.construct(rightChild[idx], leftChild, rightChild)
		return node

	def validateBinaryTreeNodes(self, n: int, leftChild: List[int], rightChild: List[int]) -> bool:
		self.seen = [False] * n
		self.ans = True
		nil = self.construct(0, leftChild, rightChild)
		return self.ans and all(self.seen)

