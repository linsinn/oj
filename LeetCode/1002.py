from collections import Counter

class Solution:
    def commonChars(self, A: List[str]) -> List[str]:
        cnt = Counter(A[0])
        for a in A:
            tmp = Counter(a)
            for k in cnt.keys():
                cnt[k] = min(cnt[k], tmp[k])
        ans = []
        for k, v in cnt.items():
            for i in range(v):
                ans.append(k)
        return ans
