class Solution:
	def reconstructMatrix(self, upper: int, lower: int, colsum: List[int]) -> List[List[int]]:
		N = len(colsum)
		ans = [[0] * N for _ in range(2)]
		tbd = []
		for i in range(N):
			if colsum[i] == 2:
				ans[0][i] = ans[1][i] = 1
				upper -= 1
				lower -= 1
			elif colsum[i] == 1:
				tbd.append(i)
		if upper < 0 or lower < 0:
			return []
		for idx in tbd:
			if upper > 0:
				ans[0][idx] = 1
				upper -= 1
			elif lower > 0:
				ans[1][idx] = 1
				lower -= 1
			else:
				return []
		if upper > 0 or lower > 0:
			return []
		return ans
