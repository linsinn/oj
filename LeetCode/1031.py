class Solution:
    def maxSumTwoNoOverlap(self, A: List[int], L: int, M: int) -> int:
        su = [0] * (len(A) + 1)
        for i, a in enumerate(A):
            su[i+1] = su[i] + a
        ans = 0
        for i in range(len(A)):
            for j in range(len(A)):
                if i + L - 1 < len(A) and j + M - 1 < len(A) and (j >= i + L or i >= j + M):
                    ans = max(ans, su[i+L]-su[i] + su[j+M]-su[j])
        return ans


