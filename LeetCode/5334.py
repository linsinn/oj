import collections
import bisect


class TweetCounts:
	def __init__(self):
		self.tweets = collections.defaultdict(list)

	def recordTweet(self, tweetName: str, time: int) -> None:
		arr = self.tweets[tweetName]
		bisect.insort_left(arr, time)

	def getTweetCountsPerFrequency(self, freq: str, tweetName: str, startTime: int, endTime: int):
		delta = 60
		if freq == 'hour':
			delta = 60 * 60
		if freq == 'day':
			delta = 24 * 60 * 60
		ans = []
		arr = self.tweets[tweetName]
		lastP = bisect.bisect_left(arr, startTime)
		i = 1
		while True:
			p = bisect.bisect(arr, min(startTime + i * delta - 1, endTime), lastP)
			ans.append(p - lastP)
			lastP = p
			if p == len(arr) or startTime + i * delta - 1 >= endTime:
				break
			i += 1
		return ans


tc = TweetCounts()
tc.recordTweet('1', 0)
tc.recordTweet('1', 60)
tc.recordTweet('1', 10)
print(tc.getTweetCountsPerFrequency('minute', '1', 0, 59))
print(tc.getTweetCountsPerFrequency('minute', '1', 0, 60))
tc.recordTweet('1', 120)
print(tc.getTweetCountsPerFrequency('hour', '1', 0, 210))



