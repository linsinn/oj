import collections
import copy

class Solution:
	def minFlips(self, mat) -> int:
		N = len(mat)
		M = len(mat[0])

		seen = set()

		def ha(m, x=-1, y=-1):
			h = 0
			for i in range(N):
				for j in range(M):
					if x != -1 and abs(i - x) + abs(j - y) <= 1:
						h = h * 2 + (m[i][j] ^ 1)
					else:
						h = h * 2 + m[i][j]
			return h

		dirs = [-1, 0, 1, 0, -1]

		que = collections.deque()
		que.append([mat, ha(mat), 0])
		while len(que) != 0:
			cur = que.popleft()
			if cur[1] == 0:
				return cur[2]
			for i in range(N):
				for j in range(M):
					h = ha(cur[0], i, j)
					if h not in seen:
						m = copy.deepcopy(cur[0])
						m[i][j] ^= 1
						for d in range(4):
							ni = i + dirs[d]
							nj = j + dirs[d+1]
							if 0 <= ni < N and 0 <= nj < M:
								m[ni][nj] ^= 1
						que.append([m, h, cur[2] + 1])
						seen.add(h)
		return -1


mat = [[0, 0], [0, 1]]
print(Solution().minFlips(mat))
