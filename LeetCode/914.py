from collections import Counter

class Solution(object):
	def hasGroupsSizeX(self, deck):
		"""
		:type deck: List[int]
		:rtype: bool
		"""
		cnt = Counter(deck)
		a = cnt.most_common()
		if len(cnt) == 1:
			return a[0][-1] > 1

		def gcd(a, b):
			return a if b == 0 else gcd(b, a % b)
	
		g = a[0][-1]
		for i in xrange(1, len(cnt)):
			g = gcd(g, a[i][-1])
			if g == 1:
				return False
		return True
			

		
