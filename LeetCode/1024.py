class Solution:
    def videoStitching(self, clips: List[List[int]], T: int) -> int:
        clips.sort()
        cur_target = 0
        best = cur_target
        i = 0
        ans = 0
        while i < len(clips):
            if clips[i][0] <= cur_target:
                best = max(best, clips[i][1])
                i += 1
            else:
                if best == cur_target:
                    return -1
                ans += 1
                cur_target = best
                if cur_target >= T:
                    return ans
        if best < T:
            return -1
        return ans + 1
