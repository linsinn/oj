import collections


class Solution:
  def uniqueOccurrences(self, arr: List[int]) -> bool:
    cnt1 = collections.Counter(arr)
    cnt2 = collections.Counter(cnt1.values())
    return all(k == 1 for k in cnt2.values())
