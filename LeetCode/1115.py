class FooBar:
    def __init__(self, n):
        self.n = n
        self.fc = 0
        self.bc = 0
        self.ff = self.bf = None

    def foo(self, printFoo: 'Callable[[], None]') -> None:
        self.fc += 1
        self.ff = printFoo
        self.check()

    def bar(self, printBar: 'Callable[[], None]') -> None:
        self.bc += 1
        self.bf = printBar
        self.check()

    def check(self):
        if self.fc == 1 and self.bc == 1:
            for i in range(self.n):
                self.ff()
                self.bf()
