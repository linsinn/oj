import bisect
from collections import Counter

class TopVotedCandidate(object):
    def __init__(self, persons, times):
        """
        :type persons: List[int]
        :type times: List[int]
        """
        self.leaders = []
        votes = Counter()
        leader, m = None, 0
        for p, t in zip(persons, times):
            votes[p] = c = votes[p] + 1
            if leader is None:
                leader = p
                m = 1
            else:
                if c >= m:
                    leader = p
                m = max(m, c)
            self.leaders.append((t, leader))

    def q(self, t):
        """
        :type t: int
        :rtype: int
        """
        idx = bisect.bisect_right(self.leaders, (t, float('inf'))) - 1
        return self.leaders[idx][1]

if __name__ == '__main__':
    t = TopVotedCandidate([0,1,1,0,0,1,0], [0,5,10,15,20,25,30])
    t.q(3)

