//
// Created by Sinn on 2/16/2020.
//

#include "LeetCode.h"

struct Cmp {
	bool operator() (const vector<int>& a, const vector<int>& b) const {
		if (a[1] != b[1]) {
			return a[1] < b[1];
		}
		return a[0] < b[0];
	}
};

class Solution {
public:
	int maxEvents(vector<vector<int>>& events) {
		sort(events.begin(), events.end(), Cmp());
		map<int, int> pos;
		int ans = 0;
		for (int i = 0; i < events.size(); ++i) {
			auto ev = events[i];
			auto it = pos.upper_bound(ev[0]);
			if (it == pos.begin()) {
				pos.emplace(ev[0], 1);
				++ans;
			} else {
				auto p = prev(it);
				if (p->first + p->second - 1 < ev[0]) {
					pos.emplace(ev[0], 1);
					++ans;
				} else if (p->first + p->second - 1 < ev[1]) {
					pos[p->first]++;
					++ans;
				}
				auto q = next(p);
				if (q != pos.end() && p->first + p->second >= q->first) {
					pos[p->first] = p->second + q->second;
					pos.erase(q);
				}
			}
		}
		return ans;
	}
};

int main() {
	vector<vector<int>> events = {{1,2},{2,3},{3,4},{1,2}};
	cout << Solution().maxEvents(events) << '\n';
}