class Solution:
  def nthUglyNumber(self, n: int, a: int, b: int, c: int) -> int:
    def gcd(n, m):
      return n if m == 0 else gcd(m, n % m)

    def lcm(n, m):
      return n * m // gcd(n, m)

    def cnt(x, y, z, n):
      return n // x + n // y + n // z - n // lcm(x, y) - n // lcm(x, z) - n // lcm(y, z) + n // lcm(x, lcm(y, z))

    lb, rb = 1, 10 ** 18 + 1
    while lb < rb:
      mid = (lb + rb) // 2
      if cnt(a, b, c, mid) < n:
        lb = mid + 1
      else:
        rb = mid
    return lb


