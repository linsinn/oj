class Solution:
	def minRemoveToMakeValid(self, s: str) -> str:
		pos = set()
		stk = []
		for i, ch in enumerate(s):
			if s[i] == '(':
				stk.append(i)
			elif s[i] == ')':
				if len(stk) == 0:
					pos.add(i)
				else:
					stk.pop()
		for p in stk:
			pos.add(p)
		ans = []
		for i, ch in enumerate(s):
			if i not in pos:
				ans.append(ch)
		return "".join(ans)
