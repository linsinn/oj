import collections

class Solution:
  def check(self, mp: dict):
    if len(mp) == 1:
      return list(mp.values())[0] == 1 or list(mp.keys())[0] == 1
    if len(mp) == 2:
      tmp = []
      for k, v in mp.items():
        tmp.append([k, v])
      tmp = sorted(tmp)
      if tmp[-1][0] == tmp[0][0] + 1 and tmp[-1][1] == 1:
        return True
      if tmp[0][0] == 1 and tmp[0][1] == 1:
        return True
    return False

  def maxEqualFreq(self, nums) -> int:
    p = collections.defaultdict(lambda: 0)
    q = collections.defaultdict(lambda: 0)
    ans = 1
    for i, num in enumerate(nums):
      if num in p:
        q[p[num]] -= 1
        if q[p[num]] == 0:
          q.pop(p[num])
      p[num] += 1
      q[p[num]] += 1
      if self.check(q):
        ans = i + 1
    return ans


nums = [2,2,1,1,5,3,3,5]
print(Solution().maxEqualFreq(nums))