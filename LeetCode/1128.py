from collections import defaultdict


class Solution:
    def numEquivDominoPairs(self, dominoes: List[List[int]]) -> int:
        mp = defaultdict(lambda: 0)
        for d in dominoes:
            t = tuple(sorted(d))
            mp[t] = mp[t] + 1
        ans = 0
        for v in mp.values():
            ans += v * (v - 1) // 2
        return ans