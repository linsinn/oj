class Solution:
  def dietPlanPerformance(self, calories: List[int], k: int, lower: int, upper: int) -> int:
    ans = 0
    tot = sum(calories[:k])
    for i in range(len(calories) - k + 1):
      if tot > upper:
        ans += 1
      elif tot < lower:
        ans -= 1
      tot -= calories[i]
      tot += 0 if i + k >= len(calories) else calories[i+k]
    return ans
