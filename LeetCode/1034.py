import copy
import collections

class Solution:
    def colorBorder(self, grid, r0: int, c0: int, color: int):
        N, M = len(grid), len(grid[0])
        ans = copy.deepcopy(grid)
        que = collections.deque()
        que.append([r0, c0])
        vis = set()
        vis.add((r0, c0))
        dirs = [0, 1, 0, -1, 0]

        def is_border(r, c):
            for d in range(4):
                nr = r + dirs[d]
                nc = c + dirs[d+1]
                if not 0 <= nr < N or not 0 <= nc < M:
                    return True
                if grid[nr][nc] != grid[r][c]:
                    return True
            return False

        while len(que) != 0:
            r, c = que.popleft()
            if is_border(r, c):
                ans[r][c] = color
            for d in range(4):
                nr = r + dirs[d]
                nc = c + dirs[d+1]
                if 0 <= nr < N and 0 <= nc < M and (nr, nc) not in vis and grid[r][c] == grid[nr][nc]:
                    que.append([nr, nc])
                    vis.add((nr, nc))
        return ans


grid = [[1, 1], [1, 2]]
r0, c0 = 0, 0
color = 3
ans = Solution().colorBorder(grid, r0, c0, color)
print(ans)
