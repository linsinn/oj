import bisect

class Solution:
    def oddEvenJumps(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        N = len(A)
        le = [-1] * N
        ge = [-1] * N
        tmpA = [[A[-1], N-1]]
        for i in range(N-2, -1, -1):
            pos = bisect.bisect_left(tmpA, [A[i], i])
            if pos < len(tmpA):
                ge[i] = tmpA[pos][1]
            tmpA.insert(pos, [A[i], i])

        tmpA = [[A[-1], -N+1]]
        for i in range(N-2, -1, -1):
            pos = bisect.bisect_right(tmpA, [A[i], -i])
            if pos > 0:
                le[i] = -tmpA[pos-1][1]
            tmpA.insert(pos, [A[i], -i])

        f = [[0] * 2 for _ in range(N)]

        def dp(i, oe):
            if f[i][oe] != 0:
                return f[i][oe]
            if i == N-1:
                return 1
            if oe == 1:
                nxt = ge[i]
            else:
                nxt = le[i]
            if nxt == -1:
                f[i][oe] = -1
                return -1
            ret = dp(nxt, oe ^ 1)
            f[i][oe] = ret
            return ret

        ans = 0
        for i in range(N):
            ret = dp(i, 1)
            ans += max(ret, 0)
        return ans


A = [1, 2, 1, 2, 1]
print(Solution().oddEvenJumps(A))