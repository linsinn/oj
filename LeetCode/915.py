class Solution(object):
	def partitionDisjoint(self, A):
		"""
		:type A: List[int]
		:rtype: int
		"""
		l = [0] * len(A)
		r = [0] * len(A)
		l[0] = A[0]
		r[-1] = A[-1]
		for i in xrange(1, len(A)):
			l[i] = max(A[i], l[i-1])
			r[len(A)-1-i] = min(A[len(A)-1-i], r[len(A)-i])
		for i in xrange(1, len(A)):
			if l[i-1] <= r[i]:
				return i
		return len(A)
