class Solution:
    def numsSameConsecDiff(self, N, K):
        """
        :type N: int
        :type K: int
        :rtype: List[int]
        """
        bits = [0] * N
        ans = set()

        def dfs(idx):
            if idx == N:
                tmp = 0
                for b in bits:
                    tmp = tmp * 10 + b
                ans.add(tmp)
                return
            if bits[idx-1] + K <= 9:
                bits[idx] = bits[idx-1] + K
                dfs(idx+1)
            if bits[idx-1] - K >= 0:
                bits[idx] = bits[idx-1] - K
                dfs(idx+1)

        for i in range(1, 10):
            bits[0] = i
            dfs(1)
        if N == 1:
            ans.add(0)
        return list(ans)
