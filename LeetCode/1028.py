from LeetCode import TreeNode

class Solution:
    def nextDashLength(self, S: str, idx: int):
        ret = 0
        while idx < len(S) and S[idx] == '-':
            ret += 1
            idx += 1
        return ret

    def nextNumber(self, S: str, idx: int):
        num = 0
        dig = 0
        while idx < len(S) and S[idx].isdigit():
            num = num * 10 + int(S[idx])
            dig += 1
            idx += 1
        return num, dig

    def dfs(self, S: str, dep: int):
        num, num_length = self.nextNumber(S, self.idx)
        self.idx += num_length
        node = TreeNode(num)
        dash_length = self.nextDashLength(S, self.idx)
        if dash_length == dep + 1:
            self.idx += dash_length
            node.left = self.dfs(S, dep+1)
        dash_length = self.nextDashLength(S, self.idx)
        if dash_length == dep + 1:
            self.idx += dash_length
            node.right = self.dfs(S, dep+1)
        return node

    def recoverFromPreorder(self, S: str) -> TreeNode:
        self.idx = 0
        return self.dfs(S, 0)