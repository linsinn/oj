class Solution:
	def makeConnected(self, n: int, connections: List[List[int]]) -> int:
		if len(connections) < n - 1:
			return -1
		gph = [[] for _ in range(n)]
		for u, v in connections:
			gph[u].append(v)
			gph[v].append(u)

		seen = [0] * n

		def dfs(u):
			seen[u] = 1
			for v in gph[u]:
				if seen[v] == 0:
					dfs(v)

		cnt = 0
		for i in range(n):
			if seen[i] == 0:
				dfs(i)
				cnt += 1

		return cnt - 1
