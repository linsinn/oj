from LeetCode import ListNode


class Solution:
	def helper(self, node, target):
		if node is None:
			return 0
		ret = self.helper(node.next, target)
		if ret == target:
			node.next = node.next.next
		return ret + 1

	def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
		nil = ListNode(-1)
		nil.next = head
		self.helper(nil, n)
		return nil.next