from LeetCode import TreeNode


class Solution:
    def dfs1(self, root: TreeNode, idx, hei, dep):
        if root is None:
            return
        self.dfs1(root.left, idx * 2, hei + 1, dep)
        self.dfs1(root.right, idx * 2 + 1, hei + 1, dep)
        if root.left is None and root.right is None:
            dep.append([idx, hei])

    def dfs2(self, root: TreeNode, idx, p):
        if root is None:
            return
        l = self.dfs2(root.left, idx * 2, p)
        if l is not None:
            return l
        r = self.dfs2(root.right, idx * 2 + 1, p)
        if r is not None:
            return r
        if idx == p:
            return root
        return None

    def find_p(self, a, b):
        ss = set()
        while a > 0:
            ss.add(a)
            a //= 2
        while b > 0:
            if b in ss:
                return b
            b //= 2
        return 1

    def lcaDeepestLeaves(self, root: TreeNode) -> TreeNode:
        depth = []
        self.dfs1(root, 1, 0, depth)
        maxi = 0
        g = []
        for d in depth:
            if d[1] == maxi:
                g.append(d[0])
            elif d[1] > maxi:
                maxi = d[1]
                g = [d[0]]
        p = g[0]
        for i in range(1, len(g)):
            p = self.find_p(p, g[i])
        return self.dfs2(root, 1, p)


root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
print(Solution().lcaDeepestLeaves(root))