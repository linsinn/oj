class Solution:
	def build(self):
		mp = {}
		for i in range(1, 4):
			mp[i] = 'I' * i
		mp[4] = 'IV'
		mp[5] = 'V'
		for i in range(6, 9):
			mp[i] = 'V' + 'I' * (i - 5)
		mp[9] = 'IX'
		for i in range(1, 4):
			mp[i * 10] = 'X' * i
		mp[40] = 'XL'
		mp[50] = 'L'
		for i in range(6, 9):
			mp[i * 10] = 'L' + 'X' * (i - 5)
		mp[90] = 'XC'
		for i in range(1, 4):
			mp[i * 100] = 'C' * i
		mp[400] = 'CD'
		mp[500] = 'D'
		for i in range(6, 9):
			mp[i * 100] = 'D' + 'C' * (i - 5)
		mp[900] = 'CM'
		for i in range(1, 4):
			mp[i * 1000] = 'M' * i
		self.mp = mp

	def intToRoman(self, num: int) -> str:
		self.build()
		ans = ""
		g = 1
		while num != 0:
			t = num % 10
			if t != 0:
				ans = self.mp[t * g] + ans
			num //= 10
			g *= 10
		return ans


print(Solution().intToRoman(58))