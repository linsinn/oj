class Solution:
    def movesToMakeZigzag(self, nums: List[int]) -> int:
        ans = 1 << 30
        cur = 0
        tmp = nums[:]
        less = True
        for i in range(1, len(nums)):
            if less:
                cur += max(0, tmp[i] - (tmp[i-1] - 1))
                tmp[i] = min(tmp[i], tmp[i-1] - 1)
            else:
                cur += max(0, tmp[i-1] - (tmp[i] - 1))
                tmp[i-1] = min(tmp[i-1], tmp[i] - 1)
            less = not less
        ans = min(ans, cur)
        cur = 0
        less = False
        tmp = nums[:]
        for i in range(1, len(nums)):
            if less:
                cur += max(0, tmp[i] - (tmp[i-1] - 1))
                tmp[i] = min(tmp[i], tmp[i-1] - 1)
            else:
                cur += max(0, tmp[i-1] - (tmp[i] - 1))
                tmp[i-1] = min(tmp[i-1], tmp[i] - 1)
            less = not less
        ans = min(ans, cur)
        return ans
