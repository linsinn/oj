class Solution:
	def pancakeSort(self, A):
		"""
		:type A: List[int]
		:rtype: List[int]
		"""
		ans = []

		def getMaxAndIdx(K):
			a, b = A[0], 0
			for i in range(K):
				if A[i] > a:
					a = A[i]
					b = i
			return a, b + 1

		N = len(A)

		while N > 0:
			a, b = getMaxAndIdx(N)
			if b != N:
				ans.append(b)
				ans.append(N)
			A[:b] = A[:b][::-1]
			A[:N] = A[:N][::-1]
			N -= 1

		return ans

