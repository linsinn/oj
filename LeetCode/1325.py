# Definition for a binary tree node.
class TreeNode:
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution:
	def dfs(self, root, target, parent, lr):
		if root is None:
			return
		self.dfs(root.left, target, root, 'l')
		self.dfs(root.right, target, root, 'r')
		if root.left is None and root.right is None and root.val == target:
			if lr == 'l':
				parent.left = None
			else:
				parent.right = None
		return root

	def removeLeafNodes(self, root: TreeNode, target: int) -> TreeNode:
		nil = TreeNode(-1)
		nil.left = root
		self.dfs(nil.left, target, nil, 'l')
		return nil.left
