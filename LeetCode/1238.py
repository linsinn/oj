class Solution(object):
	def circularPermutation(self, n, start):
		"""
		:type n: int
		:type start: int
		:rtype: List[int]
		"""
		ans = []

		def toGray(num):
			return num ^ (num >> 1)

		idx = 0
		for i in range(1 << n):
			k = toGray(i)
			if k == start:
				idx = i
			ans.append(k)
		return ans[idx:] + ans[:idx]


