class Solution:
    def canThreePartsEqualSum(self, A: List[int]) -> bool:
        su = sum(A)
        if su % 3 != 0:
            return False
        p = su // 3
        t = su // 3 * 2
        s = {A[0]}
        cur = A[0]
        for i in range(1, len(A)-1):
            cur += A[i]
            if cur == t and p in s:
                return True
            s.add(cur)
        return False

