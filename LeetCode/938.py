from LeetCode import TreeNode

class Solution(object):
	def rangeSumBST(self, root, L, R):
		"""
		:type root: TreeNode
		:type L: int
		:type R: int
		:rtype: int
		"""
		if root is None:
			return 0
		ans = 0
		if L <= root.val <= R:
			ans += root.val
		ans += self.rangeSumBST(root.left, L, R)
		ans += self.rangeSumBST(root.right, L, R)
		return ans


