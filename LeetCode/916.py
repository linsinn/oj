from collections import Counter

class Solution(object):
	def wordSubsets(self, A, B):
		"""
		:type A: List[str]
		:type B: List[str]
		:rtype: List[str]
		"""
		cnt = Counter()
		for b in B:
			t =  Counter(b)
			for k, v in t.iteritems():
				cnt[k] = max(cnt[k], v)
		ans = []
		for a in A:
			k = Counter(a)
			k.subtract(cnt)
			if k.most_common()[-1][-1] >= 0:
				ans.append(a)
		return ans
