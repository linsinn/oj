class Solution:
	def minInsertions(self, s: str) -> int:
		N = len(s)
		t = s[::-1]
		aux = [[0] * (N+1) for _ in range(N+1)]
		for i in range(N):
			for j in range(N):
				aux[i+1][j+1] = aux[i+1][j]
				aux[i+1][j+1] = max(aux[i+1][j+1], aux[i][j+1])
				aux[i+1][j+1] = max(aux[i+1][j+1], aux[i][j] + (1 if s[i] == t[j] else 0))
		return N - aux[N][N]


s = "12"
print(Solution().minInsertions("mbadm"))
