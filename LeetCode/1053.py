class Solution:
    def prevPermOpt1(self, A: List[int]) -> List[int]:
        N = len(A)
        if N == 1:
            return A
        i = N - 2
        while i >= 0:
            if A[i] > A[i+1]:
                break
            i -= 1
        if i < 0:
            return A
        s = i + 1
        for j in range(i+1, N):
            if A[i] > A[j] > A[s]:
                s = j
        A[i], A[s] = A[s], A[i]
        return A
