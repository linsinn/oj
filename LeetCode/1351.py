class Solution:
	def countNegatives(self, grid: List[List[int]]) -> int:
		cnt = 0
		for row in grid:
			for cell in row:
				if cell < 0:
					cnt += 1
		return cnt
