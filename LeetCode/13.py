class Solution:
	def build(self):
		mp = {}
		for i in range(1, 4):
			mp[i] = 'I' * i
		mp[4] = 'IV'
		mp[5] = 'V'
		for i in range(6, 9):
			mp[i] = 'V' + 'I' * (i - 5)
		mp[9] = 'IX'
		for i in range(1, 4):
			mp[i * 10] = 'X' * i
		mp[40] = 'XL'
		mp[50] = 'L'
		for i in range(6, 9):
			mp[i * 10] = 'L' + 'X' * (i - 5)
		mp[90] = 'XC'
		for i in range(1, 4):
			mp[i * 100] = 'C' * i
		mp[400] = 'CD'
		mp[500] = 'D'
		for i in range(6, 9):
			mp[i * 100] = 'D' + 'C' * (i - 5)
		mp[900] = 'CM'
		for i in range(1, 4):
			mp[i * 1000] = 'M' * i
		rmp = {}
		for k, v in mp.items():
			rmp[v] = k
		self.mp = rmp

	def romanToInt(self, s: str) -> int:
		self.build()
		j = len(s) - 1
		ans = 0
		while j >= 0:
			i = j
			while i >= 0 and s[i:j+1] in self.mp:
				i -= 1
			ans += self.mp[s[i+1:j+1]]
			j = i
		return ans
