#include "LeetCode.h"

class RLEIterator {
    vector<int> vec;
public:
    RLEIterator(vector<int> A) {
        vec = A;
        std::reverse(vec.begin(), vec.end());
    }
    int next(int n) {
        auto iter = vec.rbegin();
        while (iter != vec.rend() && *iter < n) {
            n -= *iter;
            std::advance(iter, 2);
            vec.pop_back();
            vec.pop_back();
        }
        if (iter == vec.rend())
            return -1;
        *iter -= n;
        return *(++iter);
    }
};


int main() {
    Solution sol;
    return 0;
}

