import collections


class Solution:
  def topOrder(self, n, gph):
    degs = collections.defaultdict(lambda: 0)
    for k, v in gph.items():
      for u in v:
        degs[u] += 1
    c = []
    for k, v in gph.items():
      if k not in degs:
        c.append(k)
    ret = []
    while len(c) != 0:
      tmp = []
      for u in c:
        ret.append(u)
        for v in gph[u]:
          degs[v] -= 1
          if degs[v] == 0:
            tmp.append(v)
      c = tmp
    return ret

  def sortItems(self, n: int, m: int, group, beforeItems):
    mp = collections.defaultdict(list)
    for i, grp in enumerate(group):
      if grp != -1:
        mp[grp].append(i)
    g = 0
    for v in mp.values():
      for i in v:
        group[i] = g
      g += 1
    for i, grp in enumerate(group):
      if grp == -1:
        group[i] = g
        g += 1

    mp = collections.defaultdict(list)
    for i, grp in enumerate(group):
      mp[group[i]].append(i)

    for k, v in mp.items():
      gph = collections.defaultdict(list)
      for i in v:
        if i not in gph:
          gph[i] = []
        for j in beforeItems[i]:
          if group[j] == k:
            gph[j].append(i)
      ret = self.topOrder(len(v), gph)
      if len(ret) != len(v):
        return []
      mp[k] = ret

    gph = collections.defaultdict(set)
    for i in range(n):
      if group[i] not in gph:
        gph[group[i]] = set()
      for j in beforeItems[i]:
        if group[j] != group[i]:
          gph[group[j]].add(group[i])
    ret = self.topOrder(g, gph)
    if len(ret) != g:
      return []
    ans = []
    for i in ret:
      for v in mp[i]:
        ans.append(v)
    return ans


n = 4
m = 1
group = [-1, 0, 0, -1]
bi = [[], [0], [1, 3], [2]]
print(Solution().sortItems(n, m, group, bi))