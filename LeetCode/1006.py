class Solution:
    def clumsy(self, N: int) -> int:
        ans = N * max(1, (N-1)) // max(1, (N-2)) + max(0, (N-3))
        N -= 4
        while N > 0:
            ans -= N * max(1, (N-1)) // max(1, (N-2)) - max(0, (N-3))
            N -= 4
        return ans


print(Solution().clumsy(10))