import collections


class Solution:
  def minimumMoves(self, grid) -> int:
    N = len(grid)
    st = (0, 0, 0, 1)
    que = collections.deque([[st, 0]])
    seen = set(st)

    while len(que) != 0:
      cur, step = que.popleft()
      if cur[0] == N-1 and cur[1] == N-2 and cur[2] == N-1 and cur[3] == N-1:
        return step
      if cur[0] == cur[2]:
        t = cur[3] + 1
        if t < N and grid[cur[0]][t] == 0:
          nxt = (cur[0], cur[3], cur[0], t)
          if nxt not in seen:
            seen.add(nxt)
            que.append([nxt, step + 1])
        g = cur[0] + 1
        if g < N and grid[g][cur[1]] == 0 and grid[g][cur[3]] == 0:
          nxt = (cur[0], cur[1], g, cur[1])
          if nxt not in seen:
            seen.add(nxt)
            que.append([nxt, step + 1])
          nxt = (g, cur[1], g, cur[3])
          if nxt not in seen:
            seen.add(nxt)
            que.append([nxt, step + 1])
      else:
        t = cur[2] + 1
        if t < N and grid[t][cur[1]] == 0:
          nxt = (cur[2], cur[3], t, cur[3])
          if nxt not in seen:
            seen.add(nxt)
            que.append([nxt, step + 1])
        g = cur[1] + 1
        if g < N and grid[cur[0]][g] == 0 and grid[cur[2]][g] == 0:
          nxt = (cur[0], cur[1], cur[0], g)
          if nxt not in seen:
            seen.add(nxt)
            que.append([nxt, step + 1])
          nxt = (cur[0], g, cur[2], g)
          if nxt not in seen:
            seen.add(nxt)
            que.append([nxt, step + 1])
    return -1


grid = [[0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,1,0,0,1,0],
        [0,1,0,0,1,0,1,1,0,0,0,1,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,1,0],
        [0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1],
        [0,1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1,1],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0],
        [1,0,0,0,0,0,0,1,0,0,1,0,0,0,1,1,0,0,1,1],
        [0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0],
        [0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,1,0,0,0,0],
        [0,0,0,0,1,1,1,1,0,0,0,0,1,1,0,0,0,0,1,0],
        [1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,1,0,0,1,1,0,0,1,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,1,0,0,1,1,0,0,0,1,0,0,0,0,1,0],
        [0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0]]
ans = Solution().minimumMoves(grid)
print(ans)
