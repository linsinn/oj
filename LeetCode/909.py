from collections import deque

class Solution(object):
    def snakesAndLadders(self, board):
        """
        :type board: List[List[int]]
        :rtype: int
        """
        N = len(board)
        que = deque()
        que.append((1, 0))
        vis = set([1])

        def getAxis(idx):
            x = N - 1 - (idx - 1) // N
            y = (idx - 1) % N
            if x % 2 == N % 2:
                y = N - 1 - y
            return x, y
        
        while len(que) != 0:
            q = que.popleft()
            if q[0] >= N * N:
                return q[1]
            for i in range(1, 7):
                if i > N * N:
                    continue
                x, y = getAxis(q[0] + i)
                if board[x][y] != -1:
                    if board[x][y] not in vis:
                        vis.add(board[x][y])
                        que.append((board[x][y], q[1] + 1))
                elif q[0] + i not in vis:
                    vis.add(q[0] + i)
                    que.append((q[0] + i, q[1] + 1))
        return -1

if __name__ == '__main__':
    b = [[1,1,-1],[1,1,1],[-1,1,1]]
    Solution().snakesAndLadders(b)
