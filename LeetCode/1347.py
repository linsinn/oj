import collections

class Solution:
	def minSteps(self, s: str, t: str) -> int:
		sc = collections.Counter(s)
		tc = collections.Counter(t)
		matched = 0
		for k, v in sc.items():
			g = tc.get(k, 0)
			matched += min(v, g)
		return len(s) - matched


