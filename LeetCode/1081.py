class Solution:
    def smallestSubsequence(self, text: str) -> str:
        al = set(text)
        target = 0
        for i in range(0, 26):
            if chr(ord('a') + i) in al:
                target |= 1 << i

        f = [0] * len(text)
        for i in range(len(text)-2, -1, -1):
            f[i] = f[i+1]
            f[i] |= (1 << (ord(text[i+1]) - ord('a')))

        idx = 0
        ans = []
        while target != 0:
            flag = False
            for i in range(26):
                if target & (1 << i) != 0:
                    for k in range(idx, len(text)):
                        if ord(text[k]) - ord('a') == i and ((f[k] | 1 << i) & target) >= target:
                            ans.append(chr(ord('a') + i))
                            target ^= 1 << i
                            idx = k+1
                            flag = True
                            break
                    if flag:
                        break
        return "".join(ans)


text = "leetcode"
print(Solution().smallestSubsequence(text))

