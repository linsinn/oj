//
// Created by sinn on 12/23/18.
//

#include <bits/stdc++.h>
using namespace std;


class Solution {
public:
    vector<vector<int>> points;

    int length2(vector<int>& a, vector<int>& b) {
        return (b[0] - a[0]) * (b[0] - a[0]) + (b[1] - a[1]) * (b[1] - a[1]);
    }

    int dot(vector<int>& a, vector<int>& b, vector<int>& c) {
        return (b[0] - a[0]) * (c[0] - b[0]) + (b[1] - a[1]) * (c[1] - b[1]);
    }

    int cross(vector<int>& a, vector<int>& b, vector<int>& c) {
        return (b[0] - a[0]) * (c[1] - a[1]) - (b[1] - a[1]) * (c[0] - a[0]);
    }

    int area(vector<vector<int>>& rects) {
        int a = 0, b = 1;
        for (int j = 2; j < 4; j++) {
            if (length2(rects[a], rects[j]) > length2(rects[a], rects[b]))
                b = j;
        }
        int c = -1, d = -1;
        for (int j = 1; j < 4; j++) {
            if (j != a && j != b && c == -1)
                c = j;
            if (j != a && j != b && j != c && d == -1)
                d = j;
        }
        if (dot(rects[a], rects[c], rects[b]) != 0 || dot(rects[a], rects[d], rects[b]) != 0)
            return 0;
        int x = cross(rects[a], rects[b], rects[c]);
        int y = cross(rects[a], rects[b], rects[d]);
        if (1LL * x * y >= 0 || x + y != 0)
            return 0;
        return abs(x);
    }

    int dfs(int idx, vector<vector<int>>& rects) {
        if (points.size() - idx < 4 - rects.size())
            return 0;
        if (rects.size() == 4)
            return area(rects);
        int ret = INT32_MAX;
        for (int i = idx; i < points.size(); i++) {
            rects.emplace_back(points[i]);
            int a = dfs(i + 1, rects);
            if (a != 0)
                ret = min(ret, a);
            rects.pop_back();
        }
        return ret;
    }

    double minAreaFreeRect(vector<vector<int>>& points) {
        this->points = points;
        vector<vector<int>> rects;
        int ans = dfs(0, rects);
        ans = (ans == INT32_MAX) ? 0 : ans;
        return ans;
    }
};

int main() {
    Solution sol;
//    vector<vector<int>> points = {{1,2}, {2,1}, {1, 0}, {0, 1}};
    vector<vector<int>> points = {{24420,16685},{20235,25520},{14540,20845},{20525,14500},{16876,24557},{24085,23720},{25427,18964},{21036,14573},{24420,23315},{22805,24760},{21547,25304},{16139,23952},{21360,14645},{24715,17120},{19765,25520},{19388,25491},{22340,25005},{25520,19765},{25365,21320},{23124,15443},{20845,14540},{24301,16532},{16685,24420},{25100,17875},{22125,25100},{15699,23468},{14592,21131},{25460,19155},{17837,25084},{23468,24301},{25460,20845},{18453,25304},{21131,14592},{22805,15240},{19475,25500},{15443,23124},{25355,21360},{15285,22880},{20000,25525},{24085,16280},{22163,25084},{22880,15285},{14916,22163},{16280,24085},{24875,17400},{22600,24875},{14573,21036},{25427,21036},{17120,24715},{25500,19475}};
    cout << sol.minAreaFreeRect(points) << '\n';

    return 0;
}
