
class Solution:
    def equationsPossible(self, equations: 'List[str]') -> 'bool':
        graph = [[] for _ in range(30)]
        for equs in equations:
            x = ord(equs[0]) - ord('a')
            y = ord(equs[3]) - ord('a')
            if equs[1:3] == '==' and x != y:
                graph[x].append(y)
                graph[y].append(x)
        vis = [0] * 30

        def dfs(rt, var):
            vis[rt] = var
            for ng in graph[rt]:
                if vis[ng] == 0:
                    dfs(ng, var)

        var = 1
        for i in range(30):
            if vis[i] == 0:
                dfs(i, var)
                var += 1

        for equs in equations:
            x = ord(equs[0]) - ord('a')
            y = ord(equs[3]) - ord('a')
            if equs[1:3] == '!=':
                if vis[x] == vis[y]:
                    return False
        return True


equs = ["a!=i","g==k","k==j","k!=i","c!=e","a!=e","k!=a","a!=g","g!=c"]
print(Solution().equationsPossible(equs))

