class Solution(object):
    def minDeletionSize(self, A):
        """
        :type A: List[str]
        :rtype: int
        """
        ans = 0
        for i in xrange(len(A[0])):
            for j in xrange(1, len(A)):
                if A[j][i] < A[j-1][i]:
                    ans += 1
                    break
        return ans
