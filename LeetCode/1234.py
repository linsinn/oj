import collections


class Solution:
  def check(self, l, s):
    bnd = len(s) // 4
    cnt = collections.Counter(s)
    if max(cnt.values()) <= bnd:
      return True
    tmp = collections.Counter(s[:l])
    for i in range(len(s) - l + 1):
      b = True
      for k in cnt.keys():
        if cnt[k] - tmp.get(k, 0) > bnd:
          b = False
      if b:
        return True
      if i + l < len(s):
        tmp[s[i]] -= 1
        tmp[s[i+l]] += 1
    return False

  def balancedString(self, s: str) -> int:
    l = 0
    r = len(s)
    ans = r
    while l <= r:
      mid = (l + r) // 2
      if self.check(mid, s):
        ans = mid
        r = mid - 1
      else:
        l = mid + 1
    return ans


s = "QQQW"
print(Solution().balancedString(s))
