class Solution:
    def baseNeg2(self, N: int) -> str:
        if N == 0:
            return '0'
        ans = ''
        while N != 0:
            ans += str(abs(N % -2))
            if N > 0:
                N //= 2
                N = -N
            else:
                N = (N // -2) + (1 if abs(N) % 2 != 0 else 0)
        return str(int(ans[::-1]))

print(Solution().baseNeg2(4))
