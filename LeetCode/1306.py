import collections


class Solution:
	def canReach(self, arr: List[int], start: int) -> bool:
		N = len(arr)
		seen = [0] * N
		seen[start] = 1
		que = collections.deque()
		que.append(start)
		while len(que) > 0:
			cur = que.popleft()
			if arr[cur] == 0:
				return True
			nxt = cur + arr[cur]
			if nxt < N and seen[nxt] == 0:
				que.append(nxt)
				seen[nxt] = 1
			nxt = cur - arr[cur]
			if nxt >= 0 and seen[nxt] == 0:
				que.append(nxt)
				seen[nxt] = 1
		return False
