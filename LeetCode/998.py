from LeetCode import TreeNode

class Solution:
    def helper(self, root: TreeNode, val: int):
        if root.right is None:
            root.right = TreeNode(val)
        else:
            if val > root.right.val:
                tmp = root.right
                root.right = TreeNode(val)
                root.right.left = tmp
            else:
                self.helper(root.right, val)

    def insertIntoMaxTree(self, root: TreeNode, val: int) -> TreeNode:
        if val > root.val:
            t = TreeNode(val)
            t.left = root
            return t
        else:
            self.helper(root, val)
            return root

