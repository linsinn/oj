class Solution:
	def isValid(self, s: str) -> bool:
		stk = []
		mp = {}
		mp[']'] = '['
		mp['}'] = '{'
		mp[')'] = '('
		lp = ['[', '(', '{']
		for ch in s:
			if ch in lp:
				stk.append(ch)
			else:
				if len(stk) == 0 or mp[ch] != stk[-1]:
					return False
				stk.pop()
		return len(stk) == 0
