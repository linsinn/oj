from LeetCode import TreeNode
import collections


class CBTInserter(object):

    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.arr = []
        que = collections.deque([root])
        while que:
            u = que.popleft()
            self.arr.append(u)
            if u.left:
                que.append(u.left)
            if u.right:
                que.append(u.right)

    def insert(self, v):
        """
        :type v: int
        :rtype: int
        """
        p = self.arr[(len(self.arr) - 1) // 2]
        u = TreeNode(v)
        self.arr.append(u)
        if p.left:
            p.right = u
        else:
            p.left = u
        return p.val

    def get_root(self):
        """
        :rtype: TreeNode
        """
        return self.arr[0]
