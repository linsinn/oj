import heapq
import collections


class Solution:
    def rearrangeBarcodes(self, barcodes: List[int]) -> List[int]:
        counters = collections.Counter(barcodes)
        h = []
        for e, t in counters.items():
            heapq.heappush(h, (-t, e))
        ans = []
        while len(h) > 0:
            a = heapq.heappop(h)
            ans.append(a[1])
            if len(h) > 0:
                b = heapq.heappop(h)
                ans.append(b[1])
                if b[0] + 1 < 0:
                    heapq.heappush(h, (b[0]+1, b[1]))
            if a[0] + 1 < 0:
                heapq.heappush(h, (a[0]+1, a[1]))
        return ans
