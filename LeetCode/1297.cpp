//
// Created by sinn on 12/22/19.
//

#include "LeetCode.h"

class Solution {
public:
	int maxFreq(string& s, int maxLetters, int minSize, int maxSize) {
		int MOD = 1e9 + 7;
		int MOD1 = 998244353;
		int N = s.size();
		unordered_map<int, int> cnt;
		for (int i = 0; i < N - minSize + 1; ++i) {
			set<char> ss;
			int ha = 0;
			for (int j = 0; j < minSize - 1; ++j) {
				ss.emplace(s[i + j]);
				ha = (1LL * ha * 27 + (s[i + j] - 'a' + 1)) % MOD;
			}
			for (int j = 0; j < maxSize - minSize + 1; ++j) {
				if (i + minSize - 1 + j >= N)
					break;
				ss.emplace(s[i + minSize - 1 + j]);
				ha = (1LL * ha * 27 + (s[i + minSize - 1 + j] - 'a' + 1)) % MOD;
				if (ss.size() > maxLetters) {
					break;
				}
				cnt[ha]++;
			}
		}
		int ans = 0;
		for (auto& p : cnt) {
			ans = max(ans, p.second);
		}
		return ans;
	}
};


int main() {
	return 0;
}