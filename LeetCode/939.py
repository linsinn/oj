from collections import defaultdict


class Solution(object):
	def minAreaRect(self, points):
		"""
		:type points: List[List[int]]
		:rtype: int
		"""
		xy = defaultdict(list)
		for point in points:
			xy[point[0]].append(point[1])
		xy = sorted(xy.items())
		ans = 10 ** 10
		for i in xrange(len(xy)):
			for j in xrange(i+1, len(xy)):
				width = xy[j][0] - xy[i][0]
				p, q = set(xy[j][1]), set(xy[i][1])
				l = sorted(list(p.intersection(q)))
				for j in xrange(len(l)-1):
					ans = min(ans, width * (l[j+1] - l[j]))
		return ans if ans != 10 ** 10 else 0
