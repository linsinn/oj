from LeetCode import TreeNode

import collections


class Solution:
    def maxLevelSum(self, root: TreeNode) -> int:
        que = collections.deque()
        que.append([root, 1])
        ans = [root.val, 1]
        su = 0
        lvl = 1
        while len(que) != 0:
            n = que.popleft()
            if n[1] != lvl:
                if su > ans[0]:
                    ans[0] = su
                    ans[1] = lvl
                lvl += 1
                su = 0
            su += n[0].val
            if n[0].left is not None:
                que.append([n[0].left, n[1] + 1])
            if n[0].right is not None:
                que.append([n[0].right, n[1] + 1])
        if su > ans[0]:
            ans[0] = su
            ans[1] = lvl
        return ans[1]
