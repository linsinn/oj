class Solution(object):
	def findSolution(self, customfunction, z):
		"""
		:type num: int
		:type z: int
		:rtype: List[List[int]]
		"""
		ans = []
		bnd = 1000
		foo = customfunction.f
		for i in range(1, bnd + 1):
			l, r = 1, bnd
			res = -1
			while l <= r:
				m = (l + r) // 2
				if foo(i, m) < z:
					l = m + 1
				elif foo(i, m) > z:
					r = m - 1
				else:
					res = m
					break
			if res != -1:
				ans.append([i, res])
		return ans
