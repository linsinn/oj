//
// Created by sinn on 12/22/19.
//

#include "LeetCode.h"

class Solution {
public:
	bool isPossibleDivide(vector<int>& nums, int k) {
		int N = nums.size();
		if (N % k != 0)
			return false;
		map<int, int> mp;
		for (int i = 0; i < nums.size(); i++)
			mp[nums[i]]++;
		while (!mp.empty()) {
			int be = mp.begin()->first;
			for (int i = be; i < be + k; i++) {
				auto it = mp.find(i);
				if (it == mp.end())
					return false;
				if (it->second == 1)
					mp.erase(it);
				else
					mp[i]--;
			}
		}
		return true;
	}
};


int main() {
	vector<int> nums = {1, 2, 3, 3, 4, 4, 5, 6};
	cout << Solution().isPossibleDivide(nums, 4) << '\n';
	return 0;
}