import collections


class Solution:
    def longestStrChain(self, words) -> int:
        len_to_str = collections.defaultdict(list)
        maxi_len = 0
        for word in words:
            len_to_str[len(word)].append(word)
            maxi_len = max(maxi_len, len(word))

        memos = {}

        def is_predecessor(w1, w2):
            i, j = 0, 0
            skip_once = False
            while i < len(w1) and j < len(w2):
                if w1[i] != w2[j]:
                    if not skip_once:
                        skip_once = True
                        j += 1
                    else:
                        return False
                if w1[i] != w2[j]:
                    return False
                i += 1
                j += 1
            return True

        def dfs(word):
            if word in memos:
                return memos[word]
            l = len(word)

            cur = 0
            for w in len_to_str[l+1]:
                if is_predecessor(word, w):
                    cur = max(cur, dfs(w))
            cur += 1
            memos[word] = cur
            return cur

        ans = 0
        for i in range(1, maxi_len+1):
            if i not in len_to_str:
                continue
            for word in len_to_str[i]:
                ans = max(ans, dfs(word))
        return ans


words = ["a","b","ba","bca","bda","bdca"]
print(Solution().longestStrChain(words))