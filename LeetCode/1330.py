class Solution:
	def maxValueAfterReverse(self, nums) -> int:
		N = len(nums)
		if N == 1:
			return 0
		if N == 2:
			return abs(nums[0] - nums[1])
		diff = 0
		c = [-1, -1]
		d = [-1, -1]
		for i in range(N-1, 0, -1):
			a, b = nums[i-1], nums[i]
			for idx in c:
				if idx != -1:
					g = abs(a - b) + abs(nums[idx] - nums[idx+1])
					t = abs(a - nums[idx]) + abs(b - nums[idx+1])
					diff = max(diff, t - g)
			for idx in d:
				if idx != -1:
					g = abs(a - b) + abs(nums[idx] - nums[idx-1])
					t = abs(a - nums[idx-1]) + abs(b - nums[idx])
					diff = max(diff, t - g)
			if a < b:
				if c[1] == -1 or nums[c[1]] < a:
					c[1] = i-1
				if d[0] == -1 or nums[d[0]] > b:
					d[0] = i
			else:
				if c[0] == -1 or nums[c[0]] > a:
					c[0] = i-1
				if d[1] == -1 or nums[d[1]] < b:
					d[1] = i
		for i in range(N-2, 0, -1):
			diff = max(diff, abs(nums[i+1] - nums[0]) - abs(nums[i+1] - nums[i]))
		for i in range(N-1):
			diff = max(diff, abs(nums[i+1] - nums[0]) - abs(nums[i+1] - nums[i]))
		ans = 0
		for i in range(N-1):
			ans += abs(nums[i] - nums[i+1])
		return ans + diff


nums = [2, 4, 9, 24, 2, 1, 10]
print(Solution().maxValueAfterReverse(nums))