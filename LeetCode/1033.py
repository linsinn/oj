class Solution:
    def numMovesStones(self, a: int, b: int, c: int) -> List[int]:
        stones = [a, b, c]
        stones.sort()
        maxi = stones[1] - stones[0] - 1 + stones[2] - stones[1] - 1
        mini = min(1, stones[1] - stones[0] - 1) + min(1, stones[2] - stones[1] - 1)
        if stones[1] - stones[0] == 2 or stones[2] - stones[1] == 2:
            mini = min(1, mini)
        return [mini, maxi]
