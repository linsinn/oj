class Solution:
    def longestWPI(self, hours) -> int:
        ans = score = 0
        seen = {}
        for i, h in enumerate(hours):
            score += 1 if h > 8 else -1
            if score > 0:
                ans = i + 1
            seen.setdefault(score, i)
            if score - 1 in seen:
                ans = max(ans, i - seen[score - 1])
        return ans


hours = [10,8,8,12,6,6,7,11,11,9,11]
print(Solution().longestWPI(hours))