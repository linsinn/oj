//
// Created by Sinn on 2/16/2020.
//

#include "LeetCode.h"

class Solution {
	using ll = long long;
public:
	bool isPossible(vector<int>& target) {
		multiset<ll> ss;
		ll su = 0;
		for (int t : target) {
			ss.emplace(t);
			su += t;
		}
		while (!ss.empty()) {
			ll n = *ss.rbegin();
			ss.erase(prev(ss.end()));
			if (n == 1)
				break;
			ll t = n - (su - n);
			if (t < 1) {
				return false;
			} else {
				su = su - n + t;
				if (t > 1) ss.emplace(t);
			}
		}
		return true;
	}
};

int main() {
	vector<int> arr = {9, 3, 5};
	cout << Solution().isPossible(arr) << '\n';
}