from LeetCode import TreeNode

class Solution:
    def sumRootToLeaf(self, root: TreeNode) -> int:
        ans = [0]

        def dfs(node: TreeNode, num: int):
            if node is None:
                return
            num <<= 1
            num += node.val
            if node.left is None and node.right is None:
                ans[0] += num
            dfs(node.left, num)
            dfs(node.right, num)

        dfs(root, 0)
        return ans[0]
