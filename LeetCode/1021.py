class Solution:
    def removeOuterParentheses(self, S: str) -> str:
        stk = []
        ans = ""
        for i in range(len(S)):
            if S[i] == '(':
                stk.append(i)
            else:
                b = stk.pop()
                if len(stk) == 0:
                    ans += S[b + 1:i]
        return ans
