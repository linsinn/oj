import heapq


class Solution:
    def largestSumAfterKNegations(self, A, K: int) -> int:
        heapq.heapify(A)
        for _ in range(K):
            x = heapq.heappop(A)
            heapq.heappush(A, -x)
        return sum(A)
