class Solution:
    def dfs(self, label, lb, rb, is_even, ans):
        if label > rb:
            g = self.dfs(label, lb * 2, rb * 2 + 1, not is_even, ans)
            if is_even:
                ans.append(rb - g + lb)
            else:
                ans.append(g)
            return g // 2
        else:
            ans.append(label)
            if is_even:
                return (rb - label + lb) // 2
            else:
                return label // 2

    def pathInZigZagTree(self, label: int):
        ans = []
        self.dfs(label, 1, 1, False, ans)
        ans = ans[::-1]
        return ans


print(Solution().pathInZigZagTree(26))
