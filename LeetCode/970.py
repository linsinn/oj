class Solution:
	def powerfulIntegers(self, x, y, bound):
		"""
		:type x: int
		:type y: int
		:type bound: int
		:rtype: List[int]
		"""
		ans = set()
		for i in range(32):
			for j in range(32):
				p = x ** i + y ** j
				if p <= bound:
					ans.add(p)
				else:
					break
				if y == 1:
					break
			if x == 1:
				break
		return list(ans)


