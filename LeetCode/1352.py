class ProductOfNumbers:
	def __init__(self):
		self.pd = [[1]]

	def add(self, num: int) -> None:
		if num == 0:
			self.pd.append([1])
			return
		self.pd[-1].append(self.pd[-1][-1] * num)

	def getProduct(self, k: int) -> int:
		if k >= len(self.pd[-1]):
			return 0
		return self.pd[-1][-1] // self.pd[-1][len(self.pd[-1]) - k - 1]

# Your ProductOfNumbers object will be instantiated and called as such:
obj = ProductOfNumbers()
obj.add(3)
obj.add(0)
obj.add(2)
obj.add(5)
obj.add(4)
print(obj.getProduct(2))
print(obj.getProduct(3))
print(obj.getProduct(4))
obj.add(8)
print(obj.getProduct(2))

