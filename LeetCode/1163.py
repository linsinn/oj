class Solution:
    def lastSubstring(self, s: str) -> str:
        s += '$'
        arr = self.suffix_array(s)
        return s[arr[-1]:-1]

    def suffix_array(self, s: str):
        A = 256
        n = len(s)
        cnt = [0] * (max(A, n))
        p = [0] * n
        c = [0] * n
        for ch in s:
            cnt[ord(ch)] += 1
        for i in range(1, len(cnt)):
            cnt[i] += cnt[i-1]
        for i, ch in enumerate(s):
            k = ord(ch)
            cnt[k] -= 1
            p[cnt[k]] = i
        classes = 1
        c[p[0]] = 0
        for i in range(1, n):
            if s[p[i]] != s[p[i-1]]:
                classes += 1
            c[p[i]] = classes - 1
        pn = [0] * n
        cn = [0] * n
        h = 0
        while (1 << h) < n:
            for i in range(n):
                pn[i] = (p[i] - (1 << h) + n) % n
            for i in range(classes):
                cnt[i] = 0
            for i in range(n):
                cnt[c[i]] += 1
            for i in range(1, classes):
                cnt[i] += cnt[i-1]
            for i in range(n-1, -1, -1):
                k = c[pn[i]]
                cnt[k] -= 1
                p[cnt[k]] = pn[i]
            cn[p[0]] = 0
            classes = 1
            for i in range(1, n):
                cur = [c[p[i]], c[(p[i] + (1 << h)) % n]]
                pre = [c[p[i-1]], c[(p[i-1] + (1 << h)) % n]]
                if pre != cur:
                    classes += 1
                cn[p[i]] = classes - 1
            for i in range(n):
                c[i] = cn[i]
            h += 1
        return p


print(Solution().lastSubstring(s))