class Solution(object):
	def numUniqueEmails(self, emails):
		"""
		:type emails: List[str]
		:rtype: int
		"""

		ans = set()

		for email in emails:
			name, domain = email.split('@')
			ans.add(name.split('+')[0].replace('.', '') + '@' + domain)

		return len(ans)

