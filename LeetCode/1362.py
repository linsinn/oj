import math

class Solution:
	def closestDivisors(self, num: int) -> List[int]:
		ans = [1, num + 1]
		for i in range(int(math.sqrt(num + 2)), 0, -1):
			if (num + 1) % i == 0:
				a, b = i, (num + 1) // i
				if abs(a - b) < abs(ans[0] - ans[1]):
					ans = [a, b]
			if (num + 2) % i == 0:
				a, b = i, (num + 2) // i
				if abs(a - b) < abs(ans[0] - ans[1]):
					ans = [a, b]
		return ans
