class Solution:
	def smallestDivisor(self, nums: List[int], threshold: int) -> int:
		def div(a, b):
			k = a // b
			if a % b != 0:
				k += 1
			return k

		def check(g):
			c = 0
			for num in nums:
				c += div(num, g)
			return c <= threshold

		ans = 0
		lb = 1
		rb = 10 ** 6
		while lb <= rb:
			mid = (lb + rb) // 2
			if check(mid):
				ans = mid
				rb = mid - 1
			else:
				lb = mid + 1
		return ans

