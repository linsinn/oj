class Solution:
	def extractEachPart(self, s):
		dot_idx = len(s) if '.' not in s else s.index('.')
		lp_idx = len(s) if '(' not in s else s.index('(')
		rp_idx = len(s) if ')' not in s else s.index(')')
		return dot_idx, lp_idx, rp_idx

	def isFloatZero(self, f, f_idx):
		for i in range(f_idx[0] + 1, len(f)):
			if f[i] not in ('0', '(', ')'):
				return False
		return True

	def handleInfinite0(self, f, f_idx):
		a = f[f_idx[1] + 1:f_idx[2]]
		if not a:
			return f, f_idx
		for ch in a:
			if ch != '0':
				return f, f_idx
		f = f[:f_idx[1]]
		return f, self.extractEachPart(f)

	def handleInfinite9(self, f, f_idx):
		a = f[f_idx[1] + 1:f_idx[2]]
		if not a:
			return f, f_idx
		for ch in a:
			if ch != '9':
				return f, f_idx
		if f_idx[0] + 1 == f_idx[1]:
			f = f[:f_idx[0]]
			f = str(int(f) + 1)
			return f, self.extractEachPart(f)
		float_len = f_idx[1] - f_idx[0] - 1
		g = str(int(f[f_idx[0] + 1:f_idx[1]]) + 1).zfill(float_len)
		if len(g) > float_len:
			k = str(int(f[:f_idx[0]]) + 1)
			f = k + '.' + g[1:]
		else:
			k = str(int(f[:f_idx[0]]))
			f = k + '.' + g
		return f, self.extractEachPart(f)

	def isRationalEqual(self, S, T):
		"""
		:type S: str
		:type T: str
		:rtype: bool
		"""
		s_idx = self.extractEachPart(S)
		t_idx = self.extractEachPart(T)

		S, s_idx = self.handleInfinite0(S, s_idx)
		T, t_idx = self.handleInfinite0(T, t_idx)
		S, s_idx = self.handleInfinite9(S, s_idx)
		T, t_idx = self.handleInfinite9(T, t_idx)

		s_int = S[:s_idx[0]]
		t_int = T[:t_idx[0]]

		if s_int != t_int:
			return False
		if self.isFloatZero(S, s_idx) and self.isFloatZero(T, t_idx):
			return True
		return self.handleFloatPart(S, s_idx, T, t_idx)
	
	def handleFloatPart(self, S, s_idx, T, t_idx):
		s_float = S[s_idx[0] + 1:s_idx[1]]
		t_float = T[t_idx[0] + 1:t_idx[1]]
		s_repeat = S[s_idx[1] + 1:s_idx[2]]
		t_repeat = T[t_idx[1] + 1:t_idx[2]]
		if len(s_float) > len(t_float):
			s_float, t_float = t_float, s_float
			s_repeat, t_repeat = t_repeat, s_repeat
		while len(s_float) < len(t_float):
			if len(s_repeat) == 0:
				return False
			s_float = s_float + s_repeat[0]
			s_repeat = s_repeat[1:] + s_repeat[0]
		if s_float != t_float:
			return False

		def gcd(a, b):
			return a if b == 0 else gcd(b, a % b)

		sl = len(s_repeat)
		tl = len(t_repeat)
		if bool(sl) ^ bool(tl):
			return False
		g = max(gcd(sl, tl), 1)
		s_repeat = s_repeat * (tl // g)
		t_repeat = t_repeat * (sl // g)
		return s_repeat == t_repeat

