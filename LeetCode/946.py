class Solution(object):
    def validateStackSequences(self, pushed, popped):
        """
        :type pushed: List[int]
        :type popped: List[int]
        :rtype: bool
        """
        stack = []
        i = 0
        for p in pushed:
            while stack and i < len(popped) and stack[-1] == popped[i]:
                i += 1
                stack.pop()
            stack.append(p)
        while stack and i < len(popped) and stack[-1] == popped[i]:
            i += 1
            stack.pop()
        return i == len(popped)


pushed = [1, 2, 3, 4, 5]
popped = [4, 5, 3, 2, 1]
print Solution().validateStackSequences(pushed, popped)