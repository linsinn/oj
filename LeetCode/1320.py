class Solution:
	def minimumDistance(self, word: str) -> int:
		mp = {}
		i, j = 0, 0
		for ch in range(ord('A'), ord('Z') + 1):
			mp[ch - ord('A')] = (i, j)
			j += 1
			if j == 6:
				i += 1
				j = 0

		dist = [[0] * 30 for _ in range(30)]

		for i in range(26):
			for j in range(26):
				p = mp[i]
				q = mp[j]
				dist[i][j] = dist[j][i] = abs(p[0] - q[0]) + abs(p[1] - q[1])

		word = list(word)
		for i in range(len(word)):
			word[i] = ord(word[i]) - ord('A')

		dp = [[-1] * (len(word) + 1) for _ in range(len(word) + 1)]

		dp[1][0] = 0

		for i in range(1, len(word)):
			for j in range(i):
				if dp[i+1][j] == -1:
					dp[i+1][j] = dp[i][j] + dist[word[i-1]][word[i]]
				dp[i+1][j] = min(dp[i+1][j], dp[i][j] + dist[word[i-1]][word[i]])
				t = 0
				if j == 0:
					t = dp[i][j]
				else:
					t = dp[i][j] + dist[word[i]][word[j-1]]
				if dp[i+1][i] == -1:
					dp[i+1][i] = t
				dp[i+1][i] = min(dp[i+1][i], t)

		ans = 1 << 31
		for j in range(len(word) + 1):
			if dp[len(word)][j] != -1:
				ans = min(ans, dp[len(word)][j])
		return ans

word = "HAPPY"
print(Solution().minimumDistance(word))
