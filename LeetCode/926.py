class Solution(object):
    def minFlipsMonoIncr(self, S):
        """
        :type S: str
        :rtype: int
        """
        N = len(S)
        zeros = [0] * (N + 1)
        ones = [0] * (N + 1)
        for i in xrange(1, len(S)+1):
            ones[i] = ones[i-1] + (1 if S[i-1] == '1' else 0)
        for i in xrange(len(S)-1, -1, -1):
            zeros[i] = zeros[i+1] + (1 if S[i] == '0' else 0)
        ans = N + 1
        for i in xrange(len(S)+1):
            ans = min(ans, ones[i] + zeros[i])
        return ans
