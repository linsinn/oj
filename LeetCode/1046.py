import heapq


class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        x = []
        for stone in stones:
            heapq.heappush(x, -stone)

        while len(x) > 1:
            a = heapq.heappop(x)
            b = heapq.heappop(x)
            r = abs(a - b)
            if r != 0:
                heapq.heappush(x, -r)
        return 0 if len(x) == 0 else -x[0]


