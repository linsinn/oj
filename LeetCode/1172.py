import heapq

class DinnerPlates:
  def __init__(self, capacity: int):
    self.c = capacity
    self.q = []
    self.stks = []

  def push(self, val: int) -> None:
    while self.q and self.q[0] < len(self.stks) and len(self.stks[self.q[0]]) == self.c:
      heapq.heappop(self.q)
    if not self.q:
      heapq.heappush(self.q, len(self.stks))
      self.stks.append([])
    self.stks[self.q[0]].append(val)

  def pop(self) -> int:
    while self.stks and not self.stks[-1]:
      self.stks.pop()
    return self.popAtStack(len(self.stks) - 1)

  def popAtStack(self, index: int) -> int:
    if 0 <= index < len(self.stks) and self.stks[index]:
      heapq.heappush(self.q, index)
      return self.stks[index].pop()
    return -1
