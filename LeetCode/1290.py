from LeetCode import  ListNode


class Solution:
	def getDecimalValue(self, head: ListNode) -> int:
		ans = 0
		while head is not None:
			ans = ans * 2 + head.val
			head = head.next
		return ans