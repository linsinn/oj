class Solution:
	def frogPosition(self, n: int, edges: List[List[int]], t: int, target: int) -> float:
		gph = [[] for _ in range(n+1)]
		for u, w  in edges:
			gph[u].append(w)
			gph[w].append(u)

		seen = [False] * (n + 1)
		layers = [0] * (n + 1)
		probs = [0.0] * (n + 1)

		def dfs(root, parent, prob, layer):
			layers[root] = layer
			seen[root] = True
			probs[root] = prob
			tot = len(gph[root])
			if parent != 0:
				tot -= 1
			for ch in gph[root]:
				if not seen[ch]:
					dfs(ch, root, prob * (1.0 / tot), layer + 1)

		dfs(1, 0, 1.0, 0)
		if layers[target] == t:
			return probs[target]
		elif layers[target] < t:
			if target == 1 and len(gph[target]) == 0:
				return probs[1]
			if target != 1 and len(gph[target]) <= 1:
				return probs[target]
			return 0.0
		else:
			return 0.0

