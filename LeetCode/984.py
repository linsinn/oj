class Solution:
    def strWithout3a3b(self, A, B):
        """
        :type A: int
        :type B: int
        :rtype: str
        """
        if A > B:
            more = [A, 'a']
            less = [B, 'b']
        else:
            less = [A, 'a']
            more = [B, 'b']
        ans = ''
        while more[0] > less[0] != 0:
            ans += more[1] * 2
            ans += less[1]
            more[0] -= 2
            less[0] -= 1
        while more[0] != 0:
            ans += more[1]
            more[0] -= 1
            if less[0] != 0:
                ans += less[1]
                less[0] += 1
        return ans


A = 4
B = 1
print(Solution().strWithout3a3b(A, B))