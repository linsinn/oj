class Solution:
	def xorQueries(self, arr: List[int], queries: List[List[int]]) -> List[int]:
		N = len(arr)
		aux = [0] * (N+1)
		for i in range(N):
			aux[i+1] = aux[i] ^ arr[i]
		ans = []
		for l, r in queries:
			ans.append(aux[r+1] ^ aux[l])
		return ans

