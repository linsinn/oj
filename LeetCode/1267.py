class Solution:
	def countServers(self, grid: List[List[int]]) -> int:
		tot = 0
		rows = {}
		cols = {}
		for i, row in enumerate(grid):
			for j, cell in enumerate(row):
				if cell == 1:
					tot += 1
					rows[i] = rows.get(i, 0) + 1
					cols[j] = cols.get(j, 0) + 1
		cnt = 0
		for i, row in enumerate(grid):
			for j, cell in enumerate(row):
				if cell == 1:
					if rows.get(i) == 1 and cols.get(j) == 1:
						cnt += 1
		return tot - cnt
