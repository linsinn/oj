from collections import defaultdict

class Solution:
	def maxRepOpt1(self, text: str) -> int:
		i = 0
		mp = defaultdict(list)
		while i < len(text):
			j = i + 1
			while j < len(text) and text[j] == text[i]:
				j += 1
			mp[text[i]].append([i, j])
			i = j
		ans = 0
		for vec in mp.values():
			ans = max(ans, vec[0][1] - vec[0][0])
			if len(vec) > 1:
				ans = max(ans, vec[0][1] - vec[0][0] + 1)
			for i in range(1, len(vec)):
				ans = max(ans, vec[i][1] - vec[i][0] + 1)
				if vec[i-1][1] + 1 == vec[i][0]:
					if len(vec) > 2:
						ans = max(ans, vec[i-1][1] - vec[i-1][0] + vec[i][1] - vec[i][0] + 1)
					else:
						ans = max(ans, vec[i-1][1] - vec[i-1][0] + vec[i][1] - vec[i][0])
		return ans

