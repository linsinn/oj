class Solution(object):
	def maxLength(self, arr):
		"""
		:type arr: List[str]
		:rtype: int
		"""
		A = 26
		comp = []
		for s in arr:
			t = 0
			b = True
			for ch in s:
				k = ord(ch) - ord('a')
				if ((1 << k) & t) != 0:
					b = False
					break
				t |= (1 << k)
			if b:
				comp.append(t)

		ans = 0
		for i in range(1 << len(comp)):
			t = 0
			b = True
			for j in range(len(comp)):
				if ((1 << j) & i) != 0:
					if (t & comp[j]) != 0:
						b = False
						break
					t ^= comp[j]
			if b:
				res = 0
				for j in range(A):
					if (1 << j) & t != 0:
						res += 1
				ans = max(ans, res)
		return ans


arr = ["un", "iq", "ue"]
print(Solution().maxLength(arr))
