class Solution:
  def checkStraightLine(self, coordinates: List[List[int]]) -> bool:
    coordinates = sorted(coordinates)

    def is_equal(a, b):
      c = coordinates[0]
      return (a[0] - c[0]) * (b[1] - c[1]) == (a[1] - c[1]) * (b[0] - c[0])

    for i in range(1, len(coordinates)):
      if not is_equal(coordinates[i-1], coordinates[i]):
        return False
    return True
