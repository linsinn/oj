from LeetCode import TreeNode


class Solution:
    def cnt(self, root: TreeNode, x):
        if root is None:
            return 0, -1
        l = self.cnt(root.left, x)
        r = self.cnt(root.right, x)
        root.tot = l[0] + r[0] + 1
        if root.val == x:
            return root.tot, root.tot
        else:
            if l[1] != -1:
                return root.tot, l[1]
            elif r[1] != -1:
                return root.tot, r[1]
            return root.tot, -1

    def dfs(self, root: TreeNode, x, x_cnt, met, tot):
        if root is None:
            return False
        if met:
            if root.tot > tot - root.tot:
                return True
        l = self.dfs(root.left, x, x_cnt, met or root.val == x, tot)
        r = self.dfs(root.right, x, x_cnt, met or root.val == x, tot)
        return l or r

    def btreeGameWinningMove(self, root: TreeNode, n: int, x: int) -> bool:
        tot, x_cnt = self.cnt(root, x)
        if x_cnt * 2 < tot and x != root.val:
            return True
        return self.dfs(root, x, x_cnt, False, tot)


root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
print(Solution().btreeGameWinningMove(root, 3, 1))

