from LeetCode import TreeNode

class Solution:
    def dfs(self, nums, i, j):
        if i >= j:
            return None
        root = TreeNode(nums[i])
        x = i+1
        while x < j:
            if nums[x] > nums[i]:
                break
            x += 1
        root.left = self.dfs(nums, i+1, x)
        root.right = self.dfs(nums, x, j)
        return root

    def bstFromPreorder(self, preorder):
        return self.dfs(preorder, 0, len(preorder))
