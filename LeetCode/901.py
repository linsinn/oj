class StockSpanner(object):
    def __init__(self):
        self.dp = []
        self.arr = []
        

    def next(self, price):
        """
        :type price: int
        :rtype: int
        """
        if len(self.arr) == 0:
            self.arr.append(price)
            self.dp.append(-1)
            return 1
        if price < self.arr[-1]:
            self.dp.append(len(self.dp) - 1)
            self.arr.append(price)
            return 1
        else:
            i = self.dp[-1]
            while i != -1 and self.arr[i] <= price:
                i = self.dp[i]
            self.arr.append(price)
            self.dp.append(i)
            return len(self.dp) - i - 1
