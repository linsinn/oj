//
// Created by sinn on 12/15/19.
//

#include "LeetCode.h"

class Solution {
public:
	int maxSideLength(vector<vector<int>>& mat, int threshold) {
		int N = mat.size();
		int M = mat[0].size();
		vector<vector<int>> aux0 = mat;
		vector<vector<int>> aux1 = mat;
		for (int i = 0; i < N; i++) {
			for (int j = 1; j < M; j++) {
				aux0[i][j] += aux0[i][j-1];
			}
		}
		for (int i = 1; i < N; i++) {
			for (int j = 0; j < M; j++) {
				aux1[i][j] += aux1[i-1][j];
			}
		}
		auto prev = mat;
		vector<vector<int>> nxt(N, vector<int>(M, 0));
		int ans = 0;
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j)
				if (mat[i][j] <= threshold)
					ans = 1;
		}
		for (int k = 2; k <= min(N, M); ++k) {
			bool f = false;
			for (int i = k-1; i < N; ++i) {
				for (int j = k-1; j < M; ++j) {
					int a = aux0[i][j] - (j-k < 0 ? 0 : aux0[i][j-k]);
					int b = aux1[i][j] - (i-k < 0 ? 0 : aux1[i-k][j]);
					nxt[i][j] = prev[i-1][j-1] + a + b - mat[i][j];
					if (nxt[i][j] <= threshold) {
						f = true;
						ans = k;
					}
				}
			}
			if (!f) {
				break;
			}
			prev = nxt;
			nxt.assign(N, vector<int>(M, 0));
		}
		return ans;
	}
};