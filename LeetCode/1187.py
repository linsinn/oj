import bisect
import collections


class Solution:
  def makeArrayIncreasing(self, arr1, arr2) -> int:
    N = len(arr1)
    arr2.sort()
    dp = {0: arr1[0], 1: arr2[0]}
    for i in range(1, N):
      tmp = collections.defaultdict(lambda: float('inf'))
      for k, v in dp.items():
        if v < arr1[i]:
          tmp[k] = min(tmp[k], arr1[i])
        p = bisect.bisect_right(arr2, v)
        if p < len(arr2):
          tmp[k + 1] = min(tmp[k + 1], arr2[p])
      dp = tmp
    if len(dp) == 0:
      return -1
    return min(dp.keys())


arr1 = [1, 5, 3, 6, 7]
arr2 = [1, 3, 2, 4]
print(Solution().makeArrayIncreasing(arr1, arr2))
