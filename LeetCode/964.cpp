//
// Created by sinn on 12/24/18.
//

#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    int x;
    int cost[40];
    unordered_map<int, int> memo[40];

    int dp(int i, int target) {
        if (target == 0)
            return 0;
        if (i >= 40)
            return 0x77777777;
        if (target == 1)
            return this->cost[i];
        if (this->memo[i].count(target) != 0)
            return this->memo[i][target];
        int t = target / this->x, r = target % this->x;
        int ret = min(r * this->cost[i] + dp(i+1, t), (x-r) * this->cost[i] + dp(i+1, t+1));
        this->memo[i][target] = ret;
        return ret;
    }

    int leastOpsExpressTarget(int x, int target) {
        this->x = x;
        for (int i = 1; i < 40; i++)
            this->cost[i] = i;
        this->cost[0] = 2;
        return dp(0, target) - 1;
    }
};

int main() {
    int x = 2, target = 125046;
    cout << Solution().leastOpsExpressTarget(x, target) << '\n';
}