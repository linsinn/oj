class Solution:
    def findJudge(self, N: int, trust: List[List[int]]) -> int:
        a = [0] * (N + 1)
        b = [0] * (N + 1)
        for t in trust:
            a[t[0]] += 1
            b[t[1]] += 1
        judges = []
        for i in range(N):
            if b[i+1] == N-1 and a[i+1] == 0:
                judges.append(i+1)
        return judges[0] if len(judges) == 1 else -1
