class Solution:
  def con(self, a, b, rev):
    if rev:
      return b + a
    else:
      return a + b

  def dfs(self, s, st, ed, mp, rev):
    pre = st
    idx = st
    ret = ""
    while idx <= ed:
      if s[idx] == '(':
        a = s[pre:idx]
        b = self.dfs(s, idx+1, mp[idx]-1, mp, not rev)
        if rev:
          ret = self.con(ret, a[::-1], rev)
        else:
          ret = self.con(ret, a, rev)
        ret = self.con(ret, b, rev)
        idx = mp[idx] + 1
        pre = idx
      else:
        idx += 1
    a = s[pre:idx]
    if rev:
      ret = self.con(ret, a[::-1], rev)
    else:
      ret = self.con(ret, a, rev)
    return ret

  def reverseParentheses(self, s: str) -> str:
    stk = []
    mp = {}
    for i, ch in enumerate(s):
      if ch == '(':
        stk.append(i)
      elif ch == ')':
        mp[stk[-1]] = i
        stk.pop()
    return self.dfs(s, 0, len(s)-1, mp, False)


s = "(a(b)c(ef)ed)"
print(Solution().reverseParentheses(s))