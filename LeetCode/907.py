class Solution(object):
    def sumSubarrayMins(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        MOD = 10 ** 9 + 7
        l = [0] * len(A)
        r = [0] * len(A)
        st = []
        for i in xrange(len(A)):
            while st and A[st[-1]] >= A[i]:
                st.pop(-1)
            if st:
                l[i] = i - st[-1]
            else:
                l[i] = i + 1
            st.append(i)

        st = []
        for i in xrange(len(A)-1, -1, -1):
            while st and A[st[-1]] > A[i]:
                st.pop(-1)
            if st:
                r[i] = st[-1] - i
            else:
                r[i] = len(A) - i
            st.append(i)


        ans = 0
        for i in xrange(len(A)):
            ans = ans + A[i] * (l[i] * r[i]) % MOD

        return ans % MOD

if __name__ == '__main__':
    A = [3, 1, 2, 4]
    Solution().sumSubarrayMins(A)
            
