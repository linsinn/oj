class Solution(object):
    def numMusicPlaylists(self, N, L, K):
        """
        :type N: int
        :type L: int
        :type K: int
        :rtype: int
        """
        dp = [[-1] * (N+1) for _ in xrange(L+1)]

        def dfs(i, j):
            if j < 0:
                return 0
            if i == 0:
                return +(j == 0)
            if dp[i][j] != -1:
                return dp[i][j]
            cur = dfs(i-1, j-1) * (N - (j-1))
            cur += dfs(i-1, j) * max(j-K, 0)
            dp[i][j] = cur % (10**9 + 7)
            return dp[i][j]

        return dfs(L, N)
