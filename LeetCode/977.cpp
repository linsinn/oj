//
// Created by sinn on 1/22/19.
//

#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    vector<int> sortedSquares(vector<int>& A) {
        vector<int> ans(A.begin(), A.end());
        for (int& a : ans) {
            a *= a;
        }
        sort(ans.begin(), ans.end());
        return ans;
    }
};