import collections


class Solution:
  def invalidTransactions(self, transactions: List[str]) -> List[str]:
    ans = []
    mp = collections.defaultdict(list)
    for t in transactions:
      g = t.split(',')
      mp[g[0]].append([int(g[2]), int(g[1]), g[3], t])
    for vec in mp.values():
      for i in range(len(vec)):
        if vec[i][0] > 1000:
          ans.append(vec[i][-1])
          continue
        for j in range(len(vec)):
          if i == j:
            continue
          if abs(vec[i][1] - vec[j][1]) <= 60 and vec[i][-2] != vec[j][-2]:
            ans.append(vec[i][-1])
            break
    return ans