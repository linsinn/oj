class Solution:
	def diagonalSort(self, mat: List[List[int]]) -> List[List[int]]:
		N = len(mat)
		M = len(mat[0])
		for i in range(M):
			arr = []
			j = 0
			while j < N and i + j < M:
				arr.append(mat[j][i+j])
				j += 1
			arr.sort()
			j = 0
			while j < N and i + j < M:
				mat[j][i+j] = arr[j]
				j += 1
		for i in range(1, N):
			arr = []
			j = 0
			while i + j < N and j < M:
				arr.append(mat[i+j][j])
				j += 1
			arr.sort()
			j = 0
			while i + j < N and j < M:
				mat[i+j][j] = arr[j]
				j += 1
		return mat

