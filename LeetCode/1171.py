from LeetCode import ListNode


class Solution:
  def removeZeroSumSublists(self, head: ListNode) -> ListNode:
    arr = []
    while head is not None:
      arr.append(head.val)
      head = head.next
    deled = [False] * len(arr)
    su = {0: -1}
    accu = 0
    for i, v in enumerate(arr):
      accu += v
      if accu in su:
        p = su[accu]
        if p == -1 or not deled[p]:
          for k in range(p+1, i+1):
            deled[k] = True
        elif p != -1:
          su[accu] = i
      else:
        su[accu] = i
    nil = ListNode(-1)
    head = nil
    for i, v in enumerate(arr):
      if not deled[i]:
        head.next = ListNode(v)
        head = head.next
    return nil.next


head = ListNode(2)
head.next = ListNode(1)
Solution().removeZeroSumSublists(head)
