class Solution:
    def shortestCommonSupersequence(self, str1: str, str2: str) -> str:
        N = len(str1)
        M = len(str2)
        f = [[[0 for _ in range(3)] for _ in range(M+1)] for _ in range(N+1)]
        for i in range(1, N+1):
            for j in range(1, M+1):
                if f[i-1][j][0] > f[i][j-1][0]:
                    f[i][j][0] = f[i-1][j][0]
                    f[i][j][1] = -1
                    f[i][j][2] = 0
                else:
                    f[i][j][0] = f[i][j-1][0]
                    f[i][j][1] = 0
                    f[i][j][2] = -1
                if str1[i-1] == str2[j-1]:
                    f[i][j][0] = f[i-1][j-1][0] + 1
                    f[i][j][1] = -1
                    f[i][j][2] = -1
        x, y = N, M
        com = ""
        while x != 0 and y != 0:
            dx, dy = f[x][y][1], f[x][y][2]
            if dx == -1 and dy == -1:
                com += str1[x-1]
            x += dx
            y += dy
        com = com[::-1]
        ans = str1[::]
        i, j, k = 0, 0, 0
        sh = 0
        while i < N and k < len(com):
            if str1[i] == com[k]:
                l = 0
                while str2[j] != com[k]:
                    l += 1
                    j += 1
                ans = ans[:sh+i] + str2[j-l:j] + ans[sh+i:]
                sh += l
                j += 1
                k += 1
            i += 1
        ans = ans + str2[j:]
        return ans


str1 = "bbabacaa"
str2 = "cccababab"
print(Solution().shortestCommonSupersequence(str1, str2))
