from LeetCode import ListNode


class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        nil = ListNode(-1)
        cur = nil
        carry = 0
        while l1 or l2 or carry:
            a = l1.val if l1 else 0
            b = l2.val if l2 else 0
            sum_ = a + b + carry
            cur.next = ListNode(sum_ % 10)
            carry = sum_ // 10
            cur = cur.next
            if l1:
                l1 = l1.next
            if l2:
                l2 = l2.next
        return nil.next


sol = Solution()
l1 = ListNode(1)
l2 = ListNode(2)
c = sol.addTwoNumbers(l1, l2)
print c.val
