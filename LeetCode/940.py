from collections import defaultdict


class Solution(object):
	def distinctSubseqII(self, S):
		"""
		:type S: str
		:rtype: int
		"""
		MOD = 10 ** 9 + 7
		memo = [[0] * 2 for _ in xrange(len(S))]
		memo[0][0] = memo[0][1] = 1
		pre = defaultdict(int)
		pre[S[0]] = memo[0][0]
		for i in xrange(1, len(S)):
			memo[i][0] = memo[i][1] = sum(memo[i - 1])
			memo[i][0] -= pre[S[i]]
			pre[S[i]] += memo[i][0]
			memo[i][0] %= MOD
			memo[i][1] %= MOD
		return (sum(memo[len(S) - 1]) - 1) % MOD


print Solution().distinctSubseqII('abc')
