class Solution:
    def maxSatisfied(self, customers: List[int], grumpy: List[int], X: int) -> int:
        mini_s = sum(c if g == 0 else 0 for c, g in zip(customers, grumpy))
        i, j = 0, 0
        upd = customers[0] if grumpy[0] == 1 else 0
        maxi_up = upd
        while i < len(customers):
            while j + 1 < len(customers) and j - i + 1 < X:
                j += 1
                upd += customers[j] if grumpy[j] == 1 else 0
            maxi_up = max(maxi_up, upd)
            if grumpy[i] == 1:
                upd -= customers[i]
            i += 1
        return mini_s + maxi_up
