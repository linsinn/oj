class Solution:
    def longestArithSeqLength(self, A: List[int]) -> int:
        ans = 0
        mps = [{} for _ in A]
        for i in range(len(A)-1, -1, -1):
            for j in range(i+1, len(A)):
                diff = A[j] - A[i]
                mps[i][diff] = max(mps[i].get(diff, 0), mps[j].get(diff, 0) + 1)
                ans = max(ans, mps[i][diff])
        return ans + 1
