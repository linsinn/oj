from LeetCode import TreeNode


class Solution:
    def bstToGst(self, root: TreeNode) -> TreeNode:

        def dfs0(node: TreeNode):
            if node is None:
                return 0
            l = dfs0(node.left)
            r = dfs0(node.right)
            node.val += l + r
            return node.val

        def dfs1(node: TreeNode, vv: int):
            if node is None:
                return
            node.val = vv + node.val - (node.left.val if node.left else 0)
            dfs1(node.left, node.val)
            dfs1(node.right, vv)

        dfs0(root)
        dfs1(root, 0)

        return root


root = TreeNode(4)
root.left = TreeNode(1)
root.left.left = TreeNode(0)
root.left.right = TreeNode(2)
root.left.right.right = TreeNode(3)
root.right = TreeNode(6)
root.right.left = TreeNode(5)
root.right.right = TreeNode(7)
root.right.right.right = TreeNode(8)

tree = Solution().bstToGst(root)
