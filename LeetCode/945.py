class Solution(object):
    def minIncrementForUnique(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        if len(A) == 0:
            return 0
        A.sort()
        cur = min(A)
        ans = 0
        for a in A:
            ans += max(0, cur - a)
            cur = max(a, cur) + 1
        return ans
