class Solution:
	def longestPalindrome(self, s: str) -> str:
		if len(s) == 0:
			return s
		ans = s[0]
		for i in range(len(s)):
			for k in range(1, len(s)):
				if i - k < 0 or i + k >= len(s):
					break
				if s[i - k] == s[i + k]:
					l = i + k - (i - k) + 1
					if l > len(ans):
						ans = s[i-k:i+k+1]
				else:
					break
		for i in range(len(s) - 1):
			if s[i] != s[i+1]:
				continue
			if len(ans) < 2:
				ans = s[i:i+2]
			for k in range(1, len(s)):
				if i - k < 0 or i + 1 + k >= len(s):
					break
				if s[i - k] == s[i + 1 + k]:
					l = i + 1 + k - (i - k) + 1
					if l > len(ans):
						ans = s[i-k:i+1+k+1]
				else:
					break
		return ans


s = "aacdefcaa"
print(Solution().longestPalindrome(s))