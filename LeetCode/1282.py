import collections


class Solution:
	def groupThePeople(self, groupSizes: List[int]) -> List[List[int]]:
		ss = collections.defaultdict(list)
		ans = []
		for i, gs in enumerate(groupSizes):
			ss[gs].append(i)
			if len(ss[gs]) == gs:
				ans.append(ss[gs])
				ss.pop(gs)
		return ans
