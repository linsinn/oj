class Solution:
	def minDifficulty(self, jobDifficulty: List[int], d: int) -> int:
		N = len(jobDifficulty)
		if N < d:
			return -1
		rng_maxi = [[0] * N for _ in range(N)]
		for i in range(N):
			rng_maxi[i][i] = jobDifficulty[i]
			for j in range(i+1, N):
				rng_maxi[i][j] = max(rng_maxi[i][j-1], jobDifficulty[j])
		memo = [[1 << 30] * N for _ in range(d)]
		for j in range(N):
			memo[0][j] = rng_maxi[0][j]
		for i in range(d-1):
			for j in range(N):
				for k in range(j+1, N):
					memo[i+1][k] = min(memo[i+1][k], memo[i][j] + rng_maxi[j+1][k])
		return memo[d-1][N-1]
