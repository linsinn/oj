#include "LeetCode.h"

class TopVotedCandidate {
    map<int, int> mps;
public:
    TopVotedCandidate(vector<int> persons, vector<int> times) {
        map<int, int> votes;
        int leader = persons[0], m = 1;
        mps[times[0]] = leader;
        votes[persons[0]] = 1;
        for (int i = 1; i < persons.size(); i++) {
            int p = persons[i], t = times[i];
            votes[p] += 1;
            int c = votes[p];
            if (c >= m) {
                leader = p;
            }
            m = max(m, c);
            mps[t] = leader;
        }
    }
    
    int q(int t) {
        auto iter = next(mps.upper_bound(t), -1);
        return iter->second;
    }
};

int main() {
    return 0;
}
