from collections import deque


class Solution:
    def smallestSufficientTeam(self, req_skills, people):
        mp = {}
        for i, u in enumerate(req_skills):
            mp[u] = i
        target = 0
        for g in req_skills:
            target |= (1 << mp[g])
        for i, p in enumerate(people):
            g = 0
            for q in p:
                if q in mp:
                    g |= (1 << mp[q])
            people[i] = g

        vis = set()
        vis1 = set()
        que = deque()
        que.append([0, 0])
        ans = []
        while len(que) != 0:
            p, q = que.popleft()
            if p == target:
                k = 0
                while q != 0:
                    if (q & 1) == 1:
                        ans.append(k)
                    q >>= 1
                    k += 1
                return ans
            for i in range(len(people)):
                if (q | (1 << i)) not in vis and (p | people[i]) not in vis1:
                    vis.add(q|(1<<i))
                    vis1.add(p | people[i])
                    que.append([p | people[i], q | (1 << i)])
        return ans

req_skills = ["java","nodejs","reactjs"]
people = [["java"],["nodejs"],["nodejs","reactjs"]]
print(Solution().smallestSufficientTeam(req_skills, people))


