class Solution:
	def maximum69Number (self, num: int) -> int:
		li = list(str(num))
		idx = -1
		for i, ch in enumerate(li):
			if ch == '6':
				idx = i
				break
		if idx >= 0:
			li[idx] = '9'
		return int(''.join(li))


num = 9669
print(Solution().maximum69Number(num))
