import collections


class Solution:
  def arraysIntersection(self, arr1, arr2, arr3):
    mp = collections.defaultdict(lambda : 0)
    for a in arr1:
      mp[a] += 1
    for a in arr2:
      mp[a] += 1
    for a in arr3:
      mp[a] += 1
    return list(map(lambda v: v[0], filter(lambda item: item[1] == 3, mp.items())))


arr1 = [1,2,3,4,5]
arr2 = [1,2,5,7,9]
arr3 = [1,3,4,5,8]
print(Solution().arraysIntersection(arr1, arr2, arr3))