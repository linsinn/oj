from LeetCode import TreeNode


class Solution(object):
    def verticalTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        seq = []

        def dfs(x, y, root):
            if root is None:
                return
            dfs(x-1, y+1, root.left)
            seq.append([x, y, root.val])
            dfs(x+1, y+1, root.right)
        dfs(0, 0, root)
        seq.sort(key=lambda k: (k[0], k[1]))

        ans = []
        cur = [seq[0][2]]
        for i in range(1, len(seq)):
            if seq[i][0] != seq[i-1][0]:
                ans.append(cur[:])
                cur = []
            cur.append(seq[i][2])
        ans.append(cur)
        return ans
