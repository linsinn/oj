from itertools import permutations

class Solution(object):
    def largestTimeFromDigits(self, A):
        """
        :type A: List[int]
        :rtype: str
        """
        ans = ""
        for perm in permutations(A):
            h = perm[0] * 10 + perm[1]
            m = perm[2] * 10 + perm[3]
            if 0 <= h < 24 and 0 <= m < 60:
                ans = max(ans, "%s:%s" % (str(h).zfill(2), str(m).zfill(2)))
        return ans