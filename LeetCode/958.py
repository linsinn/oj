from LeetCode import TreeNode

from collections import deque

class Solution:
    def isCompleteTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        l = [root]
        que = deque([root])
        while len(que) != 0:
            n = que.popleft()
            l.append(n.left)
            l.append(n.right)
            if n.left:
                que.append(n.left)
            if n.right:
                que.append(n.right)
        for i in range(1, len(l)):
            if l[i] and not l[i-1]:
                return False
        return True
