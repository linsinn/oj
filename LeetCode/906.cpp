#include <bits/stdc++.h>
using namespace std;

class Solution {
    using ull = unsigned long long;
    int digs[20];

    void formSP(string&& str, set<ull>& sp) {
        if (str.size() >= 10)
            return ;
        ull u = std::stoull(str);
        u *= u;
        ull x = u;
        int i = 0, j = 0;
        while (u) {
            digs[j++] = u % 10;
            u /= 10;
        }
        j--;
        while (i < j) {
            if (digs[i++] != digs[j--])
                return ;
        }
        sp.emplace(x);
    }
public:
    int superpalindromesInRange(string L, string R) {
        ull l =  std::stoull(L);
        ull r =  std::stoull(R);
        set<ull> sp;
        for (ull i = 1; i <= min((ull) std::sqrt(r) + 1, (ull) 1e5); i++) {
            string s = std::to_string(i);
            string t = s;
            std::reverse(t.begin(), t.end());
            formSP(s + t, sp);
            formSP(s + t.substr(1), sp);
        }
        vector<ull> vec(sp.begin(), sp.end());
        return std::distance(std::lower_bound(vec.begin(), vec.end(), l),
                             std::upper_bound(vec.begin(), vec.end(), r));
    }
};

int main() {
    string L = "1020762146323";
    string R = "142246798855636";
    Solution sol;
    std::cout << sol.superpalindromesInRange(L, R) << '\n';
    return 0;
}
