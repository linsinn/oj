import itertools


class Solution:
	def letterCombinations(self, digits: str):
		mp = {}
		mp["2"] = "abc"
		mp["3"] = "def"
		mp["4"] = "ghi"
		mp["5"] = "jkl"
		mp["6"] = "mno"
		mp["7"] = "pqrs"
		mp["8"] = "tuv"
		mp["9"] = "wxyz"
		if len(digits) == 0:
			return ""
		prev = mp[digits[0]]
		if len(digits) == 1:
			return list(prev)
		for dig in digits[1:]:
			res = itertools.product(prev, mp[dig])
			prev = []
			for r in res:
				prev.append("".join(r))
		return prev


digits = "2"
print(Solution().letterCombinations(digits))
