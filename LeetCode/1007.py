class Solution:
    def minDominoRotations(self, A, B) -> int:
        ans = len(A) + 1
        for i in range(1, 7):
            cur0, cur1 = 0, 0
            f = True
            for a, b in zip(A, B):
                if a != i:
                    if b == i:
                        cur0 += 1
                    else:
                        f = False
                        break
                if b != i:
                    if a == i:
                        cur1 += 1
            if f:
                ans = min(ans, cur0, cur1)
        return -1 if ans == len(A) + 1 else ans


