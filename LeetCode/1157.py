import math
import bisect
from collections import defaultdict, Counter


class MajorityChecker:
    def __init__(self, arr):
        self.MAG = int(math.sqrt(len(arr))) + 1
        self.arr = arr
        mp = defaultdict(list)
        cnt = defaultdict(lambda : 0)
        for i, a in enumerate(arr):
            cnt[a] += 1
            mp[a].append(i)
        self.mp = mp
        for k, v in cnt.items():
            if v < self.MAG:
                self.mp.pop(k)

    def query(self, left: int, right: int, threshold: int) -> int:
        if right - left + 1 < self.MAG:
            cnt = Counter()
            for i in range(left, right+1):
                cnt[self.arr[i]] += 1
            if cnt.most_common(1)[0][1] >= threshold:
                return cnt.most_common(1)[0][0]
        else:
            for k, v in self.mp.items():
                l = bisect.bisect_left(v, left)
                r = bisect.bisect_right(v, right)
                if r - l >= threshold:
                    return k
        return -1


maj = MajorityChecker([1, 1, 2, 2, 1, 1])
maj.query(2, 3, 2)

