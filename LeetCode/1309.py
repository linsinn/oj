class Solution:
	def freqAlphabets(self, s: str) -> str:
		mp = {}
		for i in range(1, 10):
			mp[str(i)] = chr(ord('a') + i - 1)
		for i in range(10, 27):
			mp[str(i) + '#'] = chr(ord('a') + i - 1)
		ans = ""
		i = 0
		while i < len(s):
			if s[i:i+3] in mp:
				ans += mp[s[i:i+3]]
				i += 3
			elif s[i:i+2] in mp:
				ans += mp[s[i:i+2]]
				i += 2
			else:
				ans += mp[s[i]]
				i += 1
		return ans
