from LeetCode import TreeNode


class Solution(object):
    def smallestFromLeaf(self, root):
        """
        :type root: TreeNode
        :rtype: str
        """
        seq = []

        def dfs(root, s):
            if root is None:
                return
            s += chr(root.val + ord('a'))
            if root.left is None and root.right is None:
                seq.append(s[::-1])
                return
            dfs(root.left, s)
            dfs(root.right, s)
        dfs(root, '')
        seq.sort()

        return seq[0]
