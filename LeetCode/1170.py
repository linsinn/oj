import bisect


class Solution:
  def numSmallerByFrequency(self, queries: List[str], words: List[str]) -> List[int]:
    def CalcFreq(s):
      mc = 'z'
      cnt = 0
      for ch in s:
        if ord(ch) == ord(mc):
          cnt += 1
        elif ord(ch) < ord(mc):
          mc = ch
          cnt = 1
      return cnt

    wf = [CalcFreq(w) for w in words]
    wf.sort()

    def FindLess(qry):
      f = CalcFreq(qry)
      p = bisect.bisect_right(wf, f)
      return len(wf) - p

    return [FindLess(qry) for qry in queries]
