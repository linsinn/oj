class Solution(object):
    def addToArrayForm(self, A, K):
        """
        :type A: List[int]
        :type K: int
        :rtype: List[int]
        """
        A = A[::-1]
        i = 0
        while K != 0 and i < len(A):
            t = K + A[i]
            A[i] = t % 10
            K = t // 10
            i += 1
        while K != 0:
            A.append(K % 10)
            K //= 10
        return A[::-1]
