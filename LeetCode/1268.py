class Solution:
	def suggestedProducts(self, products, searchWord: str):
		ans = []
		products = sorted(products)

		def search(kw):
			lb = 0
			rb = len(products) - 1
			ret = -1
			while lb <= rb:
				mid = (lb + rb) // 2
				if kw <= products[mid]:
					ret = mid
					rb = mid - 1
				else:
					lb = mid + 1
			if ret == -1 or len(products[ret]) < len(kw):
				return []
			if products[ret][:len(kw)] != kw:
				return []
			res = [products[ret]]
			for i in range(2):
				if ret + 1 + i >= len(products):
					break
				if len(products[ret+1+i]) < len(kw) or products[ret+1+i][:len(kw)] != kw:
					break
				res.append(products[ret+1+i])
			return res

		for i in range(len(searchWord)):
			res = search(searchWord[:i+1])
			ans.append(res)

		return ans


words = ["havana"]
sw = "havana"
print(Solution().suggestedProducts(words, sw))