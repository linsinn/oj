class Solution:
	def arrayRankTransform(self, arr: List[int]) -> List[int]:
		li = [[num, idx] for idx, num in enumerate(arr)]
		li.sort()
		ans = [0] * len(arr)
		rank = 1
		for i, (num, idx) in enumerate(li):
			if i > 0 and num > li[i-1][0]:
				rank += 1
			ans[idx] = rank
		return ans
