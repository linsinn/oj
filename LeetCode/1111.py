class Solution:
    def maxDepthAfterSplit(self, seq: str) -> List[int]:
        max_level = 0
        cur_level = 0
        stk = []
        mp = {}
        for i, s in enumerate(seq):
            if s == '(':
                cur_level += 1
                stk.append(i)
                max_level = max(max_level, cur_level)
            else:
                mp[stk[-1]] = i
                stk.pop()
                cur_level -= 1
        k = max_level // 2
        if max_level % 2 == 0:
            k += 1
        ans = [0] * len(seq)
        cur = 0
        for i in range(len(seq)):
            if i in mp:
                ans[i] = cur
                ans[mp[i]] = cur
            cur ^= 1
        return ans
