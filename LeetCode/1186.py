class Solution:
  def maximumSum(self, arr: List[int]) -> int:
    N = len(arr)
    pre = [0] * N
    suf = [0] * N
    pre[0] = max(0, arr[0])
    for i in range(1, N):
      pre[i] = max(0, pre[i-1]) + arr[i]
    suf[N-1] = arr[-1]
    for i in range(N-2, -1, -1):
      suf[i] = max(0, suf[i+1]) + arr[i]

    ans = 0
    for i in range(N):
      lp = 0 if i == 0 else max(0, pre[i-1])
      rp = 0 if i == N-1 else max(0, suf[i+1])
      ans = max(ans, lp + rp + arr[i], lp + rp)
    if ans == 0:
      return max(arr)
    return ans


arr = [8,-1,6,-7,-4,5,-4,7,-6]
print(Solution().maximumSum(arr))
