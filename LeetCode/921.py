# -*- coding: utf-8 -*-

class Solution(object):
	def minAddToMakeValid(self, S):
		"""
		:type S: str
		:rtype: int
		"""
		ans = 0
		cnts = 0
		for ch in S:
			if ch == '(':
				cnts += 1
			else:
				if cnts == 0:
					ans += 1
				else:
					cnts -= 1
		return ans + cnts
