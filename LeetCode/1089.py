class Solution:
    def duplicateZeros(self, arr: List[int]) -> None:
        """
        Do not return anything, modify arr in-place instead.
        """
        N = len(arr)
        i, j = 0, 0
        tmp = [0] * N
        while j < N:
            if arr[i] != 0:
                tmp[j] = arr[i]
            else:
                tmp[j] = 0
                j += 1
                if j < N:
                    tmp[j] = 0
            i += 1
            j += 1
        for i in range(N):
            arr[i] = tmp[i]
