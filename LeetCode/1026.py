from LeetCode import TreeNode


class Solution:
    def dfs(self, root: TreeNode):
        l = root.val, root.val, 0
        r = root.val, root.val, 0
        if root.left is not None:
            l = self.dfs(root.left)
        if root.right is not None:
            r = self.dfs(root.right)
        return min(l[0], r[0], root.val), max(l[1], r[1], root.val), max(abs(root.val - min(l[0], r[0])), abs(root.val - max(l[1], r[1])), l[2], r[2])

    def maxAncestorDiff(self, root: TreeNode) -> int:
        return self.dfs(root)[2]
