#include <bits/stdc++.h>
using namespace std;


constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

class Solution {
public:
	int numSubarraysWithSum(vector<int>& A, int S) {
		vector<int> nextOnePos(A.size(), 0);
		int p = A.size();
		for (int i = A.size()-1; i >= 0; i--) {
			if (A[i] == 1) {
				nextOnePos[i] = p;
				p = i;
			}
		}
		int i = p;
		int j = -1;
		int su = 0;
		int ans = 0;
		if (S == 0) {
			while (j < (int)A.size()) {
				if (j == -1) {
					ans += p * (p + 1) / 2;
					j = p;
				} else {
					ans += (nextOnePos[j] - j - 1) * (nextOnePos[j] - j) / 2;
					j = nextOnePos[j];
				}
			}
			return ans;
		}
		while (i < A.size()) {
			su += 1;
			if (su == S) {
				int r = nextOnePos[i] - i;
				int l = 1;
				if (j == -1) {
					l = p + 1;
					j = p;
				} else {
					l = nextOnePos[j] - j;
					j = nextOnePos[j];
				}
				ans  += l * r;
				su -= 1;
			}
			i = nextOnePos[i];
		}
		return ans;
	}
};


int main() {
	freopen("in.txt", "r", stdin);
	ios::sync_with_stdio(false);
	cin.tie(nullptr), cout.tie(nullptr);
	std::vector<int> v = {0, 0, 0, 0, 0};
	int S = 0;
	Solution sol;
	cout << sol.numSubarraysWithSum(v, S);
	return 0;
}
