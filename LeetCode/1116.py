from threading import Condition


class ZeroEvenOdd:
    def __init__(self, n):
        self.n = n
        self._cv = Condition()
        self._p = 0

    def zero(self, printNumber: 'Callable[[int], None]') -> None:
        for i in range(self.n):
            with self._cv:
                self._cv.wait_for(lambda: self._p == 0)
                printNumber(0)
                self._p = 1 if i % 2 == 0 else -1
                self._cv.notify_all()

    def even(self, printNumber: 'Callable[[int], None]') -> None:
        for i in range(2, self.n + 1, 2):
            with self._cv:
                self._cv.wait_for(lambda: self._p == -1)
                printNumber(i)
                self._p = 0
                self._cv.notify_all()

    def odd(self, printNumber: 'Callable[[int], None]') -> None:
        for i in range(1, self.n + 1, 2):
            with self._cv:
                self._cv.wait_for(lambda: self._p == 1)
                printNumber(i)
                self._p = 0
                self._cv.notify_all()
