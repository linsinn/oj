class Solution(object):
    def bagOfTokensScore(self, tokens, P):
        """
        :type tokens: List[int]
        :type P: int
        :rtype: int
        """
        if len(tokens) == 0:
            return 0
        tokens.sort()
        left, right = 0, len(tokens)-1
        ans = 0
        while left < len(tokens) and P > tokens[left]:
            P -= tokens[left]
            ans += 1
            left += 1
        while ans > 0 and left < right and P + tokens[right] >= tokens[left]:
            P += tokens[right]
            right -= 1
            ans -= 1
            while P >= tokens[left]:
                P -= tokens[left]
                ans += 1
                left += 1
        return ans
