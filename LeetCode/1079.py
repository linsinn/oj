from itertools import permutations


class Solution:
    def numTilePossibilities(self, tiles: str) -> int:
        ss = set()
        for l in permutations(tiles):
            for i in range(1, len(l) + 1):
                ss.add(l[0:i])
        return len(ss)
