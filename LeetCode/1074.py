import collections


class Solution:
    def numSubmatrixSumTarget(self, matrix: List[List[int]], target: int) -> int:
        N = len(matrix)
        M = len(matrix[0])

        for row in matrix:
            for i in range(M-1):
                row[i+1] += row[i]

        ans = 0

        for i in range(M):
            for j in range(i, M):
                c = collections.Counter({0: 1})
                cur = 0
                for k in range(N):
                    cur += matrix[k][j] - (matrix[k][i-1] if i > 0 else 0)
                    ans += c[cur - target]
                    c[cur] += 1
        return ans

