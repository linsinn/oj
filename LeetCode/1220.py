class Solution:
  def countVowelPermutation(self, n: int) -> int:
    MOD = 10 ** 9 + 7
    mp = {}
    for ch in ['a', 'e', 'i', 'o', 'u']:
      mp[ch] = 1

    for i in range(2, n+1):
      tmp = {}
      tmp['a'] = mp['e'] + mp['i'] + mp['u']
      tmp['e'] = mp['a'] + mp['i']
      tmp['i'] = mp['e'] + mp['o']
      tmp['o'] = mp['i']
      tmp['u'] = mp['i'] + mp['o']
      for ch in ['a', 'e', 'i', 'o', 'u']:
        mp[ch] = tmp[ch] % MOD

    return sum(mp.values()) % MOD