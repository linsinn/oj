from LeetCode import TreeNode


class FindElements:
	def __init__(self, root: TreeNode):
		self.cont = set()

		def dfs(node: TreeNode):
			self.cont.add(node.val)
			if node.left is not None:
				node.left.val = node.val * 2 + 1
				dfs(node.left)
			if node.right is not None:
				node.right.val = node.val * 2 + 2
				dfs(node.right)

		root.val = 0
		dfs(root)

	def find(self, target: int) -> bool:
		return target in self.cont