import functools

class Solution(object):
    def sumEvenAfterQueries(self, A, queries):
        """
        :type A: List[int]
        :type queries: List[List[int]]
        :rtype: List[int]
        """
        cur = functools.reduce(lambda x, y: x + (y if y % 2 == 0 else 0), A, 0)
        ans = []
        for query in queries:
            if A[query[1]] % 2 == 0:
                cur -= A[query[1]]
            A[query[1]] += query[0]
            if A[query[1]] % 2 == 0:
                cur += A[query[1]]
            ans.append(cur)
        return ans


A = [1, 2, 3, 4]
queries = [[1,0],[-3,1],[-4,0],[2,3]]
print(Solution().sumEvenAfterQueries(A, queries))