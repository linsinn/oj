class Solution:
	def breakPalindrome(self, palindrome: str) -> str:
		li = list(palindrome)
		if len(li) == 1:
			return ""
		for i in range(len(palindrome) // 2):
			if palindrome[i] != 'a':
				li[i] = 'a'
				return ''.join(li)
		li[-1] = 'b'
		return ''.join(li)
