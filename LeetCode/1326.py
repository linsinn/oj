class Solution:
	def minTaps(self, n: int, ranges) -> int:
		li = []
		for i, rng in enumerate(ranges):
			li.append([i - rng, i + rng])
		li.sort()
		stk = []
		for i in range(len(li)):
			rng = li[i]
			while len(stk) >= 2 and rng[1] > stk[-1][1] and rng[0] <= stk[-2][1]:
				stk.pop()
			if len(stk) == 1 and rng[1] > stk[-1][1] and rng[0] <= 0:
				stk.pop()
			if len(stk) == 0 and rng[0] > 0:
				return -1
			if len(stk) > 0 and rng[0] > stk[-1][1]:
				return -1
			if len(stk) == 0 or rng[1] > stk[-1][1]:
				stk.append(rng)
		if len(stk) == 1:
			return 1
		else:
			while len(stk) >= 2 and stk[-1][1] > n and stk[-2][1] >= n:
				stk.pop()
			return len(stk)


n = 17
ranges = [0,3,3,2,2,4,2,1,5,1,0,1,2,3,0,3,1,1]
print(Solution().minTaps(n, ranges))