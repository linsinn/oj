class Solution:
  def distanceBetweenBusStops(self, distance: List[int], start: int, destination: int) -> int:
    tot = sum(distance)
    u = min(start, destination)
    v = max(start, destination)
    a = sum(distance[u:v])
    return min(a, tot - a)