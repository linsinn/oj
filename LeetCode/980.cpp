//
// Created by sinn on 1/23/19.
//

#include "LeetCode.h"

class Solution {
    map<int, int> f[20];
    int N, M;
    pair<int, int> st, ed;

    const int dirs[5] = {-1, 0, 1, 0, -1};

    int dp(int x, int y, int stat, vector<vector<int>>& grid) {
        int where = x * M + y;
        stat |= (1 << where);
        if (f[where].count(stat) != 0)
            return f[where][stat];
        if (x == ed.first && y == ed.second) {
            for (int g = 0; g < N * M; g++) {
                if ((stat & (1 << g)) == 0) {
                    f[where][stat] = 0;
                    return 0;
                }
            }
            f[where][stat] = 1;
            return 1;
        }
        int cur = 0;
        for (int d = 0; d < 4; d++) {
            int nx = x + dirs[d], ny = y + dirs[d+1];
            if (nx >= 0 && nx < N && ny >= 0 && ny < M && (stat & (1 << (nx * M + ny))) == 0) {
                cur += dp(nx, ny, stat, grid);
            }
        }
        f[where][stat] = cur;
        stat ^= (1 << (x * M +  y));
        return cur;
    }
public:
    int uniquePathsIII(vector<vector<int>>& grid) {
        N = grid.size(), M = grid[0].size();
        for (int i = 0; i < N*M; i++)
            f[i].clear();
        int init = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] == 1)
                    st = {i, j};
                else if (grid[i][j] == 2)
                    ed = {i, j};
                else if (grid[i][j] == -1)
                    init |= (1 << (i * M + j));
            }
        }
        return dp(st.first, st.second, init, grid);
    }
};

int main() {
    vector<vector<int>> grid = {{1,0,0,0},{0,0,0,0},{0,0,2,-1}};
//    vector<vector<int>> grid = {{1, 0},{0, 2},};
    Solution sol;
    cout << sol.uniquePathsIII(grid) << '\n';
}