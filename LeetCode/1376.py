class Solution:
	def numOfMinutes(self, n: int, headID: int, manager, informTime) -> int:
		N = len(manager)
		childs = [[] for _ in range(N)]
		for i, m in enumerate(manager):
			if m != -1:
				childs[m].append(i)

		ans = [0]

		def dfs(root, t):
			for ch in childs[root]:
				dfs(ch, informTime[root] + t)
			ans[0] = max(ans[0], t)

		dfs(headID, 0)
		return ans[0]


n = 15
headId = 0
manager =  [-1,0,0,1,1,2,2,3,3,4,4,5,5,6,6]
informTime = [1,1,1,1,1,1,1,0,0,0,0,0,0,0,0]
print(Solution().numOfMinutes(n, headId, manager, informTime))