class Solution:
    def dp(self, l, r):
        if l == r:
            return 0
        if self.memos[l][r] != -1:
            return self.memos[l][r]
        cur = 1 << 32
        for i in range(l+1, r+1):
            g = self.ma[l][i-1] * self.ma[i][r]
            x = self.dp(l, i-1) + self.dp(i, r)
            cur = min(cur, g + x)
        self.memos[l][r] = cur
        return cur

    def mctFromLeafValues(self, arr) -> int:
        self.arr = arr
        self.N = len(arr)
        self.ma = [[0] * (self.N+1) for _ in range(self.N+1)]
        for i in range(self.N):
            for j in range(i, self.N):
                for k in range(i, j+1):
                    self.ma[i][j] = max(self.ma[i][j], arr[k])
        self.memos = [[-1] * (self.N+1) for _ in range(self.N+1)]
        return self.dp(0, self.N-1)


arr = [6, 2, 4]
print(Solution().mctFromLeafValues(arr))