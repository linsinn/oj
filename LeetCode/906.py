import bisect
import math

class Solution(object):

    def isPalindrome(self, p):
        p = str(p)
        i, j = 0, len(p) - 1
        while i < j:
            if p[i] != p[j]:
                return False
            i += 1
            j -= 1
        return True

    def superpalindromesInRange(self, L, R):
        """
        :type L: str
        :type R: str
        :rtype: int
        """
        SP = set()
        for i in xrange(1, min(int(math.sqrt(float(R))) + 1, 10**5)):
            s = str(i)
            t = s[::-1]

            x = int(s+t) ** 2
            if self.isPalindrome(x):
                SP.add(x)

            x = int(s + t[1:]) ** 2
            if self.isPalindrome(x):
                SP.add(x)

        SP = list(set(SP))
        SP.sort()
        for sp in SP:
            print sp
        return bisect.bisect_right(SP, int(R)) - bisect.bisect_left(SP, int(L))

if __name__ == '__main__':
    L = "1020762146323";
    R = "142246798855636";
    print Solution().superpalindromesInRange(L, R)
