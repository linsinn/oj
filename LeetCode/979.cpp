//
// Created by sinn on 1/22/19.
//

#include "LeetCode.h"


class Solution {
    int ans = 0;

    int dfs(TreeNode* root) {
        if (root == nullptr)
            return 0;
        int L = dfs(root->left);
        int R = dfs(root->right);
        ans += abs(L) + abs(R);
        return root->val + L + R - 1;
    }

public:
    int distributeCoins(TreeNode* root) {
        ans = 0;
        dfs(root);
        return ans;
    }
};

int main() {
    TreeNode nodes[] = {0, 2, 0, 0, 0, 0, 8, 0, 0, 0};
    nodes[0].left = &nodes[1];
    nodes[0].right = &nodes[2];
    nodes[1].left = &nodes[3];
    nodes[1].right = &nodes[4];
    nodes[2].left = &nodes[5];
    nodes[3].right = &nodes[6];
    nodes[6].right = &nodes[7];
    nodes[7].left = &nodes[8];
    nodes[7].right = &nodes[9];
    Solution sol;
    cout << sol.distributeCoins(&nodes[0]) << '\n';
}