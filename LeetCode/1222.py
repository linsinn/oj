class Solution:
  def queensAttacktheKing(self, queens: List[List[int]], king: List[int]) -> List[List[int]]:
    dirs = [-1, 0, 1, 0, -1, 1, 1, -1, -1]
    board = [[0] * 8 for _ in range(8)]
    for q in queens:
      board[q[0]][q[1]] = 1
    ans = []
    for d in range(len(dirs) - 1):
      i = 1
      while True:
        nx = king[0] + dirs[d] * i
        ny = king[1] + dirs[d+1] * i
        if 0 <= nx < 8 and 0 <= ny < 8:
          if board[nx][ny] == 1:
            ans.append([nx, ny])
            break
          i += 1
        else:
          break
    return ans
