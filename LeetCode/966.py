class Solution:
    def spellchecker(self, wordlist, queries):
        """
        :type wordlist: List[str]
        :type queries: List[str]
        :rtype: List[str]
        """

        aeiou = frozenset(['a', 'e', 'i', 'o', 'u'])

        exact = set(wordlist)
        capas = {}
        vowels = {}
        for word in wordlist:
            l_word = word.lower()
            if l_word not in capas:
                capas[l_word] = word
            l_word = list(l_word)
            for i, ch in enumerate(l_word):
                if ch in aeiou:
                    l_word[i] = 'a'
            l_word = ''.join(l_word)
            if l_word not in vowels:
                vowels[l_word] = word

        ans = []
        for query in queries:
            if query in exact:
                ans.append(query)
            elif query.lower() in capas:
                ans.append(capas[query.lower()])
            else:
                l_word = list(query.lower())
                for i, ch in enumerate(l_word):
                    if ch in aeiou:
                        l_word[i] = 'a'
                l_word = ''.join(l_word)
                ans.append(vowels.get(l_word, ''))
        return ans

