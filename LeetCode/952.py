from collections import defaultdict, Counter


class DSU(object):
    def __init__(self, cap):
        self.dsu = [i for i in xrange(cap)]

    def find(self, x):
        dsu = self.dsu
        if dsu[x] != x:
            dsu[x] = self.find(dsu[x])
        return dsu[x]

    def union(self, u, v):
        x = self.find(u)
        y = self.find(v)
        self.dsu[y] = x


class Solution(object):
    def largestComponentSize(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
                  103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199,
                  211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317]
        dsu = DSU(len(A))

        divs = defaultdict(set)

        for i, a in enumerate(A):
            while True:
                divs[a].add(i)
                for prime in primes:
                    if a % prime == 0:
                        divs[prime].add(i)
                        divs[a / prime].add(i)
                        a /= prime
                        break
                else:
                    break
        sets = [v for k, v in divs.iteritems() if k != 1 and len(v) > 1]
        for s in sets:
            cur = -1
            for i in s:
                if cur == -1:
                    cur = i
                else:
                    dsu.union(cur, i)
        cnt = defaultdict(lambda: 0)
        ans = 0
        for i in xrange(len(A)):
            cnt[dsu.find(i)] += 1
            ans = max(ans, cnt[dsu.find(i)])
        return ans


A = [99,68,70,77,35,52,53,25,62]
# A = [2,7,522,526,535,26,944,35,519,45,48,567,266,68,74,591,81,86,602,93,610,621,111,114,629,641,131,651,142,659,669,161,674,163,180,187,190,194,195,206,207,218,737,229,240,757,770,260,778,270,272,785,274,290,291,292,296,810,816,314,829,833,841,349,880,369,147,897,387,390,905,405,406,407,414,416,417,425,938,429,432,926,959,960,449,963,966,929,457,463,981,985,79,487,1000,494,508]
print len(A)
print Solution().largestComponentSize(A)
