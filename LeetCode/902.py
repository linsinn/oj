class Solution(object):
    def atMostNGivenDigitSet(self, D, N):
        """
        :type D: List[str]
        :type N: int
        :rtype: int
        """
        ans = 0
        self.digs = list(str(N))
        self.dp = [1]

        for i in range(1, len(self.digs)):
            self.dp.append(len(D) ** i)
            ans += self.dp[-1]


        def dfs(i):
            ret = 0
            if i == len(self.digs):
                return 1

            for d in D:
                if d < self.digs[i]:
                    ret += self.dp[-i-1]
                elif d == self.digs[i]:
                    ret += dfs(i+1)
                else:
                    break
            return ret
    

        ans += dfs(0)
        return ans
