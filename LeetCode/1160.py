import collections

class Solution:
    def countCharacters(self, words: List[str], chars: str) -> int:
        cnt = collections.Counter(chars)
        ans = 0
        for w in words:
            g = collections.Counter(w)
            f = True
            for k, v in g.items():
                if cnt.get(k, 0) < v:
                    f = False
                    break
            if f:
                ans += len(w)
        return ans
