from collections import deque

class Solution:
    def numEnclaves(self, A: List[List[int]]) -> int:
        N = len(A)
        M = len(A[0])
        dirs = [-1, 0, 1, 0, -1]

        vis = [[0] * M for _ in range(N)]

        def bfs(x, y):
            que = deque([[x, y]])
            while len(que) != 0:
                x, y = que.popleft()
                for i in range(4):
                    nx = x + dirs[i]
                    ny = y + dirs[i+1]
                    if 0 <= nx < N and 0 <= ny < M and not vis[nx][ny] and A[nx][ny] == 1:
                        vis[nx][ny] = 1
                        que.append([nx, ny])

        for i in range(N):
            bfs(i, -1)
            bfs(i, M)
        for i in range(M):
            bfs(-1, i)
            bfs(N, i)
        ans = 0
        for i in range(N):
            for j in range(M):
                if A[i][j] == 1 and not vis[i][j]:
                    ans += 1
        return ans





