import heapq
from collections import deque

class Solution:
	def minCost(self, grid) -> int:
		N = len(grid)
		M = len(grid[0])

		dirs = ((0, 1), (0, -1), (1, 0), (-1, 0))

		memo = [[[N*M] * 4 for _ in range(M)] for _ in range(N)]
		heap = []
		for i in range(4):
			ct = int(grid[0][0] - 1 != i)
			heapq.heappush(heap, (ct, 0, 0, i))
			memo[0][0][i] = ct
		while len(heap) != 0:
			cur = heapq.heappop(heap)
			if cur[1] == N-1 and cur[2] == M-1:
				return cur[0]
			nx = cur[1] + dirs[cur[3]][0]
			ny = cur[2] + dirs[cur[3]][1]
			if 0 <= nx < N and 0 <= ny < M:
				for i in range(4):
					ct = int(grid[nx][ny] - 1 != i) + cur[0]
					if ct < memo[nx][ny][i]:
						heapq.heappush(heap, (ct, nx, ny, i))
						memo[nx][ny][i] = ct
		return N * M


grid = [[4]]
print(Solution().minCost(grid))
