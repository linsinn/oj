class Solution:
    def largestValsFromLabels(self, values: List[int], labels: List[int], num_wanted: int, use_limit: int) -> int:
        arr = zip(values, labels)
        arr = sorted(arr, key=lambda k: k[0])
        used_num = {}
        ans = 0
        cnt = 0
        i = len(arr) - 1
        while cnt < num_wanted and i >= 0:
            if used_num.get(arr[i][1], 0) < use_limit:
                ans += arr[i][0]
                cnt += 1
                used_num[arr[i][1]] = used_num.get(arr[i][1], 0) + 1
            i -= 1
        return ans
