import bisect


class Solution:
  def countSteppingNumbers(self, low: int, high: int):
    arr = [i for i in range(10)]
    l = bisect.bisect_left(arr, low)
    ans = [arr[i] for i in range(l, len(arr))]
    while True:
      tmp = []
      for num in arr:
        if num != 0:
          lb = num % 10
          if lb > 0:
            tmp.append(num * 10 + lb - 1)
          if lb < 9:
            tmp.append(num * 10 + lb + 1)
      l = bisect.bisect_left(tmp, low)
      r = bisect.bisect_right(tmp, high)
      if tmp[0] > high:
        break
      for i in range(l, r):
        ans.append(tmp[i])
      arr = tmp
    return ans


print(Solution().countSteppingNumbers(10, 15))
