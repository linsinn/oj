class Solution:
    def corpFlightBookings(self, bookings: List[List[int]], n: int) -> List[int]:
        g = []
        for b in bookings:
            g.append([b[0], b[2]])
            g.append([b[1]+1, -b[2]])
        g.sort()
        ans = [-1] * n
        cur = 0
        for t in g:
            cur += t[1]
            if t[0] - 1 < n:
                ans[t[0]-1] = cur
        pre = 0
        for i in range(n):
            if ans[i] != -1:
                pre = ans[i]
            else:
                ans[i] = pre
        return ans
