class Solution:
	def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:
		arr = [[sum(row), i] for i, row in enumerate(mat)]
		arr.sort()
		return [r[1] for _, r in enumerate(arr[:k])]
