class Solution:
	def numOfBurgers(self, tomatoSlices: int, cheeseSlices: int) -> List[int]:
		g = tomatoSlices - 2 * cheeseSlices
		k = g // 2
		if g < 0 or g % 2 != 0 or cheeseSlices - k < 0:
			return []
		return [k, cheeseSlices - k]
