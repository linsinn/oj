# -*- coding: utf-8 -*-

class Solution(object):
	def sortArrayByParityII(self, A):
		"""
		:type A: List[int]
		:rtype: List[int]
		"""
		i, j = 0, 1
		ans = [0] * len(A)
		for a in A:
			if a % 2 == 0:
				ans[i] = a
				i += 2
			else:
				ans[j] = a
				j += 2
		return ans
				
