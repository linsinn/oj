import collections


class Solution:
	def minSetSize(self, arr) -> int:
		N = len(arr) // 2
		arr = list(collections.Counter(arr).values())
		arr.sort(reverse=True)
		ans = 0
		tot = 0
		for c in arr:
			tot += c
			ans += 1
			if tot >= N:
				break
		return ans


arr = [3,3,3,3,5,5,5,2,2,7]
print(Solution().minSetSize(arr))
