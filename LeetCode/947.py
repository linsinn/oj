from collections import defaultdict


class Solution(object):
    def removeStones(self, stones):
        """
        :type stones: List[List[int]]
        :rtype: int
        """
        rows = defaultdict(list)
        cols = defaultdict(list)
        for stone in stones:
            rows[stone[0]].append(stone[1])
            cols[stone[1]].append(stone[0])
        ans = 0
        vis = set()

        def dfs(x, y, mark):
            mark.add((x, y))
            for col in rows[x]:
                if (x, col) not in mark:
                    dfs(x, col, mark)
            for row in cols[y]:
                if (row, y) not in mark:
                    dfs(row, y, mark)

        for stone in stones:
            if tuple(stone) not in vis:
                mark = set()
                dfs(stone[0], stone[1], mark)
                ans += len(mark) - 1
                vis |= mark

        return ans


stones = [[0, 0], [0, 2], [1, 1], [2, 0], [2, 2]]
print Solution().removeStones(stones)
