//
// Created by sinn on 4/14/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string str;
int f[2002][2002];
int limit[2002];
int pre_sum[2][2002];


pair<int, int> Handle(string& s) {
    memset(f, 0, sizeof(f));
    memset(limit, 0, sizeof(limit));
    memset(pre_sum, 0, sizeof(pre_sum));

    int cur = 0;
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == '(') {
            cur++;
        } else {
            cur--;
        }
    }
    if (cur == 0) {
        return make_pair(0, 1);
    }
    int ans0 = cur;
    int t = 0;
    cur = 1;
    for (int i = 1; i < s.size(); i++) {
        if (s[i] == '(') {
            limit[t++] = cur++;
        } else {
            cur--;
        }
    }
    limit[t++] = cur;

    int line = 0;
    f[0][0] = 1, f[0][1] = 1;
    pre_sum[line][0] = 1, pre_sum[line][1] = 2;
    for (int i = 1; i < t; i++) {
        for (int j = 0; j <= limit[i]; j++) {
            f[i][j] = pre_sum[line][min(j, limit[i-1])];
            pre_sum[line^1][j] = (f[i][j] + (j == 0 ? 0 : pre_sum[line^1][j-1])) % MOD;
        }
        line ^= 1;
    }
    return make_pair(ans0, f[t-1][limit[t-1]]);
}

string Reverse(string& s) {
    string ret(s);
    for (int i = 0; i < s.size(); i++) {
        ret[s.size()-1-i] = (s[i] == '(' ? ')' : '(');
    }
    return ret;
}


void Solution() {
    int cur = 0;
    int most = 0, pos = -1;
    for (int i = 0; i < str.size(); i++) {
        if (str[i] == '(') {
            cur++;
        } else {
            cur--;
            if (cur < most) {
                pos = i;
                most = cur;
            }
        }
    }
    string s = str.substr(0, pos+1);
    s = Reverse(s);
    auto p0 = Handle(s);
    s = str.substr(pos+1);
    auto p1 = Handle(s);
    ll l = 1LL * p0.second * p1.second % MOD;
    cout << p0.first + p1.first << ' ' << l << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> str) {
        Solution();
    }
    return 0;
}
