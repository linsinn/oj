#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

using ll = long long;

ll N, M, K, T;
int pa[MAXN];
bool seen[MAXN];
vector<Pii> edges;
vector<Pii> gph[MAXN];
bool vis[MAXN];

void dfs(int rt, int ex, int& cnt) {
  vis[rt] = true;
  ++cnt;
  for (auto &v : gph[rt]) {
    if (v.second != ex && !vis[v.first]) {
      dfs(v.first, ex, cnt);
    }
  }
}

void Solution() {
  map<int, int> mp;
  set<int> ans;
  for (int i = 1; i <= edges.size(); ++i) {
    auto &e = edges[i-1];
    if (e.second == 1)
      ans.emplace(i);
    mp[e.second]++;
  }
  int s = 0;
  for (auto &p : mp) {
    if (p.second == 2)
      s = p.first;
  }
  for (int i = 1; i <= edges.size(); ++i) {
    auto &e = edges[i-1];
    if (e.second == s)
      ans.emplace(i);
    gph[e.first].emplace_back(e.second, i);
    gph[e.second].emplace_back(e.first, i);
  }
  for (int v : ans) {
    int cnt = 0;
    memset(vis, 0, sizeof(vis));
    dfs(1, v, cnt);
    if (cnt == N)
      cout << v << ' ';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    edges.resize(N);
    fora (i, 0, N) {
      cin >> edges[i].first >> edges[i].second;
    }
    Solution();
  }
  return 0;
}
