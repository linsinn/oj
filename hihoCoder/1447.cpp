#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int X, Y;

pair<int, int> rngs[MAXN];

void solution() {
    sort(rngs, rngs + N);
    int ans = 0;
    int i = 0;
    int pre = X;
    while (i < N) {
        int cur = pre - 1;
        while (i < N && rngs[i].first <= pre) {
            cur = max(cur, rngs[i].second);
            i++;
        }
        if (cur == pre - 1) {
            cout << "-1\n";
            return ;
        }
        ans += 1;
        pre = cur+1;
        if (pre >= Y)
            break;
    }
    if (pre < Y)
        cout << "-1\n";
    else
        cout << ans << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N >> X >> Y) {
        for (int i = 0; i < N; i++) {
            cin >> rngs[i].first >> rngs[i].second;
        }
        solution();
    }
    return 0;
}
