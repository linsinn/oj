//
// Created by sinn on 6/22/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e2 + 10;

using ll = long long;

int N, M, K, T;
int A[MAXN][MAXN];
ll pre_sum[MAXN][MAXN];

int Solution() {
  fora (i, 0, N) {
    fora (j, 0, M) {
      pre_sum[i][j+1] = pre_sum[i][j] + A[i][j];
    }
  }
  int ans = 0;
  fora (a, 0, M) {
    fora (b, a+1, M) {
      int i = 0, j = 0;
      ll s = 0;
      while (j < N) {
        s += pre_sum[j][b+1] - pre_sum[j][a];
        ++j;
        while (i < j && s > K) {
          s -= pre_sum[i][b+1] - pre_sum[i][a];
          ++i;
        }
        ans = max(ans, (j - i) * (b - a + 1));
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) {
    fora (i, 0, N) {
      fora (j, 0, M) {
        cin >> A[i][j];
      }
    }
    cout << Solution() << '\n';
  }
  return 0;
}
