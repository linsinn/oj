//
// Created by sinn on 1/29/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string str;

string Solution() {
    int conL = 0;
    int gotA = 0;
    for (char ch : str) {
        if (ch == 'O') {
            conL = 0;
        } else if (ch == 'L') {
            conL++;
            if (conL >= 3)
                return "NO";
        } else {
            gotA++;
            if (gotA > 1)
                return "NO";
        }
    }
    return "YES";
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        while (N--) {
            cin >> str;
            cout << Solution() << '\n';
        }
    }
    return 0;
}
