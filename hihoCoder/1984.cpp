#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
string s, t;

struct Node;

struct Node {
  bool is_array;
  vector<Node> arr;
  set<Node> se;
  bool operator < (const Node& rhs) const {
    if (is_array ^ rhs.is_array)
      return !is_array;
    if (is_array) {
      auto &p = arr;
      auto &q = rhs.arr;
      if (p.size() != q.size())
        return p.size() < q.size();
      fora (i, 0, p.size()) {
        if (p[i] < q[i])
          return true;
        if (q[i] < p[i])
          return false;
      }
      return false;
    } else {
      auto &p = se;
      auto &q = rhs.se;
      if (p.size() != q.size())
        return p.size() < q.size();
      for (auto i = p.begin(), j = q.begin(); i != p.end(); ++i, ++j) {
        if (*i < *j)
          return true;
        if (*j < *i)
          return false;
      }
      return false;
    }
  }
};

Node Helper(string &str, int st, int ed, map<int, int>& mp) {
  Node ret;
  if (str[st] == '[') {
    ret.is_array = true;
    int i = st + 1;
    while (i < ed) {
      int j = mp[i];
      ret.arr.emplace_back(Helper(str, i, j, mp));
      i = j + 1;
    }
  }
  else {
    ret.is_array = false;
    int i = st + 1;
    while (i < ed ) {
      int j = mp[i];
      ret.se.emplace(Helper(str, i, j, mp));
      i = j + 1;
    }
  } 
  return ret;
}

Node Parse(string &str) {
  map<int, int> mp;
  stack<int> a, b;
  fora (i, 0, str.size()) {
    if (str[i] == '{') {
      a.emplace(i);
    } else if (str[i] == '}') {
      mp[a.top()] = i;
      a.pop();
    } else if (str[i] == '[') {
      b.emplace(i);
    } else {
      mp[b.top()] = i;
      b.pop();
    }
  }
  return Helper(str, 0, str.size()-1, mp);
}

bool Solution() {
  map<int, int> mp;
  Node a = Parse(s);
  Node b = Parse(t);
  return !(a < b || b < a);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> s >> t;
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
