#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<ll> A;

ll Solution() {
  if (N <= 2) return N;
  sort(A.begin(), A.end());
  vector<Vec> dp(N, Vec(N, 2));
  int ans = 2;
  ford (j, N-2, 1) {
    int i = j - 1, k = j + 1;
    while (i >= 0 && k <= N-1) {
      if (A[i] + A[k] < 2 * A[j]) 
        ++k;
      else if (A[i] + A[k] > 2 * A[j]) {
        --i;
      } else {
        dp[i][j] = dp[j][k] + 1;
        ans = max(ans, dp[i][j]);
        --i;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';;
  }
  return 0;
}
