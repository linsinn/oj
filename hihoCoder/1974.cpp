#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
int cnt[50][60];
int cov[60];
int memos[60][1 << 20];

ll Solution() {
  memset(cov, 0, sizeof(cov));
  fora (i, 0, M) {
    fora (j, 0, N) {
      if (cnt[i][j] >= A[j]) 
        cov[i] |= 1 << j;
    }
  }
  memset(memos, -1, sizeof(memos));
  memos[0][cov[0]] = 1;
  memos[0][0] = 0;
  fora (i, 1, M) {
    memcpy(memos[i], memos[i-1], sizeof(memos[i]));
    fora (j, 0, 1 << N) {
      if (memos[i-1][j] != -1) {
        int k = j | cov[i];
        if (memos[i][k] == -1)
          memos[i][k] = memos[i][j] + 1;
        else 
          Min(memos[i][k], memos[i][j] + 1);
      }
    }
  }
  int ans = memos[M-1][(1 << N) - 1];
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    fora (i, 0, M) {
      fora (j, 0, N) 
        cin >> cnt[i][j];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
