//
// Created by sinn on 4/23/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 1;

using ll = long long;

int N, M, K;
bool vis[MAXN];

void Solution() {
    memset(vis, 0, sizeof(vis));
    for (int i = 2; i <= N; i++) {
        if (!vis[i]) {
            for (int k = 2; i * k <= N; k++) {
                vis[i*k] = true;
            }
        }
    }
    for (int i = 2; i <= N; i++) {
        if (!vis[i] && !vis[N-i]) {
            cout << i << ' ' << N-i << '\n';
            return ;
        }
    }
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        Solution();
    }
    return 0;
}
