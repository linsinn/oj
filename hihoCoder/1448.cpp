#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int arr[MAXN];

void Solution() {
    if (N % K != 0) {
	cout << "NO\n";
	return ;
    }
    int num = N / K;
    map<int, int> cnts;
    for (int i = 0; i < N; i++)
	cnts[arr[i]]++;
    while (!cnts.empty()) {
	int beg = cnts.begin()->first;
	for (int i = 0; i < K; i++) {
	    if (cnts.count(beg+i) == 0) {
		cout << "NO\n";
		return ;
	    }
	    cnts[beg+i]--;
	    if (cnts[beg+i] == 0)
		cnts.erase(beg+i);
	}
    }
    cout << "YES\n";
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int T;
    while (cin >> T) {
	while (T--) {
	    cin >> N >> K;
	    for (int i = 0; i < N; i++) {
		cin >> arr[i];
	    }
	    Solution();
	}
    }
    return 0;
}
