#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e3 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> gph[MAXN];
int wei[MAXN * MAXN];

bool seen[MAXN];

void Mark(vector<vector<Pii>> &pre_nodes, int u, vector<bool> &ans) {
  if (seen[u]) return ;
  seen[u] = true;
  for (auto &v : pre_nodes[u]) {
    ans[v.second] = true;
    Mark(pre_nodes, v.first, ans);
  }
}

void Solution() {
  Vec dist(N+1, INF);
  vector<vector<Pii>> pre_nodes(N+1);
  vector<bool> seen(N+1, false);
  dist[1] = 0;
  fora (i, 0, N) {
    int v = -1;
    fora (j, 1, N+1) {
      if (!seen[j] && (v == -1 || dist[j] < dist[v]))
        v = j;
    }
    if (dist[v] == INF) break;
    seen[v] = true;
    for (auto &edge : gph[v]) {
      int to = edge.first;
      int ei = edge.second;
      int w = wei[ei];
      if (dist[v] + w < dist[to]) {
        dist[to] = dist[v] + w;
        pre_nodes[to].clear();
        pre_nodes[to].emplace_back(v, ei);
      } else if (dist[v] + w == dist[to]) {
        pre_nodes[to].emplace_back(v, ei);
      }
    }
  }
  vector<bool> ans(M);
  Mark(pre_nodes, N, ans);
  for (bool res : ans) {
    if (res) cout << "YES\n";
    else cout << "NO\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    int u, v, w;
    fora (i, 0, M) {
      cin >> u >> v >> w;
      gph[u].emplace_back(v, i);
      gph[v].emplace_back(u, i);
      wei[i] = w;
    }
    Solution();
  }
  return 0;
}
