#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e3 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
string B[MAXN];
bool vis[MAXN][MAXN][11];
Vec A;

struct Node {
  int x, y;
  int s, k;
  Node() = default;
  Node(int _x, int _y, int _s, int _k) : x{_x}, y{_y}, s{_s}, k{_k} {}
  bool Arrived() {
    return x == N-1 && y == N-1;
  }
};

bool Check(int x, int y) {
  return 0 <= x && x < N && 0 <= y && y < N;
}

ll Solution() {
  memset(vis, 0, sizeof(vis));
  vis[0][0][0] = true;
  queue<Node> que;
  que.emplace(Node(0, 0, 0, 0));
  constexpr int dirs[] = {-1, 0, 1, 0, -1};
  while (!que.empty()) {
    auto n = que.front();
    que.pop();
    if (n.Arrived()) 
      return n.s;
    fora (d, 0, 4) {
      int x = n.x + dirs[d], y = n.y + dirs[d+1];
      if (Check(x, y)) {
        if (B[x][y] == '.') {
          if (!vis[x][y][n.k]) {
            que.emplace(Node(x, y, n.s+1, n.k));
            vis[x][y][n.k] = true;
          }
        } else {
          if (n.k+1 <= K && !vis[x][y][n.k+1]) {
            que.emplace(Node(x, y, n.s+1, n.k+1));
            vis[x][y][n.k+1] = true;
          }
        }
      }
    }
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) cin >> B[i];
    cout << Solution() << '\n';
  }
  return 0;
}
