#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

int Check(int idx) {
  set<int> diff;
  fora (i, 1, N) {
    if (i == idx) continue;
    if (i - 1 == idx) {
      if (i - 2 >= 0)
        diff.emplace(A[i] - A[i-2]);
    } else {
        diff.emplace(A[i] - A[i-1]);
    }
  }
  int g = *diff.begin();
  for (auto v : diff) {
    if (v % g != 0)
      return -1;
  }
  return g;
}

void Solution() {
  map<int, vector<int>> d;
  fora (i, 1, N) {
    d[A[i] - A[i-1]].emplace_back(i);
  }
  set<int> ans;
  int k = Check(-1);
  if (k != -1)
    ans.emplace(k);
  int t = d.begin()->second.at(0);
  fora (i, t-3, t+4) {
    int g = Check(i);
    if (g != -1)
      ans.emplace(g);
  }
  k = d.begin()->first;
  for (auto &p : d) {
    if (p.first % k != 0)  {
      t = p.second.at(0);
      fora (i, t-3, t+4) {
        int g = Check(i);
        if (g != -1)
          ans.emplace(g);
      }
      break;
    }
  }
  ans.emplace(1);
  set<int> res;
  for (int v : ans) {
    fora (i, 1, sqrt(v) + 1) {
      if (v % i == 0) {
        res.emplace(i);
        res.emplace(v/i);
      }
    }
  }
  for (int v : res) {
    cout << v << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    Solution();
  }
  return 0;
}
