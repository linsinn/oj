//
// Created by Sinn on 2/18/2019.
//


#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

struct Point {
    int x = 0, y = 0;
    int GetIdx() {
        return x * 4 + y;
    }
    bool operator == (const Point& p) const {
        return this->x == p.x && this->y == p.y;
    }
};

struct Pattern {
    int weight = 0;
    vector<Point> pts;
    bitset<200> mask;
} patterns[20], final;

int f[18][1 << 18];

inline int CalcLineIdx(int a, int b) {
    return a * (31 - a) / 2 + (b - a - 1);
}

inline void SetMaskBit(Point& x, Point& y, Pattern& pat) {
    int a = x.GetIdx(), b = y.GetIdx();
    if (a == b)
        return ;
    if (b < a)
        swap(a, b);
    if (a / 4 == b / 4) {
        for (int i = a+1; i <= b; i++)
            pat.mask.set(CalcLineIdx(i-1, i));
    } else if ((b - a) % 4 == 0) {
        for (int i = a+4; i <= b; i+=4)
            pat.mask.set(CalcLineIdx(i-4, i));
    } else if ((b - a) % 3 == 0 && (b-a) / 3 == b/4 - a/4) {
        for (int i = a+3; i <= b; i+=3)
            pat.mask.set(CalcLineIdx(i-3, i));
    } else if ((b - a) % 5 == 0 && (b-a) / 5 == b/4 - a/4) {
        for (int i = a+5; i <= b; i+=5)
            pat.mask.set(CalcLineIdx(i-5, i));
    } else {
        pat.mask.set(CalcLineIdx(a, b));
    }
}

void CalcMask(Pattern& pat) {
    for (int i = 1; i < pat.pts.size(); i++) {
        SetMaskBit(pat.pts[i-1], pat.pts[i], pat);
    }
}

int dp(int selected, int cur) {
    if (f[cur][selected] != -1)
        return f[cur][selected];
    int ret = -1;
    int ones = 0;
    for (int i = 0; i < N; i++) {
        if ((1 << i) & selected) {
            ones += 1;
            if (i != cur && patterns[i].pts.back() == patterns[cur].pts[0]) {
                ret = dp(selected ^ (1 << cur), i);
                if (ret == 1) {
                    break;
                }
            }
        }
    }
    if (ones == 1)
        f[cur][selected] = 1;
    else
        f[cur][selected] = max(0, ret);
    return f[cur][selected];
}

int Check(int selected) {
    int ret = -1;
    for (int i = 0; i < 20; i++) {
        if ((1 << i) & selected) {
            ret = dp(selected, i);
            if (ret == 1)
                break;
        }
    }
    if (ret != 1)
        return 1 << 30;
    else {
        int weight = 0;
        for (int i = 0; i < 20; i++)
            if ((1 << i) & selected)
                weight += patterns[i].weight;
        return weight;
    }
}

void dfs(int idx, bitset<200> cur_mask, int selected, int& ans) {
    if (cur_mask == final.mask) {
        ans = min(ans, Check(selected));
    }
    if (idx == N)
        return ;
    dfs(idx+1, cur_mask | patterns[idx].mask, selected | (1<<idx), ans);
    dfs(idx+1, cur_mask, selected, ans);
}

int Solution() {
    for (int i = 0; i < N; i++)
        CalcMask(patterns[i]);
    for (int i = 0; i < M; i++) {
        SetMaskBit(final.pts[i*2], final.pts[i*2+1], final);
    }
    int ans = 1 << 30;
    memset(f, -1, sizeof(f));
    dfs(0, bitset<200>(), 0, ans);
    return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int t;
    Point p;
    while (cin >> N) {
        for (int i = 0; i < N; i++) {
            cin >> t >> patterns[i].weight;
            for (int j = 0; j < t; j++) {
                cin >> p.x >> p.y;
                p.x -= 1;
                p.y -= 1;
                patterns[i].pts.emplace_back(p);
            }
        }
        cin >> M;
        for (int i = 0; i < M; i++) {
            cin >> p.x >> p.y;
            p.x -= 1;
            p.y -= 1;
            final.pts.emplace_back(p);
            cin >> p.x >> p.y;
            p.x -= 1;
            p.y -= 1;
            final.pts.emplace_back(p);
        }
        cout << Solution() << '\n';
    }
    return 0;
}