//
// Created by sinn on 4/7/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e1 + 1;
constexpr int dirs[] = {-1, 0, 1, 0, -1};

using ll = long long;

int N, M, K, hhp, hap;
char maze[MAXN][MAXN];
int monster_map[MAXN][MAXN];
int final_state;
struct Monster {
    int x, y;
    int hp, ap;
    bool special;
} monsters[MAXN * MAXN];
pair<int, int> entrance;
int f[1 << 20][6];

int Attack(int hp, int& buff_duration, Monster& monster) {
    int round =  monster.hp / hap + (monster.hp % hap != 0);
    int remain = round - max(0, (buff_duration - 1));
    hp -= max(remain, 0) * monster.ap;
    buff_duration = max(0, -remain);
    if (hp > 0 && monster.special)
        buff_duration = 5;
    return hp;
}

void dfs(int state, int buff_duration, int hp, int& ans) {
    if ((state & final_state) == final_state) {
        ans = max(ans, hp);
        return ;
    }
    bool vis[MAXN][MAXN];
    memset(vis, 0, sizeof vis);
    for (int i = 0; i < N * M; i++) {
        if ((state & (1 << i)) != 0) {
            int x = i / M, y = i % M;
            for (int d = 0; d < 4; d++) {
                int nx = x + dirs[d], ny = y + dirs[d+1];
                int nxt = nx * M + ny;
                int bd = buff_duration;
                if (0 <= nx && nx < N && 0 <= ny && ny < M && (state & (1 << nxt)) == 0 && !vis[nx][ny]) {
                    vis[nx][ny] = true;
                    int h;
                    if (monster_map[nx][ny] != -1) {
                        h = Attack(hp, bd, monsters[monster_map[nx][ny]]);
                    } else {
                        h = hp, bd = max(0, bd - 1);
                    }
                    if (f[state | (1 << nxt)][bd] < h) {
                        f[state | (1 << nxt)][bd] = h;
                        dfs(state | (1 << nxt), bd, h, ans);
                    }
                }
            }
        }
    }
}


void Solution() {
    memset(f, 0, sizeof(f));
    int state = 1 << (entrance.first * M + entrance.second);
    f[state][5] = hhp;
    int ans = 0;
    dfs(state, 5, hhp, ans);
    if (ans == 0)
        cout << "DEAD\n";
    else
        cout << ans << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N >> M) {
        memset(monster_map, -1, sizeof(monster_map));
        K = 0;
        final_state = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                cin >> maze[i][j];
                if (maze[i][j] == 'S' || maze[i][j] == 'M') {
                    monsters[K].x = i;
                    monsters[K].y = j;
                    monsters[K].special = maze[i][j] == 'S';
                    final_state |= 1 << (i * M + j);
                    monster_map[i][j] = K;
                    K++;
                } else if (maze[i][j] == 'D') {
                    entrance.first = i;
                    entrance.second = j;
                }
            }
        }
        for (int i = 0; i < K; i++) {
            cin >> monsters[i].hp >> monsters[i].ap;
        }
        cin >> hhp >> hap;
        Solution();
    }
    return 0;
}