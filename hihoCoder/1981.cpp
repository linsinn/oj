#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec len;
vector<vector<string>> w;
string article;

void Solution() {
  map<string, int> mp;
  int i = 0;
  while (i < article.size()) {
    int j = i + 1;
    while (j < article.size() && article[j] != ' ')
      ++j;
    string s = article.substr(i, j-i);
    i = j+1;
    mp[s]++;
  }
  fora (i, 0, M) {
    int ans = 0;
    fora (j, 0, len[i]) {
      auto it = mp.find(w[i][j]);
      if (it != mp.end())
        ans += it->second;
    }
    cout << ans << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> M) {
    len.resize(M);
    fora (i, 0, M) cin >> len[i];
    w.resize(M, vector<string>());
    fora (i, 0, M) {
      w[i].resize(len[i]);
      fora (j, 0, len[i]) {
        cin >> w[i][j];
      }
    }
    getline(cin, article);
    getline(cin, article);
    Solution();
  }
  return 0;
}
