//
// Created by sinn on 7/20/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e3 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
double p[MAXN];
double memos[MAXN][MAXN];

double Solution() {
  memset(memos, 0, sizeof(memos));
  memos[0][0] = 1;
  fora (i, 1, N+1) {
    fora (j, 0, i+1) {
      memos[i][j] += memos[i-1][j] * (1 - p[i-1]);
      if (j > 0)
        memos[i][j] += memos[i-1][j-1] * p[i-1];
    }
  }
  return memos[N][M];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) cin >> p[i];
    cout << Solution() << '\n';
  }
  return 0;
}
