#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> ps;

struct Bomb {
  int x, y;
  int d;
};
vector<Bomb> bs;

void Helper(map<int, vector<Pii>> mp, map<int, vector<Pii>>& res) {
  for (auto &p : mp) {
    auto &vec = p.second;
    sort(vec.begin(), vec.end());
    int l = vec[0].first, r = vec[0].second;
    vector<Pii> tmp;
    fora (i, 1, vec.size()) {
      if (vec[i].first > r) {
        tmp.emplace_back(l, r);
        l = vec[i].first, r = vec[i].second;
      } else {
        r = max(r, vec[i].second);
      }
    }
    tmp.emplace_back(l, r);
    res[p.first] = tmp;
  }
}

ll Solution() {
  map<int, vector<Pii>> row, col;
  for (auto bomb : bs) {
    row[bomb.x].emplace_back(bomb.y - bomb.d, bomb.y + bomb.d);
    col[bomb.y].emplace_back(bomb.x - bomb.d, bomb.x + bomb.d);
  }
  map<int, vector<Pii>> rowb, colb;
  Helper(row, rowb);
  Helper(col, colb);
  int ans = 0;
  fora (i, 0, N) {
    auto it = rowb.find(ps[i].first);
    bool found = false;
    if (it != rowb.end()) {
      auto &v = it->second;
      auto t = upper_bound(v.begin(), v.end(), make_pair(ps[i].second, INF));
      if (t != v.begin()) {
        t = prev(t);
        if (t->first <= ps[i].second && ps[i].second <= t->second) {
          found = true;
          ++ans;
        }
      }
    } 
    if (!found) {
      it = colb.find(ps[i].second);
      if (it != colb.end()) {
        auto &v = it->second;
        auto t = upper_bound(v.begin(), v.end(), make_pair(ps[i].first, INF));
        if (t != v.begin()) {
          t = prev(t);
          if (t->first <= ps[i].first && ps[i].first <= t->second)
            ++ans;
        }
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    ps.resize(N);
    fora (i, 0, N) cin >> ps[i].first >> ps[i].second;
    bs.resize(M);
    fora (i, 0, M) cin >> bs[i].x >> bs[i].y >> bs[i].d;
    cout << Solution() << '\n';
  }
  return 0;
}
