//
// Created by sinn on 5/4/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;
constexpr int dirs[] = {-1, 0, 1, 0, -1};

using ll = long long;

int N, M, K;
string board[120];
bool seen[120][120][2];

void dfs(int x, int y, int lr) {
  if (seen[x][y][lr])
    return ;
  seen[x][y][lr] = true;
  if (board[x][y] != '/' && board[x][y] != '\\') {
    seen[x][y][lr ^ 1] = true;
  }
  for (int d = 0; d < 4; d++) {
    int nx = x + dirs[d], ny = y + dirs[d+1];
    if (0 <= nx && nx < N && 0 <= ny && ny < M) {
      char cur = board[x][y], nxt = board[nx][ny];
      if (cur == '/') {
        if (lr == 0) {
          if (d == 0) {
            if (nxt == '/') dfs(nx, ny, 1);
            else dfs(nx, ny, 0);
          } else if (d == 3) {
            dfs(nx, ny, 1);
          }
        } else {
          if (d == 1) {
            dfs(nx, ny, 0);
          } else if (d == 2) {
            if (nxt == '/') dfs(nx, ny, 0);
            else dfs(nx, ny, 1);
          }
        }
      } else if (cur == '\\') {
        if (lr == 0) {
          if (d == 2) {
            if (nxt == '/') dfs(nx, ny, 0);
            else dfs(nx, ny, 1);
          } else if (d == 3) {
            dfs(nx, ny, 1);
          }
        } else {
          if (d == 0) {
            if (nxt == '/') dfs(nx, ny, 1);
            else dfs(nx, ny, 0);
          } else if (d == 1) {
            dfs(nx, ny, 0);
          }
        }
      } else {
        if (d == 0) {
          if (nxt == '/') dfs(nx, ny, 1);
          else dfs(nx, ny, 0);
        } else if (d == 1) {
          dfs(nx, ny, 0);
        } else if (d == 2) {
          if (nxt == '/') dfs(nx, ny, 0);
          else dfs(nx, ny, 1);
        } else {
          dfs(nx, ny, 1);
        }
      }
    }
  }
}

int Solution() {
  int ans = 0;
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < M; j++) {
      if (!seen[i][j][0]) {
        dfs(i, j, 0);
        ans++;
      }
      if (!seen[i][j][1]) {
        dfs(i, j, 1);
        ans++;
      }
    }
  }
  return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N >> M) {
      getline(cin, board[0]);
      for (int i = 0; i < N; i++) {
        getline(cin, board[i]);
      }
      cout << Solution() << '\n';
    }
    return 0;
}