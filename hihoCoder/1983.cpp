#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string s;

int LyndonWord() {
  int i = 0, j = 1, k;
  while (j < N) {
    for (k = 0; k < N && s[(i+k)%N] == s[(j+k)%N]; ++k);
    if (s[(i+k)%N] <= s[(j+k)%N])
      j += k+1;
    else {
      i += k+1;
      if (i < j) i = j++;
      else j = i + 1;
    }
  }
  return i;
}

void Solution() {
  N = s.size();
  int st = LyndonWord();
  fora (i, 0, N) {
    cout << s[(st+i)%N];
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    Solution();
  }
  return 0;
}
