#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int LEN = 26;
string keywords[1010];
string S;
int mark[MAXN];

struct Node {
    Node *fail;
    Node *children[26];
    char ch;
    int length;
    Node() {
        fail = this;
        memset(children, 0, sizeof children);
        length = 0;
    }
};

void BuildTrie(Node *root, const string& str) {
    for (char c : str) {
        int k = c - 'a';
        if (root->children[k] == nullptr)
            root->children[k] = new Node();
        root = root->children[k];
        root->ch = c;
    }
    root->length = str.size();
}

void BuildAC(Node *root) {
    queue<Node *> que;
    que.emplace(root);
    while (!que.empty()) {
        Node *cur = que.front();
        que.pop();
        for (int i = 0; i < 26; i++) {
            Node *nxt = cur->children[i];
            if (nxt != nullptr) {
                if (cur == root) {
                    nxt->fail = root;
                } else {
                    Node *tmp = cur->fail;
                    while (tmp != root && tmp->children[i] == nullptr)
                        tmp = tmp->fail;
                    nxt->fail = tmp->children[i];
                    if (nxt->fail == nullptr)
                        nxt->fail = root;
                }
                que.emplace(nxt);
            }
        }
    }
}

void Search(Node *root, const string& str) {
    int i = 0;
    Node *cur = root;
    while (i != str.size()) {
        int k = str[i] - 'a';
        if (cur->children[k] == nullptr) {
            if (cur == root)
                i++;
            else
                cur = cur->fail;
        } else {
            cur = cur->children[k];
            Node *tmp = cur;
            while (tmp != root && tmp->length == 0) {
                tmp = tmp->fail;
            }
            mark[i] = max(mark[i], tmp->length);
            i++;
        }
    }
}


void Solution() {
    Node *root = new Node();
    for (int i = 0; i < N; i++)
        BuildTrie(root, keywords[i]);
    BuildAC(root);
    Search(root, S);
    int leftBound = S.size();
    for (int i = S.size() - 1; i >= 0; i--) {
        leftBound = min(leftBound, i - mark[i] + 1);
        if (i >= leftBound)
            S[i] = '*';
    }
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++)
            cin >> keywords[i];
        cin >> S;
        Solution();
        cout << S;
    }
    return 0;
}

