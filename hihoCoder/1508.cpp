#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &b) {if (b > a) a = b;}

ll N, M, K, T, R;
vector<Pii> p;

double GetLen(const Pii& a, const Pii& b) {
  ll x = b.first - a.first;
  ll y = b.second - a.second;
  return sqrt(x * x + y * y);
}

double GetAng(const Pii& a, const Pii& b) {
  ll x = b.first - a.first;
  ll y = b.second - a.second;
  double ang = atan2(y, x);
  if (ang < 0) {
    return PI + PI + ang;
  } else {
    return ang;
  }
}

bool Cmp(const pair<double, int>& a, const pair<double, int>& b) {
  if (a.first != b.first)
    return a.first < b.first;
  return a.second > 0;
}

ll Solution() {
  R *= 2;
  int ans = 0;
  fora (i, 0, N) {
    vector<pair<double, int>> v;
    fora (j, 0, N) {
      if (i == j) continue;
      double l = GetLen(p[i], p[j]);
      if (l - R > 0) continue;
      double ang = GetAng(p[i], p[j]);
      if (l == R) {
        v.emplace_back(ang, 1);
        v.emplace_back(ang, -1);
      } else {
        double beta = acos(l / R);
        v.emplace_back(ang-beta, 1);
        v.emplace_back(ang+beta, -1);
      }
    }
    int k = v.size();
    fora (i, 0, k) {
      v.emplace_back(v[i]);
      v.back().first += PI * 2;
    }
    sort(v.begin(), v.end(), Cmp);
    int cur = 0;
    fora (i, 0, v.size()) {
      cur += v[i].second;
      ans = max(ans, cur+1);
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> R) {
    p.resize(N);
    fora (i, 0, N) {
      cin >> p[i].first >> p[i].second;
    }
    cout << Solution() << '\n';
  }
  return 0;
}
