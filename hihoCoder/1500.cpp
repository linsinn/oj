//
// Created by sinn on 6/9/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 50004;

using ll = long long;

ll N, M, K;
ll f[MAXN];

struct Node {
  ll in, ip;
  ll c;
  vector<Node* > children;
} nodes[2022];

ll dfs(Node* rt) {
  vector<ll> costs;
  for (Node* ch : rt->children) {
    ll mc = dfs(ch);
    costs.emplace_back(ch->c + mc);
  }
  fora (i, 0, rt->in+1) f[i] = 1e16;
  f[0] = 0;
  fora (i, 0, costs.size()) {
    ford (k, rt->in, 1) {
      f[k] = min(f[k], f[max(0LL, k - rt->children[i]->ip)] + costs[i]);
    }
  }
  return f[rt->in];
}

ll Solution() {
  ll ans = dfs(&nodes[M]);
  if (ans >= 1e16)
    return -1;
  return ans + nodes[M].c;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
  freopen("a.txt", "w", stdout);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int p, a, b, c;
    fora (i, 1, N+1) {
      cin >> p >> a >> b >> c;
      nodes[i].in = a;
      nodes[i].ip = b;
      nodes[i].c = c;
      if (p == 0)
        M = i;
      nodes[p].children.emplace_back(&nodes[i]);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
