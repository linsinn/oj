#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
bool mark[10];
int digs[10];
set<string> strs;

void dfs(int n, int k) {
    if (k == 0) {
        string s;
        for (int i = 0; i < N; i++) {
            s += '0' + digs[i]+1;
            if (mark[i])
                s += '-';
        }
        string t = s;
        int a = 0, b = 1;
        while (b < s.size()) {
            if (s[b] == '-') {
                std::sort(t.begin()+a, t.begin()+b);
                a = b + 1;
            }
            b++;
        }
        std::sort(t.begin()+a, t.begin()+b);
        if (strs.count(t) == 0)
            strs.emplace(s);
    } else if (n - 1 >= k) {
        mark[N-n] = true;
        dfs(n-1, k-1);
        mark[N-n] = false;
        dfs(n-1, k);
    }
}

void Solution() {
    for (int i = 0; i < 10; i++)
        digs[i] = i;
    do {
        for (int i = N-1; i >= 0; i--) {
            memset(mark, 0, sizeof mark);
            dfs(N, i);
        }
    } while (std::next_permutation(digs, digs+N));
    for (const string& s : strs) {
        cout << s << '\n';
    }
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        Solution();
    }
    return 0;
}
