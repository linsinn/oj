//
// Created by sinn on 12/23/18.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
pair<int, int> rc[20];
ll tot[20];

void dfs(int idx, int cur, int k, int a, int b, int c, int d) {
    if (k - cur + idx > K)
        return ;
    if (cur == k) {
        tot[k] += 1LL * a * b * (N+1-c) * (M+1-d);
        return ;
    }
    for (int i = idx; i < K; i++) {
        dfs(i+1, cur + 1, k,
                min(a, rc[i].first),
                min(b, rc[i].second),
                max(c, rc[i].first),
                max(d, rc[i].second));
    }
}

ll Solution() {
    memset(tot, 0, sizeof tot);
    for (int k = 1; k <= K; k++) {
        dfs(0, 0, k, N*2, M*2, 0, 0);
    }
    ll all = (1LL + N) * N * (1LL + M) * M / 4;
    for (int i = 1; i <= K; i++) {
        all += (i % 2 == 0 ? 1LL : -1LL) * tot[i];
    }
    return all;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N >> M >> K) {
        for (int i = 0; i < K; ++i) {
            cin >> rc[i].first >> rc[i].second;
        }
        cout << Solution() << '\n';
    }
    return 0;
}