#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> A;
map<int, vector<int>> row, col;
ll ans[5];

void Helper0() {

}

void Solution() {
  row.clear(), col.clear();
  for (auto &a : A) {
    row[a.first].emplace_back(a.second);
    col[a.second].emplace_back(a.first);
  }
  for (auto &p : row) sort(p.second.begin(), p.second.end());
  for (auto &p : col) sort(p.second.begin(), p.second.end());
  memset(ans, 0, sizeof(ans));
  ans[0] = (M - col.size()) * (N - row.size());
  for (auto &p : row) {
    auto &v = p.second;
    auto ed = col.lower_bound(v[0]);
    int cnt = 0;
    for (auto it = col.begin(); it != ed; ++it) {
      ++cnt;
      auto &w = it->second;
      auto g = upper_bound(w.begin(), w.end(), p.first);
      if (g != w.begin()) {
        if (g == w.end()) ans[2]++;
        else ans[3]++;
      } else {
        ans[2]++;
      }
    }
    ans[1] += v[0] - cnt;
    for (int i = 1; i < v.size(); ++i) {
      auto st = col.upper_bound(v[i-1]);
      auto ed = col.lower_bound(v[i]);
      cnt = 0;
      for (auto it = st; it != ed; ++it) {
        ++cnt;
        auto &w = it->second;
        auto g = upper_bound(w.begin(), w.end(), p.first);
        if (g != w.begin()) {
          if (g == w.end()) ans[3]++;
          else ans[4]++;
        } else {
          ans[3]++;
        }
      }
      ans[2] += v[i] - v[i-1] - 1 - cnt;
    }
    auto st = col.upper_bound(v.back());
    cnt = 0;
    for (auto it = st; it != col.end(); ++it) {
      ++cnt;
      auto &w = it->second;
      auto g = upper_bound(w.begin(), w.end(), p.first);
      if (g != w.begin()) {
        if (g == w.end()) ans[2]++;
        else ans[3]++;
      } else {
        ans[2]++;
      }
    }
    ans[1] += M - v.back() - 1 - cnt;
  }
  for (auto &p : col) {
    auto &v = p.second;
    auto ed = row.lower_bound(v[0]);
    int cnt = 0;
    for (auto it = row.begin(); it != ed; ++it) {
      ++cnt;
    }
    ans[1] += v[0] - cnt;
    for (int i = 1; i < v.size(); ++i) {
      auto st = row.upper_bound(v[i-1]);
      auto ed = row.lower_bound(v[i]);
      cnt = 0;
      for (auto it = st; it != ed; ++it) {
        ++cnt;
      }
      ans[2] += v[i] - v[i-1] - 1 - cnt;
    }
    auto st = row.upper_bound(v.back());
    cnt = 0;
    for (auto it = st; it != row.end(); ++it) {
      ++cnt;
    }
    ans[1] += M - v.back() - 1 - cnt;
  }
  fora (i, 0, 5) {
    cout << ans[i] << " \n"[i==4];
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) {
    A.resize(K);
    fora (i, 0, K) {
      cin >> A[i].first >> A[i].second;
    }
    Solution();
  }
  return 0;
}
