//
// Created by sinn on 2/24/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string S;
int cnts[256];

int Solution() {
    int i = 0, j = 0;
    memset(cnts, 0, sizeof(cnts));
    int ans = S.size() + 1;
    while (j < S.size()) {
        cnts[S[j]]++;
        while (cnts['h'] > 2 || cnts['i'] > 1 || cnts['o'] > 1) {
            cnts[S[i]]--;
            i++;
        }
        while (S[i] != 'h' && S[i] != 'i' && S[i] != 'o') {
            i++;
        }
        if (cnts['h'] == 2 && cnts['i'] == 1 && cnts['o'] == 1)
            ans = min(ans, j - i + 1);
        j++;
    }
    return ans == S.size() + 1 ? -1 : ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> S) {
        cout << Solution() << '\n';
    }
    return 0;
}