#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int atk[20], con[20];
int boat;
bool seen[1 << 20];
int tot;
Vec A;

bool Check(int x) {
  fora (i, 0, N) {
    if ((atk[i] & x) && !(con[i] & x) && (x & (1 << i))) {
      return false;
    }
  }
  return true;
}

struct Node {
  int n;
  int w;
  int s;
  Node(int _n, int _w, int _s) : n{_n}, w{_w}, s{_s} {}
  bool IsEnd() {
    return n == tot && w == 1;
  }
};

ll Solution() {
  memset(seen, 0, sizeof(seen));
  tot = (1 << N) - 1;
  queue<Node> que;
  que.emplace(Node(tot, 0, 0));
  seen[tot << 1] = true;
  seen[1] = true;
  while (!que.empty()) {
    auto cur = que.front(); que.pop();
    if (cur.IsEnd())
      return cur.s;
    for (int u = cur.n; u != 0; u = (u - 1) & cur.n) {
      int v = cur.n ^ u;
      if (__builtin_popcount(u) > M || !(u & boat)) continue;
      if (seen[((tot ^ v) << 1) | (cur.w ^ 1)]) continue;
      if (!Check(v) || !Check(u) || !Check(tot ^ v)) continue;
      seen[((tot ^ v) << 1) | (cur.w ^ 1)] = true;
      que.emplace(Node(tot ^ v, cur.w ^ 1, cur.s + 1));
    }
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    memset(atk, 0, sizeof(atk));
    memset(con, 0, sizeof(con));
    boat = 0;
    int a, b, c;
    int u, v;
    cin >> a >> b >> c;
    while (a--) {
      cin >> u >> v;
      atk[u] |= 1 << v;
    }
    while (b--) {
      cin >> u >> v;
      con[v] |= 1 << u;
    }
    while (c--) {
      cin >> u;
      boat |= 1 << u;
    }
    cout << Solution() << '\n';
  }
  return 0;
}
