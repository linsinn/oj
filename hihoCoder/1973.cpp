#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
string B[MAXN];

void Solution() {
  map<int, Vec> row, col;
  fora (i, 0, N) {
    fora (j, 0, M) {
      if (B[i][j] == 'B') {
        row[i].emplace_back(j);
        col[j].emplace_back(i);
      }
    }
  }
  fora (i, 0, N) {
    fora (j, 0, M) {
      if (B[i][j] == 'B') continue;
      int k = 0;
      auto it = row.find(i);
      if (it != row.end()) {
        auto &v = it->second;
        auto p = upper_bound(v.begin(), v.end(), j);
        if (p == v.end() || p == v.begin()) ++k;
        else k += 2;
      }
      it = col.find(j);
      if (it != col.end()) {
        auto &v = it->second;
        auto p = upper_bound(v.begin(), v.end(), i);
        if (p == v.end() || p == v.begin()) ++k;
        else k += 2;
      }
      B[i][j] = '0' + k;
    }
  }
  fora (i, 0, N)
    cout << B[i] << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) cin >> B[i];
    Solution();
  }
  return 0;
}
