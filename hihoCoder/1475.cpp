//
// Created by sinn on 12/16/18.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN], s[MAXN];
ll f[MAXN];
unordered_map<int, ll> h;

int Solution() {
    ll pre = 0;
    f[0] = s[0] = 0;
    for (int i = 1; i <= N; i++) {
        s[i] = s[i-1] + A[i];
        f[i] = (pre + (s[i] == 0 ? 0 : 1)) % MOD;
        f[i] = (f[i] - h[s[i]] + MOD) % MOD;
        h[s[i]] = (h[s[i]] + f[i]) % MOD;
        pre = (pre + f[i]) % MOD;
    }
    return static_cast<int>(f[N]);
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 1; i <= N; ++i) {
            cin >> A[i];
        }
        cout << Solution() << '\n';
    }
    return 0;
}