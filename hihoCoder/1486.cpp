//
// Created by sinn on 3/9/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

struct Item {
    int val = 0;
    int attrs = 0;
} items[1002];

int f[1100][1100];

int Solution() {
    memset(f, -1, sizeof(f));
    f[0][items[0].attrs] = items[0].val;
    for (int i = 1; i < N; i++) {
        memcpy(f[i], f[i-1], sizeof(f[i]));
        f[i][items[i].attrs] = max(f[i][items[i].attrs], items[i].val);
        for (int j = 0; j < (1 << M); j++) {
            if (f[i-1][j ^ items[i].attrs] != -1) {
                f[i][j] = max(f[i][j], f[i-1][j ^ items[i].attrs] + items[i].val);
            }
        }
    }
    return max(0, f[N-1][(1<<M)-1]);
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int T, a, b;
    cin >> T;
    while (T--) {
        memset(items, 0, sizeof(items));
        cin >> N >> M;
        for (int i = 0; i < N; i++) {
            cin >> items[i].val;
            cin >> a;
            while (a--) {
                cin >> b;
                items[i].attrs ^= (1 << (b-1));
            }
        }
        cout << Solution() << '\n';
    }
    return 0;
}