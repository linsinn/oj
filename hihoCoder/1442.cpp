#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int x, y;
map<int, set<int>> lines;

void Solution() {
    N = lines.size();
    ll ans = INT64_MAX;
    for (auto i = lines.begin(); i != lines.end(); i++) {
        for (auto j = next(i, 1); j != lines.end(); j++) {
            ll width = j->first - i->first;
            vector<int> v;
            std::set_intersection(i->second.begin(), i->second.end(), j->second.begin(), j->second.end(), std::back_inserter(v));
            for (int k = 0; k + 1 < v.size(); k++) {
                ans = min(ans, width * (v[k+1] - v[k]));
            }
        }
    }
    if (ans == INT64_MAX)
        ans = -1;
    cout << ans << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        int x, y;
        for (int i = 0; i < N; i++) {
            cin >> x >> y;
            lines[x].emplace(y);
        }
        Solution();
    }
    return 0;
}
