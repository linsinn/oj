#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A, B, C;


ll Solution() {
  sort(A.begin(), A.end());
  sort(B.begin(), B.end());
  sort(C.begin(), C.end());
  ll ans = INT64_MAX;
  for (int a : A) {
    auto p = upper_bound(B.begin(), B.end(), a);
    if (p != B.begin()) {
      auto g = prev(p);
      ll cur = abs(*g - a);
      auto t = upper_bound(C.begin(), C.end(), *g);
      if (t != C.begin()) {
        ans = min(ans, cur + abs(*prev(t) - *g) + abs(*prev(t) - a));
      }
      if (t != C.end()) {
        ans = min(ans, cur + abs(*t - *g) + abs(*t - a));
      }
    }
    if (p != B.end()) {
      auto g = p;
      ll cur = abs(*g - a);
      auto t = upper_bound(C.begin(), C.end(), *g);
      if (t != C.begin()) {
        ans = min(ans, cur + abs(*prev(t) - *g) + abs(*prev(t) - a));
      }
      if (t != C.end()) {
        ans = min(ans, cur + abs(*t - *g) + abs(*t - a));
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> K) {
    A.resize(N), B.resize(M), C.resize(K);
    fora (i, 0, N) cin >> A[i];
    fora (i, 0, M) cin >> B[i];
    fora (i, 0, K) cin >> C[i];
    cout << Solution() << '\n';
  }
  return 0;
}
