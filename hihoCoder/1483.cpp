//
// Created by Sinn on 2/18/2019.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M;
ll K;
int A[MAXN], tmp[MAXN];

bool Check(ll x) {
    memset(tmp, 0, sizeof(tmp));
    ll cur = 0, rk = 0;
    int l = 0;
    for (int i = 0; i < N; i++) {
        cur += tmp[A[i]];
        tmp[A[i]]++;
        while (cur > x)
            cur -= --tmp[A[l++]];
        rk += i - l + 1;
    }
    return rk >= K;
}

ll Solution() {
    int tot = 0;
    map<int, int> mps;
    for (int i = 0; i < N; i++) {
        if (mps.count(A[i]) == 0)
            mps[A[i]] = tot++;
        A[i] = mps[A[i]];
    }
    ll l = 0, r = 1LL * N * (N-1) / 2;
    while (l < r) {
        ll mid = (l + r) / 2;
        if (Check(mid))
            r = mid;
        else
            l = mid + 1;
    }
    return l;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int T;
    cin >> T;
    while (T--) {
        cin >> N >> K;
        for (int i = 0; i < N; i++)
            cin >> A[i];
        cout << Solution() << '\n';
    }
    return 0;
}