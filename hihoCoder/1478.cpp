#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

constexpr int dirs[] = {-1, 0, 1, 0, -1};

using ll = long long;

int N, M, K;
string board[MAXN];
int res[1000][1000];

void Solution() {
	memset(res, 0, sizeof res);

	std::queue<pair<int, int>> que;

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			if (board[i][j] == '0') {
				que.emplace(std::make_pair(i, j));
			}
		}
	}

	while (!que.empty()) {
		auto p = que.front();
		que.pop();
		for (int i = 0; i < 4; i++) {
			int x = p.first + dirs[i];
			int y = p.second + dirs[i+1];
			if (0 <= x && x < N && 0 <= y && y < M && board[x][y] == '1' && res[x][y] == 0) {
				res[x][y] = res[p.first][p.second] + 1;
				que.emplace(make_pair(x, y));
			}
		}
	}

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			cout << res[i][j] << " \n"[j==M-1];
		}
	}
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N >> M) {
		for (int i = 0; i < N; ++i) {
			cin >> board[i];
		}
		Solution();
    }
    return 0;
}
