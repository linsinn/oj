#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
string w[MAXN];
Vec A;

ll Solution() {
  vector<vector<bool>> memo(N, vector<bool>(M, false));
  fora (i, 0, M) {
    memo[0][i] = (w[0][i] == 'O');
  }
  fora (i, 1, N) {
    fora (j, i, M) {
      if (w[i][j] == 'O') {
        if (memo[i-1][j-1] || memo[i][j-1]) 
          memo[i][j] = true;
      }
    }
  }
  fora (i, 0, M) {
    if (memo[N-1][i])
      return i+1;
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) {
      cin >> w[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
