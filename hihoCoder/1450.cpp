#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
pair<int, int> p, tri[5];

ll cross(int a, int b, int x, int y) {
    return 1LL * a * y - 1LL * b * x;
}

bool Solution() {
    sort(tri, tri + 3);
    ll sign[3];
    for (int i = 0; i < 3; i++) {
        int j = (i + 1) % 3;
        sign[i] = cross(tri[j].first - tri[i].first, tri[j].second - tri[i].second, 
                p.first - tri[i].first, p.second - tri[i].second);
    }
    if (sign[0] >= 0 && sign[1] >= 0 && sign[2] >= 0)
        return true;
    if (sign[0] <= 0 && sign[1] <= 0 && sign[2] <= 0)
        return true;
    return false;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int T;
    while (cin >> T) {
        while (T--) {
            cin >> p.first >> p.second;
            for (int i = 0; i < 3; i++)
                cin >> tri[i].first >> tri[i].second;
            if (Solution())
                cout << "YES\n";
            else
                cout << "NO\n";
        }
    }
    return 0;
}
