#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

constexpr int dirs[] = {-1, 0, 1, 0, -1};

using ll = long long;

int N, M, K;
string Mat[505];
int dp[505][505][4];

int dfs(int x, int y, int dir) {
    if (x < 0 || y < 0 || x >= N || y >= N || Mat[x][y] == '0')
        return 0;
    if (dp[x][y][dir] != -1)
        return dp[x][y][dir];
    int k = dfs(x + dirs[dir], y + dirs[dir+1], dir);
    dp[x][y][dir] = k + 1;
    return dp[x][y][dir];
}

void Solution() {
    memset(dp, -1, sizeof dp);
    int ans = 0;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (Mat[i][j] == '1') {
                for (int d = 0; d < 4; d++)
                    dfs(i, j, d);
                ans = max(ans, *std::min_element(dp[i][j], dp[i][j]+4) - 1);
            }
        }
    }
    cout << ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        for (int i = 0; i < N; i++)
            cin >> Mat[i];
        Solution();
    }
    return 0;
}
