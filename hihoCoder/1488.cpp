//
// Created by sinn on 3/17/19.
//

#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;
using namespace __gnu_pbds;

typedef
tree<
        pair<int, int>,
        null_type,
        less<pair<int, int>>,
        rb_tree_tag,
        tree_order_statistics_node_update>
        set_t;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e4 + 5;

using ll = long long;

int N, M, K, Len;
pair<int, int> A[MAXN], B[MAXN];
set_t ss;
ll ordered_array[MAXN];
ll BITSum[MAXN], ans[MAXN];

ll GetBITSum(int idx, ll arr[]) {
    ll ret = 0;
    idx += 1;
    while (idx > 0) {
        ret += arr[idx];
        idx -= (idx & -idx);
    }
    return ret;
}

void UpdateSum(int idx, int val, ll arr[]) {
    idx += 1;
    while (idx <= N) {
        arr[idx] += val;
        idx += (idx & -idx);
    }
}

struct Query {
    int idx;
    int l, r;
    bool operator < (const Query& rhs) const {
        int a = l / Len, b = rhs.l / Len;
        return a < b || (a == b && r < rhs.r);
    }
} qrys[MAXN];

void Insert(int idx, ll& ans) {
    K++;
    UpdateSum(A[idx].second, A[idx].first, BITSum);
    UpdateSum(A[idx].second, 1, ordered_array);
    auto pos = GetBITSum(A[idx].second, ordered_array);
    ans += GetBITSum(A[idx].second, BITSum);
    ans += (K - pos) * A[idx].first;
}

void Delete(int idx, ll& ans) {
    auto pos = GetBITSum(A[idx].second, ordered_array);
    ans -= GetBITSum(A[idx].second, BITSum);
    ans -= (K - pos) * A[idx].first;
    UpdateSum(A[idx].second, -A[idx].first, BITSum);
    UpdateSum(A[idx].second, -1, ordered_array);
    K--;
}

void Solution() {
    memset(BITSum, 0, sizeof BITSum);
    memset(ordered_array, 0, sizeof ordered_array);
    memset(ans, 0, sizeof ans);
    K = 0;

    Len = static_cast<int>(sqrt(N));
    sort(qrys, qrys + M);
    sort(B, B + N);
    for (int i = 0; i < N; i++)
        A[B[i].second].second = i;

    ll cur = 0;
    for (int i = qrys[0].l; i <= qrys[0].r; i++)
        Insert(i, cur);
    int lb = qrys[0].l, rb = qrys[0].r;
    ans[qrys[0].idx] = cur;

    for (int i = 1; i < M; i++) {
        while (rb < qrys[i].r)
            Insert(++rb, cur);
        while (lb < qrys[i].l)
            Delete(lb++, cur);
        while (lb > qrys[i].l)
            Insert(--lb, cur);
        while (rb > qrys[i].r)
            Delete(rb--, cur);
        ans[qrys[i].idx] = cur;
    }
    for (int i = 0; i < M; i++)
        cout << ans[i] << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int T;
    cin >> T;
    while (T--) {
        cin >> N >> M;
        for (int i = 0; i < N; i++) {
            cin >> A[i].first;
            B[i].first = A[i].first;
            B[i].second = i;
        }
        for (int i = 0; i < M; i++) {
            cin >> qrys[i].l >> qrys[i].r;
            qrys[i].l--;
            qrys[i].r--;
            qrys[i].idx = i;
        }
        Solution();
    }
}

