#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string S;
vector<vector<bool>> is_repeated;
vector<vector<int>> dp;

void Init() {
  is_repeated.assign(N+1, vector<bool>(N+1, false));
  Vec nxt(N+1, 0);
  fora (i, 0, N-1) {
    nxt.assign(N+1, 0);
    fora (j, i+1, N) {
      int k = nxt[j-1];
      while (k > 0 && S[j] != S[i+k])
        k = nxt[i + k - 1];
      nxt[j] = k + (S[j] == S[i+k]);
    }
    fora (j, i+1, N) {
      int l = j - i + 1;
      if (nxt[j] != 0 && l % (l - nxt[j]) == 0) {
        is_repeated[i][j] = true;
      }
    }
  }
}

int dfs(int st, int ed) {
  if (dp[st][ed] != -1)
    return dp[st][ed];
  int &x = dp[st][ed];
  x = is_repeated[st][ed] ? 1 : 0;
  fora (i, st + 1, ed - 1) {
    if (is_repeated[st][i]) {
      int ret = dfs(i+1, ed);
      x += ret;
      x %= MOD;
    }
  }
  return x;
}

ll Solution() {
  Init();
  dp.assign(N+1, Vec(N+1, -1));
  return dfs(0, N-1);
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      cin >> S;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
