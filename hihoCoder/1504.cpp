//
// Created by sinn on 7/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
int r, c;
using Vec = vector<ll>;
using Mat = vector<Vec>;
constexpr int dirs[][2] = {
        {-2, -1}, {-2, 1},
        {-1, -2}, {-1, 2},
        {1, -2}, {1, 2},
        {2, -1}, {2, 1}
};

void Mul(Mat& a, Mat& b) {
  int n = a.size(), m = a[0].size();
  Mat ret(n, Vec(m, 0));
  fora (i, 0, n) {
    fora (j, 0, m) {
      fora (k, 0, m) {
        ret[i][j] = ret[i][j] + a[i][k] * b[k][j] % MOD;
        ret[i][j] %= MOD;
      }
    }
  }
  a = ret;
}

void FastPow(Mat& a, ll exp) {
  Mat ret(64, Vec(64, 0));
  fora (i, 0, 64) ret[i][i] = 1;
  while (exp) {
    if (exp & 1) {
      Mul(ret, a);
    }
    Mul(a, a);
    exp >>= 1;
  }
  a = ret;
}

ll Solution() {
  Mat tran(64, Vec(64, 0));
  fora (i, 0, 8) {
    fora (j, 0, 8) {
      fora (d, 0, 8) {
        int ni = i + dirs[d][0], nj = j + dirs[d][1];
        if (0 <= ni && ni < 8 && 0 <= nj && nj < 8) {
          tran[ni * 8 + nj][i * 8 + j] = 1;
        }
      }
    }
  }
  --r, --c;
  Mat init(1, Vec(64, 0));
  init[0][r * 8 + c] = 1;
  FastPow(tran, N);
  Mul(init, tran);
  ll ans = 0;
  fora (i, 0, 64) {
    ans = ans + init[0][i];
    ans %= MOD;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> r >> c;
    cout << Solution() << '\n';
  }
  return 0;
}
