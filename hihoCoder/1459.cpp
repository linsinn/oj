//
// Created by Sinn on 11/18/2018.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string str;

int Solution() {
    int tot[30], cur[30];
    memset(tot, 0, sizeof tot);
    memset(cur, 0, sizeof cur);
    for (char ch : str)
        tot[ch-'a']++;
    int i = 0, j = 0;
    int ans = 0;
    bool can = true;
    while (j < str.size()) {
        if (can)
            cur[str[j]-'a']++;
        can = false;
        for (int c = 0; c < 26; c++) {
            if (j-i+1 <= tot[c] && j-i+1 - cur[c] <= K) {
                ans = max(ans, j-i+1);
                can = true;
                break;
            }
        }
        if (!can) {
            cur[str[i]-'a']--;
            i++;
        } else {
            j++;
        }
    }
    return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> K) {
        cin >> str;
        cout << Solution() << '\n';
    }
    return 0;
}