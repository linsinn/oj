//
// Created by sinn on 4/28/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
vector<int> bricks[MAXN];

int Solution() {
    map<int, int> mps;
    for (int i = 0; i < N; i++) {
        int su = 0;
        for (int j = 0; j < bricks[i].size() - 1; j++) {
            su += bricks[i][j];
            mps[su] += 1;
        }
    }
    int ans = 0;
    for (auto p : mps) {
        ans = max(ans, p.second);
    }
    return N - ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        int m;
        for (int i = 0; i < N; i++) {
            cin >> m;
            bricks[i].resize(m);
            for (int j = 0; j < m; j++)
                cin >> bricks[i][j];
        }
        cout << Solution() << '\n';
    }
    return 0;
}