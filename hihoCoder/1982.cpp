#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
string num;

void Solution() {
  Vec digs(10, -1);
  fora (i, 0, num.size()) {
    int k = num[i] - '0';
    digs[k] = i;
  }
  bool chged = false;
  fora (i, 0, num.size()) {
    int k = num[i] - '0';
    ford (j, 9, k+1) {
      if (digs[j] != -1 && digs[j] > i) {
        swap(num[i], num[digs[j]]);
        chged = true;
        break;
      }
    }
    if (chged) break;
  }
  if (chged) cout << num << '\n';
  else {
    fora (i, 0, num.size()) {
      int k = num[i] - '0';
      if (i != digs[k]) {
        chged = true;
        break;
      }
    }
    if (chged) cout << num << '\n';
    else {
      swap(num[num.size()-2], num[num.size()-1]);
      cout << num << '\n';
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> num) {
    Solution();
  }
  return 0;
}
