#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int diff[MAXN], grp[MAXN];
int X, Y;

void Init() {
  memset(diff, 0, sizeof(diff));
  memset(grp, 0, sizeof(grp));
}

int FindPa(int x) {
  if (grp[x] != 0) {
    int p = FindPa(grp[x]);
    diff[x] += diff[grp[x]];
    grp[x] = p;
    return p;
  } else {
    return x;
  }
}

void Join(int u, int v, int w) {
  int x = FindPa(u);
  int y = FindPa(v);
  if (x == y)
    return ;
  int a = diff[u], b = diff[v];
  grp[x] = y;
  diff[x] = b - a + w;
}

ll Solution() {
  int g0 = FindPa(X), g1 = FindPa(Y);
  if (g0 != g1)
    return -1;
  return diff[X] - diff[Y];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M >> T) {
    Init();
    int u, v, w;
    while (M--) {
      cin >> u >> v >> w;
      Join(u, v, w);
    }
    while (T--) {
      cin >> X >> Y;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
