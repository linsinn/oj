//
// Created by sinn on 5/12/19.
//


#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

ll Solution() {
  ll ans = 0;
  int maxi[2];
  for (int i = 0; i <= 20; i++) {
    maxi[0] = maxi[1] = 0;
    for (int j = 0; j < N; j++) {
      if ((A[j] & (1 << i)) != 0) {
        maxi[0] = min(maxi[1], max(maxi[0], A[j]));
        maxi[1] = max(maxi[1], A[j]);
      }
    }
    ans = max(ans, 1LL * maxi[0] * maxi[1] * (maxi[0] & maxi[1]));
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N;
    for (int i = 0; i < N; i++) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
