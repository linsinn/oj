#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

int FasePow(ll base, ll exp) {
	ll ret = 1;
	while (exp) {
		if (exp & 1) {
			(ret *= base) %= MOD;
		}
		(base *= base) %= MOD;
		exp >>= 1;
	}
	return ret;
}

int Solution() {
	int dp[21];
	memset(dp, 0, sizeof dp);
	for (int i = 1; i <= 20; i++) {
		dp[i] = dp[i-1] + 1;
		for (int k = 1; k <= i - 2; k++) {
			dp[i] = max(dp[i], dp[i-2-k] * (k + 1));
		}
	}
	if (N <= 20)
		return dp[N];
	for (int x = 11; x <= 15; x++) {
		if ((N - x) % 5 == 0)
			return 1LL * dp[x] * FasePow(4, (N-x) / 5) % MOD;
	}
	return -1;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
		cout << Solution() << '\n';
    }
    return 0;
}
