//
// Created by sinn on 5/19/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int x[MAXN], y[MAXN];

ll Solution() {
  map<int, int> hori, veri, l45, r45;
  for (int i = 0; i < N; i++) {
    hori[y[i]] += 1;
    veri[x[i]] += 1;
    l45[y[i]-x[i]] += 1;
    r45[y[i]+x[i]] += 1;
  }
  ll ans = 0;
  for (auto& p : hori)
    ans += 1LL * p.second * (p.second - 1) / 2;
  for (auto& p : veri)
    ans += 1LL * p.second * (p.second - 1) / 2;
  for (auto& p : l45)
    ans += 1LL * p.second * (p.second - 1) / 2;
  for (auto& p : r45)
    ans += 1LL * p.second * (p.second - 1) / 2;

  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    for (int i = 0; i < N; i++) {
      cin >> x[i] >> y[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}