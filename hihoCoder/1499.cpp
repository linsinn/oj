//
// Created by sinn on 6/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

int N, M, K;
ll A[2][MAXN];

ll Solution() {
  ll sum = 0;
  fora (i, 0, 2) {
    fora (j, 0, N) {
      sum += A[i][j];
    }
  }
  ll avg = sum / 2 / N;
  unsigned long long ans = 0;

  fora (i, 0, N) {
    ll& a1 = A[0][i], &b1 = A[1][i];
    if (a1 > avg && b1 < avg) {
      ll t = min(a1-avg, avg-b1);
      a1 -= t;
      b1 += t;
      ans += t;
    }
    if (a1 < avg && b1 > avg) {
      ll t = min(b1-avg, avg-a1);
      a1 += t;
      b1 -= t;
      ans += t;
    }
    if (a1 < avg) {
      A[0][i+1] -= avg - a1;
      ans += avg - a1;
    }
    if (a1 > avg) {
      A[0][i+1] += a1 - avg;
      ans += a1 - avg;
    }
    if (b1 < avg) {
      A[1][i+1] -= avg - b1;
      ans += avg - b1;
    }
    if (b1 > avg) {
      A[1][i+1] += b1 - avg;
      ans += b1 - avg;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      fora (j, 0, 2) {
        cin >> A[j][i];
      }
    }
    cout << Solution() << '\n';
  }
  return 0;
}