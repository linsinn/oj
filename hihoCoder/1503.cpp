//
// Created by sinn on 6/30/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K, T;
vector<string> A;
vector<string> B;

bool Helper1(map<string, int> &mp, int x, multiset<string>& ms) {
  if (x == 2) {
    for (auto &p : mp) {
      if (p.second >= 2) {
        ms.emplace(p.first);
        ms.emplace(p.first);
        set<char> ss;
        for (auto &q : ms) {
          ss.emplace(q[0]);
        }
        if (ss.size() <= 2)
          return true;
        ms.erase(ms.find(p.first));
        ms.erase(ms.find(p.first));
      }
    }
    return false;
  }
  for (auto &p : mp) {
    if (p.second >= 3) {
      p.second -= 3;
      fora (i, 0, 3) ms.emplace(p.first);
      if (Helper1(mp, x - 3, ms))
        return true;
      fora (i, 0, 3) ms.erase(ms.find(p.first));
      p.second += 3;
    }
  }
  for (auto it = mp.begin(); it != mp.end(); ++it) {
    decltype(it) c, d;
    if (next(it) != mp.end()) c = next(it);
    else break;
    if (next(c) != mp.end()) d = next(c);
    else break;
    if (it->second > 0 && c->second > 0 && d->second > 0) {
      string i = it->first, j = c->first, k = d->first;
      if (i[0] == j[0] && j[0] == k[0] && j[1] - i[1] == 1 && k[1] - j[1] == 1) {
        mp[i]--;
        mp[j]--;
        mp[k]--;
        ms.emplace(i);
        ms.emplace(j);
        ms.emplace(k);
        if (Helper1(mp, x -3, ms))
          return true;
        ms.erase(ms.find(i));
        ms.erase(ms.find(j));
        ms.erase(ms.find(k));
        mp[i]++;
        mp[j]++;
        mp[k]++;
      }
    }
  }
  return false;
}

bool Helper2(map<string, int> &mp, int x, multiset<string> &ms) {
  if (x == 0) {
    set<char> ss;
    for (auto &q : ms) {
      ss.emplace(q[0]);
    }
    return ss.size() <= 2;
  }
  for (auto &p : mp) {
    if (p.second >= 2) {
      mp[p.first] -= 2;
      fora (i, 0, 2) ms.emplace(p.first);
      if (Helper2(mp, x - 2, ms))
        return true;
      fora (i, 0, 2) ms.erase(ms.find(p.first));
      mp[p.first] += 2;
    }
  }
  return false;
}

bool Ok(map<string, int> &mp) {
  multiset<string> ms;
  if (Helper1(mp, 14, ms))
    return true;
  ms.clear();
  return Helper2(mp, 14, ms);
}

ll Solution() {
  map<string, int> mp;
  fora (i, 0, A.size()) {
    mp[A[i]]++;
  }
  int ans = 0;
  if (Ok(mp)) return ans;
  fora (i, 0, B.size()) {
    mp[B[ans++]]++;
    if (Ok(mp))
      return ans;
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    string s;
    fora (i, 0, 14) {
      cin >> s;
      A.emplace_back(s);
    }
    fora (i, 0, N) {
      cin >> s;
      B.emplace_back(s);
    }
    cout << Solution() << '\n';
  }
  return 0;
}
