//
// Created by Sinn on 11/10/2018.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string str;

void Solution() {
    stack<int> st;
    vector<pair<int, int>> ans;
    for (int i = 0; i < str.size(); i++) {
        if (str[i] == '(') {
            st.emplace(i);
        } else {
            ans.emplace_back(make_pair(st.top()+1, i+1));
            st.pop();
        }
    }
    sort(ans.begin(), ans.end());
    for (auto p : ans)
        cout << p.first << ' ' << p.second << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> str) {
        Solution();
    }
    return 0;
}
