#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    string op;
    ll id, amount;
    map<ll, ll> order;
    ll ans[3] = {0, 0, 0};
    while (N--) {
      cin >> op;
      if (op == "CREATE") {
        cin >> id >> amount;
        order[id] = amount;
      } else if (op == "PAY") {
        cin >> id;
        ans[1] += order[id];
        order.erase(id);
      } else {
        cin >> id;
        ans[2] += order[id];
        order.erase(id);
      }
    }
    for (auto &p : order) {
      ans[0] += p.second;
    }
    cout << ans[1] << ' ' << ans[2] << ' ' << ans[0] << '\n';
  }
  return 0;
}
