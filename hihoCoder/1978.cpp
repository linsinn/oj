#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;
constexpr double PI = M_PI;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T &&b) {if (b < a) a = b;}
template <class T> void Max(T &a, T &&b) {if (b > a) a = b;}

ll N, M, K, T;
string s;
Vec A;

void Solution() {
  map<string, string> mp;
  mp["101"] = "A";
  mp["11"] = "B";
  mp["0"] = "C";
  mp["100"] = "D";
  string cur = "";
  string ans;
  for (char ch : s) {
    cur += ch;
    auto it = mp.find(cur);
    if (it != mp.end()) {
      ans += it->second;
      cur = "";
    }
  }
  cout << ans << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    Solution();
  }
  return 0;
}
