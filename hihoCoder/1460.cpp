//
// Created by Sinn on 11/25/2018.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string S;
int mini[20][MAXN];

inline int Min(int a, int b) {
    if (S[a] < S[b])
        return a;
    else if (S[a] == S[b])
        return (a < b) ? a : b;
    else
        return b;
}

void Build() {
    for (int j = 0; j < S.size(); j++)
        mini[0][j] = j;
    for (int i = 1; (1 << i) <= S.size(); i++) {
        for (int j = 0; j + (1 << (i-1)) < S.size(); j++) {
            mini[i][j] = Min(mini[i-1][j], mini[i-1][j + (1 << (i-1))]);
        }
    }
}

int Search(int l, int r) {
    int k = 0;
    while ((1 << (k + 1)) <= (r - l + 1))
        k++;
    return Min(mini[k][l], mini[k][r + 1 - (1<<k)]);
}

void dfs(int l, int r, set<int> &ans) {
    if (ans.size() == K || l > r)
        return;
    int k = Search(l, r);
    ans.emplace(k);
    dfs(k+1, r, ans);
    dfs(l, k-1, ans);
}

string Solution() {
    Build();
    set<int> ans;
    dfs(0, static_cast<int>(S.size() - 1), ans);
    string ret;
    for (int idx : ans)
        ret += S[idx];
    return ret;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> K) {
        cin >> S;
        cout << Solution() << '\n';
    }
    return 0;
}



//#include <bits/stdc++.h>
//using namespace std;
//
//constexpr int MOD = 1e9 + 7;
//constexpr int MAXN = 2e5 + 1;
//
//using ll = long long;
//
//int N, M, K;
//string str;
//vector<int> pos[30];
//
//bool check(vector<int> &cur, set<int>& ans) {
//    int k = 0;
//    while (ans.size() != K && k != cur.size()) {
//        for (int i = 0; i < 26 && ans.size() != K; i++) {
//            auto it = pos[i].rbegin();
//            while (it != pos[i].rend() && *it > cur[k] && ans.size() != K) {
//                ans.emplace(*it);
//                it++;
//                pos[i].pop_back();
//            }
//        }
//        k++;
//    }
//    return ans.size() == K;
//}
//
//string Solution() {
//    for (int i = 0; i < 26; i++)
//        pos[i].clear();
//    for (int i = 0; i < str.size(); i++)
//        pos[str[i] - 'a'].emplace_back(i);
//    set<int> ans;
//    for (int i = 0; i < 26; i++) {
//        vector<int> cur;
//        auto it = pos[i].rbegin();
//        while (it != pos[i].rend() && ans.size() != K) {
//            cur.emplace_back(*it);
//            ans.emplace(*it);
//            it++;
//            pos[i].pop_back();
//        }
//        if (check(cur, ans))
//            break;
//    }
//    string ret;
//    for (int idx : ans)
//        ret += str[idx];
//    return ret;
//}
//
//int main() {
//    freopen("in.txt", "r", stdin);
//    ios::sync_with_stdio(false);
//    cin.tie(nullptr), cout.tie(nullptr);
//    while (cin >> K) {
//        cin >> str;
//        cout << Solution() << '\n';
//    }
//    return 0;
//}
