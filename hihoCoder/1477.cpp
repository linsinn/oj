//
// Created by Sinn on 2018-12-31.
//

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;
vector<int> tbl[3000];
constexpr int days_of_mouth[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int N, M, K;

struct dt {
    int year, mouth, day;
    int hour, minute, second;
}a, b;

inline bool isLeapYear(int y) {
    return y % 4 == 0 && (y % 100 != 0 || y % 400 == 0);
}

int GetShift(const dt& d) {
    int ret = 0;
    for (int y = 1970; y < d.year; y++) {
        ret += 365 * 24 * 3600;
        if (isLeapYear(y))
            ret += 24 * 3600;
        ret += tbl[y].size();
    }
    for (int m = 1; m < d.mouth; m++) {
        ret += days_of_mouth[m] * 24 * 3600;
        if (isLeapYear(d.year) && m == 2)
            ret += 24 * 3600;
        if (find(tbl[d.year].begin(), tbl[d.year].end(), m) != tbl[d.year].end()) {
            ret += 1;
        }
    }
    ret += (d.day - 1) * 24 * 3600;
    ret += d.hour * 3600 + d.minute * 60 + d.second;
    return ret;
}

void Init() {
    tbl[1972] = {6, 12};
    tbl[1973] = {12};
    tbl[1974] = {12};
    tbl[1975] = {12};
    tbl[1976] = {12};
    tbl[1977] = {12};
    tbl[1978] = {12};
    tbl[1979] = {12};
    tbl[1981] = {6};
    tbl[1982] = {6};
    tbl[1983] = {6};
    tbl[1985] = {6};
    tbl[1987] = {12};
    tbl[1989] = {12};
    tbl[1990] = {12};
    tbl[1992] = {6};
    tbl[1993] = {6};
    tbl[1994] = {6};
    tbl[1995] = {12};
    tbl[1997] = {6};
    tbl[1998] = {12};
    tbl[2005] = {12};
    tbl[2008] = {12};
    tbl[2012] = {6};
    tbl[2015] = {6};
    tbl[2016] = {12};
}

int Solution() {
    Init();
    int f = GetShift(a);
    int s = GetShift(b);
    return s - f;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (~scanf("%d-%d-%d %d:%d:%d", &a.year, &a.mouth, &a.day, &a.hour, &a.minute, &a.second)) {
        scanf("%d-%d-%d %d:%d:%d", &b.year, &b.mouth, &b.day, &b.hour, &b.minute, &b.second);
        cout << Solution() << '\n';
    }
    return 0;
}