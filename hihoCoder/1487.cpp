//
// Created by sinn on 3/9/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1001;
constexpr int dirs[] = {-1, 0, 1, 0, -1};

using ll = long long;


int N, M, K;
int x, y;
int num, area, len;

unordered_map<int, int> pa;

int find_pa(int x) {
    if (pa[x] == x)
        return x;
    pa[x] = find_pa(pa[x]);
    return pa[x];
}

void Solution() {
    if (pa.count(x * MAXN + y) != 0) {
        cout << num << ' ' << area << ' ' << len << '\n';
        return ;
    }
    set<int> neighbors;
    int cnt = 0;
    for (int d = 0; d < 4; d++) {
        int nx = x + dirs[d], ny = y + dirs[d+1];
        int idx = nx * MAXN + ny;
        if (pa.count(idx) != 0) {
            neighbors.emplace(find_pa(idx));
            cnt += 1;
        }
    }
    for (int n : neighbors) {
        pa[n] = x * MAXN + y;
    }
    pa[x * MAXN + y] = x * MAXN + y;
    num -= neighbors.size() - 1;
    area += 1;
    len += -2 * cnt + 4;
    cout << num << ' ' << area << ' ' << len << '\n';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    num = 0, area = 0, len = 0;
    while (cin >> N) {
        for (int i = 0; i < N; i++) {
            cin >> x >> y;
            Solution();
        }
    }
    return 0;
}