//
// Created by sinn on 12/1/18.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int L, F, D;

int gcd(int a, int b) {
    return b == 0 ? a : gcd(b, a % b);
}

bool Solution() {
    return gcd(D, L) >= F;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int T;
    cin >> T;
    while (T--) {
        cin >> L >> F >> D;
        if (Solution())
            cout << "YES\n";
        else
            cout << "NO\n";
    }
    return 0;
}