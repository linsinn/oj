//
// Created by sinn on 12/8/18.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e3 + 1;
constexpr int dirs[] = {-1, 0, 1, 0, -1};

using ll = long long;

string board[MAXN];
int vis[MAXN][MAXN];

int N, M, K;

struct Part {
    int a, b;
    int c, d;
	int x, y;
	int flag;
    Part() {
		flag = 0;
        a = MAXN;
        b = MAXN;
        c = -MAXN;
        d = -MAXN;
		x = MAXN;
		y = MAXN;
    }
    bool operator < (const Part& oth) const {
        if (y < oth.y) {
            return true;
        } else if (y == oth.y) {
            return x < oth.x;
        } else {
            return false;
        }
    }
};

void dfs(int i, int j, Part& p, int cnt) {
    vis[i][j] = cnt;
    p.a = min(p.a, i);
    p.b = min(p.b, j);
    p.c = max(p.c, i);
    p.d = max(p.d, j);
	if (j < p.y) {
		p.y = j;
		p.x = i;
	} else if (j == p.y) {
		p.x = min(i, p.x);
	}
    for (int d = 0; d < 4; d++) {
        int ni = i + dirs[d], nj = j + dirs[d+1];
        if (ni >= 0 && ni < N && nj >= 0 && nj < M && !vis[ni][nj] && board[ni][nj] == '1') {
            dfs(ni, nj, p, cnt);
        }
    }
}


void Solution() {
    memset(vis, 0, sizeof vis);
    vector<Part> ps;
	int cnt = 1;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            if (!vis[i][j] && board[i][j] == '1') {
                Part p;
				p.flag = cnt;
                dfs(i, j, p, cnt++);
                ps.push_back(p);
            }
        }
    }
    sort(ps.begin(), ps.end());
	cnt = 1;
    for (Part& p : ps) {
		int f = p.flag;
        int n = p.c - p.a + 1, m = p.d - p.b + 1;
        cout << n << ' ' << m << '\n';
        for (int i = p.a; i <= p.c; i++) {
            for (int j = p.b; j <= p.d; j++) {
				if (vis[i][j] == f)
					cout << board[i][j];
				else
					cout << '0';
            }
            cout << '\n';
			cnt++;
        }
    }
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N >> M) {
        for (int i = 0; i < N; i++)
            cin >> board[i];
        Solution();
    }
    return 0;
}
