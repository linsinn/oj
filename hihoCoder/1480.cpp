//
// Created by sinn on 1/22/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 3e5 + 1;

using ll = long long;

int N, M, K;

ll fac[MAXN], rfac[MAXN];

template <ll base, ll exp>
constexpr ll FastPow() {
    ll ret = 1;
    ll b = base, e = exp;
    while (e) {
        if (e & 1) {
            (ret *= b) %= MOD;
        }
        (b *= b) %= MOD;
        e >>= 1;
    }
    return ret;
}

ll fast_pow(ll base, ll exp) {
    ll ret = 1;
    while (exp) {
        if (exp & 1) {
            (ret *= base) %= MOD;
        }
        (base *= base) %= MOD;
        exp >>= 1;
    }
    return ret;
}

void Init() {
    fac[0] = fac[1] = 1;
    for (int i = 2; i < MAXN; i++)
        fac[i] = fac[i-1] * i % MOD;
    rfac[MAXN-1] = FastPow<2, 2>();
    for (int i = MAXN-2; i >= 0; i--)
        rfac[i] = rfac[i+1] * (i+1) % MOD;
}

ll Solution() {
    ll ans = fac[N * M];
    for (int i = 0; i < N; i++) {
        (ans *= rfac[M + N - i - 1]) %= MOD;
        (ans *= fac[N-i-1]) %= MOD;
    }
    return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    return 0;
}

