//
// Created by sinn on 3/23/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e6 + 1;

using ll = long long;

int N, M, K, P, Q;

double f[101];

double Solution() {
    memset(f, 0, sizeof(f));
    f[100] = 1;
    for (int p = 99; p >= 0; p--) {
        int lose = min(100, p + Q);
        f[p] = 1.0 * p / 100 + (f[lose] + 1) * (100 - p) / 100;
    }
    double ans = 0;
    for (int i = 1; i <= min(9, N); i++) {
        ans += f[P / (1 << (i - 1))];
    }
    ans += max(0, N-9) * f[0];
    return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> P >> Q >> N) {
        std::cout << std::fixed;
        std::cout << std::setprecision(2);
        std::cout << Solution() << '\n';
    }
    return 0;
}
