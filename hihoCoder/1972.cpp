#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
string c[3];
ll base[30];
int l[3];
ll res[3];

// string target[] = {"YGYRGGGGGYGR", "GRRGRGGG", "RRRY"};
string target[] = {"GGGGGGGGGGGG", "RRRRRRRR", "YYYY"};

struct Node {
  ll c[3];
  int step = 0;

  void Op1() {
    fora (i, 0, 3) {
      int b = c[i] / base[l[i]-1];
      c[i] = c[i] % base[l[i]-1] * 4 + b;
    }
    ++step;
  }
  void Op2() {
    fora (i, 0, 3) {
      int b = c[i] % 4;
      c[i] = base[l[i]-1] * b + c[i] / 4; 
    }
    ++step;
  }
  void Op3() {
    int b[3];
    fora (i, 0, 3) b[i] = c[i] / base[l[i] - 1];
    fora (i, 0, 3) c[i] = c[i] - b[i] * base[l[i] - 1] + b[(i+1) % 3] * base[l[i] - 1];
    ++step;
  }
  void Op4() {
    Op3();
    Op3();
    --step;
  }
  bool operator < (const Node& rhs) const {
    if (c[0] != rhs.c[0]) return c[0] < rhs.c[0];
    if (c[1] != rhs.c[1]) return c[1] < rhs.c[1];
    return c[2] < rhs.c[2];
  }
} st, ed;

ll Solution() {
  fora (i, 0, 3) l[i] = c[i].size();
  base[0] = 1;
  fora (i, 1, 30) base[i] = base[i-1] * 4;
  fora (i, 0, 3) {
    res[i] = 0;
    fora (j, 0, l[i]) {
      if (target[i][j] == 'G')
        res[i] = res[i] * 4 + 1;
      else if (target[i][j] == 'R')
        res[i] = res[i] * 4 + 2;
      else 
        res[i] = res[i] * 4 + 3;
    }
  }
  fora (i, 0, 3) {
    st.c[i] = 0;
    ed.c[i] = res[i];
    fora (j, 0, l[i]) {
      if (c[i][j] == 'G')
        st.c[i] = st.c[i] * 4 + 1;
      else if (c[i][j] == 'R')
        st.c[i] = st.c[i] * 4 + 2;
      else 
        st.c[i] = st.c[i] * 4 + 3;
    }
  }
  queue<Node> que[2];
  set<Node> seen[2];
  que[0].emplace(st);
  que[1].emplace(ed);
  seen[0].emplace(st);
  seen[1].emplace(ed);
  int cur = 0;
  while (!que[0].empty() || !que[1].empty()) {
    if (que[1].empty() || (!que[0].empty() && que[0].front().step < que[1].front().step))
      cur = 0;
    else
      cur = 1;
    auto n = que[cur].front();
    que[cur].pop();
    auto it = seen[cur^1].find(n);
    if (it != seen[cur^1].end()) {
      return n.step + it->step;
    }
    auto p = n;
    p.Op1();
    if (seen[cur].find(p) == seen[cur].end()) {
      que[cur].emplace(p);
      seen[cur].emplace(p);
    }
    p = n;
    p.Op2();
    if (seen[cur].find(p) == seen[cur].end()) {
      que[cur].emplace(p);
      seen[cur].emplace(p);
    }
    p = n;
    if (cur == 0) {
      p.Op3();
      if (seen[cur].find(p) == seen[cur].end()) {
        que[cur].emplace(p);
        seen[cur].emplace(p);
      }
    } else {
      p.Op4();
      if (seen[cur].find(p) == seen[cur].end()) {
        que[cur].emplace(p);
        seen[cur].emplace(p);
      }
    }
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> c[0] >> c[1] >> c[2]) {
    cout << Solution() << '\n';
  }
  return 0;
}
