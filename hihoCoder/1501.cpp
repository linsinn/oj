//
// Created by sinn on 6/16/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K, T;
string cn;

string Solution() {
  string ret;
  fora (i, 0, cn.size()) {
    if (cn[i] == '_') {
      ret += ::toupper(cn[++i]);
    } else if (isupper(cn[i])) {
      ret += '_';
      ret += ::tolower(cn[i]);
    } else {
      ret += cn[i];
    }
  }
  return ret;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    while (N--) {
      cin >> cn;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
