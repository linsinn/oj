//
// Created by sinn on 3/31/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e2 + 1;

using ll = long long;

int N, M, K;
int A[MAXN], L[MAXN], pa[MAXN], fc[MAXN];
vector<int> depth[MAXN];
int dist[MAXN][MAXN];

int Check(int a, int b) {
    int x = 0, y = 0;
    while (fc[a] != a) {
        a = fc[a];
        ++x;
    }
    while (fc[b] != b) {
        b = fc[b];
        ++y;
    }
    return dist[L[a]][L[b]] - x - y;
}

void Solution() {
    if (M == 0) {
        cout << "0\n";
        return ;
    }
    int a = M - 2, b = M - 1;
    while (b > 0) {
        int i = 0, j = 0;
        bool f = true;
        while (j < depth[b].size()) {
            while (L[depth[a][i]] != -1 || (!f && Check(depth[b][j], depth[b][j-1]) > 2)) {
                f = true;
                i++;
            }
            f = false;
            fc[depth[a][i]] = depth[b][j];
            pa[depth[b][j]] = depth[a][i];
            j++;
        }
        a--;
        b--;
    }
    pa[depth[0][0]] = 0;
    for (int i = 1; i <= N; i++)
        cout << pa[i] << ' ';
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    int tmp;
    while (cin >> N >> M >> K) {
        memset(L, -1, sizeof(L));
        for (int i = 0; i < M; i++)
            cin >> A[i];
        int l;
        for (int i = 0; i < M; i++) {
            depth[i].resize(A[i]);
            for (int j = 0; j < A[i]; j++) {
                cin >> depth[i][j];
            }
        }
        for (int i = 0; i < K; i++) {
            cin >> tmp;
            L[tmp] = i;
            fc[tmp] = tmp;
        }
        for (int i = 0; i < K; i++) {
            for (int j = 0; j < K; j++) {
                cin >> dist[i][j];
            }
        }
        Solution();
    }
    return 0;
}
