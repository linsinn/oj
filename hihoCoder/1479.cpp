#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int root_node;

struct Node {
	int tot = 0;
	vector<Node* > children;
} nodes[MAXN];

void GenTot(Node* root) {
	for (Node* child : root->children) {
	    GenTot(child);
	    root->tot += child->tot;
	}
}

int dfs(Node* root, ll& ans) {
	int cnt = 0;
	ll tmp = 0;
	for (Node* child : root->children) {
		int t = dfs(child, ans);
		tmp += 1LL * t * t;
		cnt += t;
	}
	ans += (1LL * cnt * cnt - tmp) / 2;
	if (root->tot == 2 * K && root != &nodes[root_node]) {
	    ans += cnt;
	}
	return cnt + (root->tot == K ? 1 : 0);
}

ll Solution() {
	GenTot(&nodes[root_node]);
	int tot = nodes[root_node].tot;
	if (tot % 3 != 0)
		return 0;
	ll ans = 0;
	K = tot / 3;
	dfs(&nodes[root_node], ans);
	return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
	int T;
	int v, p;
    while (cin >> T) {
		while (T--) {
			cin >> N;
			for (int i = 0; i <= N; i++) {
				nodes[i].tot = 0;
				nodes[i].children.clear();
			}
			for (int i = 1; i <= N; i++) {
				cin >> v >> p;
				nodes[i].tot = v;
				nodes[p].children.emplace_back(&nodes[i]);
				if (p == 0)
					root_node = i;
			}
			cout << Solution() << '\n';
		}
    }
    return 0;
}
