//
// Created by sinn on 7/13/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
ll A[MAXN];

ll Solution() {
  unordered_map<ll, int> mp, ha;
  fora (i, 0, N) {
    fora (j, i+1, N) {
      mp[A[i]+A[j]]++;
    }
    ha[A[i]]++;
  }
  ll ans = 0;
  fora (i, 0, N) {
    fora (j, i+1, N) {
      ll x = A[i] + A[j];
      if (A[i] == A[j]) {
        ans += mp[x] - 2 * ha[A[i]] + 3;
      } else {
        ans += mp[x] - ha[A[i]] - ha[A[j]] + 1;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
