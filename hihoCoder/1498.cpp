//
// Created by sinn on 5/27/19.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

ll N, M, K, Q;

ll Solution() {
  ll base = 1;
  ll span_t = 0;
  ll ans = N;
  while (base < N) {
    ll t = N / base;
    if (N % base != 0) ++t;
    ans = min(ans, t + span_t);
    base *= 2;
    span_t += Q;
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> Q) {
    cout << Solution() << '\n';
  }
  return 0;
}