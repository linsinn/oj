//
// Created by Sinn on 2/17/2019.
//

#include <bits/stdc++.h>
using namespace std;

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

enum Type {
    OO=0, OL, LL,
    LO, OA, LA,
    AO, AL,
};

ll Solution() {
    if (N == 1)
        return 3;
    ll f[8][2], tmp[8][2];
    memset(f, 0, sizeof(f));
    for (int i = 0; i < 4; i++)
        f[i][0] = 1;
    for (int i = 4; i < 8; i++)
        f[i][1] = 1;
    for (int i = 2; i < N; i++) {
        memset(tmp, 0, sizeof tmp);
        tmp[Type::OO][0] += f[Type::OO][0];
        tmp[Type::OO][0] += f[Type::LO][0];
        tmp[Type::OO][1] += f[Type::OO][1];
        tmp[Type::OO][1] += f[Type::LO][1];
        tmp[Type::OO][1] += f[Type::AO][1];

        tmp[Type::OL][0] += f[Type::OO][0];
        tmp[Type::OL][0] += f[Type::LO][0];
        tmp[Type::OL][1] += f[Type::OO][1];
        tmp[Type::OL][1] += f[Type::LO][1];
        tmp[Type::OL][1] += f[Type::AO][1];

        tmp[Type::OA][1] += f[Type::OO][0];
        tmp[Type::OA][1] += f[Type::LO][0];

        tmp[Type::LO][0] += f[Type::OL][0];
        tmp[Type::LO][0] += f[Type::LL][0];
        tmp[Type::LO][1] += f[Type::OL][1];
        tmp[Type::LO][1] += f[Type::LL][1];
        tmp[Type::LO][1] += f[Type::AL][1];

        tmp[Type::LL][0] += f[Type::OL][0];
        tmp[Type::LL][1] += f[Type::OL][1];
        tmp[Type::LL][1] += f[Type::AL][1];

        tmp[Type::LA][1] += f[Type::OL][0];
        tmp[Type::LA][1] += f[Type::LL][0];

        tmp[Type::AO][1] += f[Type::OA][1];
        tmp[Type::AO][1] += f[Type::LA][1];

        tmp[Type::AL][1] += f[Type::OA][1];
        tmp[Type::AL][1] += f[Type::LA][1];

        memcpy(f, tmp, sizeof(f));
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 2; y++) {
                f[x][y] %= MOD;
            }
        }
    }
    ll ans = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 2; j++) {
            ans += f[i][j];
            ans %= MOD;
        }
    }
    return ans;
}

int main() {
    freopen("in.txt", "r", stdin);
    ios::sync_with_stdio(false);
    cin.tie(nullptr), cout.tie(nullptr);
    while (cin >> N) {
        cout << Solution() << '\n';
    }
    return 0;
}