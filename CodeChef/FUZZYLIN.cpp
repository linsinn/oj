#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
ll sieve[MAXN];
vector<ll> A;

void Init() {
  map<ll, ll> m;
  map<ll, ll> gcds;
  fora (i, 0, N) {
    map<ll, ll> m1;
    for (auto it = m.begin(); it != m.end(); ++it) {
      ll g = __gcd(A[i], it->first);
      m1[g] += it->second;
      gcds[g] += it->second;
    }
    m1[A[i]]++;
    gcds[A[i]]++;
    m = m1;
  }
  memset(sieve, 0, sizeof(sieve));
  for (auto it = gcds.begin(); it != gcds.end(); ++it) {
    ll k = it->first, v = it->second;
    for (int j = k; j < MAXN; j += k) 
      sieve[j] += v;
  }
}

ll Solution() {
  return sieve[K];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    Init();
    cin >> T;
    while (T--) {
      cin >> K;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
