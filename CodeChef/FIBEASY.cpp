#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 10;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int mem[100];
Vec A;

void Mult(vector<Vec> &a, vector<Vec> &b) {
  int n = a.size();
  int m = a[0].size();
  int p = b[0].size();
  vector<Vec> tmp(n, Vec(m, 0));
  fora (i, 0, n) {
    fora (j, 0, p) {
      fora (k, 0, m) {
        tmp[i][j] += a[i][k] * b[k][j];
        tmp[i][j] %= MOD;
      }
    }
  }
  a = tmp;
}

void BinPow(vector<Vec> &a, ll exp) {
  vector<Vec> tmp = {{1, 0}, {0, 1}};
  while (exp) {
    if (exp & 1) 
      Mult(tmp, a);
    Mult(a, a);
    exp >>= 1;
  }
  a = tmp;
}


ll Solution() {
  if (N == 1) return 0;
  int b = 0;
  while ((1LL << (b+1)) <= N)
    b++;
  if (mem[b] != -1) return mem[b];
  N = 1LL << b;
  vector<Vec> ans = {{1, 0}};
  vector<Vec> e = {{1, 1}, {1, 0}};
  BinPow(e, N-2);
  Mult(ans, e);
  mem[b] = ans[0][0];
  return ans[0][0];
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    memset(mem, -1, sizeof(mem));
    while (T--) {
      cin >> N;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
