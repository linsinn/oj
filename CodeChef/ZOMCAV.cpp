#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
ll C[MAXN], H[MAXN];
ll BIT1[MAXN], BIT2[MAXN];

ll GetSum(ll arr[], int x) {
  ll ret = 0;
  while (x > 0) {
    ret += arr[x];
    x -= x & (-x);
  }
  return ret;
}

void UpdateBIT(ll arr[], int x, int val) {
  while (x <= N) {
    arr[x] += val;
    x += x & (-x);
  }
}

ll Sum(ll arr1[], ll arr2[], int x) {
  return GetSum(arr1, x) * x  - GetSum(arr2, x);
}

void UpdateRange(ll arr1[], ll arr2[], int l, int r, int val) {
  UpdateBIT(arr1, l, val);
  UpdateBIT(arr1, r+1, -val);
  UpdateBIT(arr2, l, val * (l - 1));
  UpdateBIT(arr2, r+1, -val * r);
}

ll RangeSum(ll arr1[], ll arr2[], int l, int r) {
  return Sum(arr1, arr2, r) - Sum(arr1, arr2, l-1);
}


bool Solution() {
  memset(BIT1, 0, sizeof(ll) * (N+1));
  memset(BIT2, 0, sizeof(ll) * (N+1));
  fora (i, 0, N) {
    UpdateRange(BIT1, BIT2, max(1LL, i+1-C[i]), min(N, i+1+C[i]), 1);
  }
  vector<ll> rad(N);
  fora (i, 0, N) {
    rad[i] = GetSum(BIT1, i+1);
  }
  sort(rad.begin(), rad.end());
  sort(H, H+N);
  fora (i, 0, N) {
    if (rad[i] != H[i])
      return false;
  }
  return true;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      fora (i, 0, N) cin >> C[i];
      fora (i, 0, N) cin >> H[i];
      if (Solution()) cout << "YES\n";
      else cout << "NO\n";
    }
  }
  return 0;
}
