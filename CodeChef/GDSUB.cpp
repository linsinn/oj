#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  map<int, int> mp;
  for (int a : A) mp[a]++;
  N = mp.size();
  Vec arr(N);
  auto it = mp.begin();
  fora (i, 0, N) {
    arr[i] = it->second;
    ++it;
  }
  vector<Vec> dp(N, Vec(K+1, 0));
  dp[0][1] = arr[0];
  fora (i, 1, N) {
    dp[i][1] = dp[i-1][1] + arr[i];
  }
  fora (i, 1, N) {
    fora (j, 2, min(i+2, (int)K+1)) {
      dp[i][j] = dp[i-1][j] + ((ll)dp[i-1][j-1] * arr[i] % MOD); 
      dp[i][j] %= MOD;
    }
  }
  ll ans = 1;
  fora (i, 1, K+1) {
    ans += dp[N-1][i];
    ans %= MOD;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    A.resize(N);
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}
