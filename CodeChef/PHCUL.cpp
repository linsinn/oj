//
// Created by sinn on 11/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Pii st;
vector<Pii> A, B, C;

double get_dist(const Pii& x, const Pii& y) {
  double p = x.first - y.first, q = x.second - y.second;
  return sqrt(p * p + q * q);
}

double Solution() {
  vector<double> a(N, -1), b(M, -1);
  fora (i, 0, N) {
    fora (j, 0, M) {
      double d = get_dist(A[i], B[j]) + get_dist(B[j], st);
      if (a[i] < 0 || d < a[i])
        a[i] = d;
      d = get_dist(A[i], B[j]) + get_dist(A[i], st);
      if (b[j] < 0 || d < b[j])
        b[j] = d;
    }
  }
  double ans = a[0] + get_dist(A[0], C[0]);
  fora (i, 0, N) {
    fora (j, 0, K) {
      double d = get_dist(A[i], C[j]) + a[i];
      Min(ans, d);
    }
  }
  fora (i, 0, M) {
    fora (j, 0, K) {
      double d = get_dist(B[i], C[j]) + b[i];
      Min(ans, d);
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  cout.setf(ios::fixed, ios::floatfield);
  cout.precision(12);
  while (cin >> T) {
    while (T--) {
      cin >> st.first >> st.second;
      cin >> N >> M >> K;
      A.resize(N), B.resize(M), C.resize(K);
      fora (i, 0, N) cin >> A[i].first >> A[i].second;
      fora (i, 0, M) cin >> B[i].first >> B[i].second;
      fora (i, 0, K) cin >> C[i].first >> C[i].second;
      cout << Solution() << '\n';
    }
  }
  return 0;
}