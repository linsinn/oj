
#include<bits/stdc++.h>
#define put(x) cout<<#x<<" = "<<x<<endl;
using namespace std;
const int maxn=2e6+10,maxq=4e6+10;
const char addQuery='+',askQuery='?';

struct Query{
	int id,time,v,k;
	long long answer;
	char type;
	Query(){}
	Query(int _id,char _type,int _time,int _v,int _k=0,long long _answer=0){
		id=_id;
		type=_type;
		time=_time;
		v=_v;
		k=_k;
		answer=_answer;
	}
};

int n,q,H[maxn];
int qsize=0,startingTime[maxn],finishingTime[maxn],curTime=0;
long long A[maxn];
bool isLeaf[maxn];
vector<int> G[maxn];
Query Q[maxq];
int Qper[maxq];

void dfs(int a,int par=-1){
	startingTime[a]=curTime++;
	for(int b:G[a]){
		if(b!=par){
			H[b]=H[a]+1;
			dfs(b,a);
		}
	}
	if(int(G[a].size())-(par!=-1)==0){
		isLeaf[a]=1;
	}
	finishingTime[a]=curTime;
}

struct FenwickTree{
	long long s[maxn];
	bool mark[maxn];
	int history[maxn],historySize=0;
	void add(int x,long long value){
		x+=5;
		if(!mark[x]){
			mark[x]=1;
			history[historySize++]=x;
		}
		for(int i=x;i<maxn;i+=i&-i){
			s[i]+=value;
		}
	}

	// [L,R)
	void add(int L,int R,long long value){
		add(L,+value);
		add(R,-value);
	}

	long long ask(int x){
		x+=5;
		long long ans=0;
		for(int i=x;i>0;i-=i&-i){
			ans+=s[i];
		}
		return ans;
	}

	void clear(){
		for(int i=0;i<historySize;i++){
			int a=history[i];
			mark[a]=0;
			for(int j=a;j<maxn;j+=j&-j){
				s[j]=0;
			}
		}
		historySize=0;
	}
}fenwick[2];

void Init() {
  fenwick[0].clear();
  fenwick[1].clear();
  memset(startingTime, 0, sizeof(int) * (curTime + 10));
  memset(finishingTime, 0, sizeof(int) * (curTime + 10));
  qsize = 0, curTime = 0;
  memset(A, 0, sizeof(A[0]) * (n + 1));
  memset(isLeaf, 0, sizeof(isLeaf[0]) * (n + 1));
  for (int i = 0; i <= n; ++i) G[i].clear();
  memset(Q, 0, sizeof(Q[0]) * (q + 1));
  memset(Qper, 0, sizeof(n + q + 1));
}

int32_t main(){
  freopen("in.txt", "r", stdin);
  freopen("b.txt", "w", stdout);
  int a, b;
  while (~scanf("%d%d", &a, &b)) {
    printf("%d %d\n", a, b);
    scanf("%d%d", &n, &q);
    Init();
    for (int i = 0; i < n - 1; i++) {
      int a, b;
      scanf("%d%d", &a, &b);
      a--, b--;
      G[a].push_back(b);
      G[b].push_back(a);
    }
    dfs(0);
    for (int i = 0; i < n; i++) {
      scanf("%d", A + i);
      // this query doesn't need to have a valid id
      Q[qsize++] = Query(maxq, addQuery, -H[i], i, A[i]);
    }
    for (int i = 1; i <= q; i++) {
      Q[qsize].id = i;
      scanf(" %c", &Q[qsize].type);
      if (Q[qsize].type == askQuery) {
        scanf("%d", &Q[qsize].v);
        Q[qsize].v--;
        Q[qsize].time = i - H[Q[qsize].v];
      } else if (Q[qsize].type == addQuery) {
        scanf("%d%d", &Q[qsize].v, &Q[qsize].k);
        Q[qsize].v--;
        Q[qsize].time = i - H[Q[qsize].v];
      } else {
        assert(0);
      }
      qsize++;
    }
    for (int i = 0; i < qsize; i++) {
      Qper[i] = i;
    }
    sort(Qper, Qper + qsize, [](int a, int b) {
      if (Q[a].time != Q[b].time) {
        return Q[a].time < Q[b].time;
      }
      // For queries with the same time, addQuery has more priority
      return Q[a].type == '+' && Q[b].type == '?';
    });
    for (int i = 0; i < qsize; i++) {
      if (i > 0 && Q[Qper[i - 1]].time != Q[Qper[i]].time) {
        fenwick[1].clear();
      }
      if (Q[Qper[i]].type == addQuery) {
        fenwick[0].add(startingTime[Q[Qper[i]].v], finishingTime[Q[Qper[i]].v], Q[Qper[i]].k);
        fenwick[1].add(startingTime[Q[Qper[i]].v], finishingTime[Q[Qper[i]].v], Q[Qper[i]].k);
      }
      if (Q[Qper[i]].type == askQuery) {
        int fenwickId = 1;
        if (isLeaf[Q[Qper[i]].v]) {
          fenwickId = 0;
        }
        Q[Qper[i]].answer = fenwick[fenwickId].ask(startingTime[Q[Qper[i]].v]);
      }
    }
    for (int i = 0; i < qsize; i++) {
      if (Q[i].type == askQuery) {
        printf("%lld\n", Q[i].answer);
      }
    }
    printf("\n");
  }
}
