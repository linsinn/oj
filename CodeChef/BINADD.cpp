//
// Created by sinn on 12/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
string A, B;

ll Solution() {
	N = max(A.size(), B.size());
	Vec aux(N, 0);
	fora (i, 0, N) {
		int a = (i < N - A.size()) ? 0 : (A[i - (N - A.size())] - '0');
		int b = (i < N - B.size()) ? 0 : (B[i - (N - B.size())] - '0');
		aux[i] = i > 0 ? aux[i-1] : 0;
		if (a ^ b) {
			aux[i] = aux[i - 1] + 1;
		} else {
			aux[i] = 0;
		}
	}
	int ans = 0;
	int pre = -1;
	fora (i, 0, N) {
		int a = (i < N - A.size()) ? 0 : (A[i - (N - A.size())] - '0');
		int b = (i < N - B.size()) ? 0 : (B[i - (N - B.size())] - '0');
		if (b == 1) {
			if (a == 1) {
				ans = max(ans, ((i > 0) ? aux[i - 1] : 0) + 2);
			} else {
				ans = max(ans, 1);
			}
		}
	}
	return ans;
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
  	while (T--) {
  		cin >> A >> B;
  		cout << Solution() << '\n';
  	}
  }
  return 0;
}