#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  if (M < N-1) return -1;
  if (M > N * (N - 1) / 2 + N) return -1;
  if (N == 1) {
    if (M == 0) return 0;
    if (M == 1) return 1;
    else return -1;
  }
  if (N == 2 && M == 1) return 1;
  if (M - N <= 1) return 2;
  if (M <= N * 2) return 3;
  M -= N * 2;
  ll ans = 3;
  ll t = M / N;
  ans += 2 * t;
  if (t * N < M) {
    M -= t * N;
    ans += M <= N / 2 ? 1 : 2;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
