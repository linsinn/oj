#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> gph;

void Solution() {
  if (M % 2 == 0) {
    cout << "1\n";
    fora (i, 0, N) cout << "1 ";
    cout << '\n';
    return ;
  }
  int k = -1;
  fora (i, 0, N) {
    if (gph[i].size() % 2 == 1) {
      k = i;
      break;
    }
  }
  if (k != -1) {
    cout << "2\n";
    fora (i, 0, N) {
      cout << (i == k ? 2 : 1) << ' ';
    }
    cout << '\n';
    return ;
  }
  int u = -1, v = -1;
  fora (i, 0, N) {
    if (gph[i].size() != 0) {
      u = i;
      v = gph[i][0];
      break;
    }
  }
  assert(u != -1 && v != -1);
  cout << "3\n";
  fora (i, 0, N) {
    if (i == u) {
      cout << "2 ";
    } else if (i == v) {
      cout << "3 ";
    } else {
      cout << "1 ";
    }
  }
  cout << "\n";
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      int u, v;
      gph.assign(N, Vec());
      fora (i, 0, M) {
        cin >> u >> v;
        --u, --v;
        gph[u].emplace_back(v);
        gph[v].emplace_back(u);
      }
      Solution();
    }
  }
  return 0;
}
