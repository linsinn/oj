//
// Created by sinn on 11/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec base = {3, 8, 15, 24, 35, 48, 63, 80};
Vec ans;

struct Node {
  int tot;
  int mx;
  int c[8];
  Node() {
    tot = -1;
    mx = 0;
    memset(c, 0, sizeof(c));
  }
  bool operator < (const Node& rhs) const {
    if (rhs.tot == -1) return true;
    else if (tot == -1) return false;
    if (tot != rhs.tot)
      return tot < rhs.tot;
    fora (i, 0, 8) {
      if (c[i] != rhs.c[i]) {
        return c[i] > rhs.c[i];
      }
    }
    return false;
  }
};
vector<Node> dp;

void Init() {
  dp.assign(MAXN, Node());
  dp[0].tot = 0;
  fora (i, 0, dp.size()) {
    if (dp[i].tot != -1) {
      Node n;
      n.tot = dp[i].tot + 1;
      memcpy(n.c, dp[i].c, sizeof(n.c));
      fora (j, dp[i].mx, base.size()) {
        n.mx = j;
        n.c[j]++;
        if (i + base[j] < dp.size() && n < dp[i + base[j]]) {
          dp[i + base[j]] = n;
        }
        n.c[j]--;
      }
    }
  }
}

void Solution() {
  int limit = 81 * N;
  Vec sqrs;
  for (int i = 1; i * i <= limit; ++i) {
    sqrs.emplace_back(i * i);
  }
  Node ans;
  auto st = lower_bound(sqrs.begin(), sqrs.end(), N);
  for (auto it = st; it != sqrs.end(); ++it) {
    if (*it - N < dp.size())
      Min(ans, dp[*it - N]);
  }
  if (ans.tot == -1) {
    cout << "-1\n";
  } else {
    int o = N - accumulate(ans.c, ans.c + base.size(), 0);
    fora (i, 0, o) {
      cout << "1";
    }
    fora (i, 0, base.size()) {
      fora (j, 0, ans.c[i]) {
        cout << i + 2;
      }
    }
    cout << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    Init();
    while (T--) {
      cin >> N;
      Solution();
    }
  }
  return 0;
}