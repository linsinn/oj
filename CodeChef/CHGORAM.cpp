#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<vector<int>> gph;
vector<vector<int>> mi, ma;
int p[3];
int BIT[MAXN];
bool seen[MAXN];

void Update(int x, int val) {
  while (x <= N) {
    BIT[x] += 1;
    x += x & (-x);
  }
}

int Query(int x) {
  int ret = 0;
  while (x > 0) {
    ret += BIT[x];
    x -= x & (-x);
  }
  return ret;
}

void dfs(int u, ll& ans) {
  seen[u] = true;
  int a = Query(u - 1);
  int b = Query(N) - Query(u);
  int l = 0, g = 0;
  int pa = -1;
  mi[u].assign(gph[u].size(), 0);
  ma[u].assign(gph[u].size(), 0);
  Update(u, 1);
  for (int i = 0; i < gph[u].size(); ++i) {
    int v = gph[u][i];
    if (!seen[v]) {
      dfs(v, ans);
      int k = Query(u - 1);
      mi[u][i] = k - a - l;
      l += mi[u][i];
      k = Query(N) - Query(u);
      ma[u][i] = k - b - g;
      g += ma[u][i];
    } else {
      pa = i;
    }
  }
  if (pa != -1) {
    mi[u][pa] = u - 1 - l;
    ma[u][pa] = N - u - g;
    l += mi[u][pa];
    g += ma[u][pa];
  }
  for (int i = 0; i < gph[u].size(); ++i) {
    if (p[1] == 1) {
      ans += 1LL * ma[u][i] * (g - ma[u][i]);
    } else if (p[1] == 2) {
      ans += 1LL * mi[u][i] * (g - ma[u][i]);
      ans += 1LL * ma[u][i] * (l - mi[u][i]);
    } else {
      ans += 1LL * mi[u][i] * (l - mi[u][i]);
    }
  }
}

ll Solution() {
  memset(BIT, 0, sizeof(int) * (N+1));
  memset(seen, 0, sizeof(bool) * (N+1));
  mi.assign(N+1, vector<int>());
  ma.assign(N+1, vector<int>());
  ll ans = 0;
  dfs(1, ans);
  return ans / 2;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      fora (i, 0, 3) cin >> p[i];
      gph.assign(N+1, vector<int>());
      int u, v;
      fora (i, 0, N-1) {
        cin >> u >> v;
        gph[u].emplace_back(v);
        gph[v].emplace_back(u);
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}
