#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> gph;
Vec A;

struct Query {
  int id, t, val;
  char tp;
  int node;
  Query() = default;
  Query(int _t, int _v, int _n) {
    t = _t, val = _v, node = _n;
    id = -1;
    tp = '+';
  }
  bool operator < (const Query& rhs) const {
    if (t != rhs.t)
      return t < rhs.t;
    return tp == '+' && rhs.tp == '?';
  }
};
vector<Query> qrys;
vector<int> in, out, dep;
vector<bool> is_leaf;

int vis_time;
void dfs(int u, int par=-1) {
  in[u] = vis_time++;
  for (int v : gph[u]) {
    if (v != par) {
      dep[v] = dep[u] + 1;
      dfs(v, u);
    }
  }
  if (gph[u].size() - (par != -1) == 0)
    is_leaf[u] = true;
  out[u] = vis_time++;
}

struct BIT {
  Vec history;
  vector<ll> arr;
  BIT() {
    arr.assign(vis_time + 1, 0);
  }

  void Add(int x, int val) {
    history.emplace_back(x);
    while (x < arr.size()) {
      arr[x] += val;
      x += x & -x;
    }
  }

  void Add(int l, int r, int val) {
    Add(l, val);
    Add(r, -val);
  }

  ll Query(int x) {
    ll ret = 0;
    while (x > 0) {
      ret += arr[x];
      x -= x & -x;
    }
    return ret;
  }

  void Clear() {
    for (int h : history) {
      while (h < arr.size()) {
        arr[h] = 0;
        h += h & -h;
      }
    }
    history.clear();
  }
};

void Solution() {
  in.assign(N, 0);
  out.assign(N, 0);
  dep.assign(N, 0);
  is_leaf.assign(N, false);
  vis_time = 1;
  dfs(0);
  fora (i, 0, T) {
    qrys[i].t = i + 1 - dep[qrys[i].node];
  }
  fora (i, 0, N) {
    qrys.emplace_back(-dep[i], A[i], i);
  }
  sort(qrys.begin(), qrys.end());
  vector<ll> ans(T, -1);
  BIT bits[2];
  fora (i, 0, qrys.size()) {
    if (i > 0 && qrys[i].t != qrys[i-1].t)
      bits[0].Clear();
    if (qrys[i].tp == '+') {
      bits[0].Add(in[qrys[i].node], out[qrys[i].node], qrys[i].val);
      bits[1].Add(in[qrys[i].node], out[qrys[i].node], qrys[i].val);
    } else {
      int w = is_leaf[qrys[i].node] ? 1 : 0;
      ans[qrys[i].id] = bits[w].Query(in[qrys[i].node]);
    }
  }
  fora (i, 0, ans.size()) {
    if (ans[i] != -1) {
      cout << ans[i] << '\n';
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> T) {
    gph.assign(N, Vec());
    int u, v;
    fora (i, 0, N-1) {
      cin >> u >> v;
      --u, --v;
      gph[u].emplace_back(v);
      gph[v].emplace_back(u);
    }
    A.resize(N+1);
    fora (i, 0, N)
      cin >> A[i];
    qrys.resize(T);
    fora (i, 0, T) {
      cin >> qrys[i].tp >> qrys[i].node;
      --qrys[i].node;
      if (qrys[i].tp == '+')
        cin >> qrys[i].val;
      qrys[i].id = i;
    }
    Solution();
  }
  return 0;
}