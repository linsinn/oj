#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

int A, B, C, T;

int Solution() {
  if (A == 1 || C == 1) return 0;
  int ans = 0;
  fora (i, 1, B+1) {
    int g = i * i;
    ans += (1LL * max(0, A - i - 1) * max(0, C - i - 1)) % MOD;
    ans %= MOD;
    int k = min(A, i + 1);
    ford (j, k, 2) {
      int t = C - g / (j - 1) - 1;
      if (t > 0) {
        ans = (ans + t) % MOD;
      } else {
        break;
      }
    }
    k = min(C, i + 1);
    ford (j, k, 2) {
      int t = A - g / (j - 1) - 1;
      if (t > 0) {
        ans = (ans + t) % MOD;
      } else {
        break;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  scanf("%d", &T);
  while (T--) {
    scanf("%d %d %d", &A, &B, &C);
    printf("%d\n", Solution());
  }
  return 0;
}
