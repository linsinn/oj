#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A, B;

ll Solution() {
  unordered_map<int, Vec> mp;
  mp[0].emplace_back(0);
  int g = 0;
  fora (i, 0, N) {
    g ^= A[i];
    mp[g].emplace_back(i+1);
  }
  ll ans = 0;
  for (auto &p : mp) {
    auto &v = p.second;
    ll su = v.front();
    fora (i, 1, v.size()) {
      ans += 1LL * v[i] * i - su - i;
      su += v[i];
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N;
      A.resize(N);
      fora (i, 0, N) cin >> A[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}
