#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

ll Solution() {
  vector<vector<pair<ll, ll>>> dp(N+1, vector<pair<ll, ll>>(K+1, make_pair(INF, 1)));
  fora (i, 0, N) {
    dp[i][0].first = 0;
    fora (j, 1, min(i+2, int(K+1))) {
      ll a = dp[i][j-1].first + A[i];
      ll b = dp[i][j].first;
      if (a < b) {
        dp[i+1][j] = dp[i][j-1];
        dp[i+1][j].first = a;
      } else if (a > b) {
        dp[i+1][j] = dp[i][j];
      } else {
        dp[i+1][j].first = a;
        dp[i+1][j].second = dp[i][j-1].second + dp[i][j].second;
      }
    }
  }
  return dp[N][K].second;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      A.resize(N);
      fora (i, 0, N) cin >> A[i];
      cout << Solution() << '\n';
    }
  }
  return 0;
}
