//
// Created by sinn on 12/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Pii> rngs;

ll Solution() {
	sort(rngs.begin(), rngs.end());
	int rb = rngs[0].second;
	Vec ps;
	int cnt = 1;
	fora (i, 1, N) {
		if (rngs[i].first > rb) {
			ps.emplace_back(cnt);
			cnt = 1;
		} else {
			++cnt;
		}
		rb = max(rb, rngs[i].second);
	}
	ps.emplace_back(cnt);
	if (ps.size() == 2)
		return 0;
	if (ps.size() > 2) {
		cnt = 0;
		Vec mx(2, 0);
		for (int c : ps) {
			cnt += c;
			if (c > mx[0]) {
				mx[1] = mx[0];
				mx[0] = c;
			} else if (c > mx[1]) {
				mx[1] = c;
			}
		}
		return cnt - mx[0] - mx[1];
	}
	int ans = N;
	fora (i, 0, N) {
		auto it = lower_bound(rngs.begin(), rngs.end(), make_pair(rngs[i].second+1, 0));
		if (it != rngs.end()) {
			int j = distance(rngs.begin(), it);
			ans = min(ans, j - i - 1);
		}
	}
	return (ans == N) ? -1 : ans;
}

int main() {
#ifdef LOCAL_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
  	while (T--) {
  		cin >> N;
  		rngs.resize(N);
  		fora (i, 0, N) {
  			cin >> rngs[i].first >> rngs[i].second;
  		}
  		cout << Solution() << '\n';
  	}
  }
  return 0;
}
