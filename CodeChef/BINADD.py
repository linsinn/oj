def Solution(a, b):
	ans = 0
	while b > 0:
		u = a ^ b
		v = a & b
		a = u
		b = v << 1
		ans += 1
	return ans


T = int(input())
for _ in range(T):
	A = int(input(), 2)
	B = int(input(), 2)
	print(Solution(A, B))
