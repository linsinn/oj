#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;

void Solution() {
  ll t = K / (3 * N);
  K -= 3 * N * t;
  if (N % 2 == 1 && t != 0) {
    A[N / 2] = 0;
  }
  fora (i, 0, K) {
    int a = A[i % N], b = A[N - (i % N) - 1];
    A[i % N] = a ^ b;
  }
  fora (i, 0, N) {
    cout << A[i] << ' ';
  }
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      A.resize(N);
      fora (i, 0, N) cin >> A[i];
      Solution();
    }
  }
  return 0;
}
