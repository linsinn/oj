//
// Created by sinn on 12/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
string a, b;

vector<ll> fac, rfac;

template <class T, class U>
T BinPow(T base, U exp) {
	T ret = 1;
	while (exp) {
		if (exp & 1) {
			ret = ret * base % MOD;
		}
		base = base * base % MOD;
		exp >>= 1;
	}
	return ret;
}

void Init() {
	fac.resize(MAXN);
	rfac.resize(MAXN);
	fac[0] = fac[1] = 1;
	fora (i, 2, MAXN) {
		fac[i] = fac[i-1] * i % MOD;
	}
	rfac[MAXN-1] = BinPow(fac[MAXN-1], MOD-2);
	ford (i, MAXN-2, 0) {
		rfac[i] = rfac[i+1] * (i + 1) % MOD;
	}
}

ll C(int n, int k) {
	return fac[n] * rfac[k] % MOD * rfac[n - k] % MOD;
}

ll Solution() {
	Vec ca(2, 0), cb(2, 0);
	for (char ch : a)
		ca[ch - '0']++;
	for (char ch : b)
		cb[ch - '0']++;
	int mini = abs(ca[0] - cb[0]);
	int maxi = min(ca[0], cb[1]) + min(ca[1], cb[0]);
	ll ans = 0;
	for (int i = maxi; i >= mini; i -= 2) {
		ans += C(N ,i);
		ans %= MOD;
	}
	return ans;
}

int main() {
#ifdef LOCAL_DEBUG
	freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
  	Init();
  	while (T--) {
  		cin >> N >> a >> b;
  		cout << Solution() << '\n';
  	}
  }
  return 0;
}