#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
Vec A;
using Matrix = vector<Vec>;

void Mul(Matrix &a, Matrix &b) {
  int n = a.size();
  int m = a[0].size();
  int p = b[0].size();
  Matrix tmp(n, Vec(p, 0));
  fora (i, 0, n) {
    fora (j, 0, p) {
      fora (k, 0, m) {
        tmp[i][j] += 1LL * a[i][k] * b[k][j] % MOD;
        tmp[i][j] %= MOD;
      }
    }
  }
  a = tmp;
}

void Handle7() {
  M = 7;
  vector<Matrix> mats(7, Matrix (M * M, Vec(M * M, 0)));
  fora (k, 1, 7) {
    fora (i, 0, M * M) {
      int a = i / M, b = i % M;
      fora (dig, 0, 10) {
        int c = (a * 10 % M + dig % M) % M;
        int d = (dig * k % M + b) % M;
        mats[k][i][c * M + d]++;
      }
    }
  }
  vector<int> v = {1, 3, 2, 6, 4, 5};
  Matrix init(1, Vec(M * M));
  init[0][0] = 1;
  Mul(init, mats[v[0]]);
  Mul(init, mats[v[1]]);
  Mul(init, mats[v[2]]);
  Mul(init, mats[v[3]]);
  Mul(init, mats[v[4]]);
  Mul(init, mats[v[5]]);
  Mul(init, mats[v[0]]);
  Mul(init, mats[v[1]]);
  Mul(init, mats[v[2]]);
  fora (i, 0, M) {
    fora (j, 0, M) {
      cout << init[0][i * M + j] << ' ';
    }
    cout << '\n';
  }
}

ll Solution() {
  Handle7();
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> K;
      cout << Solution() << '\n';
    }
  }
  return 0;
}
