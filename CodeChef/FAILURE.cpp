//
// Created by sinn on 11/9/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<Vec> gph;
Vec comp;

void dfs(int u, int c) {
  comp[u] = c;
  for (int v : gph[u]) {
    if (comp[v] == -1) {
      dfs(v, c);
    }
  }
}

ll Solution() {
  comp.assign(N, -1);
  int tot_comp = 0;
  fora (i, 0, N) {
    if (comp[i] == -1) {
      dfs(i, tot_comp);
      tot_comp++;
    }
  }
  vector<Pii> aux(tot_comp);
  fora (i, 0, N) {
    aux[comp[i]].first++;
    aux[comp[i]].second += gph[i].size();
  }
  bool is_robust = false;
  vector<int> h;
  fora (i, 0, aux.size()) {
    auto &p = aux[i];
    if (p.second / 2 > p.first - 1) {
      is_robust = true;
      h.emplace_back(i);
    }
  }
  int ans = -1;
  if (!is_robust || h.size() > 1)
    return ans;
  fora (i, 0, N) {
    int c = comp[i];
    int a = aux[c].first - 1;
    int b = aux[c].second / 2 - gph[i].size();
    if (b <= a - 1 && h[0] == c) {
      ans = i + 1;
      break;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M;
      gph.assign(N, Vec());
      int u, v;
      fora (i, 0, M) {
        cin >> u >> v;
        --u, --v;
        gph[u].emplace_back(v);
        gph[v].emplace_back(u);
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}