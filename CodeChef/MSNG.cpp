//
// Created by sinn on 10/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;
constexpr ll LIMIT = 1e12;
vector<int> mp;
vector<int> B;
vector<string> Y;

ll GetNum(const string& num, int base) {
  ll res = 0;
  for (char ch : num) {
    int k = mp[ch];
    if (k >= base) return -1;
    res = res * base + k;
    if (res > LIMIT)
      return -1;
  }
  return res;
}

ll Solution() {
  map<ll, int> cnt;
  fora (i, 0, N) {
    if (B[i] != -1) {
      ll num = GetNum(Y[i], B[i]);
      if (num != -1)
        cnt[num] += 1;
    } else {
      set<ll> seen;
      fora (j, 2, 37) {
        ll num = GetNum(Y[i], j);
        if (num != -1 && !seen.count(num)) {
          seen.emplace(num);
          cnt[num] += 1;
        }
      }
    }
  }
  for (auto &p : cnt) {
    if (p.second == N) {
      return p.first;
    }
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    mp.resize(256, 0);
    fora (i, '0', '9'+1) {mp[i] = i - '0';}
    fora (i, 'A', 'Z'+1) {mp[i] = 10 + i - 'A';}
    while (T--) {
      cin >> N;
      B.resize(N);
      Y.resize(N);
      fora (i, 0, N) {
        cin >> B[i] >> Y[i];
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}