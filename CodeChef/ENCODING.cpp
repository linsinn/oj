#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 1e5 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
int len[2];
int tens[MAXN];
string num[2];
int dp[MAXN][10];
int cnt[MAXN][10];

pair<int, int> dfs(string &s, int idx, int dig, bool less) {
  if (idx == 0) return {dig, 1};
  if (less && dp[idx][dig] != -1)
    return {dp[idx][dig], cnt[idx][dig]};
  int cur = 0;
  int tot = 0;
  int bnd = less ? 9 : s[idx-1] - '0';
  fora (i, 0, bnd + 1) {
    auto ret = dfs(s, idx-1, i, less || (i < bnd));
    cur += ret.first;
    cur %= MOD;
    cur += 1LL * dig * tens[idx] * ret.second % MOD;
    cur %= MOD;
    tot += ret.second;
    tot %= MOD;
    if (dig == i) {
      cur -= 1LL * dig * tens[idx-1] * ret.second % MOD;
      cur += MOD;
      cur %= MOD;
    }
  }
  if (less) {
    dp[idx][dig] = cur;
    cnt[idx][dig] = tot;
  }
  return {cur, tot};
}

int Helper(string& s) {
  if (s.size() == 0)
    return 0;
  int bnd = s.back() - '0';
  int cur = 0;
  fora (i, 0, bnd+1) {
    auto ret = dfs(s, s.size()-1, i, i < bnd);
    cur += ret.first;
    cur %= MOD;
  }
  return cur;
}

ll Solution() {
  string s;
  int cy = 1;
  ford (i, len[0] - 1, 0) {
    int k = num[0][i] - '0';
    if (k - cy < 0) {
      s += '9';
    } else {
      s += (k - cy) + '0';
      cy = 0;
    }
  }
  while (s.back() == '0') {
    s.pop_back();
  }
  reverse(num[1].begin(), num[1].end());
  tens[0] = 1;
  fora (i, 1, len[1] + 1) {
    tens[i] = 1LL * tens[i-1] * 10 % MOD;
  }
  return (Helper(num[1]) - Helper(s) + MOD) % MOD;
}

int main() {
#ifdef MY_DEBUG
  freopen("../in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    memset(dp, -1, sizeof(dp));
    memset(cnt, 0, sizeof(cnt));
    while (T--) {
      fora (i, 0, 2) {
        cin >> len[i] >> num[i];
      }
      cout << Solution() << '\n';
    }
  }
  return 0;
}
