#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;

ll N, M, K, T;


ll Solution() {
  int x, y;
  vector<int> row(N+1), col(M+1);
  while (K--) {
    cin >> x >> y;
    row[x]++;
    col[y]++;
  }
  ll ans = 0;
  ll t[2] = {0, 0};
  fora (i, 0, M+1) {
    if (col[i] % 2)
      t[1]++;
  }
  t[0] = M - t[1];
  fora (i, 1, N+1) {
    ll g = row[i] % 2;
    ans += t[g ^ 1];
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> T) {
    while (T--) {
      cin >> N >> M >> K;
      cout << Solution() << '\n';
    }
  }
  return 0;
}