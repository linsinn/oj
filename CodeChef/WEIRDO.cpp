//
// Created by sinn on 11/9/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)(l); i < (int)(r); ++i)
#define ford(i, r, l) for (int i = (int)(r); i >= (int)(l); --i)

constexpr int MOD = 1e9 + 7;
constexpr int INF = INT32_MAX;
constexpr int MAXN = 2e6 + 10;
constexpr double eps = 1e-8;

using ll = long long;
using Pii = pair<int, int>;
using Pll = pair<ll, ll>;
using Vec = vector<int>;

template <class T> void Min(T &a, T b) {if (b < a) a = b;}
template <class T> void Max(T &a, T b) {if (b > a) a = b;}

ll N, M, K, T;
vector<string> vs;
vector<bool> is_vowel;
constexpr double limit = 1e7;

bool isAlice(int idx) {
  const string& str = vs[idx];
  int c[2] = {0, 0};
  int i = 0;
  while (i < min((int)str.size(), 3)) {
    c[is_vowel[str[i]]]++;
    ++i;
  }
  if (c[1] < c[0]) return false;
  while (i < str.size()) {
    c[is_vowel[str[i]]]++;
    c[is_vowel[str[i - 3]]]--;
    if (c[1] < c[0])
      return false;
    ++i;
  }
  return true;
}

void Solution() {
  int Alp = 26;
  vector<Vec> tp(2);
  fora (i, 0, N) {
    tp[isAlice(i)].emplace_back(i);
  }
  vector<Vec> x(2, Vec(Alp, 0)), f(2, Vec(Alp));
  fora (i, 0, 2) {
    for (int idx : tp[i]) {
      vector<bool> vis(Alp, false);
      for (char ch : vs[idx]) {
        int k = ch - 'a';
        f[i][k]++;
        if (!vis[k]) {
          vis[k] = true;
          x[i][k]++;
        }
      }
    }
  }
  vector<vector<double>> g(2, vector<double>());
  fora (i, 0, 2) {
    fora (j, 0, Alp) {
      if (x[i][j] != 0) {
        g[i].emplace_back(x[i][j]);
      }
    }
  }
  fora (i, 0, 2) {
    fora (j, 0, Alp) {
      if (f[i][j] != 0) {
        fora (k, 0, tp[i].size())
          g[i ^ 1].emplace_back(f[i][j]);
      }
    }
  }
  double ans = 1.0;
  int i = 0, j =0;
  while (i < g[0].size() && j < g[1].size()) {
    if (ans < limit) {
      ans *= g[1][j++];
    } else {
      ans /= g[0][i++];
    }
  }
  while (i < g[0].size()) {
    ans /= g[0][i++];
  }
  while (j < g[1].size()) {
    ans *= g[1][j++];
    if (ans > 1e7) {
      cout << "Infinity\n";
      return ;
    }
  }
  if (ans > 1e7)
    cout << "Infinity\n";
  else
    cout << ans << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  cout.setf(ios::fixed, ios::floatfield);
  cout.precision(12);
  while (cin >> T) {
    is_vowel.resize(256, false);
    is_vowel['a'] = true;
    is_vowel['e'] = true;
    is_vowel['i'] = true;
    is_vowel['o'] = true;
    is_vowel['u'] = true;
    while (T--) {
      cin >> N;
      vs.resize(N);
      fora (i, 0, N) cin >> vs[i];
      Solution();
    }
  }
  return 0;
}