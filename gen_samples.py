import random

N = 1000
G = 100
limit = 10000000

with open("in.txt", "w") as f:
	for g in range(G):
		f.write("-1 {}\n".format(g))
		T = random.randint(100, 200)
		f.write("{} {}\n".format(N, T))
		for i in range(2, N + 1):
			p = random.randrange(1, i)
			f.write("{} {}\n".format(i, p))
		for i in range(N):
			f.write("{} ".format(random.randint(1, limit)))
		f.write("\n")
		for i in range(T):
			tp = random.choice(['?', '+'])
			node = random.choice(range(N)) + 1
			f.write("{} {} ".format(tp, node))
			if tp == '+':
				v = random.randint(1, limit)
				f.write("{}".format(v))
			f.write("\n")
		f.write("\n")
