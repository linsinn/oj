//
// Created by sinn on 6/10/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
ll tot_sum, mini;
int tim;

struct Node {
  ll sum;
  int ari, lef;
} nodes[MAXN];

vector<int> edges[MAXN];
bool vis[MAXN];
map<ll, int> mi, ma, mi1, ma1;

void Init() {
  tot_sum = 0;
  tim = 0;
  fora (i, 0, N+1) {
    edges[i].clear();
  }
  mi.clear(), ma.clear();
  mi1.clear(), ma1.clear();
}

void dfs(int u, int pred) {
  nodes[u].ari = ++tim;
  for (int v : edges[u]) {
    if (v != pred) {
      dfs(v, u);
      nodes[u].sum += nodes[v].sum;
    }
  }
  nodes[u].lef = tim;
}

void uradi(int x) {
  if ((tot_sum - nodes[x].sum) % 2 == 0) {
    ll p = (tot_sum - nodes[x].sum) / 2;
    if (ma[p] && (nodes[ma1[p]].ari > nodes[x].ari || nodes[ma1[p]].lef < nodes[x].ari))
      mini = min(mini, p - nodes[x].sum);
    else if (mi[p] && (nodes[mi1[p]].ari > nodes[x].ari || nodes[mi1[p]].lef < nodes[x].ari))
      mini = min(mini, p - nodes[x].sum);
    else if (mi[p + nodes[x].sum] && nodes[mi1[p + nodes[x].sum]].ari < nodes[x].ari &&
             nodes[mi1[p + nodes[x].sum]].lef >= nodes[x].ari)
      mini = min(mini, p - nodes[x].sum);
    else if (ma[p + nodes[x].sum] && nodes[ma1[p + nodes[x].sum]].ari < nodes[x].ari &&
             nodes[ma1[p + nodes[x].sum]].lef >= nodes[x].ari)
      mini = min(mini, p - nodes[x].sum);
  }
}

void do_it(int x) {
  ll add = 3 * nodes[x].sum - tot_sum;

  if (mi[nodes[x].sum] != ma[nodes[x].sum])
    mini = min(mini, add);
  else if (mi[nodes[x].sum - add] && (nodes[mi1[nodes[x].sum - add]].ari < nodes[x].ari || nodes[mi1[nodes[x].sum - add]].ari > nodes[x].lef))
    mini = min(mini, add);
  else if (ma[nodes[x].sum - add] && (nodes[ma1[nodes[x].sum - add]].ari < nodes[x].ari || nodes[ma1[nodes[x].sum - add]].ari > nodes[x].lef))
    mini = min(mini, add);
  else if (mi[2 * nodes[x].sum - add])
    mini = min(mini, add);
  else if (mi[2 * nodes[x].sum])
    mini = min(mini, add);
}


ll Solution() {
  dfs(1, 0);

  fora (i, 1, N+1) {
    if (!mi[nodes[i].sum]) {
      mi[nodes[i].sum] = nodes[i].ari;
      ma[nodes[i].sum] = nodes[i].ari;
      mi1[nodes[i].sum] = i;
      ma1[nodes[i].sum] = i;
    } else {
      if (mi[nodes[i].sum] > nodes[i].ari) {
        mi[nodes[i].sum] = nodes[i].ari;
        mi1[nodes[i].sum] = i;
      }
      if (ma[nodes[i].sum] < nodes[i].ari) {
        ma[nodes[i].sum] = nodes[i].ari;
        ma1[nodes[i].sum] = i;
      }
    }
  }
  mini = 1e16;
  if (tot_sum % 2 == 0 && ma.count(tot_sum/2))
    mini = tot_sum / 2;
  fora (i, 1, N+1) {
    if (3 * nodes[i].sum <= tot_sum) {
      uradi(i);
    } else if (2 * nodes[i].sum < tot_sum) {
      do_it(i);
    }
  }
  if (mini >= 1e16) return -1;
  return mini;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N;
    Init();
    fora (i, 1, N+1) {
      cin >> nodes[i].sum;
      tot_sum += nodes[i].sum;
    }
    int u, v;
    fora (i, 0, N-1) {
      cin >> u >> v;
      edges[u].emplace_back(v);
      edges[v].emplace_back(u);
    }
    cout << Solution() << '\n';
  }
  return 0;
}