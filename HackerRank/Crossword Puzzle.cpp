//
// Created by sinn on 6/11/19.
//

#include <bits/stdc++.h>

using namespace std;

// Complete the crosswordPuzzle function below.

void splitWords(string& words, vector<string>& ws) {
  auto a = 0;
  auto b = words.find(';', 0);
  while (b != string::npos) {
    string w = words.substr(a, b-a);
    ws.emplace_back(w);
    a = b+1;
    b = words.find(';', a);
  }
  string w = words.substr(a);
  ws.emplace_back(w);
}

bool not_term;

vector<string> ans;
void dfs(int ind, vector<string>& grid, vector<string>& ws) {
  if (!not_term) return;
  if (ind == ws.size()) {
    if (not_term) {
      ans = grid;
      not_term = false;
    }
    return ;
  }
  int i, j, p, q, k;
  for (i = 0; i < 10; ++i) {
    for (j = 0; j < 10; ++j) {
      p = i, q = j;
      for (k = 0; k < ws[ind].size() && p + k < 10; ++k) {
        if (grid[p+k][q] != '-' && grid[p+k][q] != ws[ind][k])
          break;
      }
      if (k == ws[ind].size()) {
        vector<string> tmp = grid;
        for (k = 0; k < ws[ind].size(); ++k)
          grid[p+k][q] = ws[ind][k];
        dfs(ind + 1, grid, ws);
        grid = tmp;
      }

      for (k = 0; k < ws[ind].size() && q + k < 10; ++k) {
        if (grid[p][q+k] != '-' && grid[p][q+k] != ws[ind][k])
          break;
      }
      if (k == ws[ind].size()) {
        vector<string> tmp = grid;
        for (k = 0; k < ws[ind].size(); ++k)
          grid[p][q+k] = ws[ind][k];
        dfs(ind + 1, grid, ws);
        grid = tmp;
      }
    }
  }
}

vector<string> crosswordPuzzle(vector<string>& crossword, string words) {
  not_term = true;
  vector<string> ws;
  splitWords(words, ws);
  dfs(0, crossword, ws);
  return ans;
}

int main() {
  ofstream fout(getenv("OUTPUT_PATH"));

  vector<string> crossword(10);

  for (int i = 0; i < 10; i++) {
    string crossword_item;
    getline(cin, crossword_item);

    crossword[i] = crossword_item;
  }

  string words;
  getline(cin, words);

  vector<string> result = crosswordPuzzle(crossword, words);

  for (int i = 0; i < result.size(); i++) {
    fout << result[i];

    if (i != result.size() - 1) {
      fout << "\n";
    }
  }

  fout << "\n";

  fout.close();

  return 0;
}
