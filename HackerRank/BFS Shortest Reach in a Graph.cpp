//
// Created by sinn on 6/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
vector<int> graph[MAXN];
bool vis[MAXN];
int dist[MAXN];

void Solution() {
  memset(vis, 0, sizeof(vis));
  memset(dist, -1, sizeof(dist));
  queue<pair<int, int>> que;
  que.emplace(K, 0);
  vis[K] = true;
  while (!que.empty()) {
    auto p = que.front();
    que.pop();
    dist[p.first] = p.second;
    for (int v : graph[p.first]) {
      if (!vis[v]) {
        vis[v] = true;
        que.emplace(v, p.second+1);
      }
    }
  }
  fora (i, 0, N) {
    if (i != K) {
      cout << (dist[i] != -1 ? dist[i] * 6 : -1) << " \n"[i == N-1];
    }
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N >> M;
    fora (i, 0, N) graph[i].clear();
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      --u, --v;
      graph[u].emplace_back(v);
      graph[v].emplace_back(u);
    }
    cin >> K;
    --K;
    Solution();
  }
  return 0;
}