//
// Created by sinn on 6/3/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

int BIT[MAXN * 50];

ll GetSum(int x) {
  ll ret = 0;
  while (x != 0) {
    ret += BIT[x];
    x -= (x & -x);
  }
  return ret;
}

void AddVal(int x) {
  while (x <= M) {
    BIT[x] += 1;
    x += (x & -x);
  }
}

ll Solution() {
  memset(BIT, 0, sizeof(BIT));
  M = 0;
  fora (i, 0, N) {
    M = max(M, A[i]);
  }

  ll ans = 0;

  ford (i, N-1, 0) {
    ans += GetSum(A[i]-1);
    AddVal(A[i]);
  }

  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N;
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
