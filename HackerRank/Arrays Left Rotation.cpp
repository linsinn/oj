//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

void Solution() {
  list<int> li(A, A+N);
  while (K--) {
    li.emplace_back(li.front());
    li.pop_front();
  }
  for (int a : li) {
    cout << a << '\n';
  }
  cout << '\n';
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora(i, 0, N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}