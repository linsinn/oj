//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

int Solution() {
  queue<int> que;
  map<int, int> large, small;
  int ln = 0, sn = 0;
  int ans = 0;
  fora (i, 0, N) {
    int median = 1 << 30;
    if (que.size() == K) {
      if (ln == sn)
        median = large.begin()->first + small.rbegin()->first;
      else
        median = large.begin()->first * 2;
    }
    if (A[i] >= median) {
      ++ans;
    }

    que.emplace(A[i]);
    if (que.size() > K) {
      int x = que.front();
      que.pop();
      if (x < large.begin()->first) {
        small[x]--;
        if (small[x] == 0) {
          small.erase(x);
        }
        sn--;
      } else {
        large[x]--;
        if (large[x] == 0) {
          large.erase(x);
        }
        ln--;
      }
    }

    if (A[i] > large.begin()->first) {
      large[A[i]]++;
      ln++;
    } else {
      small[A[i]]++;
      sn++;
    }

    while (ln - sn > 1) {
      auto p = *large.begin();
      large[p.first]--;
      if (p.second == 1)
        large.erase(p.first);
      ln--;
      small[p.first]++;
      sn++;
    }

    while (ln < sn) {
      auto p = *small.rbegin();
      small[p.first]--;
      if (p.second == 1)
        small.erase(p.first);
      sn--;
      large[p.first]++;
      ln++;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
