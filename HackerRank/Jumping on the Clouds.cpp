//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

int Solution() {
  vector<int> f(N+1, -1);
  f[0] = 0;
  fora(i, 1, N) {
    if (A[i] == 1)
      continue ;
    else {
      int cur = 1 << 30;
      if (i - 1 >= 0 && A[i-1] == 0) {
        cur = min(cur, f[i-1] + 1);
      }
      if (i - 2 >= 0 && A[i-2] == 0) {
        cur = min(cur, f[i-2] + 1);
      }
      f[i] = cur;
    }
  }
  return f[N-1];
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora(i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}