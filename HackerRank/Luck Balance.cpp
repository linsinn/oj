//
// Created by sinn on 6/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
pair<int, int> p[MAXN];

int Solution() {
  sort(p, p+N);
  int imps = 0;
  fora (i, 0, N) {
    if (p[i].second == 1)
      ++imps;
  }
  imps = min(imps, K);
  int ans = 0;
  ford (i, N-1, 0) {
    if (p[i].second == 1) {
      if (imps) {
        ans += p[i].first;
        --imps;
      } else {
        ans -= p[i].first;
      }
    } else {
      ans += p[i].first;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) {
      cin >> p[i].first >> p[i].second;
    }
    cout << Solution() << '\n';
  }
  return 0;
}