//
// Created by sinn on 6/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int cost[MAXN];

int Solution() {
  sort(cost, cost+N);
  int ans = 0;
  ford (i, N-1, 0) {
    int t = (N - i - 1) / K;
    ans += cost[i] * (t + 1);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) {
      cin >> cost[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}