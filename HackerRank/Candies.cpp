//
// Created by sinn on 6/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];
int l[MAXN], r[MAXN];

ll Solution() {
  memset(l, 0, sizeof(l));
  memset(r, 0, sizeof(r));

  fora (i, 1, N) {
    if(A[i-1] < A[i]) {
      l[i] = l[i-1] + 1;
    }
  }

  ford (i, N-2, 0) {
    if (A[i+1] < A[i]) {
      r[i] = r[i+1] + 1;
    }
  }

  ll ans = 0;
  fora (i, 0, N) {
    ans += max(l[i], r[i]) + 1;
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}