//
// Created by sinn on 6/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s, t;
int f[5001][5001];

int Solution() {
  memset(f, 0, sizeof(f));
  for (int i = 1; i <= s.size(); ++i) {
    for (int j = 1; j <= t.size(); ++j) {
      f[i][j] = max(f[i-1][j], f[i][j-1]);
      if (s[i-1] == t[j-1])
        f[i][j] = max(f[i][j], f[i-1][j-1]+1);
    }
  }
  return f[s.size()][t.size()];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s >> t) {
    cout << Solution() << '\n';
  }
  return 0;
}