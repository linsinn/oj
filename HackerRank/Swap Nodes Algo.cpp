//
// Created by sinn on 6/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

struct Node {
  int val;
  Node *left, *right;
} nodes[2002];

void Swap(Node* rt, int h) {
  if (rt == nullptr)
    return ;
  if (h % K == 0) {
    swap(rt->left, rt->right);
  }
  Swap(rt->left, h+1);
  Swap(rt->right, h+1);
}

void InOrder(Node* rt) {
  if (rt == nullptr)
    return ;
  InOrder(rt->left);
  cout << rt->val << ' ';
  InOrder(rt->right);
}

void Solution() {
  Swap(&nodes[1], 1);
  InOrder(&nodes[1]);
  cout << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    int l, r;
    fora (i, 1, N+1) {
      cin >> l >> r;
      nodes[i].val = i;
      if (l != -1)
        nodes[i].left = &nodes[l];
      if (r != -1)
        nodes[i].right = &nodes[r];
    }
    cin >> M;
    while (M--) {
      cin >> K;
      Solution();
    }
  }
  return 0;
}