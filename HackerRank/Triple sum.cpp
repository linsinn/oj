//
// Created by sinn on 6/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int l[3];
int A[3][MAXN];
ll presum[MAXN];

ll Solution() {
  memset(presum, 0, sizeof(presum));
  fora (k, 0, 3) {
    sort(A[k], A[k]+l[k]);
    l[k] = unique(A[k], A[k]+l[k]) - A[k];
  }
  fora (i, 0, l[1]) {
    auto t = upper_bound(A[2], A[2]+l[2], A[1][i]) - A[2];
    presum[i+1] = presum[i] + t;
  }
  ll ans = 0;
  fora (i, 0, l[0]) {
    auto it = lower_bound(A[1], A[1]+l[1], A[0][i]) - A[1];
    ans += presum[l[1]] - presum[it];
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> l[0] >> l[1] >> l[2]) {
    fora (k, 0, 3) {
      fora (i, 0, l[k]) {
        cin >> A[k][i];
      }
    }
    cout << Solution() << '\n';
  }
  return 0;
}