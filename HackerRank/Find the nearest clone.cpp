//
// Created by sinn on 6/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 1;

using ll = long long;

int N, M, K;
vector<int> graph[MAXN];
int colors[MAXN];
bool vis[MAXN];

unsigned int BFS(int u) {
  memset(vis, 0, sizeof(vis));
  queue<pair<int, int>> que;
  que.emplace(u, 0);
  vis[u] = true;
  while (!que.empty()) {
    auto p = que.front();
    que.pop();
    for (int v : graph[p.first]) {
      if (!vis[v]) {
        if (colors[v] == K) {
          return p.second + 1;
        }
        vis[v] = true;
        que.emplace(v, p.second+1);
      }
    }
  }
  return -1;
}

int Solution() {
  unsigned int ans = -1;
  fora (i, 1, N+1) {
    if (colors[i] == K) {
      ans = min(ans, BFS(i));
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 1, N+1) graph[i].clear();
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      graph[u].emplace_back(v);
      graph[v].emplace_back(u);
    }
    fora (i, 0, N) cin >> colors[i+1];
    cin >> K;
    cout << Solution() << '\n';
  }
  return 0;
}