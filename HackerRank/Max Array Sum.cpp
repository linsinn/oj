//
// Created by sinn on 6/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

int Solution() {
  fora (i, 0, N) {
    int t = max(0, A[i]);
    if (i - 2 >= 0)
      A[i] = max(A[i], t + A[i-2]);
    if (i - 3 >= 0)
      A[i] = max(A[i], t + A[i-3]);
  }
  int ans = A[N-1];
  if (N > 1)
    ans = max(ans, A[N-2]);
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}
