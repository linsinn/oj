//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s1, s2;

int Solution() {
  set<char> c1(s1.begin(), s1.end());
  set<char> c2(s2.begin(), s2.end());
  for (auto c : s1) {
    if (c2.count(c)) {
      return 1;
    }
  }
  return false;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> s1 >> s2;
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}