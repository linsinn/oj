//
// Created by sinn on 6/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int money;
int cost[MAXN];

void Solution() {
  map<int, int> mp;
  fora (i, 0, N) {
    auto it = mp.find(money - cost[i]);
    if (it != mp.end()) {
      cout << it->second + 1 << ' ' << i + 1 << '\n';
      return ;
    }
    mp[cost[i]] = i;
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> money >> N;
    fora (i, 0, N) {
      cin >> cost[i];
    }
    Solution();
  }
  return 0;
}