//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K, Q;
int Op, V;
unordered_map<int, int> cnts, re_cnts;

void Solution() {
  if (Op == 1) {
    if (cnts.find(V) != cnts.end()) {
      int t = cnts[V];
      if (re_cnts.find(t) != re_cnts.end()) {
        re_cnts[t]--;
      }
      re_cnts[t+1]++;
    } else {
      re_cnts[1]++;
    }
    cnts[V]++;
  } else if (Op == 2) {
    if (cnts.find(V) != cnts.end()) {
      int t = cnts[V];
      if (re_cnts.find(t) != re_cnts.end()) {
        re_cnts[t]--;
      }
      re_cnts[t - 1]++;
      cnts[V]--;
      if (cnts[V] == 0)
        cnts.erase(V);
    }
  } else {
    cout << min(re_cnts[V], 1) << '\n';
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> Q) {
    while (Q--) {
      cin >> Op >> V;
      Solution();
    }
  }
  return 0;
}