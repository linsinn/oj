//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN], pos[MAXN];

void Solution() {
  fora(i, 1, N+1) {
    pos[A[i]] = i;
  }
  int i = N;
  int sw = 0;
  while (i >= 1) {
    if (A[i] != i) {
      int k = i - pos[i];
      if (k > 2) {
        cout << "Too chaotic\n";
        return;
      }
      int t = A[i-k];
      fora(j, 0, k) {
        A[i-k+j] = A[i-k + j + 1];
        sw += 1;
      }
      A[i] = t;
      fora(j, 0, k+1) {
        pos[A[i-j]] = i - j;
      }
    }
    --i;
  }
  cout << sw << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N;
    fora(i, 1, N+1) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}