//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

struct Bound {
  int bnd;
  int lr;
  int val;
  bool operator < (const Bound& rhs) {
    if (bnd < rhs.bnd) {
      return true;
    } else if (bnd == rhs.bnd) {
      return rhs.lr == 1;
    } else {
      return false;
    }
  }
} bnds[MAXN];

ll Solution() {
  sort(bnds, bnds+K);
  ll cur = 0, ans = 0;
  fora(i, 0, K) {
    if (bnds[i].lr == 0) {
      cur += bnds[i].val;
    } else {
      cur -= bnds[i].val;
    }
    ans = max(ans, cur);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    int l, r, k;
    K = 0;
    fora(i, 0, M) {
      cin >> l >> r >> k;
      bnds[K].bnd = l;
      bnds[K].lr = 0;
      bnds[K].val = k;
      bnds[K+1].bnd = r+1;
      bnds[K+1].lr = 1;
      bnds[K+1].val = k;
      K += 2;
    }
    cout << Solution() << '\n';
  }
  return 0;
}