//
// Created by sinn on 6/11/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;

string Solution() {
  if (N == 2) return "Prime";
  if (N == 1 || N % 2 == 0) return "Not prime";
  for (int div = 3; div <= sqrt(N); div += 2) {
    if (N % div == 0)
      return "Not prime";
  }
  return "Prime";
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N;
    cout << Solution() << '\n';
  }
  return 0;
}