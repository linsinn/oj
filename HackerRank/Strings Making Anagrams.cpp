//
// Created by sinn on 6/3/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s[2];
int mp[2][256];

int Solution() {
  memset(mp, 0, sizeof(mp));
  fora (i, 0, 2) {
    for (char ch : s[i]) {
      mp[i][ch]++;
    }
  }
  int ans = 0;
  for (int i = 'a'; i <= 'z'; ++i) {
    ans += max(mp[0][i], mp[1][i]) - min(mp[0][i], mp[1][i]);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s[0] >> s[1]) {
    cout << Solution() << '\n';
  }
  return 0;
}