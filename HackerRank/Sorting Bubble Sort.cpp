//
// Created by sinn on 6/1/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

void Solution() {
  int ans = 0;
  fora (i, 0, N) {
    fora (j, 0, N-1) {
      if (A[j] > A[j+1]) {
        swap(A[j], A[j+1]);
        ++ans;
      }
    }
  }
  cout << "Array is sorted in " << ans << " swaps.\n";
  cout << "First Element: " << A[0] << '\n';
  cout << "Last Element: " << A[N-1] << '\n';
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora(i, 0, N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}
