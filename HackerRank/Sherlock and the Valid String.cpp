//
// Created by sinn on 6/3/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s;
int mp[256];

int Solution() {
  memset(mp, 0, sizeof(mp));
  for (char ch : s) {
    mp[ch]++;
  }
  map<int, int> t;
  fora (i, 'a', 'z' + 1) {
    if (t.size() > 2) return 0;
    if (mp[i] != 0)
      t[mp[i]]++;
  }
  return t.size() == 1 || (t.rbegin()->second == 1 && t.rbegin()->first - t.begin()->first == 1)
    || (t.begin()->first == 1 && t.begin()->second == 1);
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    if (Solution()) cout << "YES\n";
    else cout << "NO\n";
  }
  return 0;
}