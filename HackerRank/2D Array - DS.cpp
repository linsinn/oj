//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int mat[6][6];

int Solution() {
  int ans = -10 * 180;
  fora(i, 0, 6) {
    fora(j, 0, 6) {
      if (i + 2 < 6 && j + 2 < 6) {
        int su = accumulate(&mat[i][j], &mat[i][j+3], 0);
        su += accumulate(&mat[i+2][j], &mat[i+2][j+3], 0);
        su += mat[i+1][j+1];
        ans = max(ans, su);
      }
    }
  }
  return ans;
}

int main() {
  freopen("in.txt", "r", stdin);
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  fora(i, 0, 6) {
    fora(j, 0, 6) {
      cin >> mat[i][j];
    }
  }
  cout << Solution() << '\n';
  return 0;
}