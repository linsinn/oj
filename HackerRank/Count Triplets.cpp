//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];
int f[MAXN], g[MAXN];

ll Solution() {
  memset(f, 0, sizeof(f));
  memset(g, 0, sizeof(g));
  map<ll, int> mp;
  ford(i, N-1, 0) {
    auto it = mp.find(1LL * A[i] * K);
    if (it != mp.end()) {
      f[i] = it->second;
    }
    mp[A[i]]++;
  }
  mp.clear();
  fora(i, 0, N) {
    if (A[i] % K == 0) {
      auto it = mp.find(A[i] / K);
      if (it != mp.end()) {
        g[i] = it->second;
      }
    }
    mp[A[i]]++;
  }
  ll ans = 0;
  fora(i, 0, N) {
    ans += 1LL * f[i] * g[i];
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora(i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}