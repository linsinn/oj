//
// Created by sinn on 6/6/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string a, b;
bool f[1020][1020];

string Solution() {
  memset(f, 0, sizeof(f));
  f[0][0] = true;
  fora (i, 0, a.size()) {
    fora (j, 0, b.size()+1) {
      if (f[i][j]) {
        if (j < b.size() && ::toupper(a[i]) == b[j])
          f[i+1][j+1] = true;
        if (::islower(a[i]))
          f[i+1][j] = true;
      }
    }
  }
  return f[a.size()][b.size()] ? "YES" : "NO";
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> a >> b;
    cout << Solution() << '\n';
  }
  return 0;
}