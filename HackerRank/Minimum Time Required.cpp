//
// Created by sinn on 6/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN];

bool Check(ll d) {
  ll p = 0;
  fora (i, 0, N) {
    p += d / A[i];
    if (p >= M)
      return true;
  }
  return p >= M;
}

ll Solution() {
  ll lb = 1, rb = 1LL << 60;
  while (lb < rb) {
    ll m = lb + (rb - lb) / 2;
    if (Check(m)) {
      rb = m;
    } else {
      lb = m + 1;
    }
  }
  return rb;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}