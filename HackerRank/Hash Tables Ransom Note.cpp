//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string magazines[MAXN], notes[MAXN];

int Solution() {
  map<string, int> avails, needs;
  fora(i, 0, M) {
    avails[magazines[i]]++;
  }
  fora(i, 0, N) {
    needs[notes[i]]++;
  }
  for(auto& p : needs) {
    auto it = avails.find(p.first);
    if (it == avails.end() || it->second < p.second) {
      return 0;
    }
  }
  return 1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> M >> N) {
    fora(i, 0, M) {
      cin >> magazines[i];
    }
    fora(i, 0, N) {
      cin >> notes[i];
    }
    if (Solution()) cout << "Yes\n";
    else cout << "No\n";
  }
  return 0;
}