//
// Created by sinn on 6/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e2 + 1;

using ll = long long;

int N, M, K;
string board[MAXN];
bool vis[MAXN][MAXN];
int sx, sy, ex, ey;

struct Node {
  int x, y;
  int step;
  Node (int a, int b, int c) : x{a}, y{b}, step{c} {}
  bool InBound() const {
    return 0 <= x && x < N && 0 <= y && y < N;
  }
  bool Avail() const {
    return this->InBound() && !vis[x][y] && board[x][y] == '.';
  }
  void move(int dx, int dy, queue<Node>& que) {
    int nx = x, ny = y;
    while (0 <= nx+dx && nx+dx < N && 0 <= ny+dy && ny+dy < N) {
      if (board[nx+dx][ny+dy] != '.')
        break;
      nx += dx, ny += dy;
      if (!vis[nx][ny]) {
        vis[nx][ny] = true;
        que.emplace(Node{nx, ny, step+1});
      }
    }
  }
};

int Solution() {
  memset(vis, 0, sizeof(vis));
  const int dirs[] = {-1, 0, 1, 0, -1};
  queue<Node> que;
  que.emplace(Node(sx, sy, 0));
  vis[sx][sy] = true;
  while (!que.empty()) {
    auto n = que.front();
    que.pop();
    if (n.x == ex && n.y == ey) return n.step;
    fora (i, 0, 4) {
      n.move(dirs[i], dirs[i+1], que);
    }
  }
  return -1;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> board[i];
    cin >> sx >> sy >> ex >> ey;
    cout << Solution() << '\n';
  }
  return 0;
}