//
// Created by sinn on 6/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

int N, M, K;
ll H[MAXN];

ll Solution() {
  ll ans = 0;
  H[N++] = -1;
  stack<int> stk;
  stk.emplace(-1);
  fora (i, 0, N) {
    while (stk.top() != -1 && H[i] < H[stk.top()]) {
      int x = stk.top();
      stk.pop();
      ans = max(ans, H[x] * (i - stk.top() - 1));
    }
    stk.emplace(i);
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> H[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}