//
// Created by sinn on 6/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 1;

using ll = long long;

int N, M, K;
int A[MAXN], pre[MAXN], nxt[MAXN];
int ans[MAXN];

void Solution() {
  stack<int> stk;
  fora (i, 0, N) {
    while (!stk.empty() && A[stk.top()] >= A[i]) {
      stk.pop();
    }
    if (stk.empty()) {
      pre[i] = -1;
    } else {
      pre[i] = stk.top();
    }
    stk.emplace(i);
  }
  stk = stack<int>();
  ford (i, N-1, 0) {
    while (!stk.empty() && A[stk.top()] >= A[i]) {
      stk.pop();
    }
    if (stk.empty()) {
      nxt[i] = N;
    } else {
      nxt[i] = stk.top();
    }
    stk.emplace(i);
  }
  fora (i, 0, N) {
    int l = nxt[i] - pre[i] - 1;
    ans[l] = max(ans[l], A[i]);
  }
  ford (i, N-1, 1) {
    ans[i] = max(ans[i], ans[i+1]);
  }
  fora (i, 1, N+1) {
    cout << ans[i] << " \n"[i == N];
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> A[i];
    }
    Solution();
  }
  return 0;
}