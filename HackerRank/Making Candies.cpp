//
// Created by sinn on 6/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = unsigned long long;

ll m, w, p, n;

ll Solution() {
  ll ans = n;
  ll d = 0;
  ll amount = 0;
  while (d < ans) {
    if (m > w)
      swap(m, w);
    ll g = m * w;
    if ((n - amount) / m + 1 <= w)
      return d + 1;
    ll need = (n - amount) / g;
    if ((n - amount) % g != 0)
      ++need;
    ans = min(ans, d+need);
    if (need == 1)
      break;

    ll c = (p - amount) / g;
    if ((p - amount) % g != 0)
      ++c;
    amount += c * g;
    d += c;

    for (ll x = amount / p; x >= 1; --x) {
      ll a = amount - x * p;
      ll tm = m, tw = w;
      tm += x;
      if (tm > tw) {
        ll y = (tm - tw) / 2;
        tm -= y;
        tw += y;
      }
      g = tm * tw;
      if ((n - a) / tm + 1<= tw)
        return min(d + 1, ans);
      need = (n - a) / g;
      if ((n - a) % g != 0)
        ++need;
      if (d + need >= ans)
        break;
      ans = d + need;
    }

    ll x = amount / p;
    amount -= x * p;
    m += x;
    if (m > w) {
      ll y = (m - w) / 2;
      m -= y;
      w += y;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> m >> w >> p >> n) {
    cout << Solution() << '\n';
  }
  return 0;
}