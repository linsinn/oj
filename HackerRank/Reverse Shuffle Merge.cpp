//
// Created by sinn on 6/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

int N, M, K;
string s;
int cnts[256];
int nxts[MAXN][256];
int foll[MAXN][256];
bool ban[256];

bool Check(int i) {
  fora (c, 'a', 'z'+1) {
    if (cnts[c] > foll[i][c])
      return false;
  }
  return true;
}

string Solution() {
  memset(cnts, 0, sizeof(cnts));
  memset(nxts, 0, sizeof(nxts));
  memset(foll, 0, sizeof(foll));
  memset(ban, 0, sizeof(ban));

  reverse(s.begin(), s.end());

  fora (i, 0, s.size()) {
    cnts[s[i]]++;
  }
  fora (i, 'a', 'z'+1) {
    cnts[i] /= 2;
  }

  ford (i, s.size()-1, 0) {
    memcpy(nxts[i], nxts[i+1], sizeof(nxts[i]));
    memcpy(foll[i], foll[i+1], sizeof(foll[i]));
    nxts[i][s[i]] = i;
    foll[i][s[i]]++;
  }

  string ans;
  int idx = 0;
  while (ans.size() * 2 < s.size()) {
    fora (ch, 'a', 'z'+1) {
      if (cnts[ch] == 0)
        continue;
      int p = nxts[idx][ch];
      if (Check(p)) {
        ans += (char) ch;
        cnts[ch]--;
        idx = p + 1;
        break;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> s) {
    cout << Solution() << '\n';
  }
  return 0;
}