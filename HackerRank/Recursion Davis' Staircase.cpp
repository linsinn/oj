//
// Created by sinn on 6/11/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MAXN = 2e6 + 10;

using ll = long long;
constexpr ll MOD = 1e10 + 7;

int N, M, K;
ll f[MAXN];

ll Solution() {
  memset(f, 0, sizeof(f));
  f[0] = 1;
  fora (i, 1, K+1) {
    if (i - 1 >= 0)
      (f[i] += f[i-1]) %= MOD;
    if (i - 2 >= 0)
      (f[i] += f[i-2]) %= MOD;
    if (i - 3 >= 0)
      (f[i] += f[i-3]) %= MOD;
  }
  return f[K];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) {
      cin >> K;
      cout << Solution() << '\n';
    }
  }
  return 0;
}