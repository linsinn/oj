//
// Created by sinn on 6/5/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

ll N, M, K;
ll A[MAXN];

ll Solution() {
  ll ans = 0;
  map<ll, ll> mp;
  ll pre = 0;
  fora (i, 0, N) {
    pre = (pre + A[i]) % M;
    mp[pre]++;
  }
  K = M;
  pre = 0;

  fora (i, 0, N) {
    auto it = mp.upper_bound(K);
    if (it == mp.begin())
      it = mp.end();
    --it;
    ans = max(ans, (it->first - pre + M) % M);

    pre = (pre + A[i]) % M;
    mp[pre]--;
    if (mp[pre] == 0)
      mp.erase(pre);
    K = (M + pre) % M;

  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N >> M;
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}