//
// Created by sinn on 6/11/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

ll N, M, K;
ll A[MAXN];

void Init() {
  sort(A, A+N);
}

ll Solution() {
  int l = 0, r = N;
  ll pre = 0;
  ford (i, 31, 0) {
    ll u = K & (1LL << i);
    if (u != 0) {
      auto p = lower_bound(A+l, A+r, pre + u) - A;
      if (p != l)  {
        r = p;
      } else {
        pre += 1LL << i;
      }
    } else {
      auto p = lower_bound(A+l, A+r, pre + (1LL << i)) - A;
      if (p != r) {
        l = p;
        pre += 1LL << i;
      }
    }
  }
  return K ^ A[l];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
  freopen("b.txt", "w", stdout);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    Init();
    cin >> M;
    fora (i, 0, M) {
      cin >> K;
      cout << Solution() << " \n";
    }
  }
  return 0;
}