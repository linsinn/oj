//
// Created by sinn on 6/4/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s;

ll Solution() {
  ll ans = 0;
  int i = 0, j =1;
  while (i < N) {
    while (j < N && s[j] == s[i]) {
      ++j;
    }
    int l = j - i;
    ans += 1LL * (l + 1) * l / 2;
    i = j;
    j = i + 1;
  }
  fora (i, 0, N) {
    int p = i - 1, q = i + 1;
    if (p >= 0 && q < N && s[p] != s[i]) {
      while (p >= 0 && q < N && s[p] == s[q]) {
        if (i - p >= 2 && s[p] != s[p+1]) {
          break;
        }
        --p;
        ++q;
        ++ans;
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    cin >> s;
    cout << Solution() << '\n';
  }
  return 0;
}