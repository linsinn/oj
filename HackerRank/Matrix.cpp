//
// Created by sinn on 6/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
vector<pair<int, int>> graph[MAXN];
bool is_machine[MAXN];
ll f[2][MAXN];
const ll INF = 1e18;

void dfs(int u, int p) {
  ll x = 0;
  for (auto it : graph[u]) {
    int v = it.first, w = it.second;
    if (v == p)
      continue;
    dfs(v, u);
    x += min(w + f[1][v], f[0][v]);
  }
  if (is_machine[u]) {
    f[1][u] = x;
    f[0][u] = INF;
    return ;
  }
  f[0][u] = x;
  for (auto it : graph[u]) {
    int v = it.first, w = it.second;
    if (v != p) {
      f[1][u] = min(f[1][u], x + f[1][v] - min(f[1][v]+w, f[0][v]));
    }
  }
}

ll Solution() {
  fora (i, 0, N) f[1][i] = f[0][i] = INF;
  int rt = -1;
  fora (i, 0, N) {
    if (is_machine[i]) {
      rt = i;
      dfs(i, -1);
      break;
    }
  }
  return f[1][rt];
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> K) {
    fora (i, 0, N) graph[i].clear();
    memset(is_machine, 0, sizeof(is_machine));
    int u, v, w;
    fora (i, 0, N-1) {
      cin >> u >> v >> w;
      graph[u].emplace_back(v, w);
      graph[v].emplace_back(u, w);
    }
    fora (i, 0, K) {
      cin >> w;
      is_machine[w] = true;
    }
    cout << Solution() << '\n';
  }
  return 0;
}