//
// Created by sinn on 6/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 1e5 + 10;

using ll = long long;

int N, M, K;
int A[MAXN], pre_small[MAXN];

class SegTree {
  int sz;
  vector<int> nodes;
  void push(int rt, int ll, int rr, int idx, int val) {
    if (ll > idx || rr < idx) return ;
    if (ll == rr) {
      nodes[rt] = val;
      return ;
    }
    int mid = ll + (rr - ll) / 2;
    int lc = rt << 1, rc = rt << 1 | 1;
    if (idx <= mid) push(lc, ll, mid, idx, val);
    else push(rc, mid+1, rr, idx, val);
    nodes[rt] = max(nodes[lc], nodes[rc]);
  }
  int query(int rt, int ll, int rr, int l, int r) {
    if (ll > r || rr < l) return 0;
    if (l <= ll && rr <= r) return nodes[rt];
    int mid = ll + (rr - ll) / 2;
    int lc = rt << 1, rc = rt << 1 | 1;
    return max(query(lc, ll, mid, l, r), query(rc, mid+1, rr, l, r));
  }
public:
  SegTree(int sz) : sz{sz} {
    nodes.resize(sz * 4);
  }
  void Push(int idx, int val) {
    this->push(1, 0, sz-1, idx, val);
  }
  int Query(int l, int r) {
    if (r < l)
      return 0;
    return this->query(1, 0, sz-1, l, r);
  }
};

int Solution() {
  stack<int> stk;
  fora (i, 0, N) {
    while (!stk.empty() && A[stk.top()] >= A[i]) {
      stk.pop();
    }
    pre_small[i] = stk.empty() ? -1 : stk.top();
    stk.emplace(i);
  }
  SegTree t(N);
  int ans = 0;
  fora (i, 0, N) {
    int p = pre_small[i];
    if (p != -1) {
      int q = t.Query(p+1, i-1);
      ans = max(ans, q+1);
      t.Push(i, q+1);
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora (i, 0, N) cin >> A[i];
    cout << Solution() << '\n';
  }
  return 0;
}