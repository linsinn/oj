//
// Created by sinn on 6/11/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
int x, y;
int maxi;
map<int, int> pa, sz;

int find_pa(int u) {
  if (!pa.count(u)) {
    return u;
  }
  return pa[u] = find_pa(pa[u]);
}

void merge(int u, int v) {
  u = find_pa(u);
  v = find_pa(v);
  if (u == v)
    return ;
  pa[v] = u;
  if (!sz.count(u))
    sz[u] = 1;
  if (!sz.count(v))
    sz[v] = 1;
  sz[u] += sz[v];
  maxi = max(maxi, sz[u]);
}

int Solution() {
  merge(x, y);
  return maxi;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    maxi = 1;
    fora (i, 0, N) {
      cin >> x >> y;
      cout << Solution() << '\n';
    }
  }
  return 0;
}