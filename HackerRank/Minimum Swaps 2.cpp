//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int A[MAXN], B[MAXN];

int Solution() {
  memcpy(B, A, sizeof(A));
  sort(B, B + N);
  map<int, int> pos;
  fora(i, 0, N) {
    pos[A[i]] = i;
  }
  int ans = 0;
  fora(i, 0, N) {
    if (B[i] != A[i]) {
      int k = pos[B[i]];
      swap(A[i], A[k]);
      pos[A[i]] = i;
      pos[A[k]] = k;
      ++ans;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora(i, 0, N) {
      cin >> A[i];
    }
    cout << Solution() << '\n';
  }
  return 0;
}