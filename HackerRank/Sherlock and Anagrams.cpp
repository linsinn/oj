//
// Created by sinn on 5/31/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
string s;

int Solution() {
  int ans = 0;
  map<char, int> m1, m2;
  fora(l, 1, s.size()) {
    m1.clear();
    fora(k, 0, l) {
      m1[s[k]]++;
    }
    fora(i, 0, s.size()-l+1) {
      m2.clear();
      fora(k, i+1, i+1+l) {
        m2[s[k]]++;
      }
      fora(j, i+1, s.size()-l+1) {
        auto it1 = m1.begin(), it2 = m2.begin();
        bool is = true;
        while (it1 != m1.end() && it2 != m2.end()) {
          if (it1->first != it2->first || it1->second != it2->second) {
            is = false;
            break;
          }
          ++it1;
          ++it2;
        }
        if (is && it1 == m1.end() && it2 == m2.end()) {
          ++ans;
        }
        m2[s[j]]--;
        if (m2[s[j]] == 0)
          m2.erase(s[j]);
        m2[s[j+l]]++;
      }
      m1[s[i]]--;
      if (m1[s[i]] == 0)
        m1.erase(s[i]);
      m1[s[i+l]]++;
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    fora(i, 0, N) {
      cin >> s;
      cout << Solution() << '\n';
    }
  }
  return 0;
}