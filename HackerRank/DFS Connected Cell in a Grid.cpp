//
// Created by sinn on 6/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e6 + 10;

using ll = long long;

int N, M, K;
int board[101][101];
bool vis[101][101];
const int dirs[][2] = {
{-1, -1}, {-1, 0}, {-1, 1},
{0, -1}, {0, 0}, {0, 1},
{1, -1}, {1, 0}, {1, 1},
};

int dfs(int x, int y) {
  vis[x][y] = true;
  int t = 0;
  fora (d, 0, 9) {
    int nx = x + dirs[d][0];
    int ny = y + dirs[d][1];
    if (0 <= nx && nx < N && 0 <= ny && ny < M && !vis[nx][ny] && board[nx][ny] == 1) {
      t += dfs(nx, ny);
    }
  }
  return t + 1;
}

int Solution() {
  memset(vis, 0, sizeof(vis));
  int ans = 0;
  fora (i, 0, N) {
    fora (j, 0, M) {
      if (!vis[i][j] && board[i][j] == 1) {
        ans = max(ans, dfs(i, j));
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N >> M) {
    fora (i, 0, N) {
      fora (j, 0, M) {
        cin >> board[i][j];
      }
    }
    cout << Solution() << '\n';
  }
  return 0;
}