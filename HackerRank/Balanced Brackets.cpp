//
// Created by sinn on 6/7/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;

void Solution() {
  string s;
  map<char, char> mp;
  mp[')'] = '(';
  mp['}'] = '{';
  mp[']'] = '[';
  while (N--) {
    cin >> s;
    stack<char> stk;
    bool f = true;
    for (char ch : s) {
      if (mp.count(ch) == 0)
        stk.emplace(ch);
      else {
        if (stk.empty() || stk.top() != mp[ch]) {
          f = false;
          break ;
        }
        stk.pop();
      }
    }
    if (!stk.empty() || !f)
      cout << "NO\n";
    else cout << "YES\n";
  }
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  while (cin >> N) {
    Solution();
  }
  return 0;
}