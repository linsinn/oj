//
// Created by sinn on 6/10/19.
//

struct Node {
  int data;
  struct Node *next;
};

bool has_cycle(Node *head) {
  if (head->next == nullptr)
    return false;
  Node *slow = head, *fast = head->next;
  while (slow != fast) {
    if (slow == nullptr || fast == nullptr)
      return false;
    if (fast->next == nullptr)
      return false;
    slow = slow->next;
    fast = fast->next->next;

  }
  return slow != nullptr;
}