//
// Created by sinn on 6/8/19.
//

#include <bits/stdc++.h>
using namespace std;

#define fora(i, l, r) for (int i = (int)l; i < (int)r; ++i)
#define ford(i, r, l) for (int i = (int)r; i >= (int)l; --i)

constexpr int MOD = 1e9 + 7;
constexpr int MAXN = 2e5 + 1;

using ll = long long;

int N, M, K;
int C[2];

vector<int> graph[MAXN];
bool vis[MAXN];

void dfs(int u, int& tot) {
  tot += 1;
  vis[u] = true;
  for (int v : graph[u]) {
    if (!vis[v])
      dfs(v, tot);
  }
}

ll Solution() {
  memset(vis, 0, sizeof(vis));
  int tot = 0;
  ll ans = 0;
  fora (i, 1, N+1) {
    if (!vis[i]) {
      tot = 0;
      dfs(i, tot);
      if (C[0] < C[1]) {
        ans += C[0] * tot;
      } else {
        ans += 1LL * C[1] * (tot - 1) + C[0];
      }
    }
  }
  return ans;
}

int main() {
#ifdef MY_DEBUG
  freopen("in.txt", "r", stdin);
#endif
  ios::sync_with_stdio(false);
  cin.tie(nullptr), cout.tie(nullptr);
  int T;
  cin >> T;
  while (T--) {
    cin >> N >> M >> C[0] >> C[1];
    fora (i, 1, N+1) graph[i].clear();
    int u, v;
    fora (i, 0, M) {
      cin >> u >> v;
      graph[u].emplace_back(v);
      graph[v].emplace_back(u);
    }
    cout << Solution() << '\n';
  }
  return 0;
}